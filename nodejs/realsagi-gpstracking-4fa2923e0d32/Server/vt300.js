exports.Datagps = function(data, callback){

	var Latitude = data.match("(\\d+)(\\d{2}\\.\\d+),"+"([NS]),");
	var DegreeLatitude = (parseFloat(Latitude[1]) + (parseFloat(Latitude[2]) / 60)).toFixed(6);
	var Longtitude = data.match("(\\d+)(\\d{2}\\.\\d+),"+"([EW])?,");
	var DegreeLongtitude = (parseFloat(Longtitude[1]) + (parseFloat(Longtitude[2]) / 60)).toFixed(6);
	var tempspeed = data.match("([EW]),"+"(\\d+).(\\d+)");
	var Speed = parseFloat(tempspeed[0].substring(tempspeed[0].indexOf("E,")+2))*1.85200;
	var datareturn = {
		Imei: "862170011849047",
		DegreeLatitude: DegreeLatitude,
		DegreeLongtitude: DegreeLongtitude,
		Speed: Speed
	}
	return callback(datareturn);

}

exports.sn_and_emie = function(data, callback){
	var serail = data.match("\\d{11}");
	var imie = data.match("\\d{15}");
	var idbox = new Buffer(data);
	var getsnimei = {
		Imie: imie,
		SN: serail
	}
	return callback(getsnimei);
}

