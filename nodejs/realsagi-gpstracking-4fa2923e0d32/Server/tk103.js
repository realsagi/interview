exports.Datagps = function(data, callback){

	var Imei = data.match("imei:"+"(\\d+),");
	if(Imei != null){
		var accon = data.indexOf("acc on");
		var accoff = data.indexOf("acc off");
		var speed = parseFloat(data.substring(data.lastIndexOf("E,")+2,data.lastIndexOf(",")))*1.85200;
		var dataimei = Imei[1];
		var Latitude = data.match("(\\d+)(\\d{2}\\.\\d+),"+"([NS]),");
		var DegreeLatitude = (parseFloat(Latitude[1]) + (parseFloat(Latitude[2]) / 60)).toFixed(6);
		var Longtitude = data.match("(\\d+)(\\d{2}\\.\\d+),"+"([EW])?,");
		var DegreeLongtitude = (parseFloat(Longtitude[1]) + (parseFloat(Longtitude[2]) / 60)).toFixed(6);
		var datareturn = {
			Imei: dataimei,
			DegreeLatitude: DegreeLatitude,
			DegreeLongtitude: DegreeLongtitude,
			Acc_On: accon,
			Acc_Off: accoff,
			Speed: speed
		};
		return callback(datareturn);
	}
	
} 

