var net = require('net');
var allfunction = require('../controller/allFunction');
//var HOST = '103.13.228.175';
//var HOST = '127.0.0.1';
var PORT = 9000;

var tk103 = require('./tk103.js');
var vt300 = require('./vt300.js');      
exports.LOCATIONDATATK103 = "Start";
exports.LOCATIONDATAVT300 = "Start";

try{

	var gpsserver = net.createServer(function(sock) {   

	    console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);
	    sock.on('data', function(data) {

	    	//console.log(data);
	    	console.log(data.toString());  
	        //console.log(data.toString().length);

	    	if(data.length == 17){
	    		sock.write(new Buffer('404000118724FFFFFFFFFF9001FFFF0D0A', 'hex'));
	    	}
	    	else if(data.length == 26){
	    		sock.write("LOAD");
	    	}
	    	else if(data.length == 70){
	    		vt300.sn_and_emie(data.toString(), function(requere){
		        	//console.log(requere);
		        });
	    	}
	    	else if(data.length >= 84 && data.length <= 109){
	    		tk103.Datagps(data.toString(), function(requere){
	        		exports.LOCATIONDATATK103 = requere;
					if(data.length == 26){
						sock.write("ON");
					}
	        	});
	    	}
	    	else if(data.length > 109 && data.length <= 128){
	    		vt300.Datagps(data.toString(), function(requere){
		        	exports.LOCATIONDATAVT300 = requere;
		        });
	    	}

	        //==================  LOG  FILE GPSBOX  =========================================
	        allfunction.createfile("/home/tam/logdata.txt", data+"\n("+data.length+")\n", function(callbackfromfs){
	        	//console.log(callbackfromfs);
	        })
	        //===============================================================================

	    });
	    
	    sock.on('close', function(data) {
	        //console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
	    });

	    //just added
		sock.on("error", function(err){
		    console.log("Caught flash policy server socket error: ");
			console.log(err.stack);
		});
	    
	})
	gpsserver.listen(PORT, function(){
		console.log('Server running http://'+gpsserver.address().address+":"+gpsserver.address().port);
	});
}
catch(err){
	console.log(err);
}

exports.getLOCATIONDATA = function (callback){
	var locationdata = {
			LOCATIONDATATK103: exports.LOCATIONDATATK103,
			LOCATIONDATAVT300: exports.LOCATIONDATAVT300
		}
	return callback(locationdata);
}
