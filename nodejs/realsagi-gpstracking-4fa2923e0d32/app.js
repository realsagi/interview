var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var fs = require('fs');
var sessions = require("client-sessions");
var multer  = require('multer');

var app = express();

app.set('view engine', 'ejs');
app.use(express.static(__dirname +  '/views'));
app.use(multer({ dest: __dirname +  '/views/uploads'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(sessions({
  cookieName: 'loginSession', // cookie name dictates the key name added to the request object
  secret: 'mysecretgpstrackinglogin', // should be a large unguessable string
  duration: 24 * 60 * 60 * 1000, // how long the session will stay valid in ms
  activeDuration: 1000 * 60 * 5 // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds
}));

app.set("mapkey","AIzaSyDc3WFomRM5nZ1GAeblfCv_azqGHfdsB_4");
app.set("titlename",":: Real-Time Tracking System ::");
app.set("myemail","realsagil@gmail.com");
app.set("passmail","7412374123");


eval(fs.readFileSync('controller/timeserver.js')+'');
eval(fs.readFileSync('controller/sendmail.js')+'');
eval(fs.readFileSync('controller/checklogin.js')+'');
eval(fs.readFileSync('controller/logout.js')+'');
eval(fs.readFileSync('controller/news.js')+'');
eval(fs.readFileSync('controller/controluser.js')+'');
eval(fs.readFileSync('controller/userall.js')+'');
eval(fs.readFileSync('controller/controllerpoi.js')+'');
eval(fs.readFileSync('controller/controllervehicle.js')+'');
eval(fs.readFileSync('controller/controllergpsbox.js')+'');
eval(fs.readFileSync('controller/controllergroupcar.js')+'');
eval(fs.readFileSync('controller/locationgpscar.js')+'');
eval(fs.readFileSync('controller/main.js')+'');

var server = app.listen(8080, function(){
    console.log('Server running http://'+server.address().address+":"+server.address().port);
});