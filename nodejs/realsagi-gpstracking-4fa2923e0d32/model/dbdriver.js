var fs = require('fs');
eval(fs.readFileSync('model/databaseconfig.js')+'');

var ID;  			//text PRIMARY KEY
var DriverLicense; 	//text
var Name;  			//text
var Phone; 			//text
var Picture2;  		//blob
var StartWork; 		//timestamp
var owner; 			//set

exports.clearvalue = function(){
	this.ID = undefined;
	this.DriverLicense = undefined;
	this.Name = undefined;
	this.Phone = undefined;
	this.Picture2 = undefined;
	this.StartWork = undefined;
	this.owner = undefined;
}

exports.save = function(callback){
	try{
		var query = 'INSERT INTO "Driver"("ID" , "DriverLicense", "Name", "Phone",'+
					'"Picture2", "StartWork", "owner") VALUES (\''+this.ID+'\','+
					'\''+this.DriverLicense+'\', \''+this.Name+'\', \''+this.Phone+'\','+
					'\''+this.Picture2+'\', \''+this.StartWork+'\', \''+this.owner+'\');';
		client.execute(query, queryOptions, function(err){
			if(err) {
			  	return callback("Cannot Save database at file model/dbdriver.js"+err);
			}
			else{
				return callback("Save New Driver Complate");
			} 
		}); 
	}
	catch(err){
		console.log("error at save file model/dbdriver.js"+err);
	}
}

exports.findByName = function(callback){
	try{
		var query = 'SELECT "ID", "Name" FROM "Driver" WHERE owner=\''+this.owner+'\' ;';
		client.execute(query, queryOptions, function(err, data){
			if(err) {
			  	return callback("Cannot find Name at file model/dbdriver.js");
			}
			else{
				return callback(data.rows);
			} 
		}); 
	}
	catch(err){
		console.log("error at findByName file model/dbdriver.js"+err);
	}
}

exports.deletedriver = function(callback){
	try{
		var query = 'DELETE FROM "Driver" WHERE "ID"=\''+this.ID+'\';';
		client.execute(query, queryOptions, function(err){
			if(err) {
			  	return callback("Cannot Delete Driver");
			}
			else{
				return callback("Delete Complate");
			} 
		}); 
	}
	catch(err){
		console.log("error at deletedriver file model/dbdriver.js"+err);
	}
}

exports.findByID = function(callback){
	try{
		var query = 'SELECT * FROM "Driver" WHERE "ID" = \''+this.ID+'\';';
		client.execute(query, queryOptions, function(err, data){
			if(err) {
			  	return callback("cannot findByID");
			}
			else{
				return callback(data.rows);
			} 
		}); 
	}
	catch(err){
		console.log("error at findByID file model/dbdriver.js"+err);
	}
}

exports.upDateDriver = function(callback){
	try{
		var query = 'UPDATE "Driver" SET "Name" = \''+this.Name+'\','+
					'"DriverLicense" = \''+this.DriverLicense+'\','+
					'"Phone" = \''+this.Phone+'\','+
					'"Picture2" = \''+this.Picture2+'\''+
					'WHERE "ID" = \''+this.ID+'\';';
		client.execute(query, queryOptions, function(err, result){
			if(err){
				return callback("Cannot Update Driver");
			}
			else{
				return callback("Update Driver Complate");
			}
		});
	}
	catch(err){
		console.log("error at upDateDriver file model/dbdriver.js"+err);
	}
}