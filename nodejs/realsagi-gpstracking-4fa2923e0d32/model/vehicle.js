var fs = require('fs');
eval(fs.readFileSync('model/databaseconfig.js')+'');

var ID  			//text PRIMARY KEY, ทะเบียนรถ
var Brand 			//text, ยี่ห้อรถ
var Series 			//text, รุ่นของรถ
var Year 			//text, ปีของรถ
var PlateNumber 	//text, เลขตัวถัง
var TaxDate 		//timestamp, วันต่อภาษี
var SpeedLimit 		//float, จำกัดความเร็ว
var FuelTank 		//int, ขนาดถัง
var FullTank 		//int, เต็มถัง
var MiddleTank		//int, ครึ่งถัง
var LowTank			//int, หมดถัง
var IMEI 			//text, หมายเลข อุปกรณ์ GPS
var owner 			//text, เจ้าของรถ
var Picture1 		//blob, รูปรถ 
var Picture2 		//blob, รูปรถ
var Picture3  		//blob, รูปรถ
var license 		//text,

exports.findByAll = function(callback){
	var query = 'SELECT * FROM "Vehicle";';
	client.execute(query, queryOptions, function(err, result){
	    if(err) {
	    	return callback("Error FindByAll");
	    }
	    else{
	    	return callback(result.rows);
	    } 
	});
}

exports.findByOwner = function(callback){
	var query = 'SELECT * FROM "Vehicle" WHERE owner=\''+this.owner+'\';';
	client.execute(query, queryOptions, function(err, result){
	    if(err) {
	    	return callback("Error findByOwner");
	    }
	    else{
	    	return callback(result.rows);
	    } 
	});
}

exports.upDateNotImg = function(callback){
	var query = 'UPDATE "Vehicle" SET "Brand"=\''+this.Brand+'\', "Series"=\''+this.Series+'\', "Year"=\''+this.Year+'\','+
				'"PlateNumber"=\''+this.PlateNumber+'\', "TaxDate"=\''+this.TaxDate+'\', "SpeedLimit"='+this.SpeedLimit+','+
				'"FuelTank"='+this.FuelTank+', "FullTank"='+this.FullTank+', "MiddleTank"='+this.MiddleTank+','+
				'"LowTank"='+this.LowTank+', "IMEI"=\''+this.IMEI+'\', "owner"=\''+this.owner+'\', "license"=\''+this.license+'\''+
				'WHERE "ID"=\''+this.ID+'\';';
	client.execute(query, queryOptions, function(err){
		if(err){
			return callback("Error upDateNotImg"+err);
		}
		else{
			return callback("update complate");
		}
	});
}

exports.saveall = function(callback){
	var query = 'INSERT INTO "Vehicle"("ID", "Brand", "Series", "Year", "PlateNumber", "TaxDate", "SpeedLimit", "FuelTank", '+
				'"FullTank", "MiddleTank", "LowTank", "IMEI", owner, "Picture1", "Picture2", "Picture3", license) '+
				'VALUES(\''+this.ID+'\', \''+this.Brand+'\', \''+this.Series+'\', \''+this.Year+'\', \''+this.PlateNumber+'\','+
				'\''+this.TaxDate+'\', '+this.SpeedLimit+', '+this.FuelTank+', '+this.FullTank+', '+this.MiddleTank+','+
				''+this.LowTank+', \''+this.IMEI+'\', \''+this.owner+'\', \''+this.Picture1+'\','+
				'\''+this.Picture2+'\', \''+this.Picture3+'\', \''+this.license+'\');';
	client.execute(query, queryOptions, function(err){
	    if(err) {
	    	return callback("Cannot Save new Vehicle"+err);
	    }
	    else{
	    	return callback("Save complate");
	    } 
	});
}

exports.findById = function(callback){
	var query = 'SELECT * FROM "Vehicle" WHERE "ID"=\''+this.ID+'\';';
	client.execute(query, queryOptions, function(err, result){
	    if(err) {
	    	return callback("Error FindById");
	    }
	    else{
	    	return callback(result.rows);
	    } 
	});
}

exports.findByImei = function(callback){
	var query = 'SELECT * FROM "Vehicle" WHERE "IMEI"=\''+this.IMEI+'\';';
	client.execute(query, queryOptions, function(err, result){
	    if(err) {
	    	return callback("Error FindByImei");
	    }
	    else{
	    	return callback(result.rows);
	    } 
	});
}

exports.deleteVehicle = function(callback){
	var query = 'DELETE FROM "Vehicle" WHERE "ID"=\''+this.ID+'\';';
	client.execute(query, queryOptions, function(err){
		if(err) {
		   	return callback("cannot Delet Vehicle");
		}
		else{ 		
		   	return callback("Delete Complate");
		} 
	});
}

exports.countrow = function(callback){
	var query = 'SELECT COUNT (*) FROM "Vehicle";';
	client.execute(query, queryOptions, function(err, result){
	    if(err) {
	    	return callback("Error countrow");
	    }
	    else{
	    	return callback(result.rows);
	    } 
	});
}

exports.upDateByUser = function(callback){
	var query = 'UPDATE "Vehicle" SET "Brand"=\''+this.Brand+'\', "Series"=\''+this.Series+'\', "Year"=\''+this.Year+'\','+
				'"TaxDate"=\''+this.TaxDate+'\', "SpeedLimit"='+this.SpeedLimit+', "license"=\''+this.license+'\''+
				'WHERE "ID"=\''+this.ID+'\';';
	client.execute(query, queryOptions, function(err){
		if(err) {
	    	return callback("Error upDateByUser");
	    }
	    else{
	    	return callback("Update Complate");
	    } 
	});
}