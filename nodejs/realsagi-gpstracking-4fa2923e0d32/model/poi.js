var fs = require('fs');
eval(fs.readFileSync('model/databaseconfig.js')+'');

var ID;
var Alert; 		//boolean, มีการแจ้งเตือนหรือไม่
var Email; 		//boolean, มีการแจ้งเตือนผ่านอีกเมล์หรือไม่
var Event; 		//text, ชื่อของจุดสนใจ
var Icon; 		//text, ไอคอน ของจุดสนใจ
var Latitude; 	//text, ละติจุด
var Longtitude;	//text, ลองติจุด
var Radius; 	//int, รัสมีของการแจ้งเตือน
var Sound; 		//boolean, มีการแจ้งเตือนผ่านเสียงหรือไม่
var UserID;  	//text เจ้าของคนสร้างเหตุการณ์นี้

exports.clearvalue = function(){
	this.Alert = undefined;
	this.Email = undefined;
	this.Event = undefined;
	this.Icon = undefined;
	this.Latitude = undefined;
	this.Longtitude = undefined;
	this.Radius = undefined;
	this.Sound = undefined;
	this.UserID = undefined;
}

exports.save = function(callback){
	var query = 'INSERT INTO "POI" ("ID", "Alert", "Email", "Event", "Icon", "Latitude", "Longtitude", "Radius", "Sound", "UserID")'+
				'VALUES (uuid(), '+this.Alert+', '+this.Email+', \''+this.Event+'\', \''+this.Icon+'\','+
				'\''+this.Latitude+'\', \''+this.Longtitude+'\', '+this.Radius+', '+this.Sound+', \''+this.UserID+'\');';
	client.execute(query, queryOptions, function(err){
		if(err) {
		   	return callback("cannot save new poi in file /model/poi.js"+err);
		}
		else{
		  	return callback("Save complate");
		} 
	});
}

exports.findByUserID = function(callback){
	var query = 'SELECT * FROM "POI" WHERE "UserID" = \''+this.UserID+'\';';
	client.execute(query, queryOptions, function(err, result){
		if(err) {
		   	return callback("cannot findByUserID");
		}
		else{ 		
		   	return callback(result.rows);
		} 
	});
}

exports.findeById = function(callback){
	var query = 'SELECT * FROM "POI" WHERE "ID" = '+this.ID+';';
	client.execute(query, queryOptions, function(err, result){
		if(err) {
		   	return callback("cannot findByID");
		}
		else{ 		
		   	return callback(result.rows);
		} 
	});
}

exports.DeletePoi = function(callback){
	var query = 'DELETE FROM "POI" WHERE "ID" = '+this.ID+';';
	client.execute(query, queryOptions, function(err){
		if(err) {
		   	return callback("cannot DeletPOI");
		}
		else{ 		
		   	return callback("Delete POI Complate");
		} 
	});
}

exports.UpdatePoi = function(callback){
	var query = 'UPDATE "POI" SET "Alert" = '+this.Alert+', "Email" = '+this.Email+', "Event" = \''+this.Event+'\','+
				'"Icon" = \''+this.Icon+'\', "Latitude" = \''+this.Latitude+'\', "Longtitude" = \''+this.Longtitude+'\','+
				'"Radius" = '+this.Radius+', "Sound" = '+this.Sound+' WHERE "ID" = '+this.ID+';';
	client.execute(query, queryOptions, function(err){
		if(err) {
		   	return callback("cannot UpdatePOI"+err);
		}
		else{ 		
		   	return callback("Update POI Complate");
		} 
	});
}