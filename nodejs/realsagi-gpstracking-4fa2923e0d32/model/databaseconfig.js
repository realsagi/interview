var cassandra = require('cassandra-driver');
var autlogin = new cassandra.auth.PlainTextAuthProvider('gps', 'gpspassword');
var client = new cassandra.Client({
    contactPoints:  ['103.13.228.175'], 
    portkey:        "9160", 
    keyspace:       'gpstracking', 
    authProvider:   autlogin
});

client.connect(function(err){
    if(err){
        console.log("ERROR Connect DATABASE"+err);
    }
    else{
        //console.log("Connect DataBase Complete");
    }
});

var queryOptions = {
    consistency:    cassandra.types.consistencies.quorum,
    prepare:        true
};

/* ทดลอง Query ALL Database More Record
var query = 'SELECT * FROM gpsdata';

client.execute(query, queryOptions, function (err, result) {
    if (err) console.error(err);
    else{
        console.log(result.rows.length);
    }
});
*/