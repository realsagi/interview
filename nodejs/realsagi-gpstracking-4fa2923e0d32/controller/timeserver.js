app.get('/timeserver', function(req, res){
    var date = new Date();
    var hour = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();
    res.send(hour+":"+minute+":"+second);
});