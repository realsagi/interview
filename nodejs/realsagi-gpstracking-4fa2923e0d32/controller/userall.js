app.post('/updateuser', function(req, res){
	var userall = require('./model/user');

	var name = req.body.name;
	var uname = req.body.uname;
	var ucompany = req.body.ucompany;
	var uphone =  req.body.uphone;
	var uemail = req.body.uemail;
	var uaddress = req.body.uaddress;
	if(name != undefined && uname != undefined && ucompany != undefined && uphone != undefined && uemail != undefined && uaddress != undefined){
		userall.ID = uname;
		userall.Company = ucompany;
		userall.Name = uname;
		userall.Phone = uphone;
		userall.address = uaddress;
		userall.email = uemail;
		userall.updatebyuser(function(resualt){
			if(resualt == "update complate"){
				req.loginSession.username = uname;
				req.loginSession.Name = name;
				req.loginSession.company = ucompany;
				req.loginSession.telphon = uphone;
				req.loginSession.usermail = uemail;
				req.loginSession.address = uaddress
				res.send(
	    			"Hollo "+name+"\n"
	    			+ "Username :"+uname+"\n"
	    			+ "Company :"+ucompany+"\n"
	    			+ "Telephone :"+uphone+"\n"
	    			+ "Email :"+uemail+"\n"
	    			+ "Address :"+uaddress+"\n"
	    			+ "ทำการบันทึกสำเร็จแล้วค่ะ"
	    		);
	    		userall.clearvalue();
			}
			else{
				res.send("บันทึกข้อมูลไม่สำเร็จค่ะ "+resualt);
			}
		});
	}

});

app.post('/changpassword', function(req, res){
	var userall = require('./model/user');

	var oldpass = req.body.oldpass;
	var newpass = req.body.newpass;
	var newpassagain = req.body.newpassagain;

	userall.findBydata('password', req.loginSession.username, function(resualt){
		if(resualt[0].password != oldpass){
			res.send("รหัสผ่านปัจจุบันของท่านไม่ถูกต้องค่ะ");
			userall.clearvalue();
		}
		else{

			userall.password = newpass;
			userall.ID = req.loginSession.username;

			userall.changepassword(function(resualt){
				if(resualt == "update complate"){
					res.send("update password complate");
					userall.clearvalue();
				}
				else{
					res.send("update password ไม่สำเร็จ");
					userall.clearvalue();
				}
			});
		}
	});
});

app.post('/createnewdriver', function(req, res){
	var driver = require('./model/dbdriver');
	driver.ID = req.body.iddriver;
	driver.DriverLicense = req.body.numiddirver;
	driver.Name = req.body.namedriver+"("+req.body.nicknamedriver+")";
	driver.Phone = req.body.phoneiddriver;
	driver.Picture2 = req.body.picture;
	driver.StartWork = req.body.firststartdirver;
	driver.owner = req.loginSession.username;
	driver.save(function(resualt){
		if(resualt == "Save New Driver Complate"){
			res.send("เพิ่มคนขับใหม่สำเร็จแล้วค่ะ");
		}
		else{
			res.send(resualt);
		}
	});
});

app.post('/searchallnamedriver', function(req, res){
	var driver = require('./model/dbdriver');
	driver.owner = req.loginSession.username;
	driver.findByName(function(resualt){
		if(resualt != "Cannot find Name at file model/dbdriver.js"){
			res.send(resualt);
		}
		else{
			res.send("เกิดข้อผิดพลาดกับการเชื่อมฐานข้อมูล");
		}
	});
});


app.post('/deleteinfodriver', function(req, res){
	var driver = require('./model/dbdriver');
	driver.ID = req.body.IDdriver;
	driver.deletedriver(function(resualt){
		res.send(resualt);
	});
});

app.post('/showinfodriver', function(req, res){
	try{
		var driver = require('./model/dbdriver');
		var dateFormat = require('dateformat');
		driver.ID = req.body.IDdriver;
		driver.findByID(function(resualt){
			if(resualt != "cannot findByID"){
				var olddate = new Date(resualt[0].StartWork);
				var datenow = new Date();
				var sumdate = Math.abs(datenow.getFullYear() - olddate.getFullYear()) + " ปี "+
							  Math.abs(datenow.getMonth() - olddate.getMonth()) + " เดือน "+
							  Math.abs(datenow.getDate() - olddate.getDate()) + " วัน ";
				var startatwork;
				startatwork =  dateFormat(olddate, "dd/mm/yyyy");
				var data = {
					"ID" : resualt[0].ID,
					"DriverLicense" : resualt[0].DriverLicense,
					"Name" : resualt[0].Name,
					"Phone" : resualt[0].Phone,
					"Picture2" : resualt[0].Picture2,
					"StartWork" : startatwork,
					"owner" : resualt[0].owner,
					"Daywork" : sumdate
				};
				res.send(data);
			}
			else{
				res.send(resualt);
			}
		});
	}
	catch(err){
		console.log("error app.post /showinfodriver file controller/userall.js"+err);
	}
});

app.post('/updateolddriver', function(req, res){
	try{
		var driver = require('./model/dbdriver');
		driver.ID = req.body.iddriver;
		driver.Name = req.body.namedriver+"("+req.body.nicknamedriver+")";
		driver.DriverLicense = req.body.numiddirver;
		driver.Phone = req.body.phoneiddriver;
		driver.Picture2 = req.body.picture;
		driver.upDateDriver(function(resualt){
		res.send(resualt);
	});
	}
	catch(err){
		console.log("error app.post /updateolddriver file controller/userall.js"+err);
	}
});

app.post('/checkhaveid', function(req, res){
	var driver = require('./model/dbdriver');
	driver.ID = req.body.iddriver;
	driver.findByID(function(resualt){
		if(resualt == ""){
			res.send("not have id");
		}
		else{
			res.send("have ID");
		}
	});
})