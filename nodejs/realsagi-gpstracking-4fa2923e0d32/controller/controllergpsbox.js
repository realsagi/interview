app.post('/findeuserforgpsbox', function(req,res){
	var user = require('./model/user');

	user.findByAll(function(resual){
		res.send(resual);
	});
});

app.post('/createnewgpsbox', function(req, res){
	var gpsbox = require('./model/gpsbox');

	gpsbox.imei = req.body.imie;
	gpsbox.brandbox = req.body.namegpsbox;
	gpsbox.owner = req.body.ownergpsbox;
	gpsbox.timestamp = new Date().getTime();

	gpsbox.save(function(resual){
		res.send(resual);
	});		
});

app.post('/gpsbox/findbyimie', function(req, res){
	var gpsbox = require('./model/gpsbox');

	gpsbox.imei = req.body.imie;

	gpsbox.findByImei(function(resual){
		res.send(resual);
	});

});

app.post('/gpsbox/findbyall', function(req, res){
	var gpsbox = require('./model/gpsbox');

	gpsbox.findByAll(function(resual){
		res.send(resual);
	});
});

app.post('/gpsbox/deletegpsbox', function(req,res){
	var gpsbox = require('./model/gpsbox');

	gpsbox.imei = req.body.imie;

	gpsbox.deleted(function(resual){
		res.send(resual);
	});
});

app.post('/gpsbox/updategpsbox', function(req, res){
	var gpsbox = require('./model/gpsbox');

	gpsbox.imei = req.body.imei;
	gpsbox.brandbox = req.body.brandbox;
	gpsbox.owner = req.body.owner;

	gpsbox.update(function(resual){
		res.send(resual);
	});
})

app.post('/gpsbox/findbyuses', function(req, res){
	var gpsbox = require('./model/gpsbox');

	gpsbox.uses = false;

	gpsbox.findByUses(function(resual){
		res.send(resual);
	});
});