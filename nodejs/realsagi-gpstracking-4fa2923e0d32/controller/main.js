app.get('/', function(req, res){
    if(req.loginSession.username != undefined && req.loginSession.usertype != undefined){
        res.redirect('/main');
    }
    else{
        res.render('login',{
            title: app.get("titlename")
        });
    }
});

app.get('/main', function(req, res){
    
    var newsmodel = require('./model/dbnews');
    var dateFormat = require('dateformat');

	if(req.loginSession.username == undefined){
		res.redirect('/');
	}
    else if(req.loginSession.usertype == undefined){
        res.redirect('/');
    }
    else{
        newsmodel.FindByAll(function (result){
            if(result == "Error execute function FindByAll model/dbnews.js"){
                res.redirect('/main');
            }
            else{

                var toppic = new Array();
                var timeStamp = new Array();
                var post = new Array();
                var picture  = new Array();
                var txtMessage = new Array();
                var sdatenextday = new Array();

                for(var i = 0; i < result.length; i++){
                    toppic[i] = result[i].Topic;
                    timeStamp[i] =  result[i].TimeStamp;
                    post[i] = result[i].UserID;
                    picture[i] = result[i].Pic;
                    txtMessage[i] = result[i].Message;
                }
                for(var i = 0; i < timeStamp.length-1; i++){
                    for(var j = i+1; j < timeStamp.length; j++){
                        if( timeStamp[i] < timeStamp[j] ){
                            var temp1,temp2,temp3,temp4,temp5;
                            temp1 = timeStamp[i];
                            temp2 = toppic[i];
                            temp3 = post[i];
                            temp4 = picture[i];
                            temp5 = txtMessage[i];

                            timeStamp[i] = timeStamp[j];
                            toppic[i] = toppic[j];
                            post[i] = post[j];
                            picture[i] = picture[j];
                            txtMessage[i] = txtMessage[j];

                            timeStamp[j] = temp1;
                            toppic[j] = temp2;
                            post[j] = temp3;
                            picture[j] = temp4
                            txtMessage[j] = temp5;
                        }
                    }
                }
                for(var i = 0; i < timeStamp.length; i++){
                    var datenextday = new Date().getTime();
                    var olddate = new Date(timeStamp[i]).getTime();

                    if(olddate+(1000*60*60*48) >= datenextday){
                        sdatenextday[i] = "img/new.gif"
                    }
                    else{
                        sdatenextday[i] = ""
                    }
                    timeStamp[i] =  dateFormat(timeStamp[i], "dd/mm/yyyy[HH:MM:ss]");
                }
                res.render('index',{
                    title: app.get("titlename"),
                    mapkey: app.get("mapkey"),
                    myemail: app.get("myemail"),
                    username: req.loginSession.username,
                    //<====== sector news ========= >
                    newsdata: result,
                    toppic: toppic,
                    timeStamp: timeStamp,
                    post: post,
                    txtMessage: txtMessage,
                    picture: picture,
                    sdatenextday: sdatenextday,
                    //<^^^^^^ sector news ^^^^^^^^^ >
                    nameuser: req.loginSession.Name,
                    company: req.loginSession.company,
                    phonenum: req.loginSession.telphon,
                    emailuser: req.loginSession.usermail,
                    address: req.loginSession.address
                });
            }
        });
	}
});