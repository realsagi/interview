function renderweb(res){
    res.render('user',{
        title: "เพิ่มผู้ใช้งาน",
        mapkey: app.get("mapkey")
    });
}

app.get('/user', function(req, res){
    renderweb(res);
});

app.post('/user', function(req, res){
	var user = require('./model/user');
	var fs = require('fs');
	
	var namesur = req.body.namesur;
	var useremail = req.body.useremail;
	var tel = req.body.tel;
	var typeu = req.body.type;
	var username = req.body.username;
	var password = req.body.password;
	var company = req.body.company;
	var latitude = req.body.latitude;
	var logtitude = req.body.logtitude;
	var signup = req.body.sign;
	var expri = req.body.exp;
	var verify = req.body.verify;
	var address = req.body.address;

	function insertnotimage(){
		user.ID = username;				//text PRIMARY KEY
		user.Company = company;			//text
		user.Latitude = latitude;  		//text
		user.Longtitude = logtitude; 	//text
		user.Name = namesur; 			//text
		user.Phone = tel;  				//set
		user.Type = typeu; 				//text
		user.email = useremail;			//text
		user.password = password; 		//text
		user.SignupDate = signup;		//timestamp
		user.ExpireDate = expri;		//timestamp
		user.address = address;
		if(verify == "True"){
			user.VerifyStatus = true;	//boolean
		}
		else if(verify == "False"){
			user.VerifyStatus = false;	//boolean
		}
	}
	if(req.files.picture1 == undefined && req.files.picture2 == undefined && req.files.picture3 == undefined){
		insertnotimage();
		user.save(function(resualt){
			if(resualt == "Save complate"){
				user.clearvalue();
				renderweb(res);
			}
			else{
				res.send(resualt);
			}
		})
	}
	else if(req.files.picture1 == undefined && req.files.picture2 == undefined && req.files.picture3 != undefined){
		fs.readFile(req.files.picture3.path, function(err, data){
			if(err){
				return callback("Error ReadFile at controluser.js"+err);
			}
			else{
				insertnotimage();
				user.Picture3 = new Buffer(data,'binary').toString('base64');

				user.save(function(resualt){
					if(resualt == "Save complate"){
						user.clearvalue();
						renderweb(res);
					}
					else{
						res.send(resualt);
					}
				});
			}
		});
	}
	else if(req.files.picture1 == undefined && req.files.picture2 != undefined && req.files.picture3 == undefined){
		fs.readFile(req.files.picture2.path, function(err, data){
			if(err){
				return callback("Error ReadFile at controluser.js"+err);
			}
			else{
				insertnotimage();
				user.Picture2 = new Buffer(data,'binary').toString('base64');

				user.save(function(resualt){
					if(resualt == "Save complate"){
						user.clearvalue();
						renderweb(res);
					}
					else{
						res.send(resualt);
					}
				});
			}
		});
	}
	else if(req.files.picture1 != undefined && req.files.picture2 == undefined && req.files.picture3 == undefined){
		fs.readFile(req.files.picture1.path, function(err, data){
			if(err){
				return callback("Error ReadFile at controluser.js"+err);
			}
			else{
				insertnotimage();
				user.Picture1 = new Buffer(data,'binary').toString('base64');

				user.save(function(resualt){
					if(resualt == "Save complate"){
						user.clearvalue();
						renderweb(res);
					}
					else{
						res.send(resualt);
					}
				});
			}
		});
	}
	else if(req.files.picture1 == undefined && req.files.picture2 != undefined && req.files.picture3 != undefined){
		fs.readFile(req.files.picture2.path, function(err, data){
			if(err){
				return callback("Error ReadFile at controluser.js"+err);
			}
			else{
				insertnotimage();
				user.Picture2 = new Buffer(data,'binary').toString('base64');
				fs.readFile(req.files.picture3.path, function(err, data2){
					if(err){
						return callback("Error ReadFile at controluser.js"+err);
					}
					else{
						user.Picture3 = new Buffer(data,'binary').toString('base64');
						user.save(function(resualt){
							if(resualt == "Save complate"){
								user.clearvalue();
								renderweb(res);
							}
							else{
								res.send(resualt);
							}
						});
					}
				});
			}
		});
	}
	else if(req.files.picture1 != undefined && req.files.picture2 == undefined && req.files.picture3 != undefined){
		fs.readFile(req.files.picture1.path, function(err, data){
			if(err){
				return callback("Error ReadFile at controluser.js"+err);
			}
			else{
				insertnotimage();
				user.Picture1 = new Buffer(data,'binary').toString('base64');
				fs.readFile(req.files.picture3.path, function(err, data2){
					if(err){
						return callback("Error ReadFile at controluser.js"+err);
					}
					else{
						user.Picture3 = new Buffer(data,'binary').toString('base64');
						user.save(function(resualt){
							if(resualt == "Save complate"){
								user.clearvalue();
								renderweb(res);
							}
							else{
								res.send(resualt);
							}
						});
					}
				});
			}
		});
	}
	else if(req.files.picture1 != undefined && req.files.picture2 != undefined && req.files.picture3 == undefined){
		fs.readFile(req.files.picture1.path, function(err, data){
			if(err){
				return callback("Error ReadFile at controluser.js"+err);
			}
			else{
				insertnotimage();
				user.Picture1 = new Buffer(data,'binary').toString('base64');
				fs.readFile(req.files.picture2.path, function(err, data2){
					if(err){
						return callback("Error ReadFile at controluser.js"+err);
					}
					else{
						user.Picture2 = new Buffer(data,'binary').toString('base64');
						user.save(function(resualt){
							if(resualt == "Save complate"){
								user.clearvalue();
								renderweb(res);
							}
							else{
								res.send(resualt);
							}
						});
					}
				});
			}
		});
	}
	else if(req.files.picture1 != undefined && req.files.picture2 != undefined && req.files.picture3 != undefined){
		fs.readFile(req.files.picture1.path, function(err, data){
			if(err){
				return callback("Error ReadFile at controluser.js"+err);
			}
			else{
				insertnotimage();
				user.Picture1 = new Buffer(data,'binary').toString('base64');
				fs.readFile(req.files.picture2.path, function(err, data2){
					if(err){
						return callback("Error ReadFile at controluser.js"+err);
					}
					else{
						user.Picture2 = new Buffer(data,'binary').toString('base64');
						fs.readFile(req.files.picture3.path, function(err, data){
							if(err){
								return callback("Error ReadFile at controluser.js"+err);
							}
							else{
								user.Picture3 = new Buffer(data,'binary').toString('base64');
								user.save(function(resualt){
									if(resualt == "Save complate"){
										user.clearvalue();
										renderweb(res);
									}
									else{
										res.send(resualt);
									}
								});
							}
						});
					}
				});
			}
		});
	}
});