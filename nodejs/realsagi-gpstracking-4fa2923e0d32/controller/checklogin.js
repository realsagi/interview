function checklogin(userlogin, passlogin, callback){

    var datauser = require('./model/user');
    
    datauser.findBydata(' * ', userlogin, function(result){
        //console.log(result);
        if(result != ""){
            if(userlogin == result[0].ID && passlogin == result[0].password && result[0].VerifyStatus == true) {
                var data = [true, result[0].Type, result[0].VerifyStatus, result[0]];
                return callback(data);
            }
            else{
                var data = [false, null, result[0].VerifyStatus, result[0]];
                return callback(data);
            }
        }
        else{
            var data = [false, null];
            return callback(data);
        }
    });
}
app.post('/checklogin', function(req, res){
    var username = req.body.uuName;
    var pass = req.body.ppWord;
    if(username != "" && pass != ""){
        checklogin(username, pass, function(checkpass){
            if(checkpass[0] == true){
                req.loginSession.username = username;
                req.loginSession.usertype = checkpass[1];
                req.loginSession.Name = checkpass[3].Name;
                req.loginSession.company = checkpass[3].Company;
                req.loginSession.telphon = checkpass[3].Phone;
                req.loginSession.usermail = checkpass[3].email;
                req.loginSession.address = checkpass[3].address;
                res.redirect('/main');
            }
            else{
                res.send("สถานะของท่าน ไม่พร้อมใช้งาน อาจจะเกิดจาก ถูกระงับการใช้งานชั่วคราว หรือ หมดอายุ หรืออาจะไม่มี user นี้อยู่จริง ค่ะ");
            }
        }); 
    }
    else{
        res.redirect('/');
    }
});

app.get('/getusertype', function(req,res){
    res.send(req.loginSession.usertype);
});
