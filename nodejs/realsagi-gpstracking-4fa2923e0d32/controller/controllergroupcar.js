app.post('/groupcar/save', function(req, res){
	var groupcar = require('./model/groupcar');

	groupcar.GroupName = req.body.groupname;
	groupcar.owner = req.loginSession.username;

	groupcar.save(function(resual){
		res.send(resual);
	});
});

app.post('/groupcar/findbyowner', function(req, res){
	var groupcar = require('./model/groupcar');

	groupcar.owner = req.loginSession.username;

	groupcar.findByOwner(function(resual){
		res.send(resual);
	});
});

app.post('/groupcar/updategroupname', function(req, res){
	var groupcar = require('./model/groupcar');

	groupcar.ID = req.body.groupnameid;
	groupcar.GroupName = req.body.groupname;

	groupcar.updateGroupName(function(resual){
		res.send(resual);
	})
});

app.post('/groupcar/deletegroupcar', function(req, res){
	var groupcar = require('./model/groupcar');

	groupcar.ID = req.body.groupnameid;

	groupcar.deletegroupcar(function(resual){
		res.send(resual);
	})
});

app.post('/groupcar/updatevehecle', function(req, res){
	var groupcar = require('./model/groupcar');

	groupcar.ID = req.body.idgroupcar;
	groupcar.Vehicle = req.body.arrayvehicle;

	groupcar.UpdateVehicle(function(resual){
		res.send(resual);
	});

});

app.post('/groupcar/findyid', function(req, res){
	var groupcar = require('./model/groupcar');

	groupcar.ID = req.body.idgroupcar;

	groupcar.findById(function(resual){
		res.send(resual);
	});
})

app.post('/groupcar/DeleteVehicleById', function(req, res){
	var groupcar = require('./model/groupcar');
	
	groupcar.ID = req.body.idgroupcar;
	groupcar.Vehicle = req.body.arrayvehicle;
	
	groupcar.DeleteVehicleById(function(resual){
		res.send(resual);
	});	
});