var nodemailer = require("nodemailer");
var sys = require('sys');

var newPath = __dirname + '/views/uploads';

var service = "Gmail";
var usermail = app.get("myemail");
var passmail = app.get("passmail");

var transporter = nodemailer.createTransport("SMTP",{
   service: service,
   auth: {
       user: usermail,
       pass: passmail
   }
});

app.get('/sendmail', function(req, res){
    if(req.loginSession.usertype != 1){
        res.redirect('/main');
    }
    else{
        res.render('dialogemail',{
            title: app.get("titlename"),
            myemail: app.get("myemail"),
        });
    }
});

app.post('/sendmail', function(req, res){
    try{
        var subject = req.body.name;
        var myemail = req.body.mymail;
        var sendmail = req.body.mail;
        var textcomment = req.body.comment;

        if(req.files.file != undefined){
            var mailOptions = {
                from: myemail, 
                to: sendmail, 
                subject: subject, 
                text: textcomment, 
                attachments: [
                     {   
                        fileName: req.files.file.originalname,
                        filePath: req.files.file.path
                    }
                ]
            };
        }
        else{
            var mailOptions = {
                from: myemail, 
                to: sendmail, 
                subject: subject, 
                text: textcomment, 
            };
        }
        
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
                res.send("ไม่สามารถส่ง Email ได้ พบข้อผิดพลาด"+err);
            }else{
                if(req.files.file != undefined){
                    fs.unlinkSync(req.files.file.path, function (err) {
                        if(err){
                            res.send("ลบไม่ได้ พบปัญหาเกี่ยวกับ การแนบไฟล์" + err);
                        }
                    });
                }
                console.log('Message sent: ' + info.message);
                res.send("ส่งอีเมล์เรียบร้อยแล้ว ค่ะ");
            }
        }); 
    }
    catch(err){
        res.send("ไม่สามารถส่ง Email ได้ พบข้อผิดพลาด"+err);
    }
});

