app.post('/createnewpoi', function(req, res){
	var poi = require('./model/poi');

	poi.Alert = req.body.onoffalert;
	poi.Email = req.body.alertemail;
	poi.Event = req.body.namepoi;
	poi.Icon = req.body.iconimg;
	poi.Latitude = req.body.latitude;
	poi.Longtitude = req.body.longitude;
	poi.Radius = parseInt(req.body.radius);
	poi.Sound = req.body.alertsound;
	poi.UserID = req.loginSession.username

	poi.save(function(resual){
		poi.clearvalue();
		res.send(resual);
	});
});

app.post('/finduseridpoi', function(req, res){
	var poi = require('./model/poi');
	
	poi.UserID = req.loginSession.username;
	poi.findByUserID(function(resual){
		res.send(resual);
	});
});

app.post('/deletepoi', function(req, res){
	var poi = require('./model/poi');

	poi.ID = req.body.dataid;
	poi.DeletePoi(function(resual){
		res.send(resual);
	});
});

app.post('/editpoi', function(req, res){
	var poi = require('./model/poi');

	poi.ID = req.body.dataid;
	poi.Alert = req.body.onoffalert;
	poi.Email = req.body.alertemail;
	poi.Event = req.body.namepoi;
	poi.Icon = req.body.iconimg;
	poi.Latitude = req.body.latitude;
	poi.Longtitude = req.body.longitude;
	poi.Radius = parseInt(req.body.radius);
	poi.Sound = req.body.alertsound;

	poi.UpdatePoi(function(resual){
		res.send(resual);
	});
});

app.post('/findbyid', function(req, res){
	var poi = require('./model/poi');
	
	poi.ID = req.body.dataid;
	poi.findeById(function(resual){
		res.send(resual);
	});
});