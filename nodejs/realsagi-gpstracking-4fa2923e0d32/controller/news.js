app.get('/addnews', function(req,res){
    res.render('news',{ 
    	title			: "หน้าประกาศข่าว",
    	statusforpost	: "" 
    });
});

app.post('/addnews', function(req,res){

	var newsmodel = require('./model/dbnews');
	var fs = require('fs');
	var allfunction = require('./controller/allFunction');

	function respage(result){
		if(result == "Save DB Complete"){
			res.render('news',{ 
	    		title			: "หน้าประกาศข่ว",
	    		statusforpost	: "ได้ทำการประกาศข่าวแล้ว" 
	    	});
	    }else{
	    	res.render('news',{ 
	    		title			: "หน้าประกาศข่ว",
	    		statusforpost	: "ไม่สามารถบันทึกข่าวได้ เกิดข้อผิดพลาด >>> "+result 
	    	});
	    }
	}
	
	var timestamp = new Date().getTime();
	var toppic = req.body.topic;
	var content = req.body.txcontent;
	var image = req.files.picture;
	var postby = req.body.postby;
	if(image != undefined){
		fs.readFile(image.path, function(err, data){
			if(err){
				return callback("Error ReadFile at controluser.js"+err);
			}
			else{
				var imagepic = new Buffer(data,'binary').toString('base64');
				newsmodel.InsertAll(timestamp ,toppic, content, imagepic, postby, function(result){
					respage(result);
					allfunction.deletefile(image.path, function(redata1){
							//not event
					});
				});	
			}
		});
	}
	else{
		newsmodel.InsertNopic(timestamp ,toppic, content, postby, function(result){
			respage(result);
		});	
	}
	
});
app.post('/checkupdatenews', function(req,res){
	var newsmodel = require('./model/dbnews');
	var	countallnews = req.body.mycount;
	newsmodel.Countnews(function(result){
		if(result != "Error execute function FindByAll model/dbnews.js"){
			var count = result[0].count.low;
			if(countallnews != count && countallnews != undefined){
				countallnews = count;
				res.send("อัพเดทได้เลย");
			}
			else{
				res.send("ไม่มีอัพเดท");
			}
		}
	});
});