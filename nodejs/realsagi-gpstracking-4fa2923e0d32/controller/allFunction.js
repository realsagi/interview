//@param p1: {lat:X,lng:Y}
//@param p2: {lat:X,lng:Y}
exports.get_distance = function(p1, p2) {
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = exports.rad(p2.lat - p1.lat);
    var dLong = exports.rad(p2.lng - p1.lng);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(exports.rad(p1.lat)) * Math.cos(exports.rad(p2.lat)) *
    Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d; // returns the distance in meter
};

exports.upload_file = function(req, res) {
    console.log(req.files);
    console.log(req.body);
    var repathfile = req.files.file.path;
    fs.readFile(repathfile, function (err, data){
        var newPath = __dirname + "/views/uploads/" + req.files.file.originalname;
        console.log(data);
        fs.writeFile(newPath, data, function (err) {
            if(err){
                console.log("Error");
            }else {
                console.log("Save");
            }
        });
    });
}

exports.getTimeStamp = function() {
    var now = new Date();
    return (now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDate()+" "+now.getHours()+':'+
            ((now.getMinutes() < 10)? ("0" + now.getMinutes()):(now.getMinutes()))+':'+
            ((now.getSeconds() < 10)? ("0" + now.getSeconds()):(now.getSeconds())));
}

exports.readasblob = function(path, callback){
    var fs = require('fs');
    fs.readFile(path, function(err, data){
        if(err){
            return callback("Error ReadFile at allFunction.js"+err);
        }
        else{
            var imagepic = new Buffer(data,'binary').toString('base64');
            return callback(imagepic);
        }
    });
}

exports.deletefile = function(path, callback){
    var fs = require('fs');
    fs.unlink(path, function (err) {
        if(err){
            return callback("Error Deletefile"+err);
        }
        else{
            return callback("Delete Complate");
        }
    });
}

exports.createfile = function(path, data, callback){
    var fs = require('fs');
    fs.appendFile(path, data.toString()+"\n"+data.length+"\n", function (err) {
    if (err) throw err;
        return callback('The "data to append" was appended to file!');
    });
}