app.get('/addnewvehicle', function(req,res){
    res.render('vehicle',{ 
    	title			: "เพิ่มรถใหม่ และ กล่อง gps",
    });
});

app.post('/vehicle/findbyid', function(req, res){
	var vehicle = require('./model/vehicle');

	vehicle.ID = req.body.registration;
	vehicle.findById(function(resual){
		res.send(resual);
	});

});

app.post('/vehicle/findbyimei', function(req, res){
	var vehicle = require('./model/vehicle');

	vehicle.IMEI = req.body.imeicaredit;
	vehicle.findByImei(function(resual){
		res.send(resual);
	})
});

app.post('/vehicle/updatevehicle', function(req, res){
	var vehicle = require('./model/vehicle');

	vehicle.ID = req.body.Idvehicle;
	vehicle.license = req.body.license;
	vehicle.Brand = req.body.Brand;
	vehicle.Series = req.body.Series;
	vehicle.Year = req.body.Year;
	vehicle.PlateNumber = req.body.PlateNumber;
	vehicle.TaxDate = req.body.TaxDate;
	vehicle.SpeedLimit = req.body.SpeedLimit;
	vehicle.FuelTank = req.body.FuelTank;
	vehicle.FullTank = req.body.FullTank;
	vehicle.MiddleTank = req.body.MiddleTank;
	vehicle.LowTank = req.body.LowTank;
	vehicle.IMEI = req.body.IMEI;
	vehicle.owner = req.body.owner;

	vehicle.upDateNotImg(function(resual){
		res.send(resual);
	});
});

app.post('/addnewvehicle', function(req, res){
	var vehicle = require('./model/vehicle');
	var allfunction = require('./controller/allFunction');

	vehicle.countrow(function(row){
		if(row[0].count == "0"){
			vehicle.ID = "10001";
		}
		else{
			var count = parseInt(row[0].count)+10001;
			vehicle.ID = count;
		}
		vehicle.license = req.body.registration;
		vehicle.Brand = req.body.brandcar;
		vehicle.Series = req.body.seriescar;
		vehicle.Year = req.body.yearcar;
		vehicle.PlateNumber = req.body.numtankcar;
		vehicle.TaxDate = req.body.taxdate;
		vehicle.SpeedLimit = req.body.speedlimit;
		vehicle.FuelTank = req.body.littertank;
		vehicle.FullTank = req.body.fulltank;
		vehicle.MiddleTank = req.body.middletank;
		vehicle.LowTank = req.body.lowtank;
		vehicle.IMEI = req.body.numimie;
		vehicle.owner = req.body.ownercar;

		allfunction.readasblob(req.files.picture1.path, function(resual){
			vehicle.Picture1 = resual;
			allfunction.readasblob(req.files.picture2.path, function(resual2){
				vehicle.Picture2 = resual2;
				allfunction.readasblob(req.files.picture3.path, function(resual3){
					vehicle.Picture3 = resual3;
					vehicle.saveall(function(saveresual){
						if(saveresual == "Save complate"){
							allfunction.deletefile(req.files.picture1.path, function(redata1){
								//not event
							});
							allfunction.deletefile(req.files.picture2.path, function(redata1){
								//not event
							});
							allfunction.deletefile(req.files.picture3.path, function(redata1){
								//not event
							});
							var gpsbox = require('./model/gpsbox');
							gpsbox.imei = req.body.numimie;
							gpsbox.uses = true;
							gpsbox.updteuses(function(resdataupdate){
								if(resdataupdate != "Cannot UPDATE Uses"){
									res.send(saveresual);
								}
								else{
									res.send(resdataupdate);
								}
							});
						}
						else{
							res.send(saveresual);
						}
					});
				});
			});	
		});
	});
});

app.post('/vehicle/findbyall', function(req, res){
	var vehicle = require('./model/vehicle');

	vehicle.findByAll(function(resualt){
		res.send(resualt);
	});
});

app.post('/vehicle/deletevehicle', function(req, res){
	var vehicle = require('./model/vehicle');

	vehicle.ID = req.body.registration;
	vehicle.deleteVehicle(function(resual){
		res.send(resual);
	});
});

app.post('/vehicle/findbyowner', function(req, res){
	var vehicle = require('./model/vehicle');

	vehicle.owner = req.loginSession.username;
	vehicle.findByOwner(function(resual){
		res.send(resual);
	});
});

app.post('/vehicle/updatebyuser', function(req, res){
	var vehicle = require('./model/vehicle');

	vehicle.ID = req.body.idvehicle;
	vehicle.license = req.body.editregistration;
	vehicle.Brand = req.body.editbrandcar;
	vehicle.Series = req.body.editseriescar;
	vehicle.Year = req.body.edityearcar;
	vehicle.TaxDate = req.body.edittaxdate;
	vehicle.SpeedLimit = req.body.editspeedlimitbyuser;

	vehicle.upDateByUser(function(resual){
		res.send(resual);
	});	
});