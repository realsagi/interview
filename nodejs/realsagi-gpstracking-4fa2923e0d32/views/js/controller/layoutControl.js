// JavaScript Document
var map;
var beachMarker,circle;
var positionlatitude = 14.876323046178632;
var postitionlongtitude = 102.0211782883606;
var multicargpsdata = [];

var jqxhr = $.get("/getusertype", function(data) {
    var userType = data; // 0 = Normal , 1 = Special
    var panelTool, panelResizer, panelMap;
    var startX, startWidth;

    $(window).resize(function() { setResolution(); });



    $(document).ready(function() {
        panelTool = $(".panelTool");
        panelResizer = $(".panelResizer");
        panelMap = $(".panelMap");

        setResolution();
        frameResizer();

        // Custom layout by user
        customLayoutByUser();

    });

    function customLayoutByUser() {
        if (userType == 0) {
            // Normal User
            $(".userS").remove();
            $(".userN").show();
            // Create Main Menu Event
            mainMenuCreate_UserN($(".menuBar_R .userN .mm1"), $(".panelTool .toolHome"));
            mainMenuCreate_UserN($(".menuBar_R .userN .mm2"), $(".panelTool .toolTrack"));
            mainMenuCreate_UserN($(".menuBar_R .userN .mm3"), $(".panelTool .toolHistory"));
            mainMenuCreate_UserN($(".menuBar_R .userN .mm4"), $(".panelTool .toolReport"));
            mainMenuCreate_UserN($(".menuBar_R .userN .mm5"), $(".panelTool .toolMarker"));
            mainMenuCreate_UserN($(".menuBar_R .userN .mm6"), $(".panelTool .toolSetting"));

        } else {
            // Special User
            $(".userN").remove();
            $(".userS").show();
            // Create sub menu
            subMenuCreate_UserS();
        }
    }

    // Set resolution
    function setResolution() {
        // Set height of page -----------------------------------------------------------------------------------------------------------------
        $(".mainRoot").height($(window).height());
        var content = $(window).height() - $(".mainHeader").height() - $(".mainFooter").height();
        $(".mainBody").height(content);
        panelMap.height(content);
        panelResizer.height(content);
        panelTool.height(content);

        /* Width */
        var maxWidth = $(window).width();
        if (maxWidth < 1006) { maxWidth = 1006; } /* 1000 = map + tool + resizer = 6*/
        var panelMap_Width = (maxWidth - panelTool.width()) - 6;
        if (panelMap_Width < 360) { panelMap_Width = 360; }
        panelMap.width(panelMap_Width);
        var resWidth = panelMap_Width + 6 + panelTool.width();
        $(".mainRoot").width(resWidth);
        $(".mainBody").width(resWidth);
    };


    function frameResizer(){
        panelResizer.bind('mousedown', function(e) {
            startX = e.clientX;
            startWidth = panelTool.width();
            $(document).bind('mousemove', frameResizerDrag);
            $(document).bind('mouseup', frameResizerStop);		
        });
    }

    function frameResizerDrag(e) {
        var width = startWidth + (startX - e.clientX);
        if (width < 420) {
            width = 420;
        } else if (width > 640) {
            width = 640;
        }
        panelTool.width(width);
        var maxWidth = $(window).width();
        panelMap.width(maxWidth - width - 6);
        //	setResolution();
    }

    function frameResizerStop(e) {
        $(document).unbind('mousemove', frameResizerDrag);
        $(document).unbind('mouseup', frameResizerStop);
    }

    /* Normal User Main Menu --------------------------------------------------------------------------------------------------*/
    function mainMenuCreate_UserN(mainElm, targetElm) {
        mainElm.bind('mousedown', function() {
            $(".panelTool .toolRoot").hide(); // Hide All
            targetElm.show();
        });
    }


    /* Special User Main Menu & Sub Menu -------------------------------------------------------------------------------*/
    function subMenuCreate_UserS() {
        $(".subMenu").hide(); // Hide all first

        // Sub Menu 2 Event
        subMenuEvent_UserS($(".menuBar_R .userS .mm2"), $(".subMenuRoot .sm2"));
        // Sub Menu 3 Event
        subMenuEvent_UserS($(".menuBar_R .userS .mm3"), $(".subMenuRoot .sm3"));
    }

    function subMenuEvent_UserS(mainElm, subElm) {
        subElm.hover(function() {
            $(this).data("hovered", true);
            mainElm.addClass("mmAct");
        }, function() {
            $(this).data("hovered", false).stop().fadeOut(160);
            mainElm.removeClass("mmAct");
        }); 	

        mainElm.bind('mouseenter', function() {
            subMenuClearAll_UserS(subElm);
            subElm.css("height", "auto").slideDown(160); }
        );
        mainElm.bind('mouseout', function() {
            setTimeout(function() { if (!subElm.data("hovered")) { subElm.stop().fadeOut(160); } }, 10);
        });
    }

    function subMenuClearAll_UserS(except) {
        $(".subMenuRoot .subMenu").each(function() {
            if (!$(this).is(except)) {
                $(this).stop().fadeOut(160);
            }
        });
    }
})


calltime();
var contcheckupdatenews = 0;
var countchecklocation = 0;
getLocationdata();
function calltime(){
    $.get("/timeserver",
        {
        }, 
        function(data,status){
            $('#timeserver').text(data);
        }
    );
    contcheckupdatenews++;
    if(contcheckupdatenews == 10){
        contcheckupdatenews = 0;
        checkupdatenews();
    }
    countchecklocation++;
    if(countchecklocation == 20){
        countchecklocation = 0;
    }
    loaddatalocation();
    loaddatamaprealtime();
    setTimeout("calltime()",1000);
}

function initialize() {
    var mapOptions = {
        center: {
            lat: positionlatitude,
            lng: postitionlongtitude
        },
        zoom: 5
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
}

function checkupdatenews(){
    $.post("/checkupdatenews",
        {
            mycount : $('#allnewsnum').val()
        }, 
        function(data,status){
            if(data == "อัพเดทได้เลย"){
                window.location.reload(true);
            }
            else{
                
            }
        }
    );
}
$.dataimei = undefined;
function getLocationdata(){
    $.post("/vehicle/findbyowner",
        {
        },
        function(data ,status){
            if(data != undefined){
                $.dataimei = data;
            }
        }
    );
}

function loaddatamaprealtime() {
    $.post("/gpsdata/location",  
        {
        }, 
        function(datagps,status){
            //console.log(datagps.LOCATIONDATATK103);
            //console.log(datagps.LOCATIONDATAVT300);
            if(datagps.LOCATIONDATATK103 != "No Sign GPS" || datagps.LOCATIONDATAVT300 != "No Sign GPS"){
                
            }
            if($.dataimei != undefined){
                for(var i = 0 ; i < $.dataimei.length ; i++){
                    if(datagps.LOCATIONDATATK103 != "No Sign GPS" || datagps.LOCATIONDATAVT300 != "No Sign GPS"){
                        if(datagps.LOCATIONDATATK103.Imei == $.dataimei[i].IMEI){
                            if(multicargpsdata[parseInt($.dataimei[i].ID)] != undefined){
                                multicargpsdata[parseInt($.dataimei[i].ID)].setMap(null);
                            }
                            markcarbydatagps(datagps.LOCATIONDATATK103.DegreeLatitude, datagps.LOCATIONDATATK103.DegreeLongtitude, parseInt($.dataimei[i].ID));
                        }
                        if(datagps.LOCATIONDATAVT300.Imei == $.dataimei[i].IMEI){
                            if(multicargpsdata[parseInt($.dataimei[i].ID)] != undefined){
                                multicargpsdata[parseInt($.dataimei[i].ID)].setMap(null);
                            }
                            markcarbydatagps(datagps.LOCATIONDATAVT300.DegreeLatitude, datagps.LOCATIONDATAVT300.DegreeLongtitude, parseInt($.dataimei[i].ID));
                        }
                    }
                }
            }
        }
    );
}

function markcarbydatagps(latitude, longtitude, numid){
    var myLatLng = new google.maps.LatLng(latitude, longtitude);
    multicargpsdata[numid] = new google.maps.Marker({
        position: myLatLng,
        map: map,
        //draggable:true,
        icon: "img/setpoint/Lamborghini-icon.png"
    });
    google.maps.event.addListener(multicargpsdata[numid], 'click', function() {
        $('#check'+numid+'').prop('checked', true);
    });
    //map.setZoom(17);
    //map.setCenter(multicargpsdata.getPosition());
}