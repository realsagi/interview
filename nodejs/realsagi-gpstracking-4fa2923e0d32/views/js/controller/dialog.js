var i = 0;
function probar(){
    i++;
    $( "#progressbar" ).show();
    if(i < 99){
        $( "#progressbar" ).progressbar({
            value: i
        });
    }
    setTimeout("probar()",100);
}

function gosendmail(){
    try{
        if($('#nametopic').val() == ""){
            alert("กรุณาใส่หัวข้อ เรื่องด้วย ค่ะ");
            $('#nametopic').focus();
        }
        else if($('#sendmail').val() == ""){
            alert("กรุณาใส่ Email ผู้รับด้วยค่ะ");
            $('#sendmail').focus();
        }
        else if($('#txtArea').val() == ""){
            alert("กรุณาใส่เนื้อหาที่จะส่งด้วยค่ะ");
            $('#txtArea').focus();
        }
        else{
            probar();
            $("form#formsendmail").submit();
        }

    }catch(err){
        alert("dialog.js error >> "+err);
    }

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#preview').attr('src', e.target.result);
            $('#preview').show();
        }
        reader.readAsDataURL(input.files[0]);
     }
 }