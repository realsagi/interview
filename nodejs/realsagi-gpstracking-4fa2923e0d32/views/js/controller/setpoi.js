var togglesetofpoi = 0;
var pathicon,editpathicon = "";
var multimarker = [];
var infowindow = [];
var pointMarker,circle;
var dataidforedit;
var ddData = [
  {
    text: "1",
    value: 0,
    selected: false,
    imageSrc: "img/setpoint/1.png"
  },
  {
    text: "2",
    value: 1,
    selected: false,
    imageSrc: "img/setpoint/2.png"
  },
  {
    text: "3",
    value: 2,
    selected: false,
    imageSrc: "img/setpoint/3.png"
  },
  {
    text: "4",
    value: 3,
    selected: false,
    imageSrc: "img/setpoint/4.png"
  },
  {
    text: "5",
    value: 4,
    selected: false,
    imageSrc: "img/setpoint/5.png"
  },
  {
    text: "6",
    value: 5,
    selected: false,
    imageSrc: "img/setpoint/6.png"
  },
  {
    text: "7",
    value: 6,
    selected: false,
    imageSrc: "img/setpoint/7.png"
  },
  {
    text: "8",
    value: 7,
    selected: false,
    imageSrc: "img/setpoint/8.png"
  },
  {
    text: "9",
    value: 8,
    selected: false,
    imageSrc: "img/setpoint/9.png"
  },
  {
    text: "10",
    value: 9,
    selected: false,
    imageSrc: "img/setpoint/10.png"
  },
  {
    text: "11",
    value: 10,
    selected: false,
    imageSrc: "img/setpoint/11.png"
  },
    {
    text: "12",
    value: 11,
    selected: false,
    imageSrc: "img/setpoint/12.png"
  },
  {
    text: "13",
    value: 12,
    selected: false,
    imageSrc: "img/setpoint/13.png"
  },
  {
    text: "14",
    value: 13,
    selected: false,
    imageSrc: "img/setpoint/14.png"
  }
];

function changesrctoindex(input){
  if(input == "img/setpoint/1.png"){
    return 0;
  }
  else if(input == "img/setpoint/2.png"){
    return 1;
  }
  else if(input == "img/setpoint/3.png"){
    return 2;
  }
  else if(input == "img/setpoint/4.png"){
    return 3;
  }
  else if(input == "img/setpoint/5.png"){
    return 4;
  }
  else if(input == "img/setpoint/6.png"){
    return 5;
  }
  else if(input == "img/setpoint/7.png"){
    return 6;
  }
  else if(input == "img/setpoint/8.png"){
    return 7;
  }
  else if(input == "img/setpoint/9.png"){
    return 8;
  }
  else if(input == "img/setpoint/10.png"){
    return 9;
  }
  else if(input == "img/setpoint/11.png"){
    return 10;
  }
  else if(input == "img/setpoint/12.png"){
    return 11;
  }
  else if(input == "img/setpoint/13.png"){
    return 12;
  }
}

function iconselect(){
  $('#iconselect').ddslick(
    {
      data: ddData,
      width: 170,
      imagePosition: "left",
      selectText: "เลือกภาพสัญลักษณ์",
      onSelected: function (data) {
        pathicon = data.selectedData.imageSrc;
      }
    }
  );
}

function updatmylatlng(latLng){
  var latitude = [latLng.lat()].join();
  var logtitude = [latLng.lng()].join();
  $('#lattitudepoi').val(latitude);
  $('#longtitudepoi').val(logtitude);
  circle.setMap(null);
  createcircle($('#raduiasalert').val(), latitude, logtitude);
}

function changeraduisalert(){
  if(togglesetofpoi == 1){
    circle.setMap(null);
    if($('#lattitudepoi').val() == "" && $('#longtitudepoi').val() == ""){
      createcircle($('#raduiasalert').val(), positionlatitude, postitionlongtitude);
    }
    else{
      createcircle($('#raduiasalert').val(), $('#lattitudepoi').val(), $('#longtitudepoi').val());
    }
  }
}

function createcircle(radius, latitude, logtitude){
  var myLatLng = new google.maps.LatLng(latitude, logtitude);
  circle = new google.maps.Circle({
    strokeColor: '#FF0000',
    fillColor: '#FF4A4A',
    map: map,
    center: myLatLng,
    radius: parseInt(radius)
  });
}

function createmarker(){
  var myLatLng = new google.maps.LatLng(positionlatitude, postitionlongtitude);
  var imagemapicon = 'img/pin.png';
  pointMarker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: imagemapicon,
    draggable:true,
  });
  map.setZoom(17);
  map.setCenter(pointMarker.getPosition());
  google.maps.event.addListener(pointMarker, 'drag', function() {
    updatmylatlng(pointMarker.getPosition());
  });
}

function setofpoi(){
  if($('#raduiasalert').val() == ""){
    alert("กรุณาระบุรัสมีการแจ้งเตือนก่อนค่ะ");
    $('#raduiasalert').focus();
  }
  else{
    if(togglesetofpoi == 0){
      createmarker();
      createcircle($('#raduiasalert').val(), positionlatitude, postitionlongtitude);
      togglesetofpoi = 1;
    }
    else{
      pointMarker.setMap(null);
      circle.setMap(null);
      togglesetofpoi = 0;
    }
  }
}

function clearmap(){
  if(circle != undefined){
    circle.setMap(null);
  }
  if(pointMarker != undefined){
    pointMarker.setMap(null);
  }
  for(var i = 0 ; i < multimarker.length ; i++){
    multimarker[i].setMap(null);
    infowindow[i].close();
  }
  multimarker = [];
  infowindow = [];
  togglesetofpoi = 0;
}

function onclickcheckboxsetpoi(){
  if($('#onoffalert').is(':checked') == true){
    $('#alertarea').show();
    $("#alertsound").prop('checked', true);
  }
  else{
    $("#alertsound").prop('checked', false);
    $('#alertarea').hide();
  }
}

function clearvalue(){
  $('#namepoi').val("");
  $('#lattitudepoi').val("");
  $('#longtitudepoi').val("");
  $('#raduiasalert').val("");
  pathicon = "";
  $('#onoffalert').prop('checked', false);
  $('#alertemail').prop('checked', false);
  $('#alertsound').prop('checked', false);
  $('#alertarea').hide();
  $('#iconselect').ddslick('destroy');
  iconselect();
  if(togglesetofpoi == 1){
    loadsetpoi();
    togglesetofpoi = 0;
  }
}

function createnewpoi(){
  var namepoi = $('#namepoi').val();
  var latitude = $('#lattitudepoi').val();
  var longitude = $('#longtitudepoi').val();
  var radius = $('#raduiasalert').val();
  var iconimg = pathicon;
  var onoffalert = $('#onoffalert').is(':checked');
  var alertsound = $('#alertsound').is(':checked');
  var alertemail = $('#alertemail').is(':checked');
  if(namepoi == ""){
    alert("กรุณาระบุชื่อจุดสนใจด้วยค่ะ");
    $('#namepoi').focus();
  }
  else if(latitude == "" || longitude == ""){
    alert("กรุณากดเลือกที่ icon หมุดเพื่อระบุพิกันด้วยค่ะ");
  }
  else if(radius == ""){
    alert("กรุณาระบุเลขรัสมีด้วยค่ะ");
    $('#raduiasalert').focus();
  }
  else{
    $.post("/createnewpoi", 
      {
        namepoi: namepoi,
        latitude: latitude,
        longitude: longitude,
        radius: radius,
        iconimg: iconimg,
        onoffalert: onoffalert,
        alertsound: alertsound,
        alertemail: alertemail
      }, 
      function(data,status){
        if(data == "Save complate"){
          clearvalue();
          clearmap();
          loadsetpoi();
          alert("เพิ่มจุดสนใจเรียบร้อยแล้วค่ะ");
        }
        else{
          createnewpoi();
        }
      }
    );
  }
}

function createmultimarker(data , i){
  var myLatLng = new google.maps.LatLng(data.Latitude, data.Longtitude);
  var contentstring = '<h4>'+data.Event+'</h4><br>'+
                '<h5>ละติจุด<br>'+data.Latitude+'</h5><br>'+
                '<h5>ลองจิจุด<br>'+data.Longtitude+'</h5><br>';
  var Marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: data.Icon
  });
  var info = new google.maps.InfoWindow({
      content: contentstring,
      maxWidth: 800
  });
  infowindow.push(info);
  multimarker.push(Marker);
  google.maps.event.addListener(Marker, 'click', function() {
    infowindow[i].open(map,multimarker[i]);
  });
}

function eventclickpoint(input){
  map.setZoom(17);
  map.setCenter(multimarker[input].getPosition());
  infowindow[input].open(map,multimarker[input]);
}

function deletepoi(input){
  $.post("/deletepoi",
      {
        dataid: input.alt
      },
      function(data, status){
        if(data == "Delete POI Complate"){
          loadsetpoi();
          alert("ทำการลบจุดสนใจเรียบร้อยแล้ว่ะ");
        }
      }
    );
}

function editpoi(input, value){
  $.post("/findbyid",
    {
      dataid: input.alt
    },
    function(data, status){
      $('#editdivsetpoi').show();
      $('#viewsinfopoiintable').hide();
      $("#editonoffalert").prop('checked', false);
      $("#editalertemail").prop('checked', false);
      $("#editalertsound").prop('checked', false);

      $('#editnamepoi').val(data[0].Event);
      $('#editlattitudepoi').val(data[0].Latitude);
      $('#editlongtitudepoi').val(data[0].Longtitude);
      $('#editraduiasalert').val(data[0].Radius);

      if(data[0].Alert == true){
        $("#editonoffalert").prop('checked', true);
      }
      if(data[0].Email == true){
        $("#editalertemail").prop('checked', true);
      }
      if(data[0].Sound == true){
        $("#editalertsound").prop('checked', true);
      }
      $('#editiconselect').ddslick('destroy');
      $('#editiconselect').ddslick(
        {
          data: ddData,
          width: 170,
          imagePosition: "left",
          onSelected: function (data) {
            editpathicon = data.selectedData.imageSrc;
          }
        }
      );
      dataidforedit = input.alt;
      $('#editiconselect').ddslick('select', {index:  changesrctoindex(value)});
      clearmap();
      createeditpoi($('#editlattitudepoi').val(), $('#editlongtitudepoi').val(), value);
      createeditcirclepoi($('#editlattitudepoi').val(), $('#editlongtitudepoi').val());
    }
  );
}

function createeditcirclepoi(lat, lng){
  var myLatLng = new google.maps.LatLng(lat, lng);
  if(circle != undefined){
    circle.setMap(null);
  }
  circle = new google.maps.Circle({
    strokeColor: '#FF0000',
    fillColor: '#FF4A4A',
    map: map,
    center: myLatLng,
    radius: parseInt($('#editraduiasalert').val())
  });
}

function editchangradius(){
  createeditcirclepoi($('#editlattitudepoi').val(), $('#editlongtitudepoi').val());
}

function createeditpoi(lat, lng, value){
  var myLatLng = new google.maps.LatLng(lat, lng);
  pointMarker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: value,
    draggable:true,
  });
  map.setZoom(17);
  map.setCenter(pointMarker.getPosition());
  google.maps.event.addListener(pointMarker, 'drag', function() {
    $('#editlattitudepoi').val([pointMarker.getPosition().lat()].join());
    $('#editlongtitudepoi').val([pointMarker.getPosition().lng()].join());
    createeditcirclepoi([pointMarker.getPosition().lat()].join(), [pointMarker.getPosition().lng()].join());
  });
}

function canclereditpoi(){
  $('#viewsinfopoiintable').show();
  $('#editdivsetpoi').hide();
  loadsetpoi();
}

function loadsetpoi(){
  $('#statusdatasetpoi').show()
  $('#viewsinfopoiintable').hide();
  $('#editdivsetpoi').hide();
  $('#infopoiintable > tbody > tr').remove();
  $('#infopoiintable > tbody > td').remove();
  $.post("/finduseridpoi",
    {

    },
    function(data, status){
      if(data == "cannot findByUserID"){
        loadsetpoi();
      }
      else{
        clearmap();
        $('#statusdatasetpoi').hide()
        $('#viewsinfopoiintable').show();
        $('#editdivsetpoi').hide();
        for(var i=0; i<data.length; i++){
          createmultimarker(data[i], i);
          $('#infopoiintable > tbody').append('<tr class="success">');
          $('#infopoiintable > tbody').append('<td><label onclick="eventclickpoint('+i+')">'+data[i].Event+'</label></td>');
          $('#infopoiintable > tbody').append('<td><img src="'+data[i].Icon+'" onclick="eventclickpoint('+i+')" style="cursor:pointer"></td>');
          if(data[i].Alert == true){
            if(data[i].Sound == true && data[i].Email == true){
              $('#infopoiintable > tbody').append('<td>Sound<br>Email</td>');
            }
            if(data[i].Email == true && data[i].Sound == false){
              $('#infopoiintable > tbody').append('<td>Email</td>');
            }
            if(data[i].Sound == true && data[i].Email == false){
              $('#infopoiintable > tbody').append('<td>Sound</td>');
            }
          }
          else{
            $('#infopoiintable > tbody').append('<td>NO</td>');
          }
          $('#infopoiintable > tbody').append('<td><img src="img/setpoint/edit.png" onclick="editpoi(this,\''+data[i].Icon+'\')" alt="'+data[i].ID+'" style="cursor:pointer"></td>');
          $('#infopoiintable > tbody').append('<td><img src="img/setpoint/delete.png" onclick="deletepoi(this)" alt="'+data[i].ID+'" style="cursor:pointer"></td>');
          $('#infopoiintable > tbody').append('</tr>');
        }
      }
    }
  );
}

function saveeditpoi(){
  $.post("/editpoi",
    {
      dataid: dataidforedit,
      onoffalert: $('#editonoffalert').is(':checked'),
      alertemail: $('#editalertemail').is(':checked'),
      namepoi: $('#editnamepoi').val(),
      iconimg: editpathicon,
      latitude: $('#editlattitudepoi').val(),
      longitude: $('#editlongtitudepoi').val(),
      radius: $('#editraduiasalert').val(),
      alertsound: $('#editalertsound').is(':checked')
    },
    function(data,status){
      if(data == "Update POI Complate"){
        alert("อัพเดทจุดสนใจเรียบร้อยแล้วค่ะ");
        loadsetpoi();
      }
    }
  );
}