loaduserforgpsbox();

function cleardataforgpsbox(){
	$('#imiegpsbox').val("");
	$('#namegpsbox').val("");
	$('#ownergpsbox').val("เลือกผู้ใช้");
}

function checkimeiindb(){
	$.post("/gpsbox/findbyimie",
		{
			imie: $('#imiegpsbox').val()
		},
		function(data, status){
			if(data != "" && data != "Error FindBy findByImei"){
				if($('#imiegpsbox').val() == data[0].imei){
					alert("มี IMIE นี้อยู่ในระบบแล้วค่ะ");
					$('#imiegpsbox').select();
					$('#imiegpsbox').focus();
				}
			}
			else{
				checkimeiindb();
			}
		}
	);
}

function loaduserforgpsbox(){
	onloadimiefortruck();
	cancleedit();
	cancleeditgpsbox();
	$.post("/findeuserforgpsbox",
		{
		},
		function(data,status){
			if(data != "" && data != "Error FindBy findByAll"){
				$("#ownergpsbox").find('option:not(:first)').remove();
				$("#ownercar").find('option:not(:first)').remove();
				$("#editownercar").find('option:not(:first)').remove();
				for(var i = 0 ; i < data.length ; i++){
					$('#ownergpsbox').append($('<option>', {
						value: data[i].ID,
						text: data[i].Name
					}));
					$('#ownercar').append($('<option>', {
						value: data[i].ID,
						text: data[i].Name
					}));
					$('#editownercar').append($('<option>', {
						value: data[i].ID,
						text: data[i].Name
					}));
				}
			}
			else{
				if(data != ""){
					loaduserforgpsbox();
				}
			}
		}
	);
	$.post("/gpsbox/findbyall",
		{

		},
		function(data, status){
			if(data != "Error FindBy findByAll" && data != ""){
				$("#dataimiefordeletegpsbox").find('option:not(:first)').remove();
				$("#editnumimie").find('option:not(:first)').remove();
				for(var i = 0 ; i < data.length ; i++){
					$('#dataimiefordeletegpsbox').append($('<option>', {
						value: data[i].imei,
						text: data[i].imei
					}));
					$('#editnumimie').append($('<option>', {
						value: data[i].imei,
						text: data[i].imei
					}));
				}
			}
			else{
				if(data != ""){
					loaduserforgpsbox();
				}
			}
		}
	);
}

function savedatagpsbox(){
	if($('#imiegpsbox').val() == ""){
		alert("กรุณาใส่หมายเลข IMIE เครื่องด้วยค่ะ");
		$('#imiegpsbox').focus();
	}
	else if($('#namegpsbox').val() == ""){
		alert("กรุณาใส่ชื่อรุ่นของอุปกรณ์ด้วยค่ะ");
		$('#namegpsbox').focus();
	}
	else if($('#ownergpsbox').val() == "เลือกผู้ใช้"){
		alert("กรุณาเลือกเจ้าของผู้ใช้กล่องนี้ดวยค่ะ");
	}
	else{
		$.post("/createnewgpsbox",
			{
				imie: $('#imiegpsbox').val(),
				namegpsbox: $('#namegpsbox').val(),
				ownergpsbox: $('#ownergpsbox').val()
			},
			function(data, status){
				alert(data);
				cleardataforgpsbox();
				loaduserforgpsbox();
			}
		);
	}
}

function deleteimei(){
	if($('#dataimiefordeletegpsbox').val() == "เลือกหมายเลข IMIE เครื่อง"){
		alert("กรุณาเลือกรายการที่ต้องการจะลบด้วยค่ะ");
	}
	else{
		$.post("/gpsbox/deletegpsbox",
			{
				imie: $('#dataimiefordeletegpsbox').val()
			},
			function(data, status){
				if(data == "Delete Complete"){
					alert("ลบข้อมูลเรียบร้อยค่ะ");
					loaduserforgpsbox();
				}
			}
		);
	}
}

function editimiegpsbox(){
	if($('#dataimeigpsbox').val() != ""){
		onclickeditimie();
	}
	else{
		$('#edittableeditgpsbox > tbody > tr').remove();
	}
}

function cancleeditgpsbox(){
	$('#edittableeditgpsbox > tbody > tr').remove();
}

function submiteditgpsbox(){
	$.post("/gpsbox/updategpsbox",
		{	
			imei: $('#edditemienum').val(),
			brandbox: $('#editnamegpsbox').val(),
			owner: $('#editselectowner').val()
		},
		function(data, status){
			if(data == "UPDATE Complete"){
				alert("ปรับเปรี่ยนข้อมูลเสร็จแล้วค่ะ");
			}
			else{
				submiteditgpsbox();
			}
		}
	);
}

function onclickeditimie(){
	$.post("/findeuserforgpsbox",
		{
		},
		function(data,status){
			if(data != "Error FindBy findByAll" && data != ""){
				$.post("/gpsbox/findbyimie",
					{
						imie: $('#dataimeigpsbox').val()
					},
					function(datai, status){
						if(datai != "Error FindBy findByImei" && datai != ""){
							$('#edittableeditgpsbox > tbody > tr').remove();
							$('#edittableeditgpsbox > tbody').append(''+
								'<tr>'+
							    	'<td>'+
							        	'หมายเลข IMIE: <input id="edditemienum" type="text" disabled>'+
							        '</td>'+
							    '</tr>'+
							    '<tr>'+
							    	'<td>'+
							    		'รุ่นของกล่อง: <input id="editnamegpsbox" type="text">'+
							    	'</td>'+
							    '</tr>'+
							    '<tr>'+
							    	'<td>'+
							    		'ผู้ใช้กล่อง:'+          
							    		'<select id="editselectowner">'+
											'<option>เลือกผู้ใช้</option>'+
								    	'</select>'+
							    	'</td>'+
							    '</tr>'+
							    '<tr>'+
							    	'<td>'+
							        	'<button onclick="submiteditgpsbox()">แก้ไข</button>'+
							        	'<button onclick="cancleeditgpsbox()">ยกเลิก</button>'+
							        '</td>'+
								'</tr>'
							);
							$("#editselectowner").find('option:not(:first)').remove();
							for(var i = 0 ; i < data.length ; i++){
								$('#editselectowner').append($('<option>', {
									value: data[i].ID,
									text: data[i].Name
								}));
							}
							$('#edditemienum').val(datai[0].imei);
							$('#editnamegpsbox').val(datai[0].brandbox);
							$('#editselectowner').val(datai[0].owner);
						}
						else{
							if(datai != ""){
								onclickeditimie();
							}
							else{
								alert("ไม่พบ IMEI ที่ค้นหาค่ะ");
							}
						}
					}
				);
			}
			else{
				if(data != ""){
					onclickeditimie();
				}
				else{
					alert("ไม่พบหมายเลข IMEI นี้ค่ะ");
				}
			}
		}
	);
}