function onloadimiefortruck(){
	$.post("/gpsbox/findbyuses",
		{},
		function(data, status){
			$("#numimie").find('option:not(:first)').remove();
			for(var i = 0 ; i < data.length ; i++){
				$('#numimie').append($('<option>', {
					value: data[i].imei,
					text: data[i].imei
				}))
			}
		}
	);
	$.post("/vehicle/findbyall",
		{
		},
		function(data, status){
			if(data != "" && data != "Error FindByAll")
			{
				$("#deleteregistorcar").find('option:not(:first)').remove();
				for(var i = 0 ; i < data.length ; i++){
					$('#deleteregistorcar').append($('<option>', {
						text: data[i].ID
					}));
				}
			}
			else{
				if(data != ""){
					onloadimiefortruck();
				}
			}
		}
	);
}
function previewspictuertruck1(){
	var file = document.getElementById('previewspictuertruck11').files[0];
	var reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onload = function (e) {
		$('#previewspictuertruck01').attr('src', e.target.result);
	}
}
function previewspictuertruck2(){
	var file = document.getElementById('previewspictuertruck22').files[0];
	var reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onload = function (e) {
		$('#previewspictuertruck02').attr('src', e.target.result);
	}
}
function previewspictuertruck3(){
	var file = document.getElementById('previewspictuertruck33').files[0];
	var reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onload = function (e) {
		$('#previewspictuertruck03').attr('src', e.target.result);
	}
}
function savenewvehicle(){
	if($('#registration').val() == ""){
		alert("กรุณาระบุหมายเลขทะเบียนรถด้วยค่ะ");
		$('#registration').focus();
	}
	else if($('#brandcar').val() == ""){
		alert("กรุณากรอกยี่ห้อของรถด้วยค่ะ");
		$('#brandcar').focus();
	}
	else if($('#seriescar').val() == ""){
		alert("กรุณากรอกรุ่นของรถด้วยค่ะ");
		$('#seriescar').focus();
	}
	else if($('#yearcar').val() == ""){
		alert("กรุณากรอกปีของรถด้วยค่ะ");
		$('#yearcar').focus();
	}
	else if($('#numtankcar').val() == ""){
		alert("กรุณากรอกเลขตัวถังของรถด้วยค่ะ");
		$('#numtankcar').focus();
	}
	else if($('#taxdate').val() == ""){
		alert("กรุณากรอกวันที่กำหนดต่อทะเบียนรถด้วยค่ะ");
		$('#taxdate').focus();
	}
	else if($('#speedlimit').val() == ""){
		alert("กรุณากรอกข้อมูลจำกัดความเร็วด้วยค่ะ");
		$('#speedlimit').focus();
	}
	else if($('#littertank').val() == ""){
		alert("กรุณาระบุความจุของถังน้ำมันด้วยค่ะ");
		$('#littertank').focus();
	}
	else if($('#fulltank').val() == ""){
		alert("กรุณาระบุ Volt ของน้ำมันขณะที่เต็มถังด้วยค่ะ");
		$('#fulltank').focus();
	}
	else if($('#middletank').val() == ""){
		alert("กรุณาระบุ Volt ของน้ำมันขณะเหลือครึ่งถังค่ะ");
		$('#middletank').focus();
	}
	else if($('#lowtank').val() == ""){
		alert("กรุณาระบุ Volt ของน้ำมันขณะที่หมดถัง");
		$('#lowtank').focus();
	}
	else if($('#numimie').val() == "เลือกหมายเลข IMIE"){
		alert("กรุณาเลือกหมายเลข IMIE ด้วยค่ะ");
		$('#numimie').focus();
	}
	else if($('#ownercar').val() == "เลือกเจ้าของรถ"){
		alert("กรุณาเลือกเจ้าของของรถด้วยค่ะ");
		$('#ownercar').focus();
	}
	else if($('#previewspictuertruck11').val() == ""){
		alert("กรุณาใส่รูปที่ 1 ด้วยค่ะ")
	}
	else if($('#previewspictuertruck22').val() == ""){
		alert("กรุณาใส่รูปที่ 2 ด้วยค่ะ")
	}
	else if($('#previewspictuertruck33').val() == ""){
		alert("กรุณาใส่รูปที่ 3 ด้วยค่ะ")
	}
	else{
		$('#statussavenewvehicle').show();
		$('#savenewvehicle').hide();
		var picture1 = document.getElementById('previewspictuertruck11').files[0];
		var picture2 = document.getElementById('previewspictuertruck22').files[0];
		var picture3 = document.getElementById('previewspictuertruck33').files[0];
	    var fd = new FormData();
	    fd.append('registration', $('#registration').val());
	    fd.append('brandcar', $('#brandcar').val());
	    fd.append('seriescar', $('#seriescar').val());
	    fd.append('yearcar', $('#yearcar').val());
	    fd.append('numtankcar', $('#numtankcar').val());
	    fd.append('taxdate', $('#taxdate').val());
	    fd.append('speedlimit', $('#speedlimit').val());
	    fd.append('littertank', $('#littertank').val());
	    fd.append('fulltank', $('#fulltank').val());
	    fd.append('middletank', $('#middletank').val());
	    fd.append('lowtank', $('#lowtank').val());
	    fd.append('numimie', $('#numimie').val());
	    fd.append('ownercar', $('#ownercar').val());
	    fd.append('picture1', picture1);
	    fd.append('picture2', picture2);
	    fd.append('picture3', picture3);
	    $.ajax({
	        type: 'POST',
	        url: '/addnewvehicle',
	        data: fd,
	        processData: false,
	        contentType: false
	    }).done(function(data){
	        if(data == "Save complate"){
	        	alert("บันทึกข้อมูลเรียบร้อยแล้วค่ะ");
	        	onloadimiefortruck();
	        	claernewvehicle();
	        	$('#statussavenewvehicle').hide();
				$('#savenewvehicle').show();
	        }
	        else{
	        	alert(data);
	        }
	    });
	}
}
function claernewvehicle(){
	$('#registration').val("");
	$('#brandcar').val("");
	$('#seriescar').val("");
	$('#yearcar').val("");
	$('#numtankcar').val("");
	$('#taxdate').val("");
	$('#speedlimit').val("");
	$('#littertank').val("");
	$('#fulltank').val("");
	$('#middletank').val("");
	$('#lowtank').val("");
	$('#numimie').val("เลือกหมายเลข IMIE");
	$('#ownercar').val("เลือกเจ้าของรถ");
	$('#previewspictuertruck11').val("");
	$('#previewspictuertruck01').attr('src', "/img/truck/Mercedes-Truck-icon.png");
	$('#previewspictuertruck22').val("");
	$('#previewspictuertruck02').attr('src', "/img/truck/Pipe-Truck-icon.png");
	$('#previewspictuertruck33').val("");
	$('#previewspictuertruck03').attr('src', "/img/truck/Stake-Truck-icon.png");
}
function queryeditregistorcar(){
	if($('#editregistorcar').val() != ""){
		$.post("/vehicle/findbyid",
			{	
				registration: $('#editregistorcar').val()
			},
			function(data, status){
				if(data != "Error FindById" && data != ""){
					showdiveditvehicle(data);
				}
				else{
					if(data != ""){
						queryeditregistorcar();
					}
					else{
						alert("ไม่พบ ID ที่ค้นหาค่ะ");
					}
				}
			}
		);
	}
	else{
		alert("กรุณา ระบุ ID ของรถที่จะค้นหาด้วยค่ะ");
		$('#showeditvehecle').hide();
	}
}
function cancleedit(){
	$('#showeditvehecle').hide();
}
function waitEntereditregistorcar(event){
	if(event == 13){
		queryeditregistorcar();
	}
}
function updatevehicle(){
	$.post("/vehicle/updatevehicle",
		{
			Idvehicle: $('#idvehicle').val(),
			license: $('#editregistration').val(),
			Brand: $('#editbrandcar').val(),
			Series: $('#editseriescar').val(),
			Year: $('#edityearcar').val(),
			PlateNumber: $('#editnumtankcar').val(),
			TaxDate: $('#edittaxdate').val(),
			SpeedLimit: $('#editspeedlimit').val(),
			FuelTank: $('#editlittertank').val(),
			FullTank: $('#editfulltank').val(),
			MiddleTank: $('#editmiddletank').val(),
			LowTank: $('#editlowtank').val(),
			IMEI: $('#editnumimie').val(),
			owner: $('#editownercar').val()
		},
		function(data, status){
			if(data == "update complate"){
				onloadimiefortruck();
				alert("Update เรียบร้อยแล้วค่ะ");
			}
		}
	);
}
function deletevehicle(){
	if($('#deleteregistorcar').val() == "เลือกทะเบียนรถ"){
		alert("กรุณาเลือกทะเบียนรถเพื่อใช้ในการลบข้อมูลด้วยค่ะ");
	}
	else{
		$.post("/vehicle/deletevehicle",
			{
				registration: $('#deleteregistorcar').val()
			},
			function(data, status){
				if(data == "cannot Delet Vehicle"){
					deletevehicle();
				}
				else{
					onloadimiefortruck();
					$('#showeditvehecle').hide();
					alert("ลบข้อมูลรถเรียบร้อยแล้วค่ะ");
				}
			}
		);
	}
}
function showdatagpsbox(){
	$('#dataimeigpsbox').val($('#editnumimie').val());
}
function showdiveditvehicle(data){
	$('#showeditvehecle').show();
	var date = new Date(data[0].TaxDate);
	var day = ("0" + date.getDate()).slice(-2);
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var datadate = date.getFullYear()+"-"+(month)+"-"+(day) ;
	$('#idvehicle').val(data[0].ID);
	$('#editregistration').val(data[0].license);
	$('#editbrandcar').val(data[0].Brand);
	$('#editseriescar').val(data[0].Series);
	$('#edityearcar').val(data[0].Year);
	$('#editnumtankcar').val(data[0].PlateNumber);
	$('#edittaxdate').val(datadate);
	$('#editspeedlimit').val(data[0].SpeedLimit);
	$('#editlittertank').val(data[0].FuelTank);
	$('#editfulltank').val(data[0].FullTank);
	$('#editmiddletank').val(data[0].MiddleTank);
	$('#editlowtank').val(data[0].LowTank);
	$('#editnumimie').val(data[0].IMEI);
	$('#editownercar').val(data[0].owner);
	document.getElementById("editpreviewspictuertruck01").src = "data:image/png;base64," + data[0].Picture1;
	document.getElementById("editpreviewspictuertruck02").src = "data:image/png;base64," + data[0].Picture2;
	document.getElementById("editpreviewspictuertruck03").src = "data:image/png;base64," + data[0].Picture3;
}