function buttonsave(){
  if($('#namesur').val() == ""){
    alert("กรุณาระบุชื่อ - นามสกุลด้วยค่า");
    $('#namesur').focus();
  }
  else if($('#useremail').val() == ""){
    alert("กรุณาระบุอีเมล์ ด้วยค่ะ");
    $('#useremail').focus();
  }
  else if($('#tel').val() == ""){
    alert("กรุณาระบุเบอร์โทรศัพท์ด้วยค่ะ");
    $('#tel').focus();
  }
  else if($('#type').val() == "null"){
    alert("กรุณาเลือกก set ลูกค้าด้วยค่ะ");
    $('#type').focus();
  }
  else if($('#username').val() == ""){
    alert("กรุณากรอก UserName ด้วยค่ะ");
    $('#username').focus();
  }
  else if($('#password').val() == ""){
    alert("กรุณากรอก password ด้วยค่ะ");
    $('#password').focus();
  }
  else if($('#againpassword').val() != $('#password').val()){
    alert("password ของคุณไม่ตรงกัน่ะ");
    $('#againpassword').focus();
  }
  else if($('#company').val() == ""){
    alert("กรุณากรอก ชื่อ ห้างร้าน/บริษัท ด้วยค่ะ");
    $('#company').focus();
  }
  else if($('#latitude').val() == "" || $('#logtitude').val() == ""){
    alert("กรุณาเลือกสถานที่ตั้งบริษัทด้วยคะ");
    $('#latitude').focus();
  }
  else if($('#sign').val() == ""){
    alert("กรูณากรอกวันที่ใช้งานครั้งแรกด้วยค่ะ");
    $('#sign').focus();
  }
  else if($('#exp').val() == ""){
    alert("กรุณากรอกวันหมดอายุด้วยค่ะ");
    $('#exp').focus();
  }
  else if($('input:checked').val() == undefined){
    alert("กรุณาเลือกสถานะของผู้ใช้ด้วยค่ะ");
  }
  else if($('#address').val() == ""){
    alert("กรุณากรอกที่อยู่สำหรับส่งเอกสารด้วยค่ะ");
    $('#address').focus();
  }
  else{
    $('#adduser').submit();
  }
}
function updateMarkerPosition(latLng){
  var latitude = [latLng.lat()].join();
  var logtitude = [latLng.lng()].join();
  $('#latitude').val(latitude);
  $('#logtitude').val(logtitude);
}

function initialize() {
  var now = new Date();
  var day = ("0" + now.getDate()).slice(-2);
  var month = ("0" + (now.getMonth() + 1)).slice(-2);
  var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
  $('#sign').val(today);
  var myLatlng = new google.maps.LatLng(14.897671158848201, 102.00821203613282);
  var mapOptions = {
    zoom: 8,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  var input = (document.getElementById('pac-input'));
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox((input));

  var contentString = 'drag to get position';

  var infowindow = new google.maps.InfoWindow({
      content: contentString
  });

  var marker = new google.maps.Marker({
      position: myLatlng,
      draggable:true,
      map: map,
      title: 'Love you'
  });
  var markers = [];
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerPosition(marker.getPosition());
  });

  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }
    for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      // Create a marker for each place.
      var marker = new google.maps.Marker({
        map: map,
        title: place.name,
        draggable:true,
        position: place.geometry.location
      });

      markers.push(marker);

      google.maps.event.addListener(markers[i], 'drag', function() {
        updateMarkerPosition(marker.getPosition());
      });

      bounds.extend(place.geometry.location);
    }

    map.fitBounds(bounds);
  });
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });
}


