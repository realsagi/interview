function showalllistcarbyuser(){
	$('#statusloadingtab2').show();
	$('#listcarallbyuser').hide();
	$('#editcarallbyuser').hide();
	$.post("/vehicle/findbyowner",
		{
		},
		function(data, status){
			if(data != "findByOwner" && data != "" && data != undefined){
				$("#listcarofuser > option").remove();
				for(var i = 0 ; i < data.length ; i++){
					if(data[i].ID == undefined){
						showalllistcarbyuser();
					}
					else{
						$('#statusloadingtab2').hide();
						$('#listcarallbyuser').show();
						$('#editcarallbyuser').hide();
						$('#listcarofuser').append($('<option>', {
			    			text: data[i].ID+"-"+data[i].license+"("+data[i].Brand+"-"+data[i].Series+")",
			    			value: data[i].ID
						}));
					}

				}
			}
			else{
				if(data != "" || data == undefined){
					showalllistcarbyuser();
				}
			}
		}
	);
}

function cancleeditbyuser(){
	showalllistcarbyuser();
}

function updatevehiclebyuser(){
	$.post("/vehicle/updatebyuser",
		{
			idvehicle: $('#idvehicle').val(),
			editregistration: $('#editregistration').val(),
			editbrandcar: $('#editbrandcar').val(),
			editseriescar: $('#editseriescar').val(),
			edityearcar: $('#edityearcar').val(),
			edittaxdate: $('#edittaxdate').val(),
			editspeedlimitbyuser: $('#editspeedlimitbyuser').val()
		}
		,
		function(data, status){
			if(data != "Error upDateByUser"){
				alert("อัพเดทข้อมูลเรียบร้อยแล้วค่ะ");
			}
			else{
				updatevehiclebyuser();
			}
		}
	);
}

function showeditcarallbyuser(){
	$('#statusloadingtab2').show();
	$('#listcarallbyuser').hide();
	$('#editcarallbyuser').hide();
	$.post("/vehicle/findbyid",
		{
			registration: $('#listcarofuser').val()
		},
		function(data, status){
			if(data == "Error FindById" || data[0].ID == undefined){
				showeditcarallbyuser();
			}
			else{
				$('#statusloadingtab2').hide();
				$('#listcarallbyuser').hide();
				$('#editcarallbyuser').show();
				var date = new Date(data[0].TaxDate);
				var day = ("0" + date.getDate()).slice(-2);
				var month = ("0" + (date.getMonth() + 1)).slice(-2);
				var datadate = date.getFullYear()+"-"+(month)+"-"+(day) ;
				$('#idvehicle').val(data[0].ID);
				$('#editregistration').val(data[0].license);
				$('#editbrandcar').val(data[0].Brand);
				$('#editseriescar').val(data[0].Series);
				$('#edityearcar').val(data[0].Year);
				$('#edittaxdate').val(datadate);
				$('#editspeedlimitbyuser').val(data[0].SpeedLimit);
				document.getElementById("editpreviewspictuertruck01byuser").src = "data:image/png;base64," + data[0].Picture1;
				document.getElementById("editpreviewspictuertruck02byuser").src = "data:image/png;base64," + data[0].Picture2;
				document.getElementById("editpreviewspictuertruck03byuser").src = "data:image/png;base64," + data[0].Picture3;
			}
		}
	);
}

function loadallvehiclecarbyuser(){
	$('#statusloadingtab3').show();
	$('#divgroupcar').hide();
	loadgroupcar();
	$('#grouplist').val("เลือกลุ่มของรถ");
	clcgrouplist();
	$.post("/vehicle/findbyowner",
		{
		},
		function(data, status){
			if(data != "findByOwner" && data != "" && data != undefined){
				$("#vehiclecar > option").remove();
				for(var i = 0 ; i < data.length ; i++){
					if(data[i].ID == undefined){
						loadallvehiclecarbyuser();
					}
					else{
						$('#statusloadingtab3').hide();
						$('#divgroupcar').show();
						$('#vehiclecar').append($('<option>', {
			    			text: data[i].license+"("+data[i].Brand+"-"+data[i].Series+")",
			    			value: data[i].ID
						}));
					}
				}
			}
			else{
				if(data != "" || data == undefined){
					loadallvehiclecarbyuser();
				}
			}
		}
	);
}

function clcgrouplist(){
	$("#vehicecalrforgroup > option").remove();
	if($('#grouplist').val() != "เลือกลุ่มของรถ"){
		showcarlistingroup($('#grouplist').val());
		$('#deletegroup').show();
		$('#editgroup').show();
		$('#eventgrouplist div').remove();
	}
	else{
		$('#deletegroup').hide();
		$('#editgroup').hide();
	}
}

function showcarlistingroup(input){
	$("#vehicecalrforgroup > option").remove();
	$.post("/groupcar/findyid",
		{
			idgroupcar: input
		},
		function(data, status){
			var datavehicle = data[0].Vehicle;
			if(data != "Error findByID" && data != "" && datavehicle != null){
				for(var i = 0 ; i < datavehicle.length ; i++){
					$.post("/vehicle/findbyid",
						{
							registration: datavehicle[i]
						},
						function(reqdata, status){
							for(var j = 0 ; j < reqdata.length ; j++){
								$('#vehicecalrforgroup').append($('<option>', {
					    			text: reqdata[j].license+"("+reqdata[j].Brand+"-"+reqdata[j].Series+")",
					    			value: reqdata[j].ID
								}));
							}
						}
					);
				}
			}
		}
	);
}

function vehiclecar(){
	if($('#grouplist').val() == "เลือกลุ่มของรถ"){
		alert("กรุณาเลือกกลุ่มของรถด้วยค่ะ หรือ ต้องสร้างกลุ่มก่อน")
	}
	else{
		$.post("/groupcar/updatevehecle",
			{
				idgroupcar: $('#grouplist').val(),
				arrayvehicle: $('#vehiclecar option:selected').val()
			},
			function(data, status){
				showcarlistingroup($('#grouplist').val());
			}
		);
		/*
		if($('#vehiclecar').val() != null){
			$('#vehicecalrforgroup').append($('<option>', {
	    		text: $('#vehiclecar option:selected').text(),
	    		value: $('#vehiclecar').val()
			}));
		}
		*/
	}
}

function clearvehice(){
	if($('#vehicecalrforgroup').val() != null){
		//$("#vehicecalrforgroup option[value="+$('#vehicecalrforgroup').val()+"]").remove();
		$.post("/groupcar/DeleteVehicleById",
			{
				idgroupcar: $('#grouplist').val(),
				arrayvehicle: $('#vehicecalrforgroup option:selected').val()
			},
			function(data, status){
				showcarlistingroup($('#grouplist').val());
			}
		);
	}
}

function yesuall(){
	$.post("/groupcar/deletegroupcar",
		{
			groupnameid: $('#grouplist').val()
		},
		function(data, status){
			if(data == "Delete Complate"){
				$('#grouplist').val("เลือกลุ่มของรถ");
				$('#eventgrouplist div').remove();
				loadgroupcar();
			}
		}
	);
}

function nouall(){
	$('#eventgrouplist div').remove();
}

function changeuall(){
	$.post("/groupcar/updategroupname",
		{
			groupnameid: $('#grouplist').val(),
			groupname: $('#txtchangeuall').val()
		},
		function(data, status){
			if(data == "Update Complate"){
				$('#grouplist').prop('disabled', false);
				$('#eventgrouplist div').remove();
				loadgroupcar();
			}
		}
	);
	
}

function cancleuall(){
	$('#grouplist').prop('disabled', false);
	$('#eventgrouplist div').remove();
}

function editgroup(){
	$('#grouplist').prop('disabled', 'disabled');
	$('#eventgrouplist').append("<div>ชื่อกลุ่ม: <input id=\"txtchangeuall\" type=\"text\" value="+$('#grouplist option:selected').text()+" /></div>");
	$('#eventgrouplist').append("<div><button class=\"btn\" type=\"button\" id=\"changeuall\" onclick=\"changeuall()\">เปลี่ยนชื่อ</button>"+
						"<button class=\"btn\" type=\"button\" id=\"cancleuall\" onclick=\"cancleuall()\">ยกเลิก</button></div>");
	$('#deletegroup').hide();
	$('#editgroup').hide();
	$('#txtchangeuall').focus();
}

function deletegroup(){
	$('#eventgrouplist').append("<div>คุณต้องการจะลบ ชื่อกลุ่ม ใช่ หรือ ไม่</div>");
	$('#eventgrouplist').append("<div><button class=\"btn\" type=\"button\" id=\"yes\" onclick=\"yesuall()\">ใช่</button>"+
						"<button class=\"btn\" type=\"button\" id=\"no\" onclick=\"nouall()\">ไม่</button></div>");
	$('#deletegroup').hide();
	$('#editgroup').hide();
}

function addgroupcar(){
	$.post("/groupcar/save",
		{
			groupname: $('#gorupVehicle').val()
		},
		function(data, status){
			if(data == "Save Complate"){
				$('#gorupVehicle').val("");
				loadgroupcar();
			}
		}
	);
}

function loadgroupcar(){
	$.post("/groupcar/findbyowner",
		{

		},
		function(data ,status){
			if(data != "Error findByOwner"){
				$("#grouplist").find('option:not(:first)').remove();
				for(var i = 0 ; i < data.length ; i++){
					$('#grouplist').append($('<option>', {
				    	text: data[i].GroupName,
				    	value: data[i].ID
					}));
				}
			}
			else{
				if(data != ""){
					loadgroupcar();
				}
			}
		}
	);
}

function clicksave1(){
	var name = $('#name').text();
	var uname = $('#uname').text();
	var ucompany = $('#ucompany').val();
	var uphone = $('#uphone').val();
	var uemail = $('#uemail').val();
	var uaddress = $('#uaddress').val();
	$.post("/updateuser", 
	{
		name: name,
		uname: uname,
		ucompany: ucompany,
		uphone: uphone,
		uemail: uemail,
		uaddress: uaddress
	}, 
	function(data,status){
    	alert(data);
    	$("#table1").load();
  	});
}

function clicksave2(){
	var oldpass = $('#oldpass').val();
	var newpass = $('#newpass').val();
	var newpassagain = $('#newpassagain').val();
	if(newpass == "" || oldpass == "" || newpassagain == "" || newpass != newpassagain){
		alert("รหัสผ่านใหม่ไม่ตรงกันค่ะ และ ห้ามเว็นว่างค่ะ");
	}
	else{
		$.post("/changpassword", 
		{
			oldpass: oldpass,
			newpass: newpass
		},
		function(data,status){
	    	alert(data);
	    	if(data == "update password complate"){
	    		window.location = "/logout"
	    	}
	  	});
	}

}

function previewimagedriver(){
	var file = document.getElementById('picturenewdriver').files[0];
	var img = document.getElementById('picturenewdriver');
	var reader = new FileReader();
	var size = file.size/1024;
	reader.readAsDataURL(file);
	reader.onload = function (e) {
		if(size > 70){
	    	alert("ขนาดภาพใหญ่เกินกว่า 70 KB");
	    	$('#sizeimageuserall').text("");
	    	$('#picturenewdriver').val("");
	    	$('#previewnewdriver').attr('src', "img/nobody.jpg");
	    }
	    else{
	    	$('#previewnewdriver').attr('src', e.target.result);
        	$('#previewnewdriver').show();
	    }
    }
    $('#sizeimageuserall').text("Size = "+size+" KB");
}

function savenewdriver(){
	var file = document.getElementById('picturenewdriver').files[0];
	var reader = new FileReader();

    var iddriver = $('#iddriver').val();
	var namedriver =  $('#namedriver').val();
	var nicknamedriver = $('#nicknamedriver').val();
	var numiddirver = $('#numiddirver').val();
	var phoneiddriver = $('#phoneiddriver').val();
	var firststartdirver = $('#firststartdirver').val();

	if(file == undefined){
		alert("กรุณาใส่รูปภาพด้วยค่ะ");
	}
	else if(iddriver == ""){
		alert("กรุณาระบุ รหัสพนักงานด้วยค่ะ");
		$('#iddriver').focus();
	}
	else if(namedriver == ""){
		alert("กรุณาระบุ ชื่อพนักงานด้วยค่ะ");
		$('#namedriver').focus();
	}
	else if(nicknamedriver == ""){
		alert("กรุณาระบุชื่อเล่นของพนักงานด้วยค่ะ");
		$('#nicknamedriver').focus();
	}
	else if(numiddirver == ""){
		alert("กรุณาระบุหมายเลขใบขับขี่ด้วยค่ะ");
		$('#numiddirver').focus();
	}
	else if(phoneiddriver == ""){
		alert("กรุณาระบุเบอร์โทรศัพท์ด้วยค่ะ");
		$('#phoneiddriver').focus();
	}
	else if(firststartdirver == ""){
		alert("กรุณาระบุวันที่เริ่มทำงานเป็นวันแรกด้วยค่ะ");
		$('#firststartdirver').focus();
	}
	else{
		reader.onload = function (e) {
	        $.post("/createnewdriver", 
			{
				iddriver: iddriver,
				namedriver: namedriver,
				nicknamedriver: nicknamedriver,
				numiddirver: numiddirver,
				phoneiddriver: phoneiddriver,
				firststartdirver: firststartdirver,
				picture: e.target.result
			}, 
			function(data,status){
				clearnewdriver();
				searchalldatainfodriver();
		    	alert(data);
		  	});
	    }
	    reader.readAsDataURL(file);
    }
}

function clearnewdriver(){
	$('#iddriver').val("");
	$('#namedriver').val("");
	$('#nicknamedriver').val("");
	$('#numiddirver').val("");
	$('#phoneiddriver').val("");
	$('#firststartdirver').val("");
	$('#picturenewdriver').val("");
	$('#previewnewdriver').attr('src', "img/nobody.jpg");
	$('#sizeimageuserall').text("");
}

function searchalldatainfodriver(){
	$('#statusloadingtab4datadriver').show();
	$('#datadriverbyuser').hide();
	$('#infodrivershow').hide();
	$.post("/searchallnamedriver", 
		{
		
		}, 
		function(data,status){
			if(data != "เกิดข้อผิดพลาดกับการเชื่อมฐานข้อมูล"){
				$('#statusloadingtab4datadriver').hide();
				$('#datadriverbyuser').show();
				$('#infodrivershow').hide();
				$('#datainfodriver').find('option:not(:first)').remove();
				$('#iddeletedriver').find('option:not(:first)').remove();
				for(var i = 0; i < data.length ; i++){
		  			$('#datainfodriver').append($('<option>', {
				    	text: data[i].Name,
				    	value: data[i].ID
					}));
		  			$('#iddeletedriver').append($('<option>', {
				    	text: data[i].Name,
				    	value: data[i].ID
					}));
			  	}
			  	editclearnewdriver();
			}
			else{
				searchalldatainfodriver();
			}
		}
	);
}

function deleteinfodriver(){
	if($('#iddeletedriver').val() == "เลือกชื่อคนขับ"){
		alert("กรุณาเลือกชื่อคนขับด้วย");
	}
	else{
		$.post("/deleteinfodriver", 
			{
				IDdriver : $('#iddeletedriver').val()
			}, 
			function(data,status){
				if(data == "Cannot Delete Driver"){
					deleteinfodriver();
				}
				else{
					searchalldatainfodriver();
					alert("ลบข้อมูลคนขับเรียบร้อยแล้ว");
				}
			}
		);
	}
}

function checkdatainfodriver(){
	if($('#datainfodriver').val() != "เลือกชื่อคนขับ"){
		$.post("/showinfodriver", 
			{
				IDdriver : $('#datainfodriver').val()
			}, 
			function(data,status){
				if(data == "cannot findByID"){
					checkdatainfodriver();
				}
				else{
					$('#editiddriver').val(data.ID);
					$('#editnamedriver').val(data.Name.substring(0,data.Name.indexOf('(')));
					$('#editnicknamedriver').val(data.Name.substring(data.Name.indexOf('(')+1,data.Name.indexOf(')')));
					$('#editnumiddirver').val(data.DriverLicense);
					$('#editphoneiddriver').val(data.Phone);
					$('#editfirststartdirver').val(data.StartWork);				
					$('#editdaywork').val(data.Daywork);
					$('#editpreviewnewdriver').attr('src', data.Picture2);
					$('#aeditpreviewnewdriver').attr('href', data.Picture2);
					$('#aeditpreviewnewdriver').attr('title', data.Name);
					$('#aeditpreviewnewdriver').attr('target', "_blank");
					$('#infodrivershow').show();
					editclearnewdriver();
				}
			}
		);
	}
	else{
		$('#infodrivershow').hide();
	}
}

function editinfodriver(){
	$('#editnamedriver').prop('disabled', false);
	$('#editnicknamedriver').prop('disabled', false);
	$('#editnumiddirver').prop('disabled', false);
	$('#editphoneiddriver').prop('disabled', false);
	$('#editsavedriver').prop('disabled', false);
	$('#editcleardriver').prop('disabled', false);
	$('#editpicturenewdriver').prop('disabled', false);
}

function editpreviewimagedriver(){
	var file = document.getElementById('editpicturenewdriver').files[0];
	var img = document.getElementById('editpicturenewdriver');
	var reader = new FileReader();
	var size = file.size/1024;
	reader.readAsDataURL(file);
	reader.onload = function (e) {
		if(size > 70){
	    	alert("ขนาดภาพใหญ่เกินกว่า 70 KB");
	    	$('#editsizeimageuserall').text("");
	    	$('#editpicturenewdriver').val("");
	    	$('#editpreviewnewdriver').attr('src', "img/nobody.jpg");
	    }
	    else{
	    	$('#editpreviewnewdriver').attr('src', e.target.result);
        	$('#editpreviewnewdriver').show();
	    }
    }
    $('#editsizeimageuserall').text("Size = "+size+" KB");
}

function editclearnewdriver(){
	$('#editnamedriver').prop('disabled', true);
	$('#editnicknamedriver').prop('disabled', true);
	$('#editnumiddirver').prop('disabled', true);
	$('#editphoneiddriver').prop('disabled', true);
	$('#editsavedriver').prop('disabled', true);
	$('#editcleardriver').prop('disabled', true);
	$('#editpicturenewdriver').prop('disabled', true);
	$('#editpicturenewdriver').val("");
}

function saveeditnewdriver(){
	var file = document.getElementById('editpicturenewdriver').files[0];
	var reader = new FileReader();

    var iddriver = $('#editiddriver').val();
	var namedriver =  $('#editnamedriver').val();
	var nicknamedriver = $('#editnicknamedriver').val();
	var numiddirver = $('#editnumiddirver').val();
	var phoneiddriver = $('#editphoneiddriver').val();
	var image = $('#editpreviewnewdriver').attr('src');

	if(iddriver == ""){
		alert("กรุณาระบุ รหัสพนักงานด้วยค่ะ");
		$('#iddriver').focus();
	}
	else if(namedriver == ""){
		alert("กรุณาระบุ ชื่อพนักงานด้วยค่ะ");
		$('#namedriver').focus();
	}
	else if(nicknamedriver == ""){
		alert("กรุณาระบุชื่อเล่นของพนักงานด้วยค่ะ");
		$('#nicknamedriver').focus();
	}
	else if(numiddirver == ""){
		alert("กรุณาระบุหมายเลขใบขับขี่ด้วยค่ะ");
		$('#numiddirver').focus();
	}
	else if(phoneiddriver == ""){
		alert("กรุณาระบุเบอร์โทรศัพท์ด้วยค่ะ");
		$('#phoneiddriver').focus();
	}
	else if(firststartdirver == ""){
		alert("กรุณาระบุวันที่เริ่มทำงานเป็นวันแรกด้วยค่ะ");
		$('#firststartdirver').focus();
	}
	else{
		$.post("/updateolddriver", 
			{
				iddriver: iddriver,
				namedriver: namedriver,
				nicknamedriver: nicknamedriver,
				numiddirver: numiddirver,
				phoneiddriver: phoneiddriver,
				picture: image
			}, 
			function(data,status){
				clearnewdriver();
				searchalldatainfodriver();
				editclearnewdriver();
		    	if(data == "Update Driver Complate"){
		    		alert("ปรับปรุงข้อมูลเรียบร้อยแล้วค่ะ");
		    	}
		    	else{
		    		alert(data);
		    	}
		 	}
		);
    }
}

function chackhavethisid(){
	var iddriver = $('#iddriver').val();
	$.post("/checkhaveid", 
		{
			iddriver: iddriver,
		}, 
		function(data,status){
			if(data == "have ID"){
				alert("ขออภัยค่ะ ID นี้ถูกใช้ไปแล้ว");
				$('#iddriver').val("");
				$('#iddriver').focus();
			}
		}
	);
}