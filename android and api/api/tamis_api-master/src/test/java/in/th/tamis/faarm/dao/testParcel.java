package in.th.tamis.faarm.dao;

import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class testParcel {
    @Test
    public void testQueryAllParcelType() throws SQLException {
        ParcelDAO parcel = new ParcelJDBC();
        assertNotNull(parcel.selectAllParcelType().getParcelType().get(0).toString());
    }
}
