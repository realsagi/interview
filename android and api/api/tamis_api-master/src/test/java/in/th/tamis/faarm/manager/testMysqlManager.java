package in.th.tamis.faarm.manager;

import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class testMysqlManager {

    @Test
    public void testConnectMysql() throws SQLException {
        Connection connection = MysqlConnector.getConnection();
        assertEquals("tamis", connection.getCatalog());
    }
}
