package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.manager.MysqlConnector;
import in.th.tamis.faarm.models.faarm.Address;
import in.th.tamis.faarm.models.faarm.MemberDOAE;
import in.th.tamis.faarm.models.faarm.datafromandroid.Farmer;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NewMemberTest {
    NewMemberJDBCMySQL newMemberJDBCMySQL;

    @Before
    public void setup(){
        newMemberJDBCMySQL = new NewMemberJDBCMySQL(MysqlConnector.getConnection());
    }

    @Test
    public void insertFarmer(){
        Farmer farmer = new Farmer();
        farmer.setPrefixId(1);
        farmer.setFirstName("กิตติมศักดิ์");
        farmer.setLastName("ศิวารัตน์");
        farmer.setCitizenId("2520100011499");
        farmer.setBirthDate("25290307");
        farmer.setAddress(new Address());

        newMemberJDBCMySQL.newFarmer(farmer, 55);
        MemberDOAE result =  new MemberJDBC(MysqlConnector.getConnection()).selectMemberByCitizenId("2520100011499");
        System.out.println(result.getCitizenId());
        assertEquals("กิตติมศักดิ์", result.getFirstName());
    }
}
