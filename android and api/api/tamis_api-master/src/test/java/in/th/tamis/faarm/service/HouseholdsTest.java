package in.th.tamis.faarm.service;


import com.google.gson.Gson;
import in.th.tamis.faarm.manager.DOPARestAPI;
import in.th.tamis.faarm.models.dopa.Household;
import in.th.tamis.faarm.models.faarm.HouseholdMember;
import in.th.tamis.faarm.models.faarm.Person;
import in.th.tamis.faarm.services.HouseholdMemberService;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class HouseholdsTest {

    HouseholdMemberService service;

    public HouseholdsTest() {
        service = new HouseholdMemberService(new DOPARestAPI("http://127.0.0.1:8882"));
    }

    @Test
    public void notFoundHouseholdsWhenInsertCitizenIdIncorrect() throws IOException {
        String citizenId = "1169800011950";

        Household household = service.callHouseholdMemberDOPA(citizenId);
        assertEquals(null, household);
    }

    @Test
    public void foundHouseholdDOPAByCitizenIdCorrect() throws IOException {
        String citizenId = "1169800011959";
        Household household = service.callHouseholdMemberDOPA(citizenId);
        assertNotEquals(null, household);
    }

    @Test
    public void checkCountWhenConvertIsSuccess() throws IOException {
        String citizenId = "1169800011959";
        HouseholdMember householdMember = service.findByCitizenId(citizenId);
        assertEquals(4, householdMember.getCount());
    }

    @Test
    public void checkPersonListIndex0WhenConvertIsSuccess() throws IOException {
        String citizenId = "1169800011959";
        HouseholdMember householdMember = service.findByCitizenId(citizenId);

        Person personItem0 = householdMember.getPersons().get(0);
        assertEquals("1169800011959", personItem0.getIdentityCard());
        assertEquals("กฤษฎาญชล", personItem0.getFirstName());
        assertEquals("สะดีวงศ์", personItem0.getLastName());
        assertEquals("ชาย", personItem0.getSex());
    }

    @Test
    public void checkSizePersonListWhenConvertIsSuccess() throws IOException {
        String citizenId = "1169800011959";
        HouseholdMember householdMember = service.findByCitizenId(citizenId);
        assertEquals(4, householdMember.getPersons().size());
    }

    @Test
    public void convertFAARMProfileToJson() throws IOException {
        String citizenId = "1169800011959";
        HouseholdMember householdMember = service.findByCitizenId(citizenId);

        String stringJson = new Gson().toJson(householdMember);
        String expected = "{" +
                            "\"count\":4," +
                            "\"persons\":[" +
                                "{" +
                                    "\"citizen_id\":\"1169800011959\"," +
                                    "\"first_name\":\"กฤษฎาญชล\"," +
                                    "\"last_name\":\"สะดีวงศ์\"," +
                                    "\"sex\":\"ชาย\"" +
                                "}," +
                                "{" +
                                    "\"citizen_id\":\"1309900782777\"," +
                                    "\"first_name\":\"ลดารัตน์\"," +
                                    "\"last_name\":\"เพ็งพะเนา\"," +
                                    "\"sex\":\"หญิง\"" +
                                "}," +
                                "{" +
                                    "\"citizen_id\":\"2520100011499\"," +
                                    "\"first_name\":\"กิตติมศักดิ์\"," +
                                    "\"last_name\":\"ศิวารัตน์\"," +
                                    "\"sex\":\"ชาย\"" +
                                "}," +
                                "{" +
                                    "\"citizen_id\":\"1419900230161\"," +
                                    "\"first_name\":\"ธนพล\"," +
                                    "\"last_name\":\"โสดาวิชิต\"," +
                                    "\"sex\":\"ชาย\"" +
                                "}" +
                            "]" +
                        "}";
        assertEquals(expected, stringJson);
    }
}
