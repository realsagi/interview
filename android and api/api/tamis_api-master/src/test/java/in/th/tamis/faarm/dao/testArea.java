package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.manager.MysqlConnector;
import in.th.tamis.faarm.models.faarm.area.ProfileArea;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class testArea {
    Connection connection = MysqlConnector.getConnection();
    @Test
    public void testQueryProvinceAll() throws SQLException {
        AreaDAO areaJDBC = new AreaJDBC(connection);
        assertEquals("{\"province_id\":\"81\",\"province_name\":\"กระบี่\"}", areaJDBC.selectAllProvince().getProvinces().get(0).toString());
    }

    @Test
    public void testQueryDistrictByProvinceCode(){
        AreaDAO areaJDBC = new AreaJDBC(connection);
        assertEquals("{\"district_id\":\"25\",\"district_name\":\"กัลยาณิวัฒนา\"}", areaJDBC.selectDistrictByProvinceCode("50").getDistricts().get(0).toString());
    }

    @Test
    public void testQurerySubDistrictByProviceCodeAndDistrictCode(){
        AreaDAO areaJDBC = new AreaJDBC(connection);
        assertEquals("{\"sub_district_id\":\"12\",\"sub_district_name\":\"ดอนช้าง\"}", areaJDBC.selectSubDistrictByProvinceCodeAndDistrictCode("40","01").getSubDistricts().get(0).toString());
    }

    @Test
    public void testSelectPostCode() throws UnsupportedEncodingException {
        AreaDAO areaJDBC = new AreaJDBC(connection);
        assertEquals("411101", areaJDBC.selectLocationCode("อุดร", "บ้านดุง", "ศรีสุทโธ"));
    }

    @Test
    public void testSelectZipCode(){
        AreaDAO areaJDBC = new AreaJDBC(connection);
        assertEquals("41190", areaJDBC.selectPostCode("411101"));
    }

    @Test
    public void testQueryDoc(){
        AreaDAO areaJDBC = new AreaJDBC(connection);
        ProfileArea profileArea = areaJDBC.selectProfileByDocNumberAndDocTypeId("9984", 11, "40", "02");
        assertEquals(1337741, profileArea.getProfileFarmerId());
    }
}
