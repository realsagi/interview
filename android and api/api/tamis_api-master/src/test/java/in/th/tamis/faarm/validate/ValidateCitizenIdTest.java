package in.th.tamis.faarm.validate;

import in.th.tamis.faarm.services.PersonalFAARMProfileService;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ValidateCitizenIdTest {

    ValidateCitizenId validate;

    public ValidateCitizenIdTest() {
        validate = new ValidateCitizenId();
    }

    @Test
    public void validateCitizenIdLessThan13Digi() throws IOException {
        String citizenId = "11698000119";
        boolean lessThan13Digi = validate.validatePatternCitizenId(citizenId);
        assertEquals(false, lessThan13Digi);
    }

    @Test
    public void validateCitizenIdMoreThan13Digi() throws IOException {
        String citizenId = "11698000119501";
        boolean moreThan13Digi = validate.validatePatternCitizenId(citizenId);
        assertEquals(false, moreThan13Digi);
    }

    @Test
    public void validateCitizenIdIsNotNumber() throws IOException {
        String citizenId = "116980001195a";
        boolean characterOnly = validate.validatePatternCitizenId(citizenId);
        assertEquals(false, characterOnly);
    }

    @Test
    public void validateCitizenIdIsCorrect() throws IOException {
        String citizenId = "1169800011959";
        boolean correctCitizenId = validate.validatePatternCitizenId(citizenId);
        assertEquals(true, correctCitizenId);
    }
}
