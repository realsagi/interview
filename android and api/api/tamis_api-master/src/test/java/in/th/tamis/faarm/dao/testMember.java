package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.manager.MysqlConnector;
import in.th.tamis.faarm.models.faarm.Address;
import in.th.tamis.faarm.models.faarm.MemberDOAE;
import in.th.tamis.faarm.models.faarm.datafromandroid.Farmer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class testMember {
    MemberJDBC memberJDBC;

    @Before
    public void setup(){
        memberJDBC = new MemberJDBC(MysqlConnector.getConnection());
    }
    @Test
    public void testQueryMemberByCitizenId() {
        MemberDOAE result = memberJDBC.selectMemberByCitizenId("1139900111211");
        assertEquals("นาย", result.getPreName());
        assertEquals("บรรเจิด", result.getFirstName());
        assertEquals("ลอยนภา", result.getLastName());
    }

    @Test
    public void testQueryAddressNumber(){
        MemberDOAE memberDOAE = memberJDBC.selectAddressByIdCardHouseMember("1100500664185");
        assertEquals("13060309639", memberDOAE.getHouseHoldNumber());
    }


}
