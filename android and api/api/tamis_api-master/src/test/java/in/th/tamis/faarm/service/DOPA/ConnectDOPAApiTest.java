package in.th.tamis.faarm.service.dopa;

import in.th.tamis.faarm.services.dopa.ConnectDOPAApi;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

public class ConnectDOPAApiTest {

    private String headerAuthorization = "Basic dGFtaXN4eGRkOnZ0d2lkeW90YW1pcw==";
    private String url = "https://122.154.24.188:4321/struct/services/dopaTamis.dopaTamisHttpsSoap11Endpoint";
    private String input = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:get=\"http://getpidt.ssi.nectec.th.com\">" +
            "<soapenv:Header/>" +
            "<soapenv:Body>" +
            "<get:getPersonalAndMemberAndHouseDetailDes>" +
            "<get:Pid>1169800011959</get:Pid>" +
            "<get:request_user>ec13050005</get:request_user>" +
            "</get:getPersonalAndMemberAndHouseDetailDes>" +
            "</soapenv:Body>" +
            "</soapenv:Envelope>";
    String output = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "   <soapenv:Header>" +
            "      <wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" +
            "         <wsu:Timestamp wsu:Id=\"Timestamp-875\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
            "            <wsu:Created>2015-11-09T06:10:19.623Z</wsu:Created>" +
            "            <wsu:Expires>2015-11-09T06:15:19.623Z</wsu:Expires>" +
            "         </wsu:Timestamp>" +
            "      </wsse:Security>" +
            "   </soapenv:Header>" +
            "   <soapenv:Body>" +
            "      <ns:getPersonalAndMemberAndHouseDetailDesResponse xmlns:ns=\"http://getpidt.ssi.nectec.th.com\">" +
            "         <ns:return xsi:type=\"ax25:C_popANDmembersANDhouseDes\" xmlns:ax25=\"http://myStructReturn/xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
            "            <ax25:house xsi:type=\"ax25:C_PopHOUSEDes\">" +
            "               <ax25:ADDRESS_ALLEY/>" +
            "               <ax25:ADDRESS_AMPHUR>เมืองลพบุรี</ax25:ADDRESS_AMPHUR>" +
            "               <ax25:ADDRESS_ID>16010646740</ax25:ADDRESS_ID>" +
            "               <ax25:ADDRESS_MOO>00</ax25:ADDRESS_MOO>" +
            "               <ax25:ADDRESS_NO>245/8</ax25:ADDRESS_NO>" +
            "               <ax25:ADDRESS_PROVINCE>ลพบุรี</ax25:ADDRESS_PROVINCE>" +
            "               <ax25:ADDRESS_ROAD/>" +
            "               <ax25:ADDRESS_SOI>ม.ปรางค์อรุณ</ax25:ADDRESS_SOI>" +
            "               <ax25:ADDRESS_TAMBON>เขาสามยอด</ax25:ADDRESS_TAMBON>" +
            "               <ax25:BUILDING_NAME/>" +
            "               <ax25:COMMUNITY_NAME>รีละโว้พัฒนา (14)</ax25:COMMUNITY_NAME>" +
            "               <ax25:DISPOSE_DATE>25380221</ax25:DISPOSE_DATE>" +
            "               <ax25:HOUSE_SIZE>000000</ax25:HOUSE_SIZE>" +
            "               <ax25:HOUSE_TYPE>บ้าน</ax25:HOUSE_TYPE>" +
            "               <ax25:NO_construction>ศ</ax25:NO_construction>" +
            "               <ax25:POSTCODE>00000</ax25:POSTCODE>" +
            "               <ax25:SELL_DATE>00000000</ax25:SELL_DATE>" +
            "               <ax25:TEL/>" +
            "               <ax25:VILLAGE_NAME/>" +
            "               <ax25:amount_Habitats>00000000</ax25:amount_Habitats>" +
            "               <ax25:amount_all>000000000</ax25:amount_all>" +
            "               <ax25:documents_land/>" +
            "               <ax25:house_Styles/>" +
            "               <ax25:location_Buildings>ไม่ระบุ</ax25:location_Buildings>" +
            "               <ax25:name_Registry>ท้องถิ่นเทศบาลเมืองเขาสามยอด</ax25:name_Registry>" +
            "               <ax25:no_Documents_land/>" +
            "               <ax25:status xsi:type=\"ax25:C_status_return\">" +
            "                  <ax25:servermessage>succ</ax25:servermessage>" +
            "                  <ax25:statuscode>0</ax25:statuscode>" +
            "               </ax25:status>" +
            "            </ax25:house>" +
            "            <ax25:members xsi:type=\"ax25:Members\">" +
            "               <ax25:member xsi:type=\"ax25:Member\">" +
            "                  <ax25:firstName>ธณชาคร</ax25:firstName>" +
            "                  <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "                  <ax25:pid>3160600188012</ax25:pid>" +
            "               </ax25:member>" +
            "               <ax25:member xsi:type=\"ax25:Member\">" +
            "                  <ax25:firstName>กฤษฎาญชล</ax25:firstName>" +
            "                  <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "                  <ax25:pid>1169800011959</ax25:pid>" +
            "               </ax25:member>" +
            "               <ax25:member xsi:type=\"ax25:Member\">" +
            "                  <ax25:firstName>เจษฎากล</ax25:firstName>" +
            "                  <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "                  <ax25:pid>1169800112470</ax25:pid>" +
            "               </ax25:member>" +
            "               <ax25:member xsi:type=\"ax25:Member\">" +
            "                  <ax25:firstName>ธณพร</ax25:firstName>" +
            "                  <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "                  <ax25:pid>3160600187962</ax25:pid>" +
            "               </ax25:member>" +
            "               <ax25:member xsi:type=\"ax25:Member\">" +
            "                  <ax25:firstName>ยุทธกฤษ</ax25:firstName>" +
            "                  <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "                  <ax25:pid>3160600188004</ax25:pid>" +
            "               </ax25:member>" +
            "            </ax25:members>" +
            "            <ax25:popdetail xsi:type=\"ax25:C_pop_returnDes\">" +
            "               <ax25:PID>1169800011959</ax25:PID>" +
            "               <ax25:addressDetail>245/8#00##ม.ปรางค์อรุณ##เขาสามยอด#เมืองลพบุรี#ลพบุรี</ax25:addressDetail>" +
            "               <ax25:age>030</ax25:age>" +
            "               <ax25:birthday>25280214</ax25:birthday>" +
            "               <ax25:changeNatinality/>" +
            "               <ax25:dayChangeNatinality>00000000</ax25:dayChangeNatinality>" +
            "               <ax25:dayInto>25560605</ax25:dayInto>" +
            "               <ax25:fatherName>ยุทธกฤษ</ax25:fatherName>" +
            "               <ax25:fatherNatinality>ไทย</ax25:fatherNatinality>" +
            "               <ax25:firstName>กฤษฎาญชล</ax25:firstName>" +
            "               <ax25:gender>ชาย</ax25:gender>" +
            "               <ax25:hid>16010646740</ax25:hid>" +
            "               <ax25:hostStatus>ผู้อาศัย</ax25:hostStatus>" +
            "               <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "               <ax25:motherName>ธณพร</ax25:motherName>" +
            "               <ax25:motherNatinality>ไทย</ax25:motherNatinality>" +
            "               <ax25:natinality>ไทย</ax25:natinality>" +
            "               <ax25:personStatus>มีภูมิลำเนาอยู่ในบ้านนี้</ax25:personStatus>" +
            "               <ax25:pidFather>3160600188004</ax25:pidFather>" +
            "               <ax25:pidMother>3160600187962</ax25:pidMother>" +
            "               <ax25:pop5FLname>ษฎาญชล</ax25:pop5FLname>" +
            "               <ax25:pop5Name>ท้องถิ่นเทศบาลเมืองเขาสามยอด</ax25:pop5Name>" +
            "               <ax25:pop5NameCenter>000000000000000000กฤ</ax25:pop5NameCenter>" +
            "               <ax25:pop5NameFather>สะ</ax25:pop5NameFather>" +
            "               <ax25:pop5NameMother>ดีวงศ์                      ยุ</ax25:pop5NameMother>" +
            "               <ax25:prefix>นาย</ax25:prefix>" +
            "               <ax25:status xsi:type=\"ax25:C_status_return\">" +
            "                  <ax25:servermessage>succ</ax25:servermessage>" +
            "                  <ax25:statuscode>0</ax25:statuscode>" +
            "               </ax25:status>" +
            "            </ax25:popdetail>" +
            "         </ns:return>" +
            "      </ns:getPersonalAndMemberAndHouseDetailDesResponse>" +
            "   </soapenv:Body>" +
            "</soapenv:Envelope>";

    @Test
    public void getResponseHTTPStatusIsOK () throws Exception {
        ConnectDOPAApi connectDOPAApi = Mockito.mock(ConnectDOPAApi.class);
        Mockito.when(connectDOPAApi.getDataResponse(input, url, headerAuthorization)).thenReturn(output);

        String response = connectDOPAApi.getDataResponse(input, url, headerAuthorization);

        verify(connectDOPAApi).getDataResponse(input, url, headerAuthorization);

        assertThat(response, containsString("ns:getPersonalAndMemberAndHouseDetailDesResponse"));
    }
}
