package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.manager.MysqlConnector;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class testAgriculture {
    Connection connection = MysqlConnector.getConnection();

    @Test
    public void testQueryTyppeAll() throws SQLException {
        AgricultureJDBC agricultureJDBC = new AgricultureJDBC(connection);
        assertEquals("{\"id\":10,\"name\":\"ปศุสัตว์\"}", agricultureJDBC.selectTypeAll().getTypeName().get(0).toString());
    }

    @Test
    public void testQueryOrganizationAll() throws SQLException {
        AgricultureJDBC agricultureJDBC = new AgricultureJDBC(connection);
        assertEquals(1, agricultureJDBC.selectOrganizationAll().getOrganizationsList().get(0).getId());
        assertEquals("สหกรณ์ภาคการเกษตร", agricultureJDBC.selectOrganizationAll().getOrganizationsList().get(0).getOrganizationName());
    }

    @Test
    public void testQueryByTypeCode() throws SQLException {
        AgricultureJDBC agricultureJDBC = new AgricultureJDBC(connection);
        assertEquals("{\"id\":11000,\"name\":\"ข้าวเจ้า\"}", agricultureJDBC.selectByTypeCode("1").getDetail().get(0).toString());
    }

    @Test
    public void testQueryByDetailCode() throws SQLException {
        AgricultureJDBC agricultureJDBC = new AgricultureJDBC(connection);
        assertEquals("{\"id\":10160,\"name\":\"039 หรือ เจ้าพระยา หรือ PSLCo2001-240\"}", agricultureJDBC.selectByDetailCode("011000").getBreed().get(0).toString());
    }
}
