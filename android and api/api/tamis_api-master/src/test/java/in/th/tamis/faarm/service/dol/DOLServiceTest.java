package in.th.tamis.faarm.service.dol;


import in.th.tamis.faarm.models.faarm.parcel.ParcelNS4;
import in.th.tamis.faarm.services.dol.ConnectDOLApi;
import in.th.tamis.faarm.services.dol.DOLService;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import org.mockito.Mockito;
import static org.mockito.Mockito.verify;

public class DOLServiceTest {

    private String provinceCode = "40";
    private String districtCode = "01";
    private String ns4no = "1";
    private String headerAuthorization = "" ;
    private String headerSOAPAction = "http://110.164.49.55/DolParcel2Ega/IDolParcel/getParcel";
    private String url = "http://127.0.0.1:8882/dol";
    private String input = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dol=\"http://110.164.49.55/DolParcel2Ega/\"><soapenv:Header/><soapenv:Body><dol:getParcel><dol:provincecode>40</dol:provincecode><dol:amphoecode>01</dol:amphoecode><dol:ns4no>2</dol:ns4no></dol:getParcel></soapenv:Body></soapenv:Envelope>";
    private String output = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "    <s:Body>" +
            "        <getParcelResponse xmlns=\"http://110.164.49.55/DolParcel2Ega/\">" +
            "            <getParcelResult xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">" +
            "                <Parcel>" +
            "                    <AMPHURNAME>เมืองขอนแก่น</AMPHURNAME>" +
            "                    <LANDOFFICENAME>สำนักงานที่ดินจังหวัดขอนแก่น</LANDOFFICENAME>" +
            "                    <LANDOFFICEX>16.440891</LANDOFFICEX>" +
            "                    <LANDOFFICEY>102.8382132</LANDOFFICEY>" +
            "                    <MAPX>16.42706</MAPX>" +
            "                    <MAPY>102.8372735</MAPY>" +
            "                    <PARCELNO>1</PARCELNO>" +
            "                    <PIN>w6cjkn7yd1uv</PIN>" +
            "                    <PROVINCENAME>ขอนแก่น</PROVINCENAME>" +
            "                    <SURVEYNO>4</SURVEYNO>" +
            "                    <TAMBOLNAME>ในเมือง</TAMBOLNAME>" +
            "                    <UTMLANDNO>338</UTMLANDNO>" +
            "                    <UTMMAP>5541 I 6816-07</UTMMAP>" +
            "                    <UTMSCALE>1000</UTMSCALE>" +
            "                </Parcel>" +
            "            </getParcelResult>" +
            "        </getParcelResponse>" +
            "    </s:Body>" +
            "</s:Envelope>";

    @Test
    public void getParcelWhenSentInformation() throws Exception {
        ConnectDOLApi connectDOLApi = Mockito.mock(ConnectDOLApi.class);
        Mockito.when(connectDOLApi.getDataResponse(input, url, headerAuthorization, headerSOAPAction)).thenReturn(output);

        DOLService dolService = new DOLService(connectDOLApi, url, headerSOAPAction);

        ParcelNS4 parcelNS4 = dolService.getParcelNS4(provinceCode, districtCode, ns4no);

        verify(connectDOLApi).getDataResponse(input, url, headerAuthorization, headerSOAPAction);

        assertEquals("district incorrect", "เมืองขอนแก่น", parcelNS4.getDistrictName());
        assertEquals("map x incorrect", 16.42706, parcelNS4.getMapX(), 0);
        assertEquals("map y incorrect", 102.8372735, parcelNS4.getMapY(), 0);
        assertEquals("parcel no incorrect", 1, parcelNS4.getParcelNo());
        assertEquals("province incorrect", "ขอนแก่น", parcelNS4.getProvinceName());
        assertEquals("sub district incorrect", "ในเมือง", parcelNS4.getSubDistrictName());
        assertEquals("utm land no incorrect", 338, parcelNS4.getUtmLandNo());
        assertEquals("utm map incorrect", "5541 I 6816-07", parcelNS4.getUtmMap());
        assertEquals("utm scale incorrect", 1000, parcelNS4.getUtmScale());
    }
}
