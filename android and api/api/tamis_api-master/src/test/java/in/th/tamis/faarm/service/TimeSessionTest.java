package in.th.tamis.faarm.service;

import in.th.tamis.faarm.services.login.TimeSession;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class TimeSessionTest {

    @Test
    public void testSettlementOfTimeShouldBeReturn15(){
        TimeSession timeSession = new TimeSession();

        Calendar nowTime = Calendar.getInstance();
        nowTime.set(2015,11,11,16,15,00);

        Calendar dataTime = Calendar.getInstance();
        dataTime.set(2015,11,11,16,00,00);

        long time = timeSession.getMinuteSettlement(nowTime, dataTime);
        assertEquals(15, time);
    }

    @Test
    public void testSettlementOfTimeShouldBeReturn50(){
        TimeSession timeSession = new TimeSession();

        Calendar nowTime = Calendar.getInstance();
        nowTime.set(2015,11,11,16,49,55);

        Calendar dataTime = Calendar.getInstance();
        dataTime.set(2015,11,11,16,00,00);

        long time = timeSession.getMinuteSettlement(nowTime, dataTime);
        assertEquals(50, time);
    }
}
