package in.th.tamis.faarm.service.dol;

import in.th.tamis.faarm.models.faarm.parcel.ParcelNS4;
import in.th.tamis.faarm.services.dol.ParserFromXML;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ParserFromXMLTest {

    private String soapBodySuccess = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "    <s:Body>" +
            "        <getParcelResponse xmlns=\"http://110.164.49.55/DolParcel2Ega/\">" +
            "            <getParcelResult xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">" +
            "                <Parcel>" +
            "                    <AMPHURNAME>เมืองขอนแก่น</AMPHURNAME>" +
            "                    <LANDOFFICENAME>สำนักงานที่ดินจังหวัดขอนแก่น</LANDOFFICENAME>" +
            "                    <LANDOFFICEX>16.440891</LANDOFFICEX>" +
            "                    <LANDOFFICEY>102.8382132</LANDOFFICEY>" +
            "                    <MAPX>16.42706</MAPX>" +
            "                    <MAPY>102.8372735</MAPY>" +
            "                    <PARCELNO>1</PARCELNO>" +
            "                    <PIN>w6cjkn7yd1uv</PIN>" +
            "                    <PROVINCENAME>ขอนแก่น</PROVINCENAME>" +
            "                    <SURVEYNO>4</SURVEYNO>" +
            "                    <TAMBOLNAME>ในเมือง</TAMBOLNAME>" +
            "                    <UTMLANDNO>338</UTMLANDNO>" +
            "                    <UTMMAP>5541 I 6816-07</UTMMAP>" +
            "                    <UTMSCALE>1000</UTMSCALE>" +
            "                </Parcel>" +
            "            </getParcelResult>" +
            "        </getParcelResponse>" +
            "    </s:Body>" +
            "</s:Envelope>";

    String soapBodyNotFound = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "   <s:Body>" +
            "      <getParcelResponse xmlns=\"http://110.164.49.55/DolParcel2Ega/\">" +
            "         <getParcelResult xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"/>" +
            "      </getParcelResponse>" +
            "   </s:Body>" +
            "</s:Envelope>";


    @Test
    public void getParcelDOL() throws Exception {

        ParserFromXML parserFromXML = new ParserFromXML();
        ParcelNS4 parcelNS4 = parserFromXML.getParcel(soapBodySuccess);

        assertEquals("district incorrect", "เมืองขอนแก่น", parcelNS4.getDistrictName());
        assertEquals("map x incorrect", 16.42706, parcelNS4.getMapX(), 0);
        assertEquals("map y incorrect", 102.8372735, parcelNS4.getMapY(), 0);
        assertEquals("parcel no incorrect", 1, parcelNS4.getParcelNo());
        assertEquals("province incorrect", "ขอนแก่น", parcelNS4.getProvinceName());
        assertEquals("sub district incorrect", "ในเมือง", parcelNS4.getSubDistrictName());
        assertEquals("utm land no incorrect", 338, parcelNS4.getUtmLandNo());
        assertEquals("utm map incorrect", "5541 I 6816-07", parcelNS4.getUtmMap());
        assertEquals("utm scale incorrect", 1000, parcelNS4.getUtmScale());
    }
}