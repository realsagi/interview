package in.th.tamis.faarm.service.dopa;

import in.th.tamis.faarm.models.dopa.DopaProfile;
import in.th.tamis.faarm.services.dopa.ParserFromXML;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ParserFromXMLTest {

    private String soapBodySuccess = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "   <soapenv:Header>" +
            "      <wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" +
            "         <wsu:Timestamp wsu:Id=\"Timestamp-875\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
            "            <wsu:Created>2015-11-09T06:10:19.623Z</wsu:Created>" +
            "            <wsu:Expires>2015-11-09T06:15:19.623Z</wsu:Expires>" +
            "         </wsu:Timestamp>" +
            "      </wsse:Security>" +
            "   </soapenv:Header>" +
            "   <soapenv:Body>" +
            "      <ns:getPersonalAndMemberAndHouseDetailDesResponse xmlns:ns=\"http://getpidt.ssi.nectec.th.com\">" +
            "         <ns:return xsi:type=\"ax25:C_popANDmembersANDhouseDes\" xmlns:ax25=\"http://myStructReturn/xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
            "            <ax25:house xsi:type=\"ax25:C_PopHOUSEDes\">" +
            "               <ax25:ADDRESS_ALLEY/>" +
            "               <ax25:ADDRESS_AMPHUR>เมืองลพบุรี</ax25:ADDRESS_AMPHUR>" +
            "               <ax25:ADDRESS_ID>16010646740</ax25:ADDRESS_ID>" +
            "               <ax25:ADDRESS_MOO>00</ax25:ADDRESS_MOO>" +
            "               <ax25:ADDRESS_NO>245/8</ax25:ADDRESS_NO>" +
            "               <ax25:ADDRESS_PROVINCE>ลพบุรี</ax25:ADDRESS_PROVINCE>" +
            "               <ax25:ADDRESS_ROAD/>" +
            "               <ax25:ADDRESS_SOI>ม.ปรางค์อรุณ</ax25:ADDRESS_SOI>" +
            "               <ax25:ADDRESS_TAMBON>เขาสามยอด</ax25:ADDRESS_TAMBON>" +
            "               <ax25:BUILDING_NAME/>" +
            "               <ax25:COMMUNITY_NAME>รีละโว้พัฒนา (14)</ax25:COMMUNITY_NAME>" +
            "               <ax25:DISPOSE_DATE>25380221</ax25:DISPOSE_DATE>" +
            "               <ax25:HOUSE_SIZE>000000</ax25:HOUSE_SIZE>" +
            "               <ax25:HOUSE_TYPE>บ้าน</ax25:HOUSE_TYPE>" +
            "               <ax25:NO_construction>ศ</ax25:NO_construction>" +
            "               <ax25:POSTCODE>00000</ax25:POSTCODE>" +
            "               <ax25:SELL_DATE>00000000</ax25:SELL_DATE>" +
            "               <ax25:TEL/>" +
            "               <ax25:VILLAGE_NAME/>" +
            "               <ax25:amount_Habitats>00000000</ax25:amount_Habitats>" +
            "               <ax25:amount_all>000000000</ax25:amount_all>" +
            "               <ax25:documents_land/>" +
            "               <ax25:house_Styles/>" +
            "               <ax25:location_Buildings>ไม่ระบุ</ax25:location_Buildings>" +
            "               <ax25:name_Registry>ท้องถิ่นเทศบาลเมืองเขาสามยอด</ax25:name_Registry>" +
            "               <ax25:no_Documents_land/>" +
            "               <ax25:status xsi:type=\"ax25:C_status_return\">" +
            "                  <ax25:servermessage>succ</ax25:servermessage>" +
            "                  <ax25:statuscode>0</ax25:statuscode>" +
            "               </ax25:status>" +
            "            </ax25:house>" +
            "            <ax25:members xsi:type=\"ax25:Members\">" +
            "               <ax25:member xsi:type=\"ax25:Member\">" +
            "                  <ax25:firstName>ธณชาคร</ax25:firstName>" +
            "                  <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "                  <ax25:pid>3160600188012</ax25:pid>" +
            "               </ax25:member>" +
            "               <ax25:member xsi:type=\"ax25:Member\">" +
            "                  <ax25:firstName>กฤษฎาญชล</ax25:firstName>" +
            "                  <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "                  <ax25:pid>1169800011959</ax25:pid>" +
            "               </ax25:member>" +
            "               <ax25:member xsi:type=\"ax25:Member\">" +
            "                  <ax25:firstName>เจษฎากล</ax25:firstName>" +
            "                  <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "                  <ax25:pid>1169800112470</ax25:pid>" +
            "               </ax25:member>" +
            "               <ax25:member xsi:type=\"ax25:Member\">" +
            "                  <ax25:firstName>ธณพร</ax25:firstName>" +
            "                  <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "                  <ax25:pid>3160600187962</ax25:pid>" +
            "               </ax25:member>" +
            "               <ax25:member xsi:type=\"ax25:Member\">" +
            "                  <ax25:firstName>ยุทธกฤษ</ax25:firstName>" +
            "                  <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "                  <ax25:pid>3160600188004</ax25:pid>" +
            "               </ax25:member>" +
            "            </ax25:members>" +
            "            <ax25:popdetail xsi:type=\"ax25:C_pop_returnDes\">" +
            "               <ax25:PID>1169800011959</ax25:PID>" +
            "               <ax25:addressDetail>245/8#00##ม.ปรางค์อรุณ##เขาสามยอด#เมืองลพบุรี#ลพบุรี</ax25:addressDetail>" +
            "               <ax25:age>030</ax25:age>" +
            "               <ax25:birthday>25280214</ax25:birthday>" +
            "               <ax25:changeNatinality/>" +
            "               <ax25:dayChangeNatinality>00000000</ax25:dayChangeNatinality>" +
            "               <ax25:dayInto>25560605</ax25:dayInto>" +
            "               <ax25:fatherName>ยุทธกฤษ</ax25:fatherName>" +
            "               <ax25:fatherNatinality>ไทย</ax25:fatherNatinality>" +
            "               <ax25:firstName>กฤษฎาญชล</ax25:firstName>" +
            "               <ax25:gender>ชาย</ax25:gender>" +
            "               <ax25:hid>16010646740</ax25:hid>" +
            "               <ax25:hostStatus>ผู้อาศัย</ax25:hostStatus>" +
            "               <ax25:lastName>สะดีวงศ์</ax25:lastName>" +
            "               <ax25:motherName>ธณพร</ax25:motherName>" +
            "               <ax25:motherNatinality>ไทย</ax25:motherNatinality>" +
            "               <ax25:natinality>ไทย</ax25:natinality>" +
            "               <ax25:personStatus>มีภูมิลำเนาอยู่ในบ้านนี้</ax25:personStatus>" +
            "               <ax25:pidFather>3160600188004</ax25:pidFather>" +
            "               <ax25:pidMother>3160600187962</ax25:pidMother>" +
            "               <ax25:pop5FLname>ษฎาญชล</ax25:pop5FLname>" +
            "               <ax25:pop5Name>ท้องถิ่นเทศบาลเมืองเขาสามยอด</ax25:pop5Name>" +
            "               <ax25:pop5NameCenter>000000000000000000กฤ</ax25:pop5NameCenter>" +
            "               <ax25:pop5NameFather>สะ</ax25:pop5NameFather>" +
            "               <ax25:pop5NameMother>ดีวงศ์                      ยุ</ax25:pop5NameMother>" +
            "               <ax25:prefix>นาย</ax25:prefix>" +
            "               <ax25:status xsi:type=\"ax25:C_status_return\">" +
            "                  <ax25:servermessage>succ</ax25:servermessage>" +
            "                  <ax25:statuscode>0</ax25:statuscode>" +
            "               </ax25:status>" +
            "            </ax25:popdetail>" +
            "         </ns:return>" +
            "      </ns:getPersonalAndMemberAndHouseDetailDesResponse>" +
            "   </soapenv:Body>" +
            "</soapenv:Envelope>";

    private String soapBodyUnsuccess = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "   <soapenv:Header>" +
            "      <wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" +
            "         <wsu:Timestamp wsu:Id=\"Timestamp-3278\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
            "            <wsu:Created>2015-11-11T12:27:50.323Z</wsu:Created>" +
            "            <wsu:Expires>2015-11-11T12:32:50.323Z</wsu:Expires>" +
            "         </wsu:Timestamp>" +
            "      </wsse:Security>" +
            "   </soapenv:Header>" +
            "   <soapenv:Body>" +
            "      <ns:getPersonalAndMemberAndHouseDetailDesResponse xmlns:ns=\"http://getpidt.ssi.nectec.th.com\">" +
            "         <ns:return xsi:type=\"ax25:C_popANDmembersANDhouseDes\" xmlns:ax25=\"http://myStructReturn/xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
            "            <ax25:house xsi:nil=\"true\"/>" +
            "            <ax25:members xsi:nil=\"true\"/>" +
            "            <ax25:popdetail xsi:type=\"ax25:C_pop_returnDes\">" +
            "               <ax25:PID xsi:nil=\"true\"/>" +
            "               <ax25:addressDetail xsi:nil=\"true\"/>" +
            "               <ax25:age xsi:nil=\"true\"/>" +
            "               <ax25:birthday xsi:nil=\"true\"/>" +
            "               <ax25:changeNatinality xsi:nil=\"true\"/>" +
            "               <ax25:dayChangeNatinality xsi:nil=\"true\"/>" +
            "               <ax25:dayInto xsi:nil=\"true\"/>" +
            "               <ax25:fatherName xsi:nil=\"true\"/>" +
            "               <ax25:fatherNatinality xsi:nil=\"true\"/>" +
            "               <ax25:firstName xsi:nil=\"true\"/>" +
            "               <ax25:gender xsi:nil=\"true\"/>" +
            "               <ax25:hid xsi:nil=\"true\"/>" +
            "               <ax25:hostStatus xsi:nil=\"true\"/>" +
            "               <ax25:lastName xsi:nil=\"true\"/>" +
            "               <ax25:motherName xsi:nil=\"true\"/>" +
            "               <ax25:motherNatinality xsi:nil=\"true\"/>" +
            "               <ax25:natinality xsi:nil=\"true\"/>" +
            "               <ax25:personStatus xsi:nil=\"true\"/>" +
            "               <ax25:pidFather xsi:nil=\"true\"/>" +
            "               <ax25:pidMother xsi:nil=\"true\"/>" +
            "               <ax25:pop5FLname xsi:nil=\"true\"/>" +
            "               <ax25:pop5Name xsi:nil=\"true\"/>" +
            "               <ax25:pop5NameCenter xsi:nil=\"true\"/>" +
            "               <ax25:pop5NameFather xsi:nil=\"true\"/>" +
            "               <ax25:pop5NameMother xsi:nil=\"true\"/>" +
            "               <ax25:prefix xsi:nil=\"true\"/>" +
            "               <ax25:status xsi:type=\"ax25:C_status_return\">" +
            "                  <ax25:servermessage>getPopCode Err java.lang.Exception: getAllDetailpop Err status=0 returnCode=90005 Detail:ไม่มีสิทธิในการทำงาน ( Time to work Out )</ax25:servermessage>" +
            "                  <ax25:statuscode>-1</ax25:statuscode>" +
            "               </ax25:status>" +
            "            </ax25:popdetail>" +
            "         </ns:return>" +
            "      </ns:getPersonalAndMemberAndHouseDetailDesResponse>" +
            "   </soapenv:Body>" +
            "</soapenv:Envelope>";

    private NodeList nodeList;
    private Element element;

    @Test
    public void getResponseOutOfService() throws Exception {

        ParserFromXML parserFromXML = new ParserFromXML();
        DopaProfile dopaProfile = parserFromXML.setDopaProfile(soapBodyUnsuccess);

        assertNull(dopaProfile.getDopaDetail().getCitizenId());
    }

    @Test
    public void getDopaDetail() throws Exception {

        ParserFromXML parserFromXML = new ParserFromXML();
        DopaProfile dopaProfile = parserFromXML.setDopaProfile(soapBodySuccess);

        assertEquals("birthDay incorrect", "25280214", dopaProfile.getDopaDetail().getBirthDate());
        assertEquals("citizenId incorrect", "1169800011959", dopaProfile.getDopaDetail().getCitizenId());
        assertEquals("firstName incorrect", "กฤษฎาญชล", dopaProfile.getDopaDetail().getFirstName());
        assertEquals("lastName incorrect", "สะดีวงศ์", dopaProfile.getDopaDetail().getLastName());
        assertEquals("sex incorrect", "ชาย", dopaProfile.getDopaDetail().getSex());
        assertEquals("preName incorrect", "นาย", dopaProfile.getDopaDetail().getPreName());
    }

    @Test
    public void getDopaAddressDetail() throws Exception {

        ParserFromXML parserFromXML = new ParserFromXML();
        DopaProfile dopaProfile = parserFromXML.setDopaProfile(soapBodySuccess);

        assertEquals("addressId incorrect", "16010646740", dopaProfile.getDopaHouse().getAddressId());
        assertEquals("addressNo incorrect", "245/8", dopaProfile.getDopaHouse().getAddressNo());
        assertEquals("soi incorrect", "ม.ปรางค์อรุณ", dopaProfile.getDopaHouse().getSoi());
        assertEquals("alley incorrect", "", dopaProfile.getDopaHouse().getAlley());
        assertEquals("road incorrect", "", dopaProfile.getDopaHouse().getRoad());
        assertEquals("moo incorrect", "00", dopaProfile.getDopaHouse().getMoo());
        assertEquals("tumbon incorrect", "เขาสามยอด", dopaProfile.getDopaHouse().getTumbon());
        assertEquals("ampher incorrect", "เมืองลพบุรี", dopaProfile.getDopaHouse().getAmpher());
        assertEquals("province incorrect", "ลพบุรี", dopaProfile.getDopaHouse().getProvince());

    }

    @Test
    public void getDopaHouseholds() throws Exception {

        ParserFromXML parserFromXML = new ParserFromXML();
        DopaProfile dopaProfile = parserFromXML.setDopaProfile(soapBodySuccess);

        assertEquals("member is not 5", 5, dopaProfile.getDopaMembers().size());
        assertEquals("citizenId incorrect", "1169800011959", dopaProfile.getDopaMembers().get(1).getCitizenId());
        assertEquals("firstName incorrect", "กฤษฎาญชล", dopaProfile.getDopaMembers().get(1).getFirstName());
        assertEquals("lastName incorrect", "สะดีวงศ์", dopaProfile.getDopaMembers().get(1).getLastName());
    }


}