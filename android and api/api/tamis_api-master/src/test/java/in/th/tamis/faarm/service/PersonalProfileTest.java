package in.th.tamis.faarm.service;

import com.google.gson.Gson;
import in.th.tamis.faarm.manager.DOPARestAPI;
import in.th.tamis.faarm.models.dopa.CitizenProfile;
import in.th.tamis.faarm.models.faarm.Address;
import in.th.tamis.faarm.models.faarm.PersonalProfile;
import in.th.tamis.faarm.services.PersonalFAARMProfileService;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PersonalProfileTest {

    PersonalFAARMProfileService service;

    public PersonalProfileTest() {
        service = new PersonalFAARMProfileService(new DOPARestAPI("http://127.0.0.1:8882"));
    }

    private CitizenProfile getCitizenProfile(String citizenId) throws IOException {
        return service.callPersonalProfileDOPA(citizenId);
    }

    @Test
    public void notFoundPersonalProfileDOPAWhenInsertCitizenIdIncorrect() throws IOException {
        String citizenId = "1169800011950";
        CitizenProfile citizenProfile = getCitizenProfile(citizenId);
        assertEquals(null, citizenProfile);
    }

    @Test
    public void foundPersonalProfileDOPAByCitizenIdCorrect() throws IOException {
        String citizenId = "1169800011959";
        CitizenProfile citizenProfile = getCitizenProfile(citizenId);
        assertNotEquals(null, citizenProfile);
    }

    @Test
    public void convertParameterFromPersonalProfileDOPAObjectToPersonalProfileFAARM() throws IOException {
        String citizenId = "1169800011959";
        PersonalProfile personalProfile = service.findByCitizenId(citizenId);
        assertEquals("นาย", personalProfile.getPreName());
        assertEquals("กฤษฎาญชล", personalProfile.getFirstName());
        assertEquals("สะดีวงศ์", personalProfile.getLastName());
        assertEquals(1, personalProfile.getSexId());
        assertEquals("ชาย", personalProfile.getSex());
        assertEquals("25280214", personalProfile.getBirthDate());
        assertEquals("30", personalProfile.getAge());
        assertEquals(1, personalProfile.getPersonStatusId());
        assertEquals("มีชีวิตอยู่", personalProfile.getPersonStatus());

        Address address = personalProfile.getAddress();

        assertEquals("12345678910", address.getAddressID());
        assertEquals("245/8", address.getHouseNo());
        assertEquals("", address.getMoo());
        assertEquals("", address.getAlley());
        assertEquals("ม.ปรางค์อรุณ", address.getSoi());
        assertEquals("เขาสามยอด", address.getSubDistrict());
        assertEquals("เมืองลพบุรี", address.getDistrict());
        assertEquals("ลพบุรี", address.getProvince());
    }

    @Test
    public void convertFAARMProfileToJson() throws IOException {
        String citizenId = "1169800011959";

        PersonalProfile personalProfile = service.findByCitizenId(citizenId);

        String stringJson = new Gson().toJson(personalProfile);
        String expected = "{" +
                "\"pre_name\":\"นาย\"," +
                "\"first_name\":\"กฤษฎาญชล\"," +
                "\"last_name\":\"สะดีวงศ์\"," +
                "\"sex_id\":1," +
                "\"sex\":\"ชาย\"," +
                "\"birth_date\":\"25280214\"," +
                "\"age\":\"30\"," +
                "\"person_status_id\":1," +
                "\"person_status\":\"มีชีวิตอยู่\"," +
                "\"address\":{" +
                "\"address_id\":\"12345678910\"," +
                "\"house_no\":\"245/8\"," +
                "\"moo\":\"\"," +
                "\"alley\":\"\"," +
                "\"soi\":\"ม.ปรางค์อรุณ\"," +
                "\"road\":\"\"," +
                "\"tumbol\":\"เขาสามยอด\"," +
                "\"amphur\":\"เมืองลพบุรี\"," +
                "\"province\":\"ลพบุรี\"" +
                "}" +
                "}";
        assertEquals(expected, stringJson);
    }



}
