package in.th.tamis.faarm.service;

import in.th.tamis.faarm.dao.NewMemberJDBCMySQL;
import in.th.tamis.faarm.manager.MysqlConnector;
import in.th.tamis.faarm.services.NewMember;
import in.th.tamis.faarm.services.NewMemberService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MemberServiceTest {
    @Test
    public void getMemberPrefix(){
        NewMember member = new NewMemberService(new NewMemberJDBCMySQL(MysqlConnector.getConnection()));
        assertEquals(1,member.getPrefixID("นาย"));
    }
}
