package in.th.tamis.faarm.service.dol;

import in.th.tamis.faarm.services.dol.ConnectDOLApi;

import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import org.mockito.Mockito;
import static org.mockito.Mockito.verify;

public class ConnectDOLApiTest {

    private String headerAuthorization = "";
    private String headerSOAPAction = "";
    private String url = "https://122.154.24.188:4321/struct/services/dopaTamis.dopaTamisHttpsSoap11Endpoint";
    private String input = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dol=\"http://110.164.49.55/DolParcel2Ega/\"><soapenv:Header/><soapenv:Body><dol:getParcel><dol:provincecode>40</dol:provincecode><dol:amphoecode>01</dol:amphoecode><dol:ns4no>1</dol:ns4no></dol:getParcel></soapenv:Body></soapenv:Envelope>";
    String output = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "    <s:Body>" +
            "        <getParcelResponse xmlns=\"http://110.164.49.55/DolParcel2Ega/\">" +
            "            <getParcelResult xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">" +
            "                <Parcel>" +
            "                    <AMPHURNAME>เมืองขอนแก่น</AMPHURNAME>" +
            "                    <LANDOFFICENAME>สำนักงานที่ดินจังหวัดขอนแก่น</LANDOFFICENAME>" +
            "                    <LANDOFFICEX>16.440891</LANDOFFICEX>" +
            "                    <LANDOFFICEY>102.8382132</LANDOFFICEY>" +
            "                    <MAPX>16.42706</MAPX>" +
            "                    <MAPY>102.8372735</MAPY>" +
            "                    <PARCELNO>1</PARCELNO>" +
            "                    <PIN>w6cjkn7yd1uv</PIN>" +
            "                    <PROVINCENAME>ขอนแก่น</PROVINCENAME>" +
            "                    <SURVEYNO>4</SURVEYNO>" +
            "                    <TAMBOLNAME>ในเมือง</TAMBOLNAME>" +
            "                    <UTMLANDNO>338</UTMLANDNO>" +
            "                    <UTMMAP>5541 I 6816-07</UTMMAP>" +
            "                    <UTMSCALE>1000</UTMSCALE>" +
            "                </Parcel>" +
            "            </getParcelResult>" +
            "        </getParcelResponse>" +
            "    </s:Body>" +
            "</s:Envelope>";

    @Test
    public void getResponseHTTPStatusIsOK () throws Exception {
        ConnectDOLApi connectDOLApi = Mockito.mock(ConnectDOLApi.class);
        Mockito.when(connectDOLApi.getDataResponse(input, url, headerAuthorization, headerSOAPAction)).thenReturn(output);

        String response = connectDOLApi.getDataResponse(input, url, headerAuthorization, headerSOAPAction);

        verify(connectDOLApi).getDataResponse(input, url, headerAuthorization, headerSOAPAction);

        assertThat(response, containsString("getParcelResponse"));
    }
}
