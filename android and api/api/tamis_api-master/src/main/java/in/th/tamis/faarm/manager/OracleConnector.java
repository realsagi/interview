package in.th.tamis.faarm.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class OracleConnector {

    private static String database = "FarmerDB";
    private static String user = "doae_center";
    private static String pass = "1234567";
    private static String port = "1520";
    private static String ipAddress = "164.115.20.128";


//    private static String database = "FAARM";
//    private static String user = "SYSTEM";
//    private static String pass = "password";
//    private static String port = "1520";
//    private static String ipAddress = "223.207.171.27";


    private static java.lang.String driver = "oracle.jdbc.driver.OracleDriver";
    private static java.lang.String url = "jdbc:oracle:thin:@"+ipAddress+":"+port+":"+database;

    private static Connection connection = null;
    public static Connection getConnection(){
        try {
            Class.forName(driver);
            if(connection == null)
                connection = DriverManager.getConnection(url, user, pass);
                connection.setAutoCommit(true);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
