package in.th.tamis.faarm.services.career;

import com.google.gson.JsonObject;
import in.th.tamis.faarm.models.faarm.career.ProfileCareer;

import java.util.ArrayList;
import java.util.List;

public class CareerServiceStub implements Career {
    @Override
    public ProfileCareer getCareer() {
        ProfileCareer profileCareer = new ProfileCareer();

        JsonObject jsonObject1 = new JsonObject();
        jsonObject1.addProperty("id", 1);
        jsonObject1.addProperty("name", "ประกอบอาชีพเกษตร");

        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.addProperty("id", 2);
        jsonObject2.addProperty("name", "รับเงินเดือนประจำ");

        JsonObject jsonObject3 = new JsonObject();
        jsonObject3.addProperty("id", 3);
        jsonObject3.addProperty("name", "รับจ้างทางการเกษร");

        JsonObject jsonObject4 = new JsonObject();
        jsonObject4.addProperty("id", 4);
        jsonObject4.addProperty("name", "ประกอบธุรกิจการค้า");

        JsonObject jsonObject5 = new JsonObject();
        jsonObject5.addProperty("id", 5);
        jsonObject5.addProperty("name", "รับจ้างทั่วไป");

        JsonObject jsonObject6 = new JsonObject();
        jsonObject6.addProperty("id", 6);
        jsonObject6.addProperty("name", "อื่น ๆ");

        List<JsonObject> jsonObjects = new ArrayList<>();
        jsonObjects.add(jsonObject1);
        jsonObjects.add(jsonObject2);
        jsonObjects.add(jsonObject3);
        jsonObjects.add(jsonObject4);
        jsonObjects.add(jsonObject5);
        jsonObjects.add(jsonObject6);


        profileCareer.setCareer(jsonObjects);


        return profileCareer;
    }

}
