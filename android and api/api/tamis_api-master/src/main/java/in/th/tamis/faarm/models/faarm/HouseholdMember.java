package in.th.tamis.faarm.models.faarm;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HouseholdMember {

    List<Person> persons;

    public int getCount() {
        return persons.size();
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public void addPerson(Person person) {
        if (persons == null)
            persons = new ArrayList<Person>();
        persons.add(person);
    }
}
