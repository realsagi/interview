package in.th.tamis.faarm.models.faarm.datafromandroid;

import com.google.gson.annotations.SerializedName;

public class ImagesFarmer {
    @SerializedName("citizen_id")
    private  String citizenId;
    @SerializedName("file_name")
    private String filename;
    @SerializedName("image")
    private byte[] images;

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getImages() {
        return images;
    }

    public void setImages(byte[] images) {
        this.images = images;
    }
}
