package in.th.tamis.faarm.dao;

import com.google.gson.Gson;
import in.th.tamis.faarm.models.faarm.Address;
import in.th.tamis.faarm.models.faarm.Person;
import in.th.tamis.faarm.models.faarm.datafromandroid.DetailAgriculturalOperations;
import in.th.tamis.faarm.models.faarm.datafromandroid.DetailRegisterPlantation;
import in.th.tamis.faarm.models.faarm.datafromandroid.Farmer;

import java.io.File;
import java.sql.*;

public class NewMemberJDBCORACLE implements NewMemberDAO {
    private Connection connection;

    public NewMemberJDBCORACLE(Connection connection) {
        this.connection = connection;
    }

    @Override
    public int newFarmer(Farmer farmer, int userId) {
        int profileId = genProFileId();
        profileId = insertFarmer(farmer, profileId,userId);
        insertFaarm(profileId);
        insertCareer(profileId,farmer,userId);
        insertVerifyPerson(profileId,"กรมการปกครอง");
        return profileId;
    }

    private void insertCareer(int profileId, Farmer farmer, int userId) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String insert = "INSERT INTO PROFILE_CAREER" +
                "(PROFILE_CENTER_ID,CAREER_MAIN,CAREER_MINOR,CREATE_DATE,CREATE_USER)" +
                "VALUES" +
                "(?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setLong(1, profileId);
            preparedStatement.setLong(2, Integer.parseInt(farmer.getCareerMain()));
            preparedStatement.setLong(3, Integer.parseInt(farmer.getCareerMinor()));
            preparedStatement.setTimestamp(4, timestamp);
            preparedStatement.setLong(5, userId);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private int genProFileId() {
        int resultId = 0 ;
        String sql = "SELECT MAX(PROFILE_CENTER_ID) FROM PROFILE_CENTER";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery();
            result.next();
            resultId = result.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultId+1;
    }

    private int insertFarmer(Farmer farmer, int profileId, int userId) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        int year = Integer.parseInt(String.valueOf(timestamp).substring(0, 4)) + 543;
        String insert = "INSERT INTO PROFILE_CENTER" +
                "(PROFILE_PREFIX, PROFILE_NAME, PROFILE_SURNAME, PROFILE_IDCARD, PROFILE_BORN_YEAR, " +
                "PROFILE_BIRTHDAY, PROFILE_TELEPHONE, PROFILE_MOBILE, PROFILE_HOUSENUMBER1, PROFILE_SOI1, " +
                "PROFILE_ROAD, PROFILE_TOK, PROFILE_MOO1,PROFILE_TAMBON1, PROFILE_AMPHUR1, " +
                "PROFILE_PROVINCE1, PROFILE_ID, PROVINCE_CODE, AMPHUR_CODE, TAMBON_CODE," +
                "CREATE_DATE,CREATE_USER,ZIP_CODE,PROFILE_CENTER_ID,YEAR,PROFILE_IDHOUSE,STATUS_MARRY) VALUES" +
                "(?,?,?,?,?," +
                "?,?,?,?,?," +
                "?,?,?,?,?," +
                "?,?,?,?,?," +
                "?,?,?,PROFILE_CENTER_SEQ.NEXTVAL,?," +
                "?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setLong(1, farmer.getPrefixId());
            preparedStatement.setString(2, farmer.getFirstName());
            preparedStatement.setString(3, farmer.getLastName());
            preparedStatement.setString(4, farmer.getCitizenId());
            preparedStatement.setLong(5, farmer.getYear());
            preparedStatement.setLong(6, Integer.parseInt(farmer.getBirthDate()));
            preparedStatement.setString(7, farmer.getHomePhone());
            preparedStatement.setString(8, farmer.getMobilePhone());

            Address address = farmer.getAddress();
            preparedStatement.setString(9, address.getHouseNo());
            preparedStatement.setString(10, address.getSoi());
            preparedStatement.setString(11, address.getRoad());
            preparedStatement.setString(12, address.getAlley());
            preparedStatement.setString(13, address.getMoo());

            preparedStatement.setString(14, address.getSubDistrict());
            preparedStatement.setString(15, address.getDistrict());
            preparedStatement.setString(16, address.getProvince());
            preparedStatement.setLong(17, profileId);

            AreaDAO areaDAO = new AreaJDBC(connection);
            String provinceCode = areaDAO.getProvinceCode(address.getProvince());
            String districtCode = areaDAO.getDistrictCode(provinceCode, address.getDistrict());
            String subDistrictCode = areaDAO.getSubDistrictCode(provinceCode, districtCode, address.getSubDistrict());

            preparedStatement.setString(18, provinceCode);
            preparedStatement.setString(19, districtCode);
            preparedStatement.setString(20, subDistrictCode);
            preparedStatement.setTimestamp(21, timestamp);
            preparedStatement.setLong(22, userId);
            preparedStatement.setString(23, address.getZipCode());
            preparedStatement.setLong(24, year);
            preparedStatement.setString(25, address.getAddressID());
            preparedStatement.setLong(26, farmer.getStatus());

            preparedStatement.execute();

            if (farmer.getNameImageProfile()!=null)
                addImageFarmer(farmer.getNameImageProfile(), farmer.getCitizenId(), String.valueOf(profileId));
            if (farmer.getNameImageSign()!=null)
                addImageSign(farmer.getNameImageSign(), farmer.getCitizenId(), String.valueOf(profileId));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return profileId;
    }

    private void insertFaarm(int profileId) {
        String insert = "INSERT INTO FAARM (PROFILE_CENTER_ID,FAARM_ID) VALUES(?,FAARM_SEQ.NEXTVAL)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setLong(1, profileId);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void insertVerifyPerson(int profileId, String detail) {
        String insert = "INSERT INTO PROFILE_CENTER_VERIFY " +
                "(PROFILE_CENTER_ID,DETAIL,PROFILE_CENTER_VERIFY_ID)" +
                " VALUES" +
                "(?,?,PROFILE_AREA_VERIFY_SEQ.NEXTVAL)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setLong(1, profileId);
            preparedStatement.setString(2,detail);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int findPrefixID(String prefix) {
        int prefixId = 6;
        String sql = "SELECT PREFIX_ID FROM PREFIX WHERE PREFIX_NAME = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, prefix);
            ResultSet result = preparedStatement.executeQuery();
            result.next();
            prefixId = result.getInt("PREFIX_ID");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prefixId;
    }

    private void addImageFarmer(String nameImageProfile, String citizenId, String profileId) {
        String insert = "INSERT INTO PROFILE_CENTER_IMAGE " +
                "(PROFILE_CENTER_ID,PATH_IMAGE,PROFILE_CENTER_IMAGE_ID)VALUES(?,?,PROFILE_CENTER_IMAGE_SEQ)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setString(1,profileId);
            String filename = "images" + File.separator + citizenId + File.separator + nameImageProfile;
            preparedStatement.setString(2,filename);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addImageSign(String nameImageSign, String citizenId, String profileId) {
        String insert = "INSERT INTO PROFILE_SIGN_IMAGE " +
                "(PROFILE_ID,PATH_IMAGE,SIGN_IMAGE_ID)VALUES(?,?,PROFILE_SIGN_IMAGE_SEQ)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setString(1,profileId);
            String filename = "images" + File.separator + citizenId + File.separator + nameImageSign;
            preparedStatement.setString(2,filename);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int addNewHouseholdMember(int profileCenterId, Person householdMember, int statusMember, int userId) {
        int sexId = 1;
        int statusHelp = 2;
        int profileMemberId = 0;
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String insert = "INSERT INTO PROFILE_MEMBER" +
                "(MEMBER_NAME, MEMBER_SURNAME, MEMBER_IDCARD, MEMBER_GENDER, MEMBER_HELP, " +
                "MEMBER_FLAX, PROFILE_CENTER_ID,CREATE_DATE, CREATE_USER, PROFILE_MEMBER_ID) VALUES" +
                "(?,?,?,?,?," +
                "?,?,?,?,PROFILE_MEMBER_SEQ.NEXTVAL)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, householdMember.getFirstName());
            preparedStatement.setString(2, householdMember.getLastName());
            preparedStatement.setString(3, householdMember.getIdentityCard());
            householdMember.getSex();
            if (householdMember.getSex().equals("ชาย")) {
                sexId = 0;
            }
            preparedStatement.setLong(4, sexId);
            if (householdMember.isStatusHelpWork()) {
                statusHelp = 1;
            }
            preparedStatement.setLong(5, statusHelp);
            preparedStatement.setLong(6, statusMember);
            preparedStatement.setLong(7, profileCenterId);
            preparedStatement.setTimestamp(8, timestamp);
            preparedStatement.setLong(9, userId);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String sql = "SELECT MAX(PROFILE_MEMBER_ID) FROM PROFILE_MEMBER";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery();
            result.next();
            profileMemberId = result.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return profileMemberId;
    }

    @Override
    public void addMemberOrg(int profileMemberId, String org) {
        String insert = "INSERT INTO PROFILE_MEMBER_ORG" +
                "(PROFILE_MEMBER_ID, KASET_ORG_ID,PROFILE_MEMBER_ORG_ID) VALUES" +
                "(?,?,PROFILE_MEMBER_ORG_SEQ.NEXTVAL)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setLong(1, profileMemberId);
            preparedStatement.setLong(2, Integer.parseInt(org));
            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int addDetailPlantation(int profileCenterId, DetailRegisterPlantation plantation, int index, String citizenId, int userId) {
        int areaId = genAreaId();
        insertPlantation(profileCenterId, plantation, index, citizenId, areaId,userId);
        if(plantation.getCoordinates().size() != 0)
            insertPlantationCoordinates(areaId, new Gson().toJson(plantation.getCoordinates()),userId);
        if(plantation.getParcelDoc().getVerifyDOL())
            insertVerifyPlantation(areaId,"กรมที่ดิน");
        return areaId;
    }

    private int genAreaId() {
        int resultId = 0 ;
        String sql = "SELECT MAX(PROFILE_AREA_ID) FROM PROFILE_AREA";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery();
            result.next();
            resultId = result.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultId+1;
    }

    private void insertPlantation(int profileCenterId, DetailRegisterPlantation plantation, int index, String citizenId, int areaId, int userId) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String insert = "INSERT INTO PROFILE_AREA" +
                "(PROFILE_CENTER_ID, AREA_NUM, AREA_TYPE, WATER_CONTROL, DOC_TYPE_ID, " +
                "DOC_NO, DOC_RAWANG, DOC_RAI, DOC_NGAN, DOC_WA, " +
                "MOO, PROVINCE_CODE, AMPHUR_CODE, TAMBON_CODE, OWNER_NAME, " +
                "ID_CARD, AREA_PX, AREA_PY, AREA_LAT, AREA_LNG," +
                "PLANT_AREA_ID2,CREATE_DATE,CREATE_USER,PROFILE_AREA_ID) VALUES" +
                "(?,?,?,?,?," +
                "?,?,?,?,?," +
                "?,?,?,?,?," +
                "?,?,?,?,?," +
                "?,?,?,PROFILE_AREA_ID_SEQ.NEXTVAL)";
        try {
            int waterControl = 2;
            if (plantation.getIrrigationArea().equals("ในเขตชลประทาน")) {
                waterControl = 1;
            }
            PreparedStatement preparedStatement = connection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, profileCenterId);
            preparedStatement.setLong(2, index);
            preparedStatement.setLong(3, getHoldingNumber(plantation.getHolding()));
            preparedStatement.setLong(4, waterControl);
            preparedStatement.setLong(5, Integer.parseInt(plantation.getLicenseType()));
            preparedStatement.setString(6, plantation.getParcelDoc().getParcelNumber());
            preparedStatement.setString(7, plantation.getRaWangNumber());
            preparedStatement.setLong(8, plantation.getRai());
            preparedStatement.setLong(9, plantation.getNgan());
            preparedStatement.setLong(10, plantation.getSquareWah());

            Address address = plantation.getAddress();

            preparedStatement.setString(11, plantation.getAddress().getMoo());
            preparedStatement.setString(12, address.getProvince());
            preparedStatement.setString(13, address.getDistrict());
            preparedStatement.setString(14, address.getSubDistrict());
            preparedStatement.setString(15, plantation.getCorporateOwnershipLicense());
            preparedStatement.setString(16, citizenId);
            preparedStatement.setDouble(17, plantation.getParcelDoc().getMapX());
            preparedStatement.setDouble(18, plantation.getParcelDoc().getMapY());
            preparedStatement.setDouble(19, plantation.getCoordinatesCenter().getLatitude());
            preparedStatement.setDouble(20, plantation.getCoordinatesCenter().getLongitude());
            preparedStatement.setLong(21, areaId);
            preparedStatement.setTimestamp(22, timestamp);
            preparedStatement.setLong(23, userId);
            preparedStatement.execute();
            if(plantation.getFileNameImagePlantation()!=null){
                addImagePlantation(plantation.getFileNameImagePlantation(), citizenId, String.valueOf(areaId));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void insertPlantationCoordinates(int areaId, String coordinates, int userId) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String insert = "INSERT INTO PROFILE_AREA_COORDINATES(AREA_ID,COORDINATES,AREA_COORDINATES_ID,CREATE_DATE,CREATE_USER)VALUES(?,?,PROFILE_AREA_COORDINATES_SEQ.NEXTVAL,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setLong(1, areaId);
            preparedStatement.setString(2, coordinates);
            preparedStatement.setTimestamp(3, timestamp);
            preparedStatement.setInt(4, userId);

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addImagePlantation(String fileNameImagePlantation, String citizenId, String detailPlantationId) {
        String insert = "INSERT INTO PROFILE_AREA_IMAGE " +
                "(AREA_ID,PATH_IMAGE,PROFILE_AREA_IMAGE_ID)VALUES(?,?,PROFILE_AREA_IMAGE_SEQ.NEXTVAL)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setString(1, detailPlantationId);
            String filename = "images" + File.separator + citizenId + File.separator + fileNameImagePlantation;
            preparedStatement.setString(2, filename);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void insertVerifyPlantation(int areaId,String detail) {
        String insert = "INSERT INTO PROFILE_AREA_VERIFY (PROFILE_AREA_ID,DETAIL,PROFILE_AREA_VERIFY_ID)VALUES(?,?,PROFILE_AREA_VERIFY_SEQ.NEXTVAL)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setLong(1, areaId);
            preparedStatement.setString(2,detail);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addDetailActivity(int areaId, DetailAgriculturalOperations activity, String citizenId, int profileCenterId, int userId) {
        int activityId = 0;
        insertActivity(areaId, activity, citizenId, profileCenterId, userId);
        activityId = getActivityId();
        if(activity.getFileNameImageActivity()!=null){
            addImageActivity(activity.getFileNameImageActivity(), citizenId, activityId);
        }
        if(activity.getCoordinates().size()!=0){
            addCoordinatesActivity(activityId,new Gson().toJson(activity.getCoordinates()),userId);
        }
    }

    private void insertActivity(int areaId, DetailAgriculturalOperations activity, String citizenId, int profileCenterId, int userId) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String insert = "INSERT INTO PROFILE_ACTIVITY" +
                "(PROFILE_CENTER_ID, TYPE_CODE, DETAIL_CODE, PLANT_DATE, PRODUCE_DATE, " +
                "ACT_RAI, ACT_NGAN, ACT_WA, PROD_RAI, PROD_NGAN, " +
                "PROD_WA, PRODUCE, BREED_CODE, RICE_TYPE, ID_CARD," +
                " AREA_ID,CREATE_DATE,CREATE_USER)VALUES(" +
                "?,?,?,?,?," +
                "?,?,?,?,?," +
                "?,?,?,?,?," +
                "?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, profileCenterId);
            preparedStatement.setString(2,activity.getNamePlants());
            preparedStatement.setString(3,activity.getSeeds());
            preparedStatement.setDate(4, new Date(activity.getPlantingDate()));
            preparedStatement.setDate(5, new Date(activity.getHarvestDate()));
            preparedStatement.setLong(6, activity.getPlantedRai());
            preparedStatement.setLong(7, activity.getPlantedNgan());
            preparedStatement.setDouble(8, activity.getPlantedSquareWah());
            preparedStatement.setLong(9, activity.getHarvestedRai());
            preparedStatement.setLong(10, activity.getHarvestedNgan());
            preparedStatement.setDouble(11, activity.getHarvestedSquareWah());
            preparedStatement.setDouble(12, activity.getEstimatedProductivity());
            preparedStatement.setString(13,activity.getSpecies());
            preparedStatement.setString(14, activity.getCategoryPlants());
            preparedStatement.setString(15,citizenId);
            preparedStatement.setLong(16, areaId);
            preparedStatement.setTimestamp(17, timestamp);
            preparedStatement.setLong(18, userId);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int getActivityId() {
        String sql = "SELECT MAX(ACTIVITY_ID) FROM PROFILE_ACTIVITY";
        int activityId = 0;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeQuery();
            ResultSet generatedKeys = preparedStatement.executeQuery();
            generatedKeys.next();
            activityId = generatedKeys.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return activityId;
    }

    private void addImageActivity(String fileNameImageActivity, String citizenId, int activityId) {
        String insert = "INSERT INTO PROFILE_ACTIVITY_IMAGE " +
                "(ACTIVITY_ID,PATH_IMAGE,PROFILE_ACTIVITY_IMAGE_ID)VALUES(?,?,PROFILE_ACTIVITY_IMAGE_SEQ.NEXTVAL)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setInt(1, activityId);
            String filename = "images" + File.separator + citizenId + File.separator + fileNameImageActivity;
            preparedStatement.setString(2, filename);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addCoordinatesActivity(int activityId, String detail, int userId) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String insert = "INSERT INTO PROFILE_ACTIVITY_COORDINATES" +
                "(ACTIVITY_ID,COORDINATES,ACTIVITY_COORDINATES_ID,CREATE_DATE,CREATE_USER)VALUES" +
                "(?,?,PROFILE_ACT_CDN_SEQ.NEXTVAL,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setLong(1, activityId);
            preparedStatement.setString(2, detail);
            preparedStatement.setTimestamp(3, timestamp);
            preparedStatement.setInt(4, userId);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private int getHoldingNumber(String holding) {
        int result = 2;
        switch (holding) {
            case "เช่า":
                result = 3;
                break;
            case "อื่นๆ":
                result = 4;
        }
        return result;
    }

}
