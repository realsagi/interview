package in.th.tamis.faarm.models.dopa;

import java.util.ArrayList;
import java.util.List;

public class DopaProfile {

    DopaDetail dopaDetail;
    DopaAddressDetail dopaAddressDetail;
    List<DopaMember> dopaMembers;

    public DopaProfile() {
        this.dopaDetail = new DopaDetail();
        this.dopaAddressDetail = new DopaAddressDetail();
        this.dopaMembers = new ArrayList<DopaMember>();
    }

    public DopaDetail getDopaDetail() {
        return dopaDetail;
    }

    public void setDopaDetail(DopaDetail dopaDetail) {
        this.dopaDetail = dopaDetail;
    }

    public DopaAddressDetail getDopaHouse() {
        return dopaAddressDetail;
    }

    public void setDopaHouse(DopaAddressDetail dopaAddressDetail) {
        this.dopaAddressDetail = dopaAddressDetail;
    }

    public List<DopaMember> getDopaMembers() {
        return dopaMembers;
    }

    public void setDopaMembers(List<DopaMember> dopaMembers) {
        this.dopaMembers = dopaMembers;
    }
}
