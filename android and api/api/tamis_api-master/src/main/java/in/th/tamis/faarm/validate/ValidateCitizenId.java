package in.th.tamis.faarm.validate;

public class ValidateCitizenId {

    public boolean validatePatternCitizenId(String citizenId) {
        boolean flag = false;
        if(citizenId != null && citizenId.length() == 13 && isNumeric(citizenId)) {
            flag = true;
        }
        return flag;
    }

    private boolean isNumeric(String citizenId) {
        return citizenId.matches("\\d*?\\d+");
    }

}
