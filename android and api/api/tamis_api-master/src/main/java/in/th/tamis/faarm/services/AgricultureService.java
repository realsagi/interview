package in.th.tamis.faarm.services;

import in.th.tamis.faarm.dao.AgricultureJDBC;
import in.th.tamis.faarm.models.faarm.FarmersOrganization;
import in.th.tamis.faarm.models.faarm.agriculture.Breed;
import in.th.tamis.faarm.models.faarm.agriculture.Detail;
import in.th.tamis.faarm.models.faarm.agriculture.Type;

import java.sql.Connection;

public class AgricultureService implements Agriculture {

    private Connection connection;

    public AgricultureService(Connection connection){
        this.connection = connection;
    }

    @Override
    public FarmersOrganization getFarmersOrganization() {
        return new AgricultureJDBC(connection).selectOrganizationAll();
    }

    @Override
    public Type getType() {
        return new AgricultureJDBC(connection).selectTypeAll();
    }

    @Override
    public Detail getDetailAll() {
        return new AgricultureJDBC(connection).selectDetailAll();
    }

    @Override
    public Detail getDetail(String typeCode) {
        return new AgricultureJDBC(connection).selectByTypeCode(typeCode);
    }

    @Override
    public Breed getBreedAll() {
        return new AgricultureJDBC(connection).selectBreedAll();
    }

    @Override
    public Breed getBreed(String detailCode) {
        return new AgricultureJDBC(connection).selectByDetailCode(detailCode);
    }
}
