package in.th.tamis.faarm.services;

import in.th.tamis.faarm.models.faarm.HouseholdMember;

import java.io.IOException;

public interface HouseholdService {
    HouseholdMember findByCitizenId(String citizenId) throws IOException;
}
