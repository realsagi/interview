package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.models.faarm.Address;
import in.th.tamis.faarm.models.faarm.FarmersOrganization;
import in.th.tamis.faarm.models.faarm.MemberDOAE;
import in.th.tamis.faarm.models.faarm.Person;
import in.th.tamis.faarm.models.faarm.datafromandroid.Farmer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;

public class MemberJDBC implements MemberDAO {

    private Connection connection;

    public MemberJDBC(Connection connection) {
        this.connection = connection;
    }

    @Override
    public MemberDOAE selectMemberByCitizenId(String citizenId) {

        String sqlQuery = "SELECT PREFIX_NAME, " +
                "PROFILE_NAME, PROFILE_SURNAME, " +
                "PROFILE_IDCARD, PROFILE_CENTER_ID " +
                "FROM PROFILE_CENTER " +
                "inner join PREFIX " +
                "on PROFILE_CENTER.PROFILE_PREFIX=PREFIX.PREFIX_ID " +
                "where PROFILE_IDCARD = \'" + citizenId + "\'";
        MemberDOAE memberDOAE = new MemberDOAE();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                memberDOAE.setCitizenId(resultSet.getString("PROFILE_IDCARD"));
                memberDOAE.setPreName(resultSet.getString("PREFIX_NAME"));
                memberDOAE.setFirstName(resultSet.getString("PROFILE_NAME"));
                memberDOAE.setLastName(resultSet.getString("PROFILE_SURNAME"));
                memberDOAE.setMemberNo(resultSet.getString("PROFILE_CENTER_ID"));
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return memberDOAE;
    }

    @Override
    public MemberDOAE selectAddressByIdCardHouseMember(String citizenId) {
        String sqlQueryMember = "SELECT PREFIX_NAME,PROFILE_NAME,PROFILE_SURNAME,PROFILE_IDCARD,PROFILE_IDHOUSE " +
                "FROM PROFILE_MEMBER " +
                "INNER JOIN PROFILE_CENTER " +
                "ON PROFILE_CENTER.PROFILE_ID=PROFILE_MEMBER.PROFILE_CENTER_ID " +
                "INNER JOIN PREFIX " +
                "ON PROFILE_CENTER.PROFILE_PREFIX=PREFIX.PREFIX_ID " +
                "WHERE PROFILE_MEMBER.MEMBER_IDCARD = ?";
        MemberDOAE memberDOAE = new MemberDOAE();
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQueryMember);
            statement.setString(1, citizenId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                memberDOAE.setCitizenId(resultSet.getString("PROFILE_IDCARD"));
                memberDOAE.setHouseHoldNumber(resultSet.getString("PROFILE_IDHOUSE"));
                memberDOAE.setPreName(resultSet.getString("PREFIX_NAME"));
                memberDOAE.setFirstName(resultSet.getString("PROFILE_NAME"));
                memberDOAE.setLastName(resultSet.getString("PROFILE_SURNAME"));
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return memberDOAE;
    }
}
