package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.models.faarm.Person;
import in.th.tamis.faarm.models.faarm.datafromandroid.DetailAgriculturalOperations;
import in.th.tamis.faarm.models.faarm.datafromandroid.DetailRegisterPlantation;
import in.th.tamis.faarm.models.faarm.datafromandroid.Farmer;

public interface NewMemberDAO {
    int newFarmer(Farmer farmer, int userId);
    int findPrefixID(String prefix);
    int addNewHouseholdMember(int profileCenterId, Person householdMember, int statusMember, int userId);
    void addMemberOrg(int profileMemberId, String org);

    int addDetailPlantation(int profileCenterId, DetailRegisterPlantation plantation, int index, String citizenId, int userId);

    void addDetailActivity(int profileAreaId, DetailAgriculturalOperations activity, String citizenId, int profileCenterId, int userId);
}
