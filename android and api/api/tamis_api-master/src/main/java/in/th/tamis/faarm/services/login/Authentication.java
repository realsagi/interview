package in.th.tamis.faarm.services.login;

public interface Authentication {
    String userLogin(String userName, String passWord);
    boolean userLogout(String token);
    String userLoginToken(String token);
}
