package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.models.faarm.area.District;
import in.th.tamis.faarm.models.faarm.area.ProfileArea;
import in.th.tamis.faarm.models.faarm.area.Province;
import in.th.tamis.faarm.models.faarm.area.SubDistrict;

public interface AreaDAO {
    Province selectAllProvince();
    District selectAllDistrict();
    District selectDistrictByProvinceCode(String provinceCode);
    SubDistrict selectAllSubDistrict();
    SubDistrict selectSubDistrictByProvinceCodeAndDistrictCode(String provinceCode, String districtCode);
    String selectPostCode(String locationCode);
    String selectLocationCode(String provinceName, String districtName, String subDistrictName);
    ProfileArea selectProfileByDocNumberAndDocTypeId(String docNumber, int docTypeId, String provinceCode, String districtCode);
    String getProvinceCode(String provinceName);
    String getDistrictCode(String provinceCode, String districtName);
    String getSubDistrictCode(String provinceCode, String districtCode, String subDistrictName);

}
