package in.th.tamis.faarm.manager;

import in.th.tamis.faarm.models.dopa.CitizenProfile;
import in.th.tamis.faarm.models.dopa.Household;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.Query;

public class DOPARestAPI implements DOPAApi{
    final private Retrofit retrofit;
    private DOPAApi service;

    public DOPARestAPI(String baseURL){
        retrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(DOPAApi.class);
    }

    @Override
    public Call<CitizenProfile> loadProfile(@Query("CitizenID") String citizenId) {
        return service.loadProfile(citizenId);
    }

    @Override
    public Call<Household> loadHousehold(@Query("CitizenID") String citizenId) {
        return service.loadHousehold(citizenId);
    }
}
