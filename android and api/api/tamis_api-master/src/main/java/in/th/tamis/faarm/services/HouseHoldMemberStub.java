package in.th.tamis.faarm.services;

import in.th.tamis.faarm.models.faarm.HouseholdMember;
import in.th.tamis.faarm.models.faarm.Person;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HouseHoldMemberStub implements HouseholdService {
    @Override
    public HouseholdMember findByCitizenId(String citizenId) throws IOException {
        HouseholdMember householdMember = new HouseholdMember();
        List<Person> persons = new ArrayList<>();

        Person person = new Person();
        person.setIdentityCard("1169800011959");
        person.setFirstName("กฤษฎาญชล");
        person.setLastName("สะดีวงศ์");
        person.setSex("ชาย");
        persons.add(person);

        Person person1 = new Person();
        person1.setIdentityCard("1309900782777");
        person1.setFirstName("ลดารัตน์");
        person1.setLastName("เพ็งพะเนา");
        person1.setSex("หญิง");
        persons.add(person1);

        Person person2 = new Person();
        person2.setIdentityCard("2520100011499");
        person2.setFirstName("กิตติมศักดิ์");
        person2.setLastName("ศิวารัตน์");
        person2.setSex("ชาย");
        persons.add(person2);

        Person person3 = new Person();
        person3.setIdentityCard("1419900230161");
        person3.setFirstName("ธนพล");
        person3.setLastName("โสดาวิชิต");
        person3.setSex("ชาย");
        persons.add(person3);


        householdMember.setPersons(persons);
        return householdMember;
    }
}
