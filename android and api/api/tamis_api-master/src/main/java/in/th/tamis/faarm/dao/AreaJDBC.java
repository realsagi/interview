package in.th.tamis.faarm.dao;

import com.google.gson.JsonObject;
import in.th.tamis.faarm.models.faarm.area.District;
import in.th.tamis.faarm.models.faarm.area.ProfileArea;
import in.th.tamis.faarm.models.faarm.area.Province;
import in.th.tamis.faarm.models.faarm.area.SubDistrict;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AreaJDBC implements AreaDAO {

    private Connection connection;

    public AreaJDBC(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Province selectAllProvince() {
        String sqlQuery = "SELECT PROVINCE_CODE, PROVINCE_NAME FROM PROVINCE ORDER BY PROVINCE_NAME ASC";
        Province province = new Province();
        List<JsonObject> provinceList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("province_id", resultSet.getString("PROVINCE_CODE"));
                jsonObject.addProperty("province_name", resultSet.getString("PROVINCE_NAME"));
                provinceList.add(jsonObject);
            }
            province.setProvinces(provinceList);
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return province;
    }

    @Override
    public District selectAllDistrict() {
        String sqlQuery = "SELECT PROVINCE_CODE, AMPHUR_CODE, AMPHUR_NAME from AMPHUR";
        District district = new District();
        List<JsonObject> districtList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("province_id", resultSet.getString("PROVINCE_CODE"));
                jsonObject.addProperty("district_id", resultSet.getString("AMPHUR_CODE"));
                jsonObject.addProperty("district_name", resultSet.getString("AMPHUR_NAME"));
                districtList.add(jsonObject);
            }
            district.setDistricts(districtList);
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return district;
    }

    @Override
    public District selectDistrictByProvinceCode(String provinceCode) {
        String sqlQuery = "SELECT AMPHUR_CODE, AMPHUR_NAME FROM AMPHUR where PROVINCE_CODE = " + provinceCode + " order by AMPHUR_NAME ASC";
        District district = new District();
        List<JsonObject> districtList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("district_id", resultSet.getString("AMPHUR_CODE"));
                jsonObject.addProperty("district_name", resultSet.getString("AMPHUR_NAME"));
                districtList.add(jsonObject);
            }
            district.setDistricts(districtList);
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return district;
    }

    @Override
    public SubDistrict selectAllSubDistrict() {
        String sqlQuery = "SELECT PROVINCE_CODE, AMPHUR_CODE, TAMBON_CODE, TAMBON_NAME from TAMBON";
        SubDistrict subDistrict = new SubDistrict();
        List<JsonObject> subDistrictList = new ArrayList<>();try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("province_id", resultSet.getString("PROVINCE_CODE"));
                jsonObject.addProperty("district_id", resultSet.getString("AMPHUR_CODE"));
                jsonObject.addProperty("sub_district_id", resultSet.getString("TAMBON_CODE"));
                jsonObject.addProperty("sub_district_name", resultSet.getString("TAMBON_NAME"));
                subDistrictList.add(jsonObject);
            }
            subDistrict.setSubDistricts(subDistrictList);
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subDistrict;
    }

    @Override
    public SubDistrict selectSubDistrictByProvinceCodeAndDistrictCode(String provinceCode, String districtCode) {
        String sqlQuery = "SELECT TAMBON_CODE, TAMBON_NAME " +
                "FROM TAMBON " +
                "where PROVINCE_CODE = " + provinceCode + " and AMPHUR_CODE = " + districtCode + " order by TAMBON_NAME ASC";
        SubDistrict subDistrict = new SubDistrict();
        List<JsonObject> subDistrictList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("sub_district_id", resultSet.getString("TAMBON_CODE"));
                jsonObject.addProperty("sub_district_name", resultSet.getString("TAMBON_NAME"));
                subDistrictList.add(jsonObject);
            }
            subDistrict.setSubDistricts(subDistrictList);
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subDistrict;
    }

    @Override
    public String selectPostCode(String locationCode) {
        String result = null;
        String sqlQuery = "SELECT POSTCODE FROM POSTCODE WHERE LOCATION_CODE = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, locationCode);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = resultSet.getString("POSTCODE");
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String selectLocationCode(String provinceName, String districtName, String subDistrictName) {
        String result = null;
        String sqlQuery = "SELECT PROVINCE.PROVINCE_CODE, AMPHUR.AMPHUR_CODE, TAMBON.TAMBON_CODE " +
                "FROM TAMBON " +
                "INNER JOIN PROVINCE " +
                "ON PROVINCE.PROVINCE_CODE = TAMBON.PROVINCE_CODE " +
                "INNER JOIN AMPHUR " +
                "ON AMPHUR.AMPHUR_CODE = TAMBON.AMPHUR_CODE " +
                "WHERE PROVINCE.PROVINCE_NAME like ? " +
                "AND AMPHUR.AMPHUR_NAME like ? " +
                "AND TAMBON.TAMBON_NAME like ? " +
                "AND PROVINCE.PROVINCE_CODE = AMPHUR.PROVINCE_CODE";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, "%" + provinceName + "%");
            statement.setString(2, "%" + districtName + "%");
            statement.setString(3, "%" + subDistrictName + "%");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = resultSet.getString("PROVINCE_CODE");
                result += resultSet.getString("AMPHUR_CODE");
                result += resultSet.getString("TAMBON_CODE");
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public ProfileArea selectProfileByDocNumberAndDocTypeId(String docNumber, int docTypeId, String provinceCode, String districtCode) {
        String sqlQuery = "SELECT  PROFILE_CENTER_ID,OWNER_NO,OWNER_NAME,DOC_RAI,DOC_NGAN,DOC_WA,STATUS_DEL,DOC_NO,DOC_TYPE_ID " +
                "FROM PROFILE_AREA " +
                "WHERE PROVINCE_CODE = ? " +
                "AND AMPHUR_CODE = ? " +
                "AND DOC_NO = ? " +
                "AND DOC_TYPE_ID = ?";
        ProfileArea profileArea = new ProfileArea();
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, provinceCode);
            statement.setString(2, districtCode);
            statement.setString(3, docNumber);
            statement.setInt(4, docTypeId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                profileArea.setProfileFarmerId(resultSet.getInt("PROFILE_CENTER_ID"));
                profileArea.setOwnerId(resultSet.getString("OWNER_NO"));
                profileArea.setOwnerName(resultSet.getString("OWNER_NAME"));
                profileArea.setDocRai(resultSet.getInt("DOC_RAI"));
                profileArea.setDocNgan(resultSet.getInt("DOC_NGAN"));
                profileArea.setDocWa(resultSet.getFloat("DOC_WA"));
                profileArea.setStatusDelete(resultSet.getInt("STATUS_DEL"));
                profileArea.setDocNumber(resultSet.getString("DOC_NO"));
                profileArea.setDocTypeId(resultSet.getInt("DOC_TYPE_ID"));
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return profileArea;
    }

    @Override
    public String getProvinceCode(String provinceName) {
        String provinceCode = "";
        String sql = "SELECT PROVINCE_CODE FROM PROVINCE WHERE PROVINCE_NAME = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, provinceName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                provinceCode = resultSet.getString("PROVINCE_CODE");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return provinceCode;
    }

    @Override
    public String getDistrictCode(String provinceCode, String districtName) {
        String districtCode = "";
        String sql = "SELECT AMPHUR_CODE FROM AMPHUR WHERE PROVINCE_CODE = ? AND AMPHUR_NAME = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, provinceCode);
            preparedStatement.setString(2, districtName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                districtCode = resultSet.getString("AMPHUR_CODE");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return districtCode;
    }

    @Override
    public String getSubDistrictCode(String provinceCode, String districtCode, String subDistrictName) {
        String subDistrictCode = "";
        String sql = "SELECT TAMBON_CODE FROM TAMBON WHERE PROVINCE_CODE = ? AND AMPHUR_CODE = ? AND TAMBON_NAME = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, provinceCode);
            preparedStatement.setString(2, districtCode);
            preparedStatement.setString(3, subDistrictName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                subDistrictCode = resultSet.getString("TAMBON_CODE");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subDistrictCode;
    }

}
