package in.th.tamis.faarm.models.faarm.parcel;

import com.google.gson.annotations.SerializedName;

public class ParcelNS4 {

    @SerializedName("province_name")
    String provinceName;

    @SerializedName("district_name")
    String districtName;

    @SerializedName("sub_district_name")
    String subDistrictName;

    @SerializedName("parcel_no")
    int parcelNo;

    @SerializedName("map_x")
    double mapX;

    @SerializedName("map_y")
    double mapY;

    @SerializedName("utm_land_no")
    int utmLandNo;

    @SerializedName("utm_map")
    String utmMap;

    @SerializedName("utm_scale")
    int utmScale;

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getSubDistrictName() {
        return subDistrictName;
    }

    public void setSubDistrictName(String subDistrictName) {
        this.subDistrictName = subDistrictName;
    }

    public int getParcelNo() {
        return parcelNo;
    }

    public void setParcelNo(int parcelNo) {
        this.parcelNo = parcelNo;
    }

    public double getMapX() {
        return mapX;
    }

    public void setMapX(double mapX) {
        this.mapX = mapX;
    }

    public double getMapY() {
        return mapY;
    }

    public void setMapY(double mapY) {
        this.mapY = mapY;
    }

    public int getUtmLandNo() {
        return utmLandNo;
    }

    public void setUtmLandNo(int utmLandNo) {
        this.utmLandNo = utmLandNo;
    }

    public String getUtmMap() {
        return utmMap;
    }

    public void setUtmMap(String utmMap) {
        this.utmMap = utmMap;
    }

    public int getUtmScale() {
        return utmScale;
    }

    public void setUtmScale(int utmScale) {
        this.utmScale = utmScale;
    }
}
