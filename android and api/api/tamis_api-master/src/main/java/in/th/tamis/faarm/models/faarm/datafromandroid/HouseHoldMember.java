package in.th.tamis.faarm.models.faarm.datafromandroid;

import com.google.gson.annotations.SerializedName;
import in.th.tamis.faarm.models.faarm.Person;

import java.util.List;

public class HouseHoldMember {

    @SerializedName("count")
    private int count;

    @SerializedName("persons")
    private List<Person> person;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Person> getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = (List<Person>) person;
    }
}