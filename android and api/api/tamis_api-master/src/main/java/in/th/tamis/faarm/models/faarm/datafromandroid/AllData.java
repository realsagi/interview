package in.th.tamis.faarm.models.faarm.datafromandroid;

import com.google.gson.annotations.SerializedName;

public class AllData {
    @SerializedName("farmer") Farmer farmer;
    @SerializedName("householdMember") HouseHoldMember houseHoldMembers;
    @SerializedName("registerPlantation") RegisterPlantation registerPlantation;

    public int getUserId() {
        return userId;
    }

    @SerializedName("userId") int userId;


    public AllData(Farmer farmer, HouseHoldMember houseHoldMembers, RegisterPlantation registerPlantation) {
        this.farmer = farmer;
        this.houseHoldMembers = houseHoldMembers;
        this.registerPlantation = registerPlantation;
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public HouseHoldMember getHouseHoldMembers() {
        return houseHoldMembers;
    }

    public RegisterPlantation getRegisterPlantation() {
        return registerPlantation;
    }
}
