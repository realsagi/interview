package in.th.tamis.faarm.models.faarm;

import com.google.gson.annotations.SerializedName;

public class Person {

    @SerializedName("citizen_id")
    private String identityCard;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("sex")
    private String sex;

    public FarmersOrganization getFarmersOrganization() {
        return farmersOrganization;
    }

    public void setFarmersOrganization(FarmersOrganization farmersOrganization) {
        this.farmersOrganization = farmersOrganization;
    }

    public boolean isStatusHelpWork() {
        return statusHelpWork;
    }

    public void setStatusHelpWork(boolean statusHelpWork) {
        this.statusHelpWork = statusHelpWork;
    }

    @SerializedName("farmersOrganization")
    private FarmersOrganization farmersOrganization;

    @SerializedName("statusHelpWork")
    private boolean statusHelpWork;
    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
