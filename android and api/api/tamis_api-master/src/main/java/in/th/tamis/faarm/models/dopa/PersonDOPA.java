package in.th.tamis.faarm.models.dopa;

import com.google.gson.annotations.SerializedName;

public class PersonDOPA {

    @SerializedName("CitizenID")
    private String identityCard;

    @SerializedName("NameTH_FirstName")
    private String firstName;

    @SerializedName("NameTH_SurName")
    private String lastName;

    @SerializedName("Sex")
    private String sex;

    @SerializedName("BirthDate")
    private String birthDate;

    @SerializedName("AddressID")
    private String addressId;


    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }
}
