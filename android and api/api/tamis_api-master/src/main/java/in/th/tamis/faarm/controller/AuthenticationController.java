package in.th.tamis.faarm.controller;


import com.google.gson.Gson;
import in.th.tamis.faarm.services.login.Authentication;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AuthenticationController extends HttpServlet {

    private Authentication authentication;

    public AuthenticationController(Authentication authentication){
        this.authentication = authentication;
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        if("/login".equals(request.getRequestURI())){
            String userName = request.getParameter("user_name");
            String passWord = request.getParameter("password");

            String mfUser = authentication.userLogin(userName, passWord);

            if(mfUser != null){
                out.print(new Gson().toJson(mfUser));
            }else {
                out.print(new Gson().toJson(false));
            }
        }
        if("/logout".equals(request.getRequestURI())){
            String token = request.getParameter("token");
            authentication.userLogout(token);
            out.print(new Gson().toJson(true));
        }
        if("/login/token".equals(request.getRequestURI())){
            String token = request.getParameter("token");
            out.print(new Gson().toJson(authentication.userLoginToken(token)));
        }
        out.close();
    }
}
