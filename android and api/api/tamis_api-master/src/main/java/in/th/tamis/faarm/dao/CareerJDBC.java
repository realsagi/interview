package in.th.tamis.faarm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CareerJDBC implements CareerDAO {
    Connection connection;
    public CareerJDBC(Connection connection) {
        this.connection = connection;
    }

    @Override
    public int getCareerId(String careerName) {
        int careerId = 0;
        String sql = "SELECT CAREER_ID FROM CAREER WHERE CAREER_NAME = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,careerName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                careerId = resultSet.getInt("CAREER_ID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return careerId;
    }
}
