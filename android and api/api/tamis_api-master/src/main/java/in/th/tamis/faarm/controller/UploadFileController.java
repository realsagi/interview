package in.th.tamis.faarm.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;

@MultipartConfig(fileSizeThreshold=1024*1024*2, maxFileSize=1024*1024*10, maxRequestSize=1024*1024*50)
public class UploadFileController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String fileName = request.getParameter("path_name").substring(1).substring(0, request.getParameter("path_name").substring(1).length()-1);
        final String citizenId = request.getParameter("citizen_id").substring(1).substring(0, request.getParameter("citizen_id").substring(1).length()-1);
        final Part filePart = request.getPart("photo");

        OutputStream out = null;
        InputStream inputStream = null;

        final PrintWriter writer = response.getWriter();

        try {
            File folder = new File("." + File.separator + "images" + File.separator + citizenId + File.separator );
            if (!folder.exists()) {
                folder.mkdirs();
            }
            out = new FileOutputStream(new File("." + File.separator + "images" + File.separator + citizenId + File.separator + fileName));
            inputStream = filePart.getInputStream();
            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            response.setStatus(HttpServletResponse.SC_OK);
            PrintWriter printWriter = response.getWriter();
            printWriter.println("{\"message\":\"upload complete\"}");
            printWriter.close();
        } catch (FileNotFoundException fne) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            PrintWriter printWriter = response.getWriter();
            printWriter.println("{\"message\":\"upload not complete\"}");
            printWriter.close();
        } finally {
            if (out != null) {
                out.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (writer != null) {
                writer.close();
            }
        }
    }
}


