package in.th.tamis.faarm.services.dopa;

import in.th.tamis.faarm.models.dopa.DopaMember;
import in.th.tamis.faarm.models.dopa.DopaProfile;
import in.th.tamis.faarm.models.faarm.Address;
import in.th.tamis.faarm.models.faarm.HouseholdMember;
import in.th.tamis.faarm.models.faarm.Person;
import in.th.tamis.faarm.models.faarm.PersonalProfile;

public class DOPAService {

    String headerAuthorization = "Basic dGFtaXN4eGRkOnZ0d2lkeW90YW1pcw==";
    ConnectDOPAApi connectDOPAApi;
    String url;

    public DOPAService(ConnectDOPAApi connectDOPAApi, String url) {
        this.connectDOPAApi = connectDOPAApi;
        this.url = url;
    }

    public DOPAService(ConnectDOPAApi connectDOPAApi) {
        this.connectDOPAApi =  connectDOPAApi;
    }

    public PersonalProfile getPersonalProfile(String citizenId) {
        DopaProfile dopaProfile = getDopaProfile(citizenId);
        return getPersonalProfile(dopaProfile);
    }

    public HouseholdMember getHouseholdMember(String citizenId){
        DopaProfile dopaProfile = getDopaProfile(citizenId);
        HouseholdMember householdMember = new HouseholdMember();
        for (DopaMember member :dopaProfile.getDopaMembers()) {
            Person person = new Person();
            person.setFirstName(member.getFirstName());
            person.setLastName(member.getLastName());
            person.setIdentityCard(member.getCitizenId());
            person.setSex(member.getSex());
            householdMember.addPerson(person);
        }
        return householdMember;
    }

    private DopaProfile getDopaProfile(String citizenId) {
        String input = getInput(citizenId);
        ParserFromXML parserFromXML = new ParserFromXML();
        DopaProfile dopaProfile = null;
        try {
            dopaProfile = parserFromXML.setDopaProfile(connectDOPAApi.getDataResponse(input, url, headerAuthorization));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dopaProfile;
    }

    private String getInput(String citizenId) {
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:get=\"http://getpidt.ssi.nectec.th.com\">" +
                "<soapenv:Header/>" +
                "<soapenv:Body>" +
                "<get:getPersonalAndMemberAndHouseDetailDes>" +
                "<get:Pid>" + citizenId + "</get:Pid>" +
                "<get:request_user>ec13050005</get:request_user>" +
                "</get:getPersonalAndMemberAndHouseDetailDes>" +
                "</soapenv:Body>" +
                "</soapenv:Envelope>";
    }

    private PersonalProfile getPersonalProfile(DopaProfile dopaProfile) {
        PersonalProfile personalProfile = new PersonalProfile();
        personalProfile.setPreName(dopaProfile.getDopaDetail().getPreName());
        personalProfile.setFirstName(dopaProfile.getDopaDetail().getFirstName());
        personalProfile.setLastName(dopaProfile.getDopaDetail().getLastName());
        personalProfile.setBirthDate(dopaProfile.getDopaDetail().getBirthDate());
        personalProfile.setCitizenId(dopaProfile.getDopaDetail().getCitizenId());

        personalProfile.setAddress(new Address());
        personalProfile.getAddress().setHouseNo(dopaProfile.getDopaHouse().getAddressNo());
        personalProfile.getAddress().setAddressID(dopaProfile.getDopaHouse().getAddressId());
        personalProfile.getAddress().setAlley(dopaProfile.getDopaHouse().getAlley());
        personalProfile.getAddress().setDistrict(dopaProfile.getDopaHouse().getAmpher());
        personalProfile.getAddress().setMoo(dopaProfile.getDopaHouse().getMoo());
        personalProfile.getAddress().setProvince(dopaProfile.getDopaHouse().getProvince());
        personalProfile.getAddress().setRoad(dopaProfile.getDopaHouse().getRoad());
        personalProfile.getAddress().setSoi(dopaProfile.getDopaHouse().getSoi());
        personalProfile.getAddress().setSubDistrict(dopaProfile.getDopaHouse().getTumbon());
        return personalProfile;
    }
}