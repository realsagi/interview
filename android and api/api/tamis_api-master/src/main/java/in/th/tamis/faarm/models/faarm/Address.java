package in.th.tamis.faarm.models.faarm;

import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("address_id")
    private String addressID;

    @SerializedName("house_no")
    private String houseNo;

    @SerializedName("moo")
    private String moo;

    @SerializedName("alley")
    private String alley;

    @SerializedName("soi")
    private String soi;

    @SerializedName("road")
    private String road;

    @SerializedName("tumbol")
    private String subDistrict;

    @SerializedName("amphur")
    private String district;

    @SerializedName("province")
    private String province;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    @SerializedName("zipCode")
    private String zipCode;

    public String getAddressID() {
        return addressID;
    }

    public void setAddressID(String addressID) {
        this.addressID = addressID;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    public String getAlley() {
        return alley;
    }

    public void setAlley(String alley) {
        this.alley = alley;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
