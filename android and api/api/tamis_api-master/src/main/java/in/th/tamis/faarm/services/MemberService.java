package in.th.tamis.faarm.services;

import in.th.tamis.faarm.dao.MemberDAO;
import in.th.tamis.faarm.models.faarm.FarmersOrganization;
import in.th.tamis.faarm.models.faarm.MemberDOAE;
import in.th.tamis.faarm.models.faarm.Person;
import in.th.tamis.faarm.models.faarm.datafromandroid.AllData;
import in.th.tamis.faarm.models.faarm.datafromandroid.Farmer;

import java.util.List;

public class MemberService implements Member{

    private MemberDAO memberDAO;


    public MemberService(MemberDAO memberDAO) {
        this.memberDAO = memberDAO;
    }

    @Override
    public MemberDOAE getMemberDAO(String citizenId) {
        return memberDAO.selectMemberByCitizenId(citizenId);
    }

    @Override
    public MemberDOAE getHouseHoldIdOfHouseMember(String citizenId) {
        return memberDAO.selectAddressByIdCardHouseMember(citizenId);
    }



}
