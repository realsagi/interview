package in.th.tamis.faarm.services;

import in.th.tamis.faarm.models.faarm.parcel.ParcelNS4;
import in.th.tamis.faarm.models.faarm.parcel.ParcelType;

import java.io.IOException;

public interface Parcel {
    ParcelNS4 findParcelType(String provinceCode, String districtCode, String type, String parcelNo) throws IOException;
    ParcelType getParcelType();
}
