package in.th.tamis.faarm.controller;

import com.google.gson.Gson;
import in.th.tamis.faarm.models.faarm.PersonalProfile;
import in.th.tamis.faarm.services.PersonalProfileService;
import in.th.tamis.faarm.services.dopa.DOPAService;
import in.th.tamis.faarm.validate.ValidateCitizenId;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class PersonalProfileController extends HttpServlet {

    DOPAService service;

    public PersonalProfileController(DOPAService dopaService) {
        service = dopaService;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String citizenId = request.getParameter("citizen_id");
        ValidateCitizenId validateCitizenId = new ValidateCitizenId();
        if (validateCitizenId.validatePatternCitizenId(citizenId)) {
            try {
                String stringJson = new Gson().toJson(service.getPersonalProfile(citizenId));
                response.setStatus(HttpServletResponse.SC_OK);
                PrintWriter out = response.getWriter();
                if (service.getPersonalProfile(citizenId).getAddress().getAddressID() != null) {
                    out.println(stringJson);
                } else {
                    out.println("{}");
                }
                out.close();
            } catch (Exception e) {
                response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
