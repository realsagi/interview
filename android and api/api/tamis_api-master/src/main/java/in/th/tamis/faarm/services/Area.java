package in.th.tamis.faarm.services;

import in.th.tamis.faarm.models.faarm.area.District;
import in.th.tamis.faarm.models.faarm.area.ProfileArea;
import in.th.tamis.faarm.models.faarm.area.Province;
import in.th.tamis.faarm.models.faarm.area.SubDistrict;

public interface Area {
    Province getProvince();
    District getAllDistrict();
    District getDistrict(String provinceCode);
    SubDistrict getAllSubDistrict();
    SubDistrict getSubDistrict(String provinceCode, String districtCode);
    String getPostCode(String locationCode);
    String getLocationCode(String provinceName, String districtName, String subDistrictName);
    ProfileArea getProfileArea(String docNumber, int docTypeId, String provinceCode, String districtCode);
}
