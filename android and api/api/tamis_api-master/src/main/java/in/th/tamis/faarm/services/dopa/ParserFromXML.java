package in.th.tamis.faarm.services.dopa;

import in.th.tamis.faarm.models.dopa.DopaMember;
import in.th.tamis.faarm.models.dopa.DopaProfile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class ParserFromXML {

    private NodeList nodeList;
    private Element element;
    private DopaProfile dopaProfile = new DopaProfile();

    public DopaProfile setDopaProfile(String stringXML) throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(new ByteArrayInputStream(stringXML.getBytes("UTF-8")));

        if (apiStatus(document)) {
            getDetail(document, dopaProfile);
            getAddressDetail(document, dopaProfile);
            getHouseholdMember(document, dopaProfile);
        }

        return dopaProfile;
    }

    private boolean apiStatus(Document document) {
        nodeList = document.getElementsByTagName("ax25:popdetail");
        Element elementDetail = (Element) nodeList.item(0);
        if (elementDetail == null) {
            return false;
        } else {
            NodeList nodeListStatus = elementDetail.getElementsByTagName("ax25:status");
            Element elementStatus = (Element) nodeListStatus.item(0);
            return "0".equals(elementStatus.getElementsByTagName("ax25:statuscode").item(0).getTextContent());
        }
    }

    private void getDetail(Document document, DopaProfile dopaProfile) {
        nodeList = document.getElementsByTagName("ax25:popdetail");
        element = (Element) nodeList.item(0);
        dopaProfile.getDopaDetail().setCitizenId(element.getElementsByTagName("ax25:PID").item(0).getTextContent());
        dopaProfile.getDopaDetail().setFirstName(element.getElementsByTagName("ax25:firstName").item(0).getTextContent());
        dopaProfile.getDopaDetail().setLastName(element.getElementsByTagName("ax25:lastName").item(0).getTextContent());
        dopaProfile.getDopaDetail().setSex(element.getElementsByTagName("ax25:gender").item(0).getTextContent());
        dopaProfile.getDopaDetail().setPreName(element.getElementsByTagName("ax25:prefix").item(0).getTextContent());
        dopaProfile.getDopaDetail().setBirthDay(element.getElementsByTagName("ax25:birthday").item(0).getTextContent());
    }

    private void getAddressDetail(Document document, DopaProfile dopaProfile) {
        nodeList = document.getElementsByTagName("ax25:house");
        element = (Element) nodeList.item(0);
        dopaProfile.getDopaHouse().setAlley(element.getElementsByTagName("ax25:ADDRESS_ALLEY").item(0).getTextContent());
        dopaProfile.getDopaHouse().setAmpher(element.getElementsByTagName("ax25:ADDRESS_AMPHUR").item(0).getTextContent());
        dopaProfile.getDopaHouse().setAddressId(element.getElementsByTagName("ax25:ADDRESS_ID").item(0).getTextContent());
        dopaProfile.getDopaHouse().setMoo(element.getElementsByTagName("ax25:ADDRESS_MOO").item(0).getTextContent());
        dopaProfile.getDopaHouse().setAddressNo(element.getElementsByTagName("ax25:ADDRESS_NO").item(0).getTextContent());
        dopaProfile.getDopaHouse().setProvince(element.getElementsByTagName("ax25:ADDRESS_PROVINCE").item(0).getTextContent());
        dopaProfile.getDopaHouse().setRoad(element.getElementsByTagName("ax25:ADDRESS_ROAD").item(0).getTextContent());
        dopaProfile.getDopaHouse().setSoi(element.getElementsByTagName("ax25:ADDRESS_SOI").item(0).getTextContent());
        dopaProfile.getDopaHouse().setTambon(element.getElementsByTagName("ax25:ADDRESS_TAMBON").item(0).getTextContent());
    }

    public void getHouseholdMember(Document document, DopaProfile dopaProfile) {
        nodeList = document.getElementsByTagName("ax25:members");
        Element elements = (Element) nodeList.item(0);
        NodeList subNodeList = elements.getElementsByTagName("ax25:member");

        for (int j = 0; j < subNodeList.getLength(); j++) {
            Element subElements = (Element) nodeList.item(0);
            DopaMember dopaMember = new DopaMember();
            dopaMember.setFirstName(subElements.getElementsByTagName("ax25:firstName").item(j).getTextContent());
            dopaMember.setLastName(subElements.getElementsByTagName("ax25:lastName").item(j).getTextContent());
            dopaMember.setCitizenId(subElements.getElementsByTagName("ax25:pid").item(j).getTextContent());
            dopaMember.setSex(subElements.getElementsByTagName("ax25:gender").item(j).getTextContent());
            dopaProfile.getDopaMembers().add(dopaMember);
        }
    }
}
