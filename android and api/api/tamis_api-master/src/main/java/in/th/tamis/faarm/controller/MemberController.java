package in.th.tamis.faarm.controller;

import com.google.gson.Gson;
import in.th.tamis.faarm.services.Member;
import in.th.tamis.faarm.validate.ValidateCitizenId;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MemberController extends HttpServlet {

    Member memberService;

    public MemberController(Member memberService) {
        this.memberService = memberService;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if("/member".equals(request.getRequestURI())){
            String citizenId = request.getParameter("citizen_id");
            ValidateCitizenId validateCitizenId = new ValidateCitizenId();
            if(validateCitizenId.validatePatternCitizenId(citizenId)){
                response.setStatus(HttpServletResponse.SC_OK);
                PrintWriter out = response.getWriter();
                out.println(new Gson().toJson(memberService.getMemberDAO(citizenId)));
                out.close();
            }
        }else if("/member/household_id".equals(request.getRequestURI())){
            String citizenId = request.getParameter("citizen_id");
            ValidateCitizenId validateCitizenId = new ValidateCitizenId();
            if(validateCitizenId.validatePatternCitizenId(citizenId)){
                response.setStatus(HttpServletResponse.SC_OK);
                PrintWriter out = response.getWriter();
                out.println(new Gson().toJson(memberService.getHouseHoldIdOfHouseMember(citizenId)));
                out.close();
            }
        }
        else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

}
