package in.th.tamis.faarm.models.login;

import com.google.gson.annotations.SerializedName;

public class MFUser {
    @SerializedName("user_id")          private int userId;
    @SerializedName("user_name")        private String userName;
    @SerializedName("password")         private String password;
    @SerializedName("email")            private String email;
    @SerializedName("user_level")       private String level;
    @SerializedName("status_active")    private int statusActive;
    @SerializedName("status_reset")     private int statusReset;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getStatusActive() {
        return statusActive;
    }

    public void setStatusActive(int statusActive) {
        this.statusActive = statusActive;
    }

    public int getStatusReset() {
        return statusReset;
    }

    public void setStatusReset(int statusReset) {
        this.statusReset = statusReset;
    }
}
