package in.th.tamis.faarm.controller;

import com.google.gson.Gson;
import in.th.tamis.faarm.models.faarm.parcel.ParcelNS4;
import in.th.tamis.faarm.services.dol.DOLService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ParcelController extends HttpServlet {

    DOLService service;

    public ParcelController(DOLService dolService) {
        service = dolService;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String provinceCode = request.getParameter("province_code");
        String districtCode = request.getParameter("district_code");
        String type = request.getParameter("type");
        String parcelNo = request.getParameter("parcel_no");

        if(!verifyRequest(provinceCode, districtCode, parcelNo) && type.equals("28")) {
            connectDOLApiService(response, provinceCode, districtCode, parcelNo);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private boolean verifyRequest(String provinceCode, String districtCode, String parcelNo) {
        return provinceCode.equals("") || districtCode.equals("") || parcelNo.equals("") ;
    }

    private void connectDOLApiService(HttpServletResponse response, String provinceCode, String districtCode, String parcelNo) throws IOException {
        try {
            ParcelNS4 parcelNS4 = service.getParcelNS4(provinceCode, districtCode, parcelNo);
            requestParcel(response, parcelNS4);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
        }
    }

    private void requestParcel(HttpServletResponse response, ParcelNS4 parcelNS4) throws IOException {
        String stringJson = new Gson().toJson(parcelNS4);
        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter printWriter = response.getWriter();
        if (parcelNS4.getParcelNo() != 0) {
            printWriter.println(stringJson);
        } else {
            printWriter.println("{}");
        }
        printWriter.close();
    }
}
