package in.th.tamis.faarm.models.faarm.agriculture;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Detail {
    @SerializedName("detail")
    private List<JsonObject> detail;

    public List<JsonObject> getDetail() {
        return detail;
    }

    public void setDetail(List<JsonObject> detail) {
        this.detail = detail;
    }
}
