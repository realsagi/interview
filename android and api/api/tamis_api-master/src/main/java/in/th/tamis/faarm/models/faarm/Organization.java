package in.th.tamis.faarm.models.faarm;

import com.google.gson.annotations.SerializedName;

public class Organization {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String organizationName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }
}
