package in.th.tamis.faarm.models.faarm.datafromandroid;

import com.google.gson.annotations.SerializedName;

public class ParcelDoc {
    @SerializedName("province_name")                    private String provinceName;
    @SerializedName("district_name")                    private String districtName;
    @SerializedName("sub_district_name")                private String subDistrictName;
    @SerializedName("parcel_no")                        private String parcelNumber;
    @SerializedName("map_x")                            private Double mapX;
    @SerializedName("map_y")                            private Double mapY;
    @SerializedName("utm_land_no")                      private int utmLandNo;
    @SerializedName("utm_map")                          private String utmMap;
    @SerializedName("utm_scale")                        private int utmScale;

    @SerializedName("verify_dol")                       private Boolean verifyDOL;
    @SerializedName("parcel_typ_id")                    private int parcelTypeID;

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getSubDistrictName() {
        return subDistrictName;
    }

    public void setSubDistrictName(String subDistrictName) {
        this.subDistrictName = subDistrictName;
    }

    public String getParcelNumber() {
        return parcelNumber;
    }

    public void setParcelNumber(String parcelNumber) {
        this.parcelNumber = parcelNumber;
    }

    public Double getMapX() {
        return mapX;
    }

    public void setMapX(Double mapX) {
        this.mapX = mapX;
    }

    public Double getMapY() {
        return mapY;
    }

    public void setMapY(Double mapY) {
        this.mapY = mapY;
    }

    public int getUtmLandNo() {
        return utmLandNo;
    }

    public void setUtmLandNo(int utmLandNo) {
        this.utmLandNo = utmLandNo;
    }

    public String getUtmMap() {
        return utmMap;
    }

    public void setUtmMap(String utmMap) {
        this.utmMap = utmMap;
    }

    public int getUtmScale() {
        return utmScale;
    }

    public void setUtmScale(int utmScale) {
        this.utmScale = utmScale;
    }

    public Boolean getVerifyDOL() {
        return verifyDOL;
    }

    public void setVerifyDOL(Boolean verifyDOL) {
        this.verifyDOL = verifyDOL;
    }

    public int getParcelTypeID() {
        return parcelTypeID;
    }

    public void setParcelTypeID(int parcelTypeID) {
        this.parcelTypeID = parcelTypeID;
    }
}
