package in.th.tamis.faarm.services.dol;

import com.google.gson.JsonElement;
import in.th.tamis.faarm.dao.ParcelJDBC;
import in.th.tamis.faarm.models.faarm.parcel.ParcelNS4;
import in.th.tamis.faarm.models.faarm.parcel.ParcelType;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class DOLService {

    ConnectDOLApi connectDOLApi;
    String headerAuthorization = "";
    String url = "";
    String headerSOAPAction = "";

    public DOLService(ConnectDOLApi connectDOLApi) {
        this.connectDOLApi =  connectDOLApi;
    }

    public DOLService(ConnectDOLApi connectDOLApi, String url, String headerSOAPAction) {
        this.connectDOLApi = connectDOLApi;
        this.url = url;
        this.headerSOAPAction = headerSOAPAction;
    }

    public ParcelNS4 getParcelNS4(String provinceCode, String districtCode, String ns4no) throws Exception {
        String input = getInput(provinceCode, districtCode, ns4no);

        ParserFromXML parserFromXML = new ParserFromXML();
        ParcelNS4 parcelNS4 = null;
        parcelNS4 = parserFromXML.getParcel(connectDOLApi.getDataResponse(input, url, headerAuthorization, headerSOAPAction));

        return parcelNS4;
    }

    private String getInput(String provinceCode, String districtCode, String ns4no) {
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dol=\"http://110.164.49.55/DolParcel2Ega/\">" +
                "<soapenv:Header/>" +
                "<soapenv:Body>" +
                "<dol:getParcel>" +
                "<dol:provincecode>" + provinceCode + "</dol:provincecode>" +
                "<dol:amphoecode>" + districtCode + "</dol:amphoecode>" +
                "<dol:ns4no>" + ns4no + "</dol:ns4no>" +
                "</dol:getParcel>" +
                "</soapenv:Body>" +
                "</soapenv:Envelope>";
    }

    public ParcelType getParcelType() {
        return new ParcelJDBC().selectAllParcelType();
    }
}
