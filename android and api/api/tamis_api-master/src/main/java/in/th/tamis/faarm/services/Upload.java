package in.th.tamis.faarm.services;

import in.th.tamis.faarm.models.faarm.datafromandroid.ImagesFarmer;

import java.io.IOException;

public interface Upload {
    byte[] convertImageToByte(String ImageName)throws IOException;
    boolean writeImageFromByte(byte[] data,String path)throws IOException;

    String genPath(ImagesFarmer imagesFarmer);
}
