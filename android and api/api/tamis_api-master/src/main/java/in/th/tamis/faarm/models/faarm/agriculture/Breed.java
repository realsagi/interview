package in.th.tamis.faarm.models.faarm.agriculture;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Breed {
    @SerializedName("breed")
    private List<JsonObject> breed;

    public List<JsonObject> getBreed() {
        return breed;
    }

    public void setBreed(List<JsonObject> breed) {
        this.breed = breed;
    }
}
