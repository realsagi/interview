package in.th.tamis.faarm.services.dol;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class ConnectDOLApi {

    public String getDataResponse(String input, String url, String headerAuthorization, String headerSOAPAction) throws Exception {

        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                builder.build());
        CloseableHttpClient client = HttpClients.custom().setSSLSocketFactory(
                sslsf).build();

        HttpPost post = getHttpPost(url, headerSOAPAction);

        HttpEntity entity = new ByteArrayEntity(input.getBytes());
        post.setEntity(entity);

        HttpResponse response = client.execute(post);

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        return result.toString();
    }

    private HttpPost getHttpPost(String url, String headerSOAPAction) {
        HttpPost post = new HttpPost(url);

        post.setHeader("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)");
        post.setHeader("Content-Type", "text/xml;charset=UTF-8");
        post.setHeader("SOAPAction", headerSOAPAction);
        post.setHeader("Accept-Encoding", "gzip,deflate");
        post.setHeader("Connection", "Keep-Alive");
        return post;
    }
}
