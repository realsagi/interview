package in.th.tamis.faarm.services;

import in.th.tamis.faarm.dao.AreaJDBC;
import in.th.tamis.faarm.models.faarm.area.District;
import in.th.tamis.faarm.models.faarm.area.ProfileArea;
import in.th.tamis.faarm.models.faarm.area.Province;
import in.th.tamis.faarm.models.faarm.area.SubDistrict;

import java.sql.Connection;

public class AreaService implements Area{
    private AreaJDBC areaJDBC;
    public AreaService(Connection connection){
        areaJDBC = new AreaJDBC(connection);
    }

    @Override
    public Province getProvince() {
        return areaJDBC.selectAllProvince();
    }

    @Override
    public District getAllDistrict() {
        return areaJDBC.selectAllDistrict();
    }

    @Override
    public District getDistrict(String provinceCode) {
        return areaJDBC.selectDistrictByProvinceCode(provinceCode);
    }

    @Override
    public SubDistrict getAllSubDistrict() {
        return areaJDBC.selectAllSubDistrict();
    }

    @Override
    public SubDistrict getSubDistrict(String provicneCode, String districtCode) {
        return areaJDBC.selectSubDistrictByProvinceCodeAndDistrictCode(provicneCode, districtCode);
    }

    @Override
    public String getPostCode(String locationCode) {
        return areaJDBC.selectPostCode(locationCode);
    }

    @Override
    public String getLocationCode(String provinceName, String districtName, String subDistrictName) {
        return areaJDBC.selectLocationCode(provinceName, districtName, subDistrictName);
    }

    @Override
    public ProfileArea getProfileArea(String docNumber, int docTypeId, String provinceCode, String districtCode) {
        return areaJDBC.selectProfileByDocNumberAndDocTypeId(docNumber, docTypeId, provinceCode, districtCode);
    }
}
