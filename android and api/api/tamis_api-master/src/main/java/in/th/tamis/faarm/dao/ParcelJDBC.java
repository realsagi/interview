package in.th.tamis.faarm.dao;

import com.google.gson.JsonObject;
import in.th.tamis.faarm.manager.MysqlConnector;
import in.th.tamis.faarm.models.faarm.parcel.ParcelType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ParcelJDBC implements ParcelDAO {
    @Override
    public ParcelType selectAllParcelType() {
        String sqlQuery = "SELECT DOC_TYPE_ID, DOC_TYPE_NAME FROM DOC_TYPE order by DOC_TYPE_NAME ASC";
        ParcelType parcelType = new ParcelType();
        List<JsonObject> parcelTypeList = new ArrayList<>();
        try {
            Statement statement = MysqlConnector.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("parcel_no", resultSet.getString("DOC_TYPE_ID"));
                jsonObject.addProperty("parcel_type_name", resultSet.getString("DOC_TYPE_NAME"));
                parcelTypeList.add(jsonObject);
            }
            parcelType.setParcelType(parcelTypeList);
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return parcelType;
    }
}
