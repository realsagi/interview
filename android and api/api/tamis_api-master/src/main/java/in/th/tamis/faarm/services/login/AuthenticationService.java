package in.th.tamis.faarm.services.login;

import in.th.tamis.faarm.dao.AuthenticationJDBC;
import in.th.tamis.faarm.models.login.MFUser;
import in.th.tamis.faarm.models.login.UserLogin;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.Calendar;

public class AuthenticationService implements Authentication {

    private Connection connection;

    public AuthenticationService(Connection connection){
        this.connection = connection;
    }

    @Override
    public String userLogin(String userName, String passWord) {
        AuthenticationJDBC authenticationJDBC = new AuthenticationJDBC(connection);
        MFUser mfUser = authenticationJDBC.selectUserLogin(userName, passWord);
        String token = authenticationJDBC.selectTokenByUserNameOnUserLogin(userName);
        if(token == null){
            if(mfUser.getUserId() != 0){
                double random = Math.random() * 100000000000000.00;
                BigDecimal bigJustParsed = new BigDecimal(random);

                try {
                    MessageDigest md = MessageDigest.getInstance("MD5");
                    md.update(bigJustParsed.toString().getBytes());
                    byte byteData[] = md.digest();
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < byteData.length; i++) {
                        sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
                    }
                    String newToken = sb.toString();
                    new AuthenticationJDBC(connection).saveLog(mfUser.getUserId());
                    saveTokenLoginToDataBase(mfUser, newToken);
                    return newToken;

                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public boolean userLogout(String token) {
        return new AuthenticationJDBC(connection).deleteUserLoginByToken(token);
    }

    @Override
    public String userLoginToken(String token) {
        return new AuthenticationJDBC(connection).selectUserLoginByToken(token);
    }

    private boolean saveTokenLoginToDataBase(MFUser mfUser, String token) {
        UserLogin userLogin = new UserLogin();
        userLogin.setUserId(mfUser.getUserId());
        userLogin.setUserName(mfUser.getUserName());
        userLogin.setTokenKey(token);
        userLogin.setDateTimeLogin(Calendar.getInstance());
        return new AuthenticationJDBC(connection).saveUserLogin(userLogin);
    }


}
