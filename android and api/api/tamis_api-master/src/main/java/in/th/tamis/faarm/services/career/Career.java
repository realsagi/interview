package in.th.tamis.faarm.services.career;

import in.th.tamis.faarm.models.faarm.career.ProfileCareer;

public interface Career {
    ProfileCareer getCareer();
}
