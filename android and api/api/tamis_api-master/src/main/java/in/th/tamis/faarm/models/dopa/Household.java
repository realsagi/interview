package in.th.tamis.faarm.models.dopa;

import com.google.gson.annotations.SerializedName;
import in.th.tamis.faarm.models.faarm.Person;

import java.util.List;

public class Household {

    @SerializedName("Count")
    String count;

    @SerializedName("Person")
    List<PersonDOPA> persons;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<PersonDOPA> getPersons() {
        return persons;
    }

    public void setPersons(List<PersonDOPA> persons) {
        this.persons = persons;
    }
}
