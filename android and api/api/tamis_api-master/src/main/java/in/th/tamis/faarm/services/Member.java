package in.th.tamis.faarm.services;
import in.th.tamis.faarm.models.faarm.MemberDOAE;
import in.th.tamis.faarm.models.faarm.Person;
import in.th.tamis.faarm.models.faarm.datafromandroid.AllData;
import in.th.tamis.faarm.models.faarm.datafromandroid.Farmer;
import in.th.tamis.faarm.models.faarm.datafromandroid.HouseHoldMember;

import java.util.List;

public interface Member {
    MemberDOAE getMemberDAO(String citizenId);
    MemberDOAE getHouseHoldIdOfHouseMember(String citizenId);
}
