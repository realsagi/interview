package in.th.tamis.faarm.services;

import in.th.tamis.faarm.models.faarm.PersonalProfile;

import java.io.IOException;

public interface PersonalProfileService {

    PersonalProfile findByCitizenId(String citizenId) throws IOException;
}
