package in.th.tamis.faarm.services.dol;

import in.th.tamis.faarm.models.faarm.parcel.ParcelNS4;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class ParserFromXML {

    private NodeList nodeList;
    private ParcelNS4 parcelNS4 = new ParcelNS4();

    public ParcelNS4 getParcel(String stringXML) throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(new ByteArrayInputStream(stringXML.getBytes("UTF-8")));

        if(apiStatus(document)) {
            setDetailParcel(document);
        }

        return parcelNS4;
    }

    private boolean apiStatus(Document document) {
        nodeList = document.getElementsByTagName("Parcel");
        Element element = (Element) nodeList.item(0);
        if(element == null)
            return false;
        return true;
    }

    private void setDetailParcel(Document document) {
        nodeList = document.getElementsByTagName("Parcel");
        Element element = (Element) nodeList.item(0);

        parcelNS4.setDistrictName(element.getElementsByTagName("AMPHURNAME").item(0).getTextContent());
        parcelNS4.setMapX(Double.parseDouble(element.getElementsByTagName("MAPX").item(0).getTextContent()));
        parcelNS4.setMapY(Double.parseDouble(element.getElementsByTagName("MAPY").item(0).getTextContent()));
        parcelNS4.setParcelNo(Integer.parseInt(element.getElementsByTagName("PARCELNO").item(0).getTextContent()));
        parcelNS4.setProvinceName(element.getElementsByTagName("PROVINCENAME").item(0).getTextContent());
        parcelNS4.setSubDistrictName(element.getElementsByTagName("TAMBOLNAME").item(0).getTextContent());
        parcelNS4.setUtmLandNo(Integer.parseInt(element.getElementsByTagName("UTMLANDNO").item(0).getTextContent()));
        parcelNS4.setUtmMap(element.getElementsByTagName("UTMMAP").item(0).getTextContent());
        parcelNS4.setUtmScale(Integer.parseInt(element.getElementsByTagName("UTMSCALE").item(0).getTextContent()));
    }
}
