package in.th.tamis.faarm.services;

import in.th.tamis.faarm.models.faarm.Person;
import in.th.tamis.faarm.models.faarm.datafromandroid.DetailRegisterPlantation;
import in.th.tamis.faarm.models.faarm.datafromandroid.Farmer;
import in.th.tamis.faarm.models.faarm.datafromandroid.RegisterPlantation;

import java.util.List;

public interface NewMember {
    int createNewMember(Farmer detailFarmer, int userId);
    int getPrefixID(String prefix);
    void addNewHouseholdMember(int profileCenterId, List<Person> person, String citizenIdFarmer, int userId);
    void addDetailPlantation(int profileCenterId, List<DetailRegisterPlantation> detailRegisterPlantation, String citizenId, int userId);
}
