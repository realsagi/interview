package in.th.tamis.faarm.services;

import com.google.gson.Gson;
import in.th.tamis.faarm.manager.DOPAApi;
import in.th.tamis.faarm.models.dopa.Household;
import in.th.tamis.faarm.models.dopa.PersonDOPA;
import in.th.tamis.faarm.models.faarm.HouseholdMember;
import in.th.tamis.faarm.models.faarm.Person;
import retrofit.Call;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HouseholdMemberService implements HouseholdService{

    DOPAApi service;

    public HouseholdMemberService(DOPAApi dopaRestAPI) {
        service = dopaRestAPI;
    }

    public HouseholdMember findByCitizenId(String citizenId) throws IOException {
        Household household = callHouseholdMemberDOPA(citizenId);
        return convertToHouseholdMember(household);
    }

    public Household callHouseholdMemberDOPA(String citizenId) throws IOException {
        Household household;
        Call<Household> call = service.loadHousehold(citizenId);
        household = call.execute().body();
        return household;
    }

    public HouseholdMember convertToHouseholdMember(Household household) {
        HouseholdMember householdMember = new HouseholdMember();
        int count = Integer.parseInt(household.getCount());

        List<Person> persons = new ArrayList<>();
        for(int index =0 ; index < count ; index++){
            Person person = new Person();
            PersonDOPA personDOPA = household.getPersons().get(index);

            person.setIdentityCard(personDOPA.getIdentityCard());
            person.setFirstName(personDOPA.getFirstName());
            person.setLastName(personDOPA.getLastName());
            person.setSex(personDOPA.getSex());

            persons.add(person);
        }

        householdMember.setPersons(persons);
        return householdMember;
    }
}
