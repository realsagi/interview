package in.th.tamis.faarm.models.faarm.datafromandroid;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.List;

public class DetailAgriculturalOperations {
    @SerializedName("namePlants")
    private String namePlants;
    @SerializedName("breeds")
    private String seeds;
    @SerializedName("details")
    private String species;
    @SerializedName("categoryPlants")
    private String categoryPlants;
    @SerializedName("plantingDate")
    private long plantingDate;
    @SerializedName("harvestDate")
    private long harvestDate;
    @SerializedName("plantedRai")
    private int plantedRai;
    @SerializedName("plantedNgan")
    private int plantedNgan;
    @SerializedName("plantedSquareWah")
    private int plantedSquareWah;
    @SerializedName("harvestedRai")
    private int harvestedRai;
    @SerializedName("harvestedNgan")
    private int harvestedNgan;
    @SerializedName("harvestedSquareWah")
    private int harvestedSquareWah;
    @SerializedName("estimatedProductivity")
    private double estimatedProductivity;
    @SerializedName("fileSnapShotMapAgriculture")
    private File fileSnapShotMapAgriculture;
    @SerializedName("imageDetailAgriculture")
    private File imageDetailAgriculture;
    @SerializedName("coordinates")
    private List<Coordinates> coordinates;

    public void setFileNameImageActivity(String fileNameImageActivity) {
        this.fileNameImageActivity = fileNameImageActivity;
    }

    private String fileNameImageActivity;

    public String getNamePlants() {
        return namePlants;
    }

    public void setNamePlants(String namePlants) {
        this.namePlants = namePlants;
    }

    public String getSeeds() {
        return seeds;
    }

    public void setSeeds(String seeds) {
        this.seeds = seeds;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getCategoryPlants() {
        return categoryPlants;
    }

    public void setCategoryPlants(String categoryPlants) {
        this.categoryPlants = categoryPlants;
    }

    public long getPlantingDate() {
        return plantingDate;
    }

    public void setPlantingDate(long plantingDate) {
        this.plantingDate = plantingDate;
    }

    public long getHarvestDate() {
        return harvestDate;
    }

    public void setHarvestDate(long harvestDate) {
        this.harvestDate = harvestDate;
    }

    public int getPlantedRai() {
        return plantedRai;
    }

    public void setPlantedRai(int plantedRai) {
        this.plantedRai = plantedRai;
    }

    public int getPlantedNgan() {
        return plantedNgan;
    }

    public void setPlantedNgan(int plantedNgan) {
        this.plantedNgan = plantedNgan;
    }

    public int getPlantedSquareWah() {
        return plantedSquareWah;
    }

    public void setPlantedSquareWah(int plantedSquareWah) {
        this.plantedSquareWah = plantedSquareWah;
    }

    public int getHarvestedRai() {
        return harvestedRai;
    }

    public void setHarvestedRai(int harvestedRai) {
        this.harvestedRai = harvestedRai;
    }

    public int getHarvestedNgan() {
        return harvestedNgan;
    }

    public void setHarvestedNgan(int harvestedNgan) {
        this.harvestedNgan = harvestedNgan;
    }

    public int getHarvestedSquareWah() {
        return harvestedSquareWah;
    }

    public void setHarvestedSquareWah(int harvestedSquareWah) {
        this.harvestedSquareWah = harvestedSquareWah;
    }

    public double getEstimatedProductivity() {
        return estimatedProductivity;
    }

    public void setEstimatedProductivity(double estimatedProductivity) {
        this.estimatedProductivity = estimatedProductivity;
    }

    public File getFileSnapShotMapAgriculture() {
        return fileSnapShotMapAgriculture;
    }

    public void setFileSnapShotMapAgriculture(File fileSnapShotMapAgriculture) {
        this.fileSnapShotMapAgriculture = fileSnapShotMapAgriculture;
    }

    public List<Coordinates> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Coordinates> coordinates) {
        this.coordinates = coordinates;
    }

    public File getImageDetailAgriculture() {
        return imageDetailAgriculture;
    }

    public void setImageDetailAgriculture(File imageDetailAgriculture) {
        this.imageDetailAgriculture = imageDetailAgriculture;
    }

    public String getFileNameImageActivity() {
        return fileNameImageActivity;
    }
}
