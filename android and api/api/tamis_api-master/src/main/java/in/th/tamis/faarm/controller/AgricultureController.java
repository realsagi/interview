package in.th.tamis.faarm.controller;

import com.google.gson.Gson;
import in.th.tamis.faarm.services.Agriculture;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AgricultureController extends HttpServlet {

    Agriculture service;
    public AgricultureController(Agriculture organizationJDBC) {
        service = organizationJDBC;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        PrintWriter out = response.getWriter();
        response.setStatus(HttpServletResponse.SC_OK);
        Gson gson = new Gson();

        if("/agriculture/community".equals(request.getRequestURI())){
            out.println(gson.toJson(service.getFarmersOrganization()));
        }
        else if("/agriculture/type".equals(request.getRequestURI())){
            out.println(gson.toJson(service.getType()));
        }
        else if("/agriculture/detail_all".equals(request.getRequestURI())){
            out.println(gson.toJson(service.getDetailAll()));
        }
        else if("/agriculture/detail".equals(request.getRequestURI())){
            String typeId = request.getParameter("type_id");
            out.print(gson.toJson(service.getDetail(typeId)));
        }
        else if("/agriculture/breed_all".equals(request.getRequestURI())){
            out.println(gson.toJson(service.getBreedAll()));
        }
        else if("/agriculture/breed".equals(request.getRequestURI())){
            String detailId = request.getParameter("detail_id");
            out.print(gson.toJson(service.getBreed(detailId)));
        }
        else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        out.close();
    }
}
