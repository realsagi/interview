package in.th.tamis.faarm.models.faarm;

import com.google.gson.annotations.SerializedName;

public class PersonalProfile {

    @SerializedName("pre_name")
    private String preName;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("citizen_id")
    private String citizenId;

    @SerializedName("sex_id")
    private int sexId;

    @SerializedName("sex")
    private String sex;

    @SerializedName("birth_date")
    private String birthDate;

    @SerializedName("age")
    private String age;

    @SerializedName("person_status_id")
    private int personStatusId;

    @SerializedName("person_status")
    private String personStatus;

    @SerializedName("address")
    private Address Address;

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName = preName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSexId() {
        return sexId;
    }

    public void setSexId(int sexId) {
        this.sexId = sexId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getPersonStatusId() {
        return personStatusId;
    }

    public void setPersonStatusId(int personStatusId) {
        this.personStatusId = personStatusId;
    }

    public String getPersonStatus() {
        return personStatus;
    }

    public void setPersonStatus(String personStatus) {
        this.personStatus = personStatus;
    }

    public in.th.tamis.faarm.models.faarm.Address getAddress() {
        return Address;
    }

    public void setAddress(in.th.tamis.faarm.models.faarm.Address address) {
        Address = address;
    }
}