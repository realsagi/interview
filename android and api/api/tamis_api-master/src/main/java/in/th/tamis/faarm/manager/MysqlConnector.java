package in.th.tamis.faarm.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConnector {

    private static String database = "tamis";
    private static String user = "root";
    //private static String pass = "1234";
    private static String pass = "spr1nt3r";
    private static String port = "3306";
    private static String ipAddress = "localhost";
    private static String unicode = "?autoReconnect=true&useUnicode=true&characterEncoding=utf-8";
    private static java.lang.String driver = "com.mysql.jdbc.Driver";
    private static java.lang.String url = "jdbc:mysql://"+ipAddress+":"+port+"/"+database+unicode;

    private static Connection connection = null;
    public static Connection getConnection(){
        try {
            Class.forName(driver);
            if(connection == null)
                connection = DriverManager.getConnection(url, user, pass);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
