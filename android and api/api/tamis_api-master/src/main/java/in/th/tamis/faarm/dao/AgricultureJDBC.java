package in.th.tamis.faarm.dao;

import com.google.gson.JsonObject;
import in.th.tamis.faarm.models.faarm.FarmersOrganization;
import in.th.tamis.faarm.models.faarm.Organization;
import in.th.tamis.faarm.models.faarm.agriculture.Breed;
import in.th.tamis.faarm.models.faarm.agriculture.Detail;
import in.th.tamis.faarm.models.faarm.agriculture.Type;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AgricultureJDBC implements AgricultureDAO{

    private Connection connection;

    public AgricultureJDBC(Connection connection){
        this.connection = connection;
    }

    @Override
    public Breed selectBreedAll() {
        String sqlQuery = "SELECT BREED_CODE, BREED_NAME, DETAIL_CODE FROM KASET_BREED";
        Breed breed = new Breed();
        List<JsonObject> breedList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while(resultSet.next()){
                if(resultSet.getString("BREED_CODE") != null) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("detail_code", resultSet.getString("DETAIL_CODE"));
                    jsonObject.addProperty("id", Integer.parseInt(resultSet.getString("BREED_CODE")));
                    jsonObject.addProperty("name", resultSet.getString("BREED_NAME"));
                    breedList.add(jsonObject);
                }
            }
            breed.setBreed(breedList);
            resultSet.close();
            statement.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return breed;
    }

    @Override
    public Breed selectByDetailCode(String detailCode) {
        String sqlQuery = "SELECT BREED_CODE, BREED_NAME  FROM KASET_BREED where DETAIL_CODE = "+detailCode+" order by BREED_NAME";
        Breed breed = new Breed();
        List<JsonObject> breedList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while(resultSet.next()){
                int breedId = Integer.parseInt(resultSet.getString("BREED_CODE"));
                String name = resultSet.getString("BREED_NAME");
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("id", breedId);
                jsonObject.addProperty("name", name);
                breedList.add(jsonObject);
            }
            breed.setBreed(breedList);
            resultSet.close();
            statement.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return breed;
    }

    @Override
    public Detail selectDetailAll() {
        String sqlQuery = "SELECT DETAIL_CODE, DETAIL_NAME, TYPE_CODE FROM KASET_DETAIL";
        Detail detail = new Detail();
        List<JsonObject> detailList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("type_code", resultSet.getString("TYPE_CODE"));
                jsonObject.addProperty("id", Integer.parseInt(resultSet.getString("DETAIL_CODE")));
                jsonObject.addProperty("name", resultSet.getString("DETAIL_NAME"));
                detailList.add(jsonObject);
            }
            detail.setDetail(detailList);
            resultSet.close();
            statement.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return detail;
    }

    @Override
    public Detail selectByTypeCode(String typeCode) {
        String sqlQuery = "SELECT DETAIL_CODE, DETAIL_NAME FROM KASET_DETAIL where TYPE_CODE = "+typeCode+" order by DETAIL_NAME";
        Detail detail = new Detail();
        List<JsonObject> detailList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while(resultSet.next()){
                int detailId = Integer.parseInt(resultSet.getString("DETAIL_CODE"));
                String name = resultSet.getString("DETAIL_NAME");
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("id", detailId);
                jsonObject.addProperty("name", name);
                detailList.add(jsonObject);
            }
            detail.setDetail(detailList);
            resultSet.close();
            statement.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return detail;
    }

    @Override
    public Type selectTypeAll() {
        String sqlQuery = "SELECT TYPE_CODE, TYPE_NAME FROM KASET_TYPE";

        Type type = new Type();
        List<JsonObject> organizationList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while(resultSet.next()){
                int typeId = Integer.parseInt(resultSet.getString("TYPE_CODE"));
                String name = resultSet.getString("TYPE_NAME");
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("id", typeId);
                jsonObject.addProperty("name", name);
                organizationList.add(jsonObject);
            }
            type.setTypeName(organizationList);
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return type;
    }

    @Override
    public FarmersOrganization selectOrganizationAll() {
        String sqlQuery = "SELECT KASET_ORG_ID, KASET_ORG_NAME FROM KASET_ORG order by KASET_ORG_ID ASC";
        FarmersOrganization farmersOrganization = new FarmersOrganization();
        List<Organization> organizationList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            Organization organization;
            while(resultSet.next()){
                organization = new Organization();
                organization.setId(Integer.parseInt(resultSet.getString("KASET_ORG_ID")));
                String organizationName = resultSet.getString("KASET_ORG_NAME")
                        .replace(" ", "")
                        .replace("\t", "");
                organization.setOrganizationName(organizationName);
                organizationList.add(organization);
            }
            farmersOrganization.setOrganizationsList(organizationList);
            resultSet.close();
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return farmersOrganization;
    }
}
