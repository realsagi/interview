package in.th.tamis.faarm.models.faarm.datafromandroid;

import com.google.gson.annotations.SerializedName;
import in.th.tamis.faarm.models.faarm.Address;

import java.io.File;

public class Farmer {

    @SerializedName("first_name")String firstName;
    @SerializedName("imagePerson")byte[] imagePerson;
    @SerializedName("fileImageProFile")File fileImageProFile;
    @SerializedName("pre_name")String preName;
    @SerializedName("last_name")String lastName;
    @SerializedName("sexId")Integer sexId;
    @SerializedName("birth_date")String birthDate;
    @SerializedName("age")String age;
    @SerializedName("personStatusId")int personStatusId;
    @SerializedName("person_status")String personStatus;
    @SerializedName("citizen_id")String citizenId;
    @SerializedName("sex")String sex;
    @SerializedName("mobile_phone")String mobilePhone;
    @SerializedName("home_phone")String homePhone;
    @SerializedName("status")int status;
    @SerializedName("address")Address address;
    String nameImageProfile;
    String nameImageSign;

    public String getNameImageSign() {
        return nameImageSign;
    }

    public String getNameImageProfile() {
        return nameImageProfile;
    }

    public int getPrefixId() {
        return prefixId;
    }

    public void setPrefixId(int prefixId) {
        this.prefixId = prefixId;
    }

    int prefixId;

    public byte[] getSignAuthentication() {
        return signAuthentication;
    }

    public void setSignAuthentication(byte[] signAuthentication) {
        this.signAuthentication = signAuthentication;
    }

    byte[] signAuthentication;

    String careerMain;
    String careerMinor;

    public String getCareerMain() {
        return careerMain;
    }

    public void setCareerMain(String careerMain) {
        this.careerMain = careerMain;
    }

    public String getCareerMinor() {
        return careerMinor;
    }

    public void setCareerMinor(String careerMinor) {
        this.careerMinor = careerMinor;
    }

    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName = preName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }


    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public byte[] getImagePerson() {
        return imagePerson;
    }

    public void setImagePerson(byte[] imagePerson) {
        this.imagePerson = imagePerson;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSexId() {
        return sexId;
    }

    public void setSexId(int sexId) {
        this.sexId = sexId;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getPersonStatusId() {
        return personStatusId;
    }

    public void setPersonStatusId(int personStatusId) {
        this.personStatusId = personStatusId;
    }

    public String getPersonStatus() {
        return personStatus;
    }

    public void setPersonStatus(String personStatus) {
        this.personStatus = personStatus;
    }

    public File getFileImageProFile() {
        return fileImageProFile;
    }

    public void setFileImageProFile(File fileImageProFile) {
        this.fileImageProFile = fileImageProFile;
    }

    public int getYear() {
        return Integer.parseInt(birthDate)/10000;
        }
        }