package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.models.faarm.FarmersOrganization;
import in.th.tamis.faarm.models.faarm.agriculture.Breed;
import in.th.tamis.faarm.models.faarm.agriculture.Detail;
import in.th.tamis.faarm.models.faarm.agriculture.Type;

public interface AgricultureDAO {
    Breed selectBreedAll();
    Breed selectByDetailCode(String detailCode);
    Detail selectDetailAll();
    Detail selectByTypeCode(String typeCode);
    Type selectTypeAll();
    FarmersOrganization selectOrganizationAll();
}
