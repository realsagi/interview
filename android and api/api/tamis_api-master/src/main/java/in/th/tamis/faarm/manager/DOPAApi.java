package in.th.tamis.faarm.manager;

import in.th.tamis.faarm.models.dopa.CitizenProfile;
import in.th.tamis.faarm.models.dopa.Household;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface DOPAApi {
    @GET("/ws/dopa/personal/profile/extra")
    Call<CitizenProfile> loadProfile(@Query("CitizenID") String citizenId);

    @GET("/ws/dopa/house/person")
    Call<Household> loadHousehold(@Query("CitizenID") String citizenId);
}
