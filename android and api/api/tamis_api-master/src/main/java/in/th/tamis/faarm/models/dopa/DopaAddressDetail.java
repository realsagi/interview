package in.th.tamis.faarm.models.dopa;

public class DopaAddressDetail {

    String alley;
    String ampher;
    String addressId;
    String moo;
    String addressNo;
    String province;
    String road;
    String soi;
    String tumbon;

    public String getAlley() {
        return alley;
    }

    public void setAlley(String alley) {
        this.alley = alley;
    }

    public String getAmpher() {
        return ampher;
    }

    public void setAmpher(String ampher) {
        this.ampher = ampher;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    public String getAddressNo() {
        return addressNo;
    }

    public void setAddressNo(String addressNo) {
        this.addressNo = addressNo;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public String getTumbon() {
        return tumbon;
    }

    public void setTambon(String tumbon) {
        this.tumbon = tumbon;
    }
}
