package in.th.tamis.faarm.models.faarm.datafromandroid;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegisterPlantation {
    @SerializedName("detailRegisterPlantation")
    private List<DetailRegisterPlantation> detailRegisterPlantation;

    public List<DetailRegisterPlantation> getDetailRegisterPlantation() {
        return detailRegisterPlantation;
    }

    public void setDetailRegisterPlantation(List<DetailRegisterPlantation> detailRegisterPlantation) {
        this.detailRegisterPlantation = detailRegisterPlantation;
    }
}
