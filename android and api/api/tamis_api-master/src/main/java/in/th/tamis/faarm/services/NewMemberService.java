package in.th.tamis.faarm.services;

import in.th.tamis.faarm.dao.NewMemberDAO;
import in.th.tamis.faarm.models.faarm.Person;
import in.th.tamis.faarm.models.faarm.datafromandroid.DetailAgriculturalOperations;
import in.th.tamis.faarm.models.faarm.datafromandroid.DetailRegisterPlantation;
import in.th.tamis.faarm.models.faarm.datafromandroid.Farmer;

import java.util.List;

public class NewMemberService implements NewMember {
    NewMemberDAO newMemberDAO;
    public NewMemberService(NewMemberDAO newMemberDAO) {
        this.newMemberDAO = newMemberDAO;
    }


    @Override
    public int createNewMember(Farmer farmer, int userId) {
        farmer.setPrefixId(getPrefixID(farmer.getPreName()));
        int profileCenterId = newMemberDAO.newFarmer(farmer,userId);
        return profileCenterId;
    }

    @Override
    public int getPrefixID(String prefix) {
        return newMemberDAO.findPrefixID(prefix);
    }

    @Override
    public void addNewHouseholdMember(int profileCenterId, List<Person> person, String citizenIdFarmer, int userId) {
        int profileMemberId;
        int statusMember = 2;
        for (Person householdMember : person) {
            if(householdMember.getIdentityCard().equals(citizenIdFarmer)){
                statusMember = 1;
            }
            profileMemberId = newMemberDAO.addNewHouseholdMember(profileCenterId, householdMember,statusMember,userId);
            if(householdMember.getFarmersOrganization() != null) {
                addMemberOrg(profileMemberId, householdMember.getFarmersOrganization().getIdList());
            }
        }
    }

    @Override
    public void addDetailPlantation(int profileCenterId, List<DetailRegisterPlantation> detailRegisterPlantation, String citizenId, int userId) {
        int profileAreaId;
        int indexPlantation = 1;
        for (DetailRegisterPlantation plantation : detailRegisterPlantation){
            profileAreaId = newMemberDAO.addDetailPlantation(profileCenterId,plantation,indexPlantation++,citizenId,userId);
            if(plantation.getDetailAgriculturalOperationses() != null){
                for(DetailAgriculturalOperations activity : plantation.getDetailAgriculturalOperationses()) {
                    newMemberDAO.addDetailActivity(profileAreaId, activity, citizenId, profileCenterId,userId);
                }
            }
        }
    }

    private void addMemberOrg(int profileMemberId, List<String> farmersOrganization) {
        for (String org : farmersOrganization){
            newMemberDAO.addMemberOrg(profileMemberId,org);
        }
    }
}
