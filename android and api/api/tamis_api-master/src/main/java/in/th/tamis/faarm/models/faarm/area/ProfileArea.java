package in.th.tamis.faarm.models.faarm.area;

public class ProfileArea {

    private int ProfileFarmerId;
    private String ownerId;
    private String ownerName;
    private int docRai;
    private int docNgan;
    private float docWa;
    private int statusDelete;
    private String docNumber;
    private int docTypeId;

    public int getProfileFarmerId() {
        return ProfileFarmerId;
    }

    public void setProfileFarmerId(int profileFarmerId) {
        ProfileFarmerId = profileFarmerId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public int getDocRai() {
        return docRai;
    }

    public void setDocRai(int docRai) {
        this.docRai = docRai;
    }

    public int getDocNgan() {
        return docNgan;
    }

    public void setDocNgan(int docNgan) {
        this.docNgan = docNgan;
    }

    public float getDocWa() {
        return docWa;
    }

    public void setDocWa(float docWa) {
        this.docWa = docWa;
    }

    public int getStatusDelete() {
        return statusDelete;
    }

    public void setStatusDelete(int statusDelete) {
        this.statusDelete = statusDelete;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public int getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeId(int docTypeId) {
        this.docTypeId = docTypeId;
    }
}
