package in.th.tamis.faarm.dao;

import com.google.gson.Gson;
import in.th.tamis.faarm.models.login.MFUser;
import in.th.tamis.faarm.models.login.UserLogin;
import in.th.tamis.faarm.services.login.TimeSession;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class AuthenticationJDBC implements AuthenticationDAO {

    private Connection connection;

    public AuthenticationJDBC(Connection connection){
        this.connection = connection;
    }

    @Override
    public MFUser selectUserLogin(String userName, String passWord) {

        String sqlQuery = "SELECT USER_ID, USER_USERNAME, USER_PASSWORD, USER_EMAIL, USER_LEVEL, STATUS_ACTIVE, STATUS_RESET " +
                "FROM MF_USER " +
                "WHERE USER_USERNAME = '"+userName+"' AND " +
                "USER_PASSWORD = '"+passWord+"'";

        MFUser mfUser = new MFUser();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while(resultSet.next()){
                mfUser.setUserId(resultSet.getInt("USER_ID"));
                mfUser.setUserName(resultSet.getString("USER_USERNAME"));
                mfUser.setPassword(resultSet.getString("USER_PASSWORD"));
                mfUser.setEmail(resultSet.getString("USER_EMAIL"));
                mfUser.setLevel(resultSet.getString("USER_LEVEL"));
                mfUser.setStatusActive(resultSet.getInt("STATUS_ACTIVE"));
                mfUser.setStatusReset(resultSet.getInt("STATUS_RESET"));
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(new Gson().toJson(mfUser));
        return mfUser;
    }

    @Override
    public boolean deleteUserLoginByToken(String token) {
        String sqlQuery = "DELETE FROM USER_LOGIN WHERE TOKEN_KEY=?";
        boolean status = false;
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, token);
            statement.setPoolable(false);
            status = statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public boolean saveUserLogin(UserLogin userLogin) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String sqlQuery = "insert into USER_LOGIN (USER_USERNAME, TOKEN_KEY, USER_ID,DATE_TIME_ACTIVE) " +
                "VALUES (?, ?, ?, ?)";

        boolean status = false;
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, userLogin.getUserName());
            statement.setString(2, userLogin.getTokenKey());
            statement.setInt(3, userLogin.getUserId());
            statement.setTimestamp(4, timestamp);
            statement.setPoolable(false);
            status = statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public String selectUserLoginByToken(String token) {
        String sqlQuery = "SELECT USER_ID, DATE_TIME_ACTIVE FROM USER_LOGIN WHERE TOKEN_KEY=?";
        String result = null;
        Calendar timeNow = Calendar.getInstance();
        Calendar sqlTime = Calendar.getInstance();
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, token);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                result = String.valueOf(resultSet.getInt("USER_ID"));

                Date date = resultSet.getDate("DATE_TIME_ACTIVE");
                Time time = resultSet.getTime("DATE_TIME_ACTIVE");

                String dateMain = date.toString();
                int year = Integer.parseInt(dateMain.substring(0,dateMain.indexOf("-")));
                int month = Integer.parseInt(dateMain.substring(dateMain.indexOf("-")+1, dateMain.lastIndexOf("-"))) - 1;
                int day = Integer.parseInt(dateMain.substring(dateMain.lastIndexOf("-")+1));

                String timeMain = time.toString();
                int hours = Integer.parseInt(timeMain.substring(0,timeMain.indexOf(":")));
                int minute = Integer.parseInt(timeMain.substring(timeMain.indexOf(":")+1, timeMain.lastIndexOf(":")));
                int second = Integer.parseInt(timeMain.substring(timeMain.lastIndexOf(":")+1));

                sqlTime.set(year, month, day, hours, minute, second);
            }
            long time = new TimeSession().getMinuteSettlement(timeNow, sqlTime);
            if(time <= 15){
                if(result != null){
                    updateTimeOfToken(token);
                }
            }else {
                result = null;
                deleteUserLoginByToken(token);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String selectTokenByUserNameOnUserLogin(String username) {
        String sqlQuery = "SELECT USER_USERNAME, DATE_TIME_ACTIVE, TOKEN_KEY FROM USER_LOGIN WHERE USER_USERNAME='"+username+"'";
        String token = null;
        Calendar timeNow = Calendar.getInstance();
        Calendar sqlTime = Calendar.getInstance();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while(resultSet.next()){
                token = resultSet.getString("TOKEN_KEY");

                Date date = resultSet.getDate("DATE_TIME_ACTIVE");
                Time time = resultSet.getTime("DATE_TIME_ACTIVE");

                String dateMain = date.toString();
                int year = Integer.parseInt(dateMain.substring(0,dateMain.indexOf("-")));
                int month = Integer.parseInt(dateMain.substring(dateMain.indexOf("-")+1, dateMain.lastIndexOf("-"))) - 1;
                int day = Integer.parseInt(dateMain.substring(dateMain.lastIndexOf("-")+1));

                String timeMain = time.toString();
                int hours = Integer.parseInt(timeMain.substring(0,timeMain.indexOf(":")));
                int minute = Integer.parseInt(timeMain.substring(timeMain.indexOf(":")+1, timeMain.lastIndexOf(":")));
                int second = Integer.parseInt(timeMain.substring(timeMain.lastIndexOf(":")+1));

                sqlTime.set(year, month, day, hours, minute, second);
            }
            long time = new TimeSession().getMinuteSettlement(timeNow, sqlTime);
            if(time > 15) {
                deleteUserLoginByToken(token);
                token = null;
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return token;

    }

    @Override
    public void saveLog(int userId) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String sqlQuery = "insert into LOG_LOGIN (USER_ID,DATE_TIME_LOGIN) " +
                "VALUES (?, ?)";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, userId);
            statement.setTimestamp(2, timestamp);
            statement.setPoolable(false);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateTimeOfToken(String token) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String sqlQuery = "UPDATE USER_LOGIN SET DATE_TIME_ACTIVE=? WHERE TOKEN_KEY=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setTimestamp(1, timestamp);
            statement.setString(2, token);
            statement.setPoolable(false);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getNowTime() {
        Calendar date = Calendar.getInstance();
        int year = date.get(Calendar.YEAR);
        int month = date.get(Calendar.MONTH)+1;
        int day = date.get(Calendar.DAY_OF_MONTH);
        int hour = date.get(Calendar.HOUR_OF_DAY);
        int minute = date.get(Calendar.MINUTE);
        int second = date.get(Calendar.SECOND);
        String dateFormat = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+second;
        return dateFormat;
    }
}
