package in.th.tamis.faarm;

import in.th.tamis.faarm.controller.*;
import in.th.tamis.faarm.dao.*;
import in.th.tamis.faarm.manager.MysqlConnector;
import in.th.tamis.faarm.manager.OracleConnector;
import in.th.tamis.faarm.services.*;
import in.th.tamis.faarm.services.career.Career;
import in.th.tamis.faarm.services.career.CareerServiceStub;
import in.th.tamis.faarm.services.dol.ConnectDOLApi;
import in.th.tamis.faarm.services.dol.DOLService;
import in.th.tamis.faarm.services.dopa.ConnectDOPAApi;
import in.th.tamis.faarm.services.dopa.DOPAService;
import in.th.tamis.faarm.services.login.Authentication;
import in.th.tamis.faarm.services.login.AuthenticationService;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.DispatcherType;
import javax.servlet.MultipartConfigElement;
import javax.servlet.Servlet;
import java.sql.Connection;
import java.util.EnumSet;

public class TamisServer {

    private static Connection connection;

    public static void main(String[] args) throws Exception
    {
        String dopaUrl = dopaUrl(args);
        String dolUrl = dolUrl(args);
        connection = createConnection(args);


        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        context.addFilter(CharsetFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));

        DOPAService dopaService = new DOPAService(new ConnectDOPAApi(), dopaUrl);
        context.addServlet(createServletHolder(new PersonalProfileController(dopaService)), "/personal_profile");
        context.addServlet(createServletHolder(new HouseholdController(dopaService)), "/household_members");

        DOLService dolService = new DOLService(new ConnectDOLApi(), dolUrl, "http://110.164.49.55/DolParcel2Ega/IDolParcel/getParcel");
        context.addServlet(createServletHolder(new ParcelController(dolService)), "/parcel");
        context.addServlet(createServletHolder(new ParcelTypeController(dolService)), "/parcel_type");

        Agriculture agricultureService = new AgricultureService(connection);
        AgricultureController agricultureController = new AgricultureController(agricultureService);
        context.addServlet(createServletHolder(agricultureController), "/agriculture/community");
        context.addServlet(createServletHolder(agricultureController), "/agriculture/type");
        context.addServlet(createServletHolder(agricultureController), "/agriculture/detail_all");
        context.addServlet(createServletHolder(agricultureController), "/agriculture/detail");
        context.addServlet(createServletHolder(agricultureController), "/agriculture/breed_all");
        context.addServlet(createServletHolder(agricultureController), "/agriculture/breed");

        MemberDAO memberDAO = new MemberJDBC(connection);
        Member memberService = new MemberService(memberDAO);
        MemberController memberController = new MemberController(memberService);
        context.addServlet(createServletHolder(memberController), "/member");
        context.addServlet(createServletHolder(memberController), "/member/household_id");

        Area areaService = new AreaService(connection);
        AreaController areaController = new AreaController(areaService);
        context.addServlet(createServletHolder(areaController), "/area/province");
        context.addServlet(createServletHolder(areaController), "/area/all_district");
        context.addServlet(createServletHolder(areaController), "/area/district");
        context.addServlet(createServletHolder(areaController), "/area/all_sub_district");
        context.addServlet(createServletHolder(areaController), "/area/sub_district");
        context.addServlet(createServletHolder(areaController), "/area/postcode");
        context.addServlet(createServletHolder(areaController), "/area/location_code");
        context.addServlet(createServletHolder(areaController), "/area/profile_doc");

        NewMemberDAO newMemberDAO;
        if(args.length > 0 && args[0].equals("production")) {
            newMemberDAO = new NewMemberJDBCORACLE(connection);
        } else{
            newMemberDAO = new NewMemberJDBCMySQL(connection);
        }
        NewMember addNewMemberService = new NewMemberService(newMemberDAO);

        NewMemberController addNewMember = new NewMemberController(addNewMemberService);
        context.addServlet(createServletHolder(addNewMember), "/add_new_farmer");
        //context.addServlet(createServletHolder(addNewMember), "/upload_image");
        ServletHolder fileUploadServletHolder = new ServletHolder(new UploadFileController());
        fileUploadServletHolder.getRegistration().setMultipartConfig(new MultipartConfigElement("/images"));
        context.addServlet(fileUploadServletHolder, "/upload_image");


        Career careerService = new CareerServiceStub();
        ProfileCareerController careerController = new ProfileCareerController(careerService);
        context.addServlet(createServletHolder(careerController), "/career");

        Authentication authenticationService = new AuthenticationService(connection);
        AuthenticationController authenticationController = new AuthenticationController(authenticationService);
        context.addServlet(createServletHolder(authenticationController), "/login");
        context.addServlet(createServletHolder(authenticationController), "/logout");
        context.addServlet(createServletHolder(authenticationController), "/login/token");

        Server server = new Server(8080);
        server.setHandler(context);

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Connection createConnection(String[] args) {
        Connection connection;
        if(args.length > 0 && args[0].equals("production")){
            connection = OracleConnector.getConnection();
        }else {
            connection = MysqlConnector.getConnection();
        }
        return connection;
    }

    private static String dopaUrl(String[] args) {
        if(args.length > 0 && args[0].equals("production")) {
            return "https://122.154.24.188:4321/struct/services/dopaTamis.dopaTamisHttpsSoap11Endpoint";
        }
        return "http://localhost:8882/dopa";
    }

    private static String dolUrl(String[] args) {
        if(args.length > 0 && args[0].equals("production")) {
            return "http://164.115.20.131/adxgateway/services/DolParcelDetail.DolParcelDetailHttpSoap11Endpoint";
        }
        return "http://localhost:8882/dol";
    }

    private static ServletHolder createServletHolder(Object object){
        return new ServletHolder((Servlet) object);
    }
}
