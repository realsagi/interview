package in.th.tamis.faarm.models.faarm.datafromandroid;

import com.google.gson.annotations.SerializedName;
import in.th.tamis.faarm.models.faarm.Address;

import java.io.File;
import java.util.List;

public class DetailRegisterPlantation {
    @SerializedName("licenseType")
    private String licenseType;

    @SerializedName("licenseNumber")
    private String licenseNumber;

    @SerializedName("raWangNumber")
    private String raWangNumber;

    @SerializedName("rai")
    private int rai;

    @SerializedName("ngan")
    private int ngan;

    @SerializedName("squareWah")
    private int squareWah;

    @SerializedName("corporateOwnershipLicense")
    private String corporateOwnershipLicense;

    @SerializedName("address")
    private Address address;

    @SerializedName("irrigationArea")
    private String irrigationArea;

    @SerializedName("holding")
    private String holding;

    @SerializedName("coordinates")
    private List<Coordinates> coordinates;

    @SerializedName("coordinatesCenter")
    private Coordinates coordinatesCenter;

    @SerializedName("detailAgriculturalOperationses")
    private List<DetailAgriculturalOperations> detailAgriculturalOperationses;

    @SerializedName("fileSnapShotMap")
    private File fileSnapShotMap;

    @SerializedName("imageDetailPlantation")
    private File imageDetailPlantation;

    private ParcelDoc parcelDoc;

    public String getFileNameImagePlantation() {
        return fileNameImagePlantation;
    }

    String fileNameImagePlantation;

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getRaWangNumber() {
        return raWangNumber;
    }

    public void setRaWangNumber(String raWangNumber) {
        this.raWangNumber = raWangNumber;
    }

    public int getRai() {
        return rai;
    }

    public void setRai(int rai) {
        this.rai = rai;
    }

    public int getNgan() {
        return ngan;
    }

    public void setNgan(int ngan) {
        this.ngan = ngan;
    }

    public int getSquareWah() {
        return squareWah;
    }

    public void setSquareWah(Double squareWah) {
        this.squareWah = squareWah.intValue();
    }

    public String getCorporateOwnershipLicense() {
        return corporateOwnershipLicense;
    }

    public void setCorporateOwnershipLicense(String corporateOwnershipLicense) {
        this.corporateOwnershipLicense = corporateOwnershipLicense;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getIrrigationArea() {
        return irrigationArea;
    }

    public void setIrrigationArea(String irrigationArea) {
        this.irrigationArea = irrigationArea;
    }

    public String getHolding() {
        return holding;
    }

    public void setHolding(String holding) {
        this.holding = holding;
    }

    public List<Coordinates> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Coordinates> coordinates) {
        this.coordinates = coordinates;
    }

    public List<DetailAgriculturalOperations> getDetailAgriculturalOperationses() {
        return detailAgriculturalOperationses;
    }

    public void setDetailAgriculturalOperations(List<DetailAgriculturalOperations> detailAgriculturalOperationses) {
        this.detailAgriculturalOperationses = detailAgriculturalOperationses;
    }

    public File getFileSnapShotMap() {
        return fileSnapShotMap;
    }

    public void setFileSnapShotMap(File fileSnapShotMap) {
        this.fileSnapShotMap = fileSnapShotMap;
    }

    public File getImageDetailPlantation() {
        return imageDetailPlantation;
    }

    public void setImageDetailPlantation(File imageDetailPlantation) {
        this.imageDetailPlantation = imageDetailPlantation;
    }

    public Coordinates getCoordinatesCenter() {
        return coordinatesCenter;
    }

    public void setCoordinatesCenter(Coordinates coordinatesCenter) {
        this.coordinatesCenter = coordinatesCenter;
    }

    public ParcelDoc getParcelDoc() {
        return parcelDoc;
    }

    public void setParcelDoc(ParcelDoc parcelDoc) {
        this.parcelDoc = parcelDoc;
    }
}
