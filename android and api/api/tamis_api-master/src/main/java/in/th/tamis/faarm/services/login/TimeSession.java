package in.th.tamis.faarm.services.login;

import java.util.Calendar;

public class TimeSession {

    public long getMinuteSettlement(Calendar nowTime, Calendar dataTime) {
        long time = nowTime.getTime().getTime() - dataTime.getTime().getTime();
        return Math.round(time/1000.0/60.0);
    }
}
