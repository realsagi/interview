package in.th.tamis.faarm.models.faarm;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FarmersOrganization {

    @SerializedName("community")
    private List<Organization> organizationsList;
    @SerializedName("idList")
    private List<String> idList;

    public List<String> getIdList() {
        return idList;
    }

    public void setIdList(List<String> idList) {
        this.idList = idList;
    }

    public List<Organization> getOrganizationsList() {
        return organizationsList;
    }

    public void setOrganizationsList(List<Organization> organizationsList) {
        this.organizationsList = organizationsList;
    }
}
