package in.th.tamis.faarm.models.faarm;

import com.google.gson.annotations.SerializedName;

public class Images {
    @SerializedName("path")String path;
    @SerializedName("imagePerson")byte[] photo;

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
