package in.th.tamis.faarm.services;

import in.th.tamis.faarm.manager.DOPAApi;
import in.th.tamis.faarm.models.dopa.CitizenProfile;
import in.th.tamis.faarm.models.faarm.Address;
import in.th.tamis.faarm.models.faarm.PersonalProfile;
import retrofit.Call;

import java.io.IOException;

public class PersonalFAARMProfileService implements PersonalProfileService {

    DOPAApi service;

    public PersonalFAARMProfileService(DOPAApi dopaRestAPI) {
        service = dopaRestAPI;
    }


    public PersonalProfile findByCitizenId(String citizenId) throws IOException {
        CitizenProfile citizenProfile = callPersonalProfileDOPA(citizenId);
        return convertToPersonalProfile(citizenProfile);
    }

    public CitizenProfile callPersonalProfileDOPA(String citizenId) throws IOException {;
        Call<CitizenProfile> call = service.loadProfile(citizenId);
        return call.execute().body();
    }

    public PersonalProfile convertToPersonalProfile(CitizenProfile citizenProfile) {
        PersonalProfile personalProfile = new PersonalProfile();

        personalProfile.setPreName(citizenProfile.getThPreName());
        personalProfile.setFirstName(citizenProfile.getThFirstName());
        personalProfile.setLastName(citizenProfile.getThLastName());
        personalProfile.setSexId(citizenProfile.getSexId());
        personalProfile.setSex(citizenProfile.getSex());
        personalProfile.setBirthDate(citizenProfile.getBirthDate());
        personalProfile.setAge(citizenProfile.getAge());
        personalProfile.setPersonStatusId(citizenProfile.getPersonStatusId());
        personalProfile.setPersonStatus(citizenProfile.getPersonStatus());

        Address address = new Address();
        address.setAddressID(citizenProfile.getAddressId());
        address.setHouseNo(citizenProfile.getAddressNo());
        address.setMoo(citizenProfile.getAddressMoo());
        address.setAlley(citizenProfile.getAddressAlley());
        address.setSoi(citizenProfile.getAddressSoi());
        address.setRoad(citizenProfile.getAddressRoad());
        address.setSubDistrict(citizenProfile.getAddressTumbol());
        address.setDistrict(citizenProfile.getAddressAmphur());
        address.setProvince(citizenProfile.getAddressProvince());

        personalProfile.setAddress(address);

        return personalProfile;
    }

}
