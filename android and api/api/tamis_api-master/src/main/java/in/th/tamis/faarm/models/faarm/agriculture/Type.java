package in.th.tamis.faarm.models.faarm.agriculture;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Type {
    @SerializedName("type_name")
    private List<JsonObject> typeName;

    public List<JsonObject> getTypeName() {
        return typeName;
    }

    public void setTypeName(List<JsonObject> typeName) {
        this.typeName = typeName;
    }
}
