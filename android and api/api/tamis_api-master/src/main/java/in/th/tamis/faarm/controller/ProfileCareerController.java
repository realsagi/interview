package in.th.tamis.faarm.controller;

import com.google.gson.Gson;
import in.th.tamis.faarm.models.faarm.career.ProfileCareer;
import in.th.tamis.faarm.services.career.Career;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ProfileCareerController extends HttpServlet {
    Career service;
    public ProfileCareerController(Career careerService) {
        service = careerService;
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        if ("/career".equals(request.getRequestURI())) {
            ProfileCareer career = service.getCareer();
            out.println(gson.toJson(career));
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        out.close();
    }
}
