package in.th.tamis.faarm.models.login;

import java.util.Calendar;

public class UserLogin {

    private int userId;
    private String userName;
    private String tokenKey;
    private Calendar dateTimeLogin;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    public Calendar getDateTimeLogin() {
        return dateTimeLogin;
    }

    public void setDateTimeLogin(Calendar dateTimeLogin) {
        this.dateTimeLogin = dateTimeLogin;
    }
}
