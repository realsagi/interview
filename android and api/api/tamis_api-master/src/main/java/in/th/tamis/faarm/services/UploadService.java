package in.th.tamis.faarm.services;


import in.th.tamis.faarm.models.faarm.datafromandroid.ImagesFarmer;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import javax.imageio.ImageIO;
import java.io.InputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.io.File;
public class UploadService implements Upload{

    @Override
    public byte[] convertImageToByte(String ImageName) throws IOException {
        Path path = Paths.get(ImageName);
        byte[] data = Files.readAllBytes(path);
        return data;
    }

    @Override
    public boolean writeImageFromByte(byte[] data,String path) {
        createFolder(path);
        try{
            BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(data));
            ImageIO.write(bufferedImage, getFileExtension(path), new File(path));
            return true;
        }catch (IOException e){
            return false;
        }
    }

    private void createFolder(String path) {
        String folder = new File(path).getParent();
        if(Files.notExists(Paths.get(folder))){
            new File(folder).mkdirs();
        }
    }

    @Override
    public String genPath(ImagesFarmer imagesFarmer) {
        Path path = Paths.get(System.getProperty("user.home"));
        return path.toString() + File.separator + "images" + File.separator + imagesFarmer.getCitizenId() + File.separator + imagesFarmer.getFilename();
    }

    private String getFileExtension(String path) {
        if(path.lastIndexOf(".") != -1 && path.lastIndexOf(".") != 0)
            return path.substring(path.lastIndexOf(".")+1);
        else return "";
    }

}
