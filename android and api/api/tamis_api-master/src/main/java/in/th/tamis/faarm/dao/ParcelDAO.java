package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.models.faarm.parcel.ParcelType;

public interface ParcelDAO {
    ParcelType selectAllParcelType();
}
