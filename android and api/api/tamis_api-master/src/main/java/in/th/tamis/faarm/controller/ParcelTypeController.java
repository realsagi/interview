package in.th.tamis.faarm.controller;


import com.google.gson.Gson;
import in.th.tamis.faarm.services.dol.DOLService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ParcelTypeController extends HttpServlet {

    DOLService service;

    public ParcelTypeController(DOLService dolService) {
        service = dolService;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        if("/parcel_type".equals(request.getRequestURI())){
            out.println(gson.toJson(service.getParcelType()));
        }
        else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        out.close();
    }
}
