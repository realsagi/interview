package in.th.tamis.faarm.controller;

import com.google.gson.Gson;
import in.th.tamis.faarm.models.faarm.area.ProfileArea;
import in.th.tamis.faarm.services.Area;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AreaController extends HttpServlet {

    Area areaService;
    public AreaController(Area areaService){
        this.areaService = areaService;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_OK);
        Gson gson = new Gson();
        PrintWriter out = response.getWriter();
        if("/area/province".equals(request.getRequestURI())){
            out.println(gson.toJson(areaService.getProvince()));
        }
        else if("/area/all_district".equals(request.getRequestURI())){
            out.println(gson.toJson(areaService.getAllDistrict()));
        }
        else if("/area/district".equals(request.getRequestURI())){
            String provinceCode = request.getParameter("province_code");
            out.println(gson.toJson(areaService.getDistrict(provinceCode)));
        }
        else if("/area/all_sub_district".equals(request.getRequestURI())){
            out.print(gson.toJson(areaService.getAllSubDistrict()));
        }
        else if("/area/sub_district".equals(request.getRequestURI())){
            String provinceCode = request.getParameter("province_code");
            String districtCode = request.getParameter("district_code");
            out.println(gson.toJson(areaService.getSubDistrict(provinceCode, districtCode)));
        }
        else if("/area/postcode".equals(request.getRequestURI())){
            String locationCode = request.getParameter("location_code");
            out.println(areaService.getPostCode(locationCode));
        }
        else if("/area/location_code".equals(request.getRequestURI())){
            String provinceName = request.getParameter("province");
            String districtName = request.getParameter("district");
            String subDistrictName = request.getParameter("sub_district");
            out.println(areaService.getLocationCode(provinceName,districtName, subDistrictName));
        }
        else if("/area/profile_doc".equals(request.getRequestURI())){
            String parcelTypeId = request.getParameter("parcel_type");
            String parcelNumber = request.getParameter("parcel_number");
            String provinceCode = request.getParameter("province_code");
            String districtCode = request.getParameter("district_code");
            ProfileArea profileArea = areaService.getProfileArea(
                    parcelNumber,
                    Integer.parseInt(parcelTypeId),
                    provinceCode,
                    districtCode
            );

            if(profileArea.getProfileFarmerId() != 0){
                out.println(true);
            }else {
                out.println(false);
            }
        }
        else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        out.close();
    }
}
