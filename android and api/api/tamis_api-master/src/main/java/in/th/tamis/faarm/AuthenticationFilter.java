package in.th.tamis.faarm;

import javax.servlet.*;
import java.io.IOException;

public class AuthenticationFilter implements Filter {

    private String accessToken;

    public void init(FilterConfig filterConfig) throws ServletException {
        accessToken = filterConfig.getInitParameter("accessToken");
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

    }

    public void destroy() {

    }
}
