package in.th.tamis.faarm.models.dopa;

import com.google.gson.annotations.SerializedName;

public class CitizenProfile {

    @SerializedName("CitizenID")                private String citizenId;
    @SerializedName("NameEN_Prefix")            private String engPreName;
    @SerializedName("NameEN_FirstName")         private String engFirstName;
    @SerializedName("NameEN_MiddleName")        private String engMiddleName;
    @SerializedName("NameEN_SurName")           private String engLastName;
    @SerializedName("NameTH_Prefix")            private String thPreName;
    @SerializedName("NameTH_FirstName")         private String thFirstName;
    @SerializedName("NameTH_MiddleName")        private String thMiddleName;
    @SerializedName("NameTH_SurName")           private String thLastName;
    @SerializedName("SexID")                    private int sexId;
    @SerializedName("Sex")                      private String sex;
    @SerializedName("BirthDate")                private String birthDate;
    @SerializedName("Age")                      private String age;
    @SerializedName("PersonStatusID")           private int personStatusId;
    @SerializedName("PersonStatus")             private String personStatus;
    @SerializedName("AddressID")                private String addressId;
    @SerializedName("Address_No")               private String addressNo;
    @SerializedName("Address_Moo")              private String addressMoo;
    @SerializedName("Address_Alley")            private String addressAlley;
    @SerializedName("Address_Soi")              private String addressSoi;
    @SerializedName("Address_Road")             private String addressRoad;
    @SerializedName("Address_Tumbol")           private String addressTumbol;
    @SerializedName("Address_Amphur")           private String addressAmphur;
    @SerializedName("Address_Province")         private String addressProvince;
    @SerializedName("IssueDate")                private String issueDate;
    @SerializedName("ExpireDate")               private String expireDate;
    @SerializedName("IssuerID")                 private String issuerId;
    @SerializedName("IssuerPlace")              private String issuerPlace;
    @SerializedName("IssuerAgency")             private String issuerAgency;
    @SerializedName("Nationality")              private String nationality;
    @SerializedName("Nationality_Old")          private String nationalityOld;
    @SerializedName("Nationality_ChangeDate")   private String nationalityChangeDate;
    @SerializedName("Father_CitizenID")         private String fatherCitizenId;
    @SerializedName("Father_Nationality")       private String fatherNationality;
    @SerializedName("Father_NameTH_FirstName")  private String fatherTHFirstName;
    @SerializedName("Father_FirstName")         private String fatherFirstName;
    @SerializedName("Mother_CitizenID")         private String motherCitizenId;
    @SerializedName("Mother_Nationality")       private String motherNationality;
    @SerializedName("Mother_NameTH_FirstName")  private String motherTHFirstName;
    @SerializedName("Mother_FirstName")         private String motherFirstName;
    @SerializedName("DomicileType")             private String domicileType;
    @SerializedName("DomicileStatus")           private String domicileStatus;
    @SerializedName("DomicileDate")             private String domicileDate;

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getEngPreName() {
        return engPreName;
    }

    public void setEngPreName(String engPreName) {
        this.engPreName = engPreName;
    }

    public String getEngFirstName() {
        return engFirstName;
    }

    public void setEngFirstName(String engFirstName) {
        this.engFirstName = engFirstName;
    }

    public String getEngMiddleName() {
        return engMiddleName;
    }

    public void setEngMiddleName(String engMiddleName) {
        this.engMiddleName = engMiddleName;
    }

    public String getEngLastName() {
        return engLastName;
    }

    public void setEngLastName(String engLastName) {
        this.engLastName = engLastName;
    }

    public String getThPreName() {
        return thPreName;
    }

    public void setThPreName(String thPreName) {
        this.thPreName = thPreName;
    }

    public String getThFirstName() {
        return thFirstName;
    }

    public void setThFirstName(String thFirstName) {
        this.thFirstName = thFirstName;
    }

    public String getThMiddleName() {
        return thMiddleName;
    }

    public void setThMiddleName(String thMiddleName) {
        this.thMiddleName = thMiddleName;
    }

    public String getThLastName() {
        return thLastName;
    }

    public void setThLastName(String thLastName) {
        this.thLastName = thLastName;
    }

    public int getSexId() {
        return sexId;
    }

    public void setSexId(int sexId) {
        this.sexId = sexId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getPersonStatusId() {
        return personStatusId;
    }

    public void setPersonStatusId(int personStatusId) {
        this.personStatusId = personStatusId;
    }

    public String getPersonStatus() {
        return personStatus;
    }

    public void setPersonStatus(String personStatus) {
        this.personStatus = personStatus;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getAddressNo() {
        return addressNo;
    }

    public void setAddressNo(String addressNo) {
        this.addressNo = addressNo;
    }

    public String getAddressMoo() {
        return addressMoo;
    }

    public void setAddressMoo(String addressMoo) {
        this.addressMoo = addressMoo;
    }

    public String getAddressAlley() {
        return addressAlley;
    }

    public void setAddressAlley(String addressAlley) {
        this.addressAlley = addressAlley;
    }

    public String getAddressSoi() {
        return addressSoi;
    }

    public void setAddressSoi(String addressSoi) {
        this.addressSoi = addressSoi;
    }

    public String getAddressRoad() {
        return addressRoad;
    }

    public void setAddressRoad(String addressRoad) {
        this.addressRoad = addressRoad;
    }

    public String getAddressTumbol() {
        return addressTumbol;
    }

    public void setAddressTumbol(String addressTumbol) {
        this.addressTumbol = addressTumbol;
    }

    public String getAddressAmphur() {
        return addressAmphur;
    }

    public void setAddressAmphur(String addressAmphur) {
        this.addressAmphur = addressAmphur;
    }

    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getIssuerId() {
        return issuerId;
    }

    public void setIssuerId(String issuerId) {
        this.issuerId = issuerId;
    }

    public String getIssuerPlace() {
        return issuerPlace;
    }

    public void setIssuerPlace(String issuerPlace) {
        this.issuerPlace = issuerPlace;
    }

    public String getIssuerAgency() {
        return issuerAgency;
    }

    public void setIssuerAgency(String issuerAgency) {
        this.issuerAgency = issuerAgency;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNationalityOld() {
        return nationalityOld;
    }

    public void setNationalityOld(String nationalityOld) {
        this.nationalityOld = nationalityOld;
    }

    public String getNationalityChangeDate() {
        return nationalityChangeDate;
    }

    public void setNationalityChangeDate(String nationalityChangeDate) {
        this.nationalityChangeDate = nationalityChangeDate;
    }

    public String getFatherCitizenId() {
        return fatherCitizenId;
    }

    public void setFatherCitizenId(String fatherCitizenId) {
        this.fatherCitizenId = fatherCitizenId;
    }

    public String getFatherNationality() {
        return fatherNationality;
    }

    public void setFatherNationality(String fatherNationality) {
        this.fatherNationality = fatherNationality;
    }

    public String getFatherTHFirstName() {
        return fatherTHFirstName;
    }

    public void setFatherTHFirstName(String fatherTHFirstName) {
        this.fatherTHFirstName = fatherTHFirstName;
    }

    public String getFatherFirstName() {
        return fatherFirstName;
    }

    public void setFatherFirstName(String fatherFirstName) {
        this.fatherFirstName = fatherFirstName;
    }

    public String getMotherCitizenId() {
        return motherCitizenId;
    }

    public void setMotherCitizenId(String motherCitizenId) {
        this.motherCitizenId = motherCitizenId;
    }

    public String getMotherNationality() {
        return motherNationality;
    }

    public void setMotherNationality(String motherNationality) {
        this.motherNationality = motherNationality;
    }

    public String getMotherTHFirstName() {
        return motherTHFirstName;
    }

    public void setMotherTHFirstName(String motherTHFirstName) {
        this.motherTHFirstName = motherTHFirstName;
    }

    public String getMotherFirstName() {
        return motherFirstName;
    }

    public void setMotherFirstName(String motherFirstName) {
        this.motherFirstName = motherFirstName;
    }

    public String getDomicileType() {
        return domicileType;
    }

    public void setDomicileType(String domicileType) {
        this.domicileType = domicileType;
    }

    public String getDomicileStatus() {
        return domicileStatus;
    }

    public void setDomicileStatus(String domicileStatus) {
        this.domicileStatus = domicileStatus;
    }

    public String getDomicileDate() {
        return domicileDate;
    }

    public void setDomicileDate(String domicileDate) {
        this.domicileDate = domicileDate;
    }
}
