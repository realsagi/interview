package in.th.tamis.faarm.models.faarm.parcel;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParcelType {

    @SerializedName("parcel_type")
    List<JsonObject> parcelType;

    public List<JsonObject> getParcelType() {
        return parcelType;
    }

    public void setParcelType(List<JsonObject> parcelType) {
        this.parcelType = parcelType;
    }
}
