package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.models.faarm.FarmersOrganization;
import in.th.tamis.faarm.models.faarm.MemberDOAE;
import in.th.tamis.faarm.models.faarm.Person;
import in.th.tamis.faarm.models.faarm.datafromandroid.Farmer;

import java.util.List;

public interface MemberDAO {
    MemberDOAE selectMemberByCitizenId(String citizenId);
    MemberDOAE selectAddressByIdCardHouseMember(String citizenId);


}
