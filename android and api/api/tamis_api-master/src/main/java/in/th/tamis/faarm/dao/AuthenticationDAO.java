package in.th.tamis.faarm.dao;

import in.th.tamis.faarm.models.login.MFUser;
import in.th.tamis.faarm.models.login.UserLogin;

import java.util.Calendar;

public interface AuthenticationDAO {
    MFUser selectUserLogin(String userName, String passWord);
    boolean deleteUserLoginByToken(String token);
    boolean saveUserLogin(UserLogin userLogin);
    String selectUserLoginByToken(String token);
    String selectTokenByUserNameOnUserLogin(String username);

    void saveLog(int userId);
}
