package in.th.tamis.faarm.services;

import in.th.tamis.faarm.models.faarm.Address;
import in.th.tamis.faarm.models.faarm.PersonalProfile;
import in.th.tamis.faarm.services.PersonalProfileService;

import java.io.IOException;

public class PersonalFAARMProfileStub implements PersonalProfileService {
    @Override
    public PersonalProfile findByCitizenId(String citizenId) throws IOException {
        PersonalProfile personalProfile = new PersonalProfile();

        personalProfile.setPreName("นาย");
        personalProfile.setFirstName("กิตติมศักดิ์");
        personalProfile.setLastName("ศิวารัตน์");
        personalProfile.setSexId(1);
        personalProfile.setSex("ชาย");
        personalProfile.setBirthDate("24910909");
        personalProfile.setAge("67");
        personalProfile.setPersonStatusId(1);
        personalProfile.setPersonStatus("มีชีวิตอยู่");

        Address address = new Address();
        address.setAddressID("12345678910");
        address.setHouseNo("345/5");
        address.setMoo("8");
        address.setAlley("ตรอกข้าวสาร");
        address.setSoi("ซอยสุคนธวิท 44");
        address.setRoad("ถนนสุคนธวิท");
        address.setSubDistrict("ตำบลตลาดกระทุ่มแบน");
        address.setDistrict("อำเภอกระทุ่มแบน");
        address.setProvince("จังหวัดสมุทรสาคร");

        personalProfile.setAddress(address);

        return personalProfile;
    }
}
