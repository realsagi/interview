package in.th.tamis.faarm.services;

import in.th.tamis.faarm.models.faarm.FarmersOrganization;
import in.th.tamis.faarm.models.faarm.agriculture.Breed;
import in.th.tamis.faarm.models.faarm.agriculture.Detail;
import in.th.tamis.faarm.models.faarm.agriculture.Type;

public interface Agriculture {
    FarmersOrganization getFarmersOrganization();
    Type getType();
    Detail getDetailAll();
    Detail getDetail(String typeCode);
    Breed getBreedAll();
    Breed getBreed(String detailCode);
}
