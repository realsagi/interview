package in.th.tamis.faarm.controller;

import com.google.gson.Gson;
import in.th.tamis.faarm.services.NewMember;
import in.th.tamis.faarm.models.faarm.datafromandroid.AllData;
import in.th.tamis.faarm.models.faarm.datafromandroid.ImagesFarmer;
import in.th.tamis.faarm.services.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


public class NewMemberController extends HttpServlet {
    NewMember newMemberService;

    public NewMemberController(NewMember newMemberService) {
        this.newMemberService = newMemberService;
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_OK);
        Gson gson = new Gson();
        BufferedReader reader = request.getReader();
        if("/add_new_farmer".equals(request.getRequestURI())){
            AllData data = gson.fromJson(reader, AllData.class);
            int profileCenterId = newMemberService.createNewMember(data.getFarmer(),data.getUserId());
            newMemberService.addNewHouseholdMember(profileCenterId, data.getHouseHoldMembers().getPerson(),data.getFarmer().getCitizenId(),data.getUserId());
            newMemberService.addDetailPlantation(profileCenterId,data.getRegisterPlantation().getDetailRegisterPlantation(),data.getFarmer().getCitizenId(),data.getUserId());
        }
        if("/upload_image".equals(request.getRequestURI())){
            ImagesFarmer imagesFarmer = gson.fromJson(reader, ImagesFarmer.class);
            Upload uploadService = new UploadService();
            String imagePath = uploadService.genPath(imagesFarmer);
            System.out.println(imagePath);
            uploadService.writeImageFromByte(imagesFarmer.getImages(),imagePath);
        }
    }
}
