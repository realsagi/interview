package th.in.tamis.models;

import com.google.gson.annotations.SerializedName;

public class ImagesFarmer {
    @SerializedName("citizen_id")
    private  String citizenId;
    @SerializedName("file_name")
    private String filename;
    @SerializedName("image")
    private byte[] images;

    public ImagesFarmer(String filename, String citizenId, byte[] images) {
        this.filename = filename;
        this.images = images;
        this.citizenId = citizenId;
    }
}
