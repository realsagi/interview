package th.in.tamis.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class District {
    @SerializedName("district")
    List<JsonObject> districts;

    public List<JsonObject> getDistricts() {
        return districts;
    }

    public void setDistricts(List<JsonObject> districts) {
        this.districts = districts;
    }
}
