package th.in.tamis.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AgricultureBreed {

    @SerializedName("breed")
    private List<JsonObject> breed;

    public List<JsonObject> getAgricultureBreed() {
        return breed;
    }

    public void setAgricultureBreed(List<JsonObject> breed) {
        this.breed = breed;
    }
}
