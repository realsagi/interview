package th.in.tamis.models;

import java.io.File;
import java.util.List;

public class DetailRegisterPlantation {
    private String licenseType;
    private String licenseNumber;
    private String raWangNumber;
    private int rai;
    private int ngan;
    private int squareWah;
    private String corporateOwnershipLicense;
    private Address address;
    private String irrigationArea;
    private String holding;
    private List<Coordinates> coordinates;
    private Coordinates coordinatesCenter;
    private List<DetailAgriculturalOperations> detailAgriculturalOperationses;
    private File fileSnapShotMap;
    private File imageDetailPlantation;
    private ParcelDoc parcelDoc;
    private String fileNameImagePlantation;

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getRaWangNumber() {
        return raWangNumber;
    }

    public void setRaWangNumber(String raWangNumber) {
        this.raWangNumber = raWangNumber;
    }

    public int getRai() {
        return rai;
    }

    public void setRai(int rai) {
        this.rai = rai;
    }

    public int getNgan() {
        return ngan;
    }

    public void setNgan(int ngan) {
        this.ngan = ngan;
    }

    public int getSquareWah() {
        return squareWah;
    }

    public void setSquareWah(Double squareWah) {
        this.squareWah = squareWah.intValue();
    }

    public String getCorporateOwnershipLicense() {
        return corporateOwnershipLicense;
    }

    public void setCorporateOwnershipLicense(String corporateOwnershipLicense) {
        this.corporateOwnershipLicense = corporateOwnershipLicense;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getIrrigationArea() {
        return irrigationArea;
    }

    public void setIrrigationArea(String irrigationArea) {
        this.irrigationArea = irrigationArea;
    }

    public String getHolding() {
        return holding;
    }

    public void setHolding(String holding) {
        this.holding = holding;
    }

    public List<Coordinates> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Coordinates> coordinates) {
        this.coordinates = coordinates;
    }

    public List<DetailAgriculturalOperations> getDetailAgriculturalOperationses() {
        return detailAgriculturalOperationses;
    }

    public void setDetailAgriculturalOperations(List<DetailAgriculturalOperations> detailAgriculturalOperationses) {
        this.detailAgriculturalOperationses = detailAgriculturalOperationses;
    }

    public File getFileSnapShotMap() {
        return fileSnapShotMap;
    }

    public void setFileSnapShotMap(File fileSnapShotMap) {
        this.fileSnapShotMap = fileSnapShotMap;
    }

    public File getImageDetailPlantation() {
        return imageDetailPlantation;
    }

    public void setImageDetailPlantation(File imageDetailPlantation) {
        this.imageDetailPlantation = imageDetailPlantation;
    }

    public Coordinates getCoordinatesCenter() {
        return coordinatesCenter;
    }

    public void setCoordinatesCenter(Coordinates coordinatesCenter) {
        this.coordinatesCenter = coordinatesCenter;
    }

    public ParcelDoc getParcelDoc() {
        return parcelDoc;
    }

    public void setParcelDoc(ParcelDoc parcelDoc) {
        this.parcelDoc = parcelDoc;
    }

    public void setFileNameImagePlantation(String fileNameImagePlantation) {
        this.fileNameImagePlantation = fileNameImagePlantation;
    }
}
