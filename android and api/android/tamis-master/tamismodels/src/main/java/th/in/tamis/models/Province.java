package th.in.tamis.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Province {
    @SerializedName("province")
    List<JsonObject> provinces;

    public List<JsonObject> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<JsonObject> provinces) {
        this.provinces = provinces;
    }
}
