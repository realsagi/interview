package th.in.tamis.models;

public class AllData {
    private Farmer farmer;
    private HouseHoldMember householdMember;
    private RegisterPlantation registerPlantation;

    public int getUserId() {
        return userId;
    }

    private int userId;

    public AllData(Farmer farmer, HouseHoldMember householdMembers, RegisterPlantation registerPlantation,int userId) {
        this.farmer = farmer;
        this.householdMember = householdMembers;
        this.registerPlantation = registerPlantation;
        this.userId = userId;
    }


    public Farmer getFarmer() {
        return farmer;
    }

    public HouseHoldMember getHouseholdMember() {
        return householdMember;
    }

    public RegisterPlantation getRegisterPlantation() {
        return registerPlantation;
    }
}
