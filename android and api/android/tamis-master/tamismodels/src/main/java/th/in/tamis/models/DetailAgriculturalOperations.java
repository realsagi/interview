package th.in.tamis.models;

import java.io.File;
import java.util.List;

public class DetailAgriculturalOperations {

    private String namePlants;
    private String breeds;
    private String details;
    private String categoryPlants;
    private long plantingDate;
    private long harvestDate;
    private int plantedRai;
    private int plantedNgan;
    private int plantedSquareWah;
    private int harvestedRai;
    private int harvestedNgan;
    private int harvestedSquareWah;
    private double estimatedProductivity;
    private File fileSnapShotMapAgriculture;
    private File imageDetailAgriculture;
    private List<Coordinates> coordinates;
    private String fileNameImageActivity;

    public String getNamePlants() {
        return namePlants;
    }

    public void setNamePlants(String namePlants) {
        this.namePlants = namePlants;
    }

    public String getBreeds() {
        return breeds;
    }

    public void setBreeds(String breeds) {
        this.breeds = breeds;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCategoryPlants() {
        return categoryPlants;
    }

    public void setCategoryPlants(String categoryPlants) {
        this.categoryPlants = categoryPlants;
    }

    public long getPlantingDate() {
        return plantingDate;
    }

    public void setPlantingDate(long plantingDate) {
        this.plantingDate = plantingDate;
    }

    public long getHarvestDate() {
        return harvestDate;
    }

    public void setHarvestDate(long harvestDate) {
        this.harvestDate = harvestDate;
    }

    public int getPlantedRai() {
        return plantedRai;
    }

    public void setPlantedRai(int plantedRai) {
        this.plantedRai = plantedRai;
    }

    public int getPlantedNgan() {
        return plantedNgan;
    }

    public void setPlantedNgan(int plantedNgan) {
        this.plantedNgan = plantedNgan;
    }

    public int getPlantedSquareWah() {
        return plantedSquareWah;
    }

    public void setPlantedSquareWah(int plantedSquareWah) {
        this.plantedSquareWah = plantedSquareWah;
    }

    public int getHarvestedRai() {
        return harvestedRai;
    }

    public void setHarvestedRai(int harvestedRai) {
        this.harvestedRai = harvestedRai;
    }

    public int getHarvestedNgan() {
        return harvestedNgan;
    }

    public void setHarvestedNgan(int harvestedNgan) {
        this.harvestedNgan = harvestedNgan;
    }

    public int getHarvestedSquareWah() {
        return harvestedSquareWah;
    }

    public void setHarvestedSquareWah(int harvestedSquareWah) {
        this.harvestedSquareWah = harvestedSquareWah;
    }

    public double getEstimatedProductivity() {
        return estimatedProductivity;
    }

    public void setEstimatedProductivity(double estimatedProductivity) {
        this.estimatedProductivity = estimatedProductivity;
    }

    public File getFileSnapShotMapAgriculture() {
        return fileSnapShotMapAgriculture;
    }

    public void setFileSnapShotMapAgriculture(File fileSnapShotMapAgriculture) {
        this.fileSnapShotMapAgriculture = fileSnapShotMapAgriculture;
    }

    public List<Coordinates> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Coordinates> coordinates) {
        this.coordinates = coordinates;
    }

    public File getImageDetailAgriculture() {
        return imageDetailAgriculture;
    }

    public void setImageDetailAgriculture(File imageDetailAgriculture) {
        this.imageDetailAgriculture = imageDetailAgriculture;
    }

    public void setFileNameImageActivity(String fileNameImageActivity) {
        this.fileNameImageActivity = fileNameImageActivity;
    }
}
