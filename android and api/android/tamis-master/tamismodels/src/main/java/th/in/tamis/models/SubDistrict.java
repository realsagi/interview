package th.in.tamis.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubDistrict {

    @SerializedName("sub_district")
    List<JsonObject> subDistricts;

    public List<JsonObject> getSubDistricts() {
        return subDistricts;
    }

    public void setSubDistricts(List<JsonObject> subDistricts) {
        this.subDistricts = subDistricts;
    }
}
