package th.in.tamis.models;

import com.google.gson.annotations.SerializedName;

public class Person {
    @SerializedName("citizen_id")
    private String citizenId;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("sex")
    private String sex;

    @SerializedName("farmersOrganization")
    private FarmersOrganization farmersOrganization;

    @SerializedName("statusHelpWork")
    private boolean statusHelpWork;

    public boolean isStatusHelpWork() {
        return statusHelpWork;
    }

    public void setStatusHelpWork(boolean statusHelpWork) {
        this.statusHelpWork = statusHelpWork;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public FarmersOrganization getFarmersOrganization() {
        return farmersOrganization;
    }

    public void setFarmersOrganization(FarmersOrganization farmersOrganization) {
        this.farmersOrganization = farmersOrganization;
    }
}
