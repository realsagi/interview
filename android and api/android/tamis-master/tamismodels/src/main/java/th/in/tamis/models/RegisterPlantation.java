package th.in.tamis.models;

import java.util.List;

public class RegisterPlantation {

    private List<DetailRegisterPlantation> detailRegisterPlantation;

    public List<DetailRegisterPlantation> getDetailRegisterPlantation() {
        return detailRegisterPlantation;
    }

    public void setDetailRegisterPlantation(List<DetailRegisterPlantation> detailRegisterPlantation) {
        this.detailRegisterPlantation = detailRegisterPlantation;
    }
}
