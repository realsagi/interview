package th.in.tamis.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AgricultureType {

    @SerializedName("type_name")
    private List<JsonObject> typeName;

    public List<JsonObject> getListOfAgricultureType() {
        return typeName;
    }

    public void setListOfAgricultureType(List<JsonObject> typeName) {
        this.typeName = typeName;
    }
}
