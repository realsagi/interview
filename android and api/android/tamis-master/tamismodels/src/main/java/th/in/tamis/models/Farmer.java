package th.in.tamis.models;

import com.google.gson.annotations.SerializedName;

import java.io.File;

public class Farmer {

    private byte[] imagePerson;
    private File fileImageProFile;

    @SerializedName("pre_name")
    private String preName;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("sex_id")
    private int sexId;

    @SerializedName("birth_date")
    private String birthDate;

    @SerializedName("age")
    private String age;

    @SerializedName("person_status_id")
    private int personStatusId;

    @SerializedName("person_status")
    private String personStatus;

    @SerializedName("citizen_id")
    private String citizenId;

    @SerializedName("sex")
    private String sex;

    private String mobilePhone;
    private String homePhone;
    private int status;

    @SerializedName("address")
    private Address address;

    private String careerMain;
    private String careerMinor;
    private String nameImageProfile;

    private String nameImageSign;

    public String getCareerMain() {
        return careerMain;
    }

    public void setCareerMain(String careerMain) {
        this.careerMain = careerMain;
    }

    public String getCareerMinor() {
        return careerMinor;
    }

    public void setCareerMinor(String careerMinor) {
        this.careerMinor = careerMinor;
    }

    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName = preName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }


    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public byte[] getImagePerson() {
        return imagePerson;
    }

    public void setImagePerson(byte[] imagePerson) {
        this.imagePerson = imagePerson;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSexId() {
        return sexId;
    }

    public void setSexId(int sexId) {
        this.sexId = sexId;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getPersonStatusId() {
        return personStatusId;
    }

    public void setPersonStatusId(int personStatusId) {
        this.personStatusId = personStatusId;
    }

    public String getPersonStatus() {
        return personStatus;
    }

    public void setPersonStatus(String personStatus) {
        this.personStatus = personStatus;
    }

    public File getFileImageProFile() {
        return fileImageProFile;
    }

    public void setFileImageProFile(File fileImageProFile) {
        this.fileImageProFile = fileImageProFile;
    }

    public void setNameImageProfile(String nameImageProfile) {
        this.nameImageProfile = nameImageProfile;
    }

    public void setNameImageSign(String nameImageSign) {
        this.nameImageSign = nameImageSign;
    }
}