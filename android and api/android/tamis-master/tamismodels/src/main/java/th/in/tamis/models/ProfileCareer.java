package th.in.tamis.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileCareer {
    @SerializedName("career")
    List<JsonObject> career;

    public List<JsonObject> getCareer() {
        return career;
    }

    public void setCareer(List<JsonObject> career) {
        this.career = career;
    }
}
