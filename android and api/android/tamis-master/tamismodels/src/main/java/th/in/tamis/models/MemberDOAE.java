package th.in.tamis.models;

import com.google.gson.annotations.SerializedName;

public class MemberDOAE {

    @SerializedName("citizen_id") String citizenId;

    @SerializedName("pre_name") String preName;

    @SerializedName("first_name") String firstName;

    @SerializedName("last_name") String lastName;

    @SerializedName("member_no") String memberNo;

    @SerializedName("household_number") String houseHoldNumber;

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName = preName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getHouseHoldNumber() {
        return houseHoldNumber;
    }

    public void setHouseHoldNumber(String houseHoldNumber) {
        this.houseHoldNumber = houseHoldNumber;
    }
}
