package th.in.tamis.success;


import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import th.in.tamis.activity.MainActivity;
import th.in.tamis.tamis.R;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class HouseholdRegistrationActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivity = new ActivityTestRule<>(MainActivity.class);

    ViewAction viewAction = new ViewAction() {
        @Override
        public Matcher<View> getConstraints() {
            return null;
        }

        @Override
        public String getDescription() {
            return null;
        }

        @Override
        public void perform(UiController uiController, View view) {
            View button = view.findViewById(R.id.buttonAddDetailHousehold);
            button.performClick();
        }
    };

    @Before
    public void setUp() throws Exception {
        onView(withId(R.id.buttonReadFromUsb)).perform(click());
        sleep(1000);
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(scrollTo());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(click());
        onView(withText(R.string.agree)).perform(click());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(scrollTo());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(click());
    }

    @Test
    public void addHouseholdMemberHelpFramer() {
        onView(withId(R.id.etAddressID)).check(matches(withText("74980061109")));
        onView(withId(R.id.rvHouseholdMemberRecyclerView)).perform(
                RecyclerViewActions.actionOnItemAtPosition(1, viewAction));
        sleep(500);
        onView(withText("เป็นสมาชิกองค์กร")).check(matches(isDisplayed()));
        onView(withId(R.id.rbHelp)).perform(click());
        onView(withText("สหกรณ์ภาคการเกษตร")).perform(click());
        onView(withId(R.id.btnSave)).perform(scrollTo(), click());
        onView(withText("ช่วยทำการเกษตร")).check(matches(isDisplayed()));
        onView(withId(R.id.rvHouseholdMemberRecyclerView)).perform(
                RecyclerViewActions.actionOnItemAtPosition(1, viewAction));

        onView(withId(R.id.rbHelp)).check(matches(isChecked()));
        onView(withText("สหกรณ์ภาคการเกษตร")).check(matches(isChecked()));
    }

}


