package th.in.tamis.success;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import th.in.tamis.activity.MainActivity;
import th.in.tamis.tamis.R;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class ConfirmRegisterFarmerActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivity = new ActivityTestRule<>(MainActivity.class);


    @Before
    public void goToPage() throws Exception {
        onView(ViewMatchers.withId(R.id.buttonReadFromUsb)).perform(click());
        sleep(1000);
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(scrollTo());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(click());
        onView(withText(R.string.agree)).check(matches(isDisplayed()));
        onView(withText(R.string.agree)).perform(click());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(scrollTo());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(click());
        onView(withId(R.id.btnSaveDataFarmer)).perform(click());
        onView(withId(R.id.btnVerifyInformationAndSave)).perform(click());
    }

    @Test
    public void registerSuccess() {
        onView(withText("ลงชื่อ")).check(matches(isDisplayed()));
        onView(withId(R.id.buttonSave)).perform(click());
        onView(withText("รับรองข้อมูล")).check(matches(isDisplayed()));
        onView(withId(R.id.btnSend)).perform(click());
        onView(withText("ขึ้นทะเบียนเกษตรกร")).check(matches(isDisplayed()));
    }
}

