package th.in.tamis.success;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.view.View;
import android.widget.DatePicker;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import th.in.tamis.activity.MainActivity;
import th.in.tamis.tamis.R;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.containsString;

public class DetailRegisterAgriculturalOperationsActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivity = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void goToPage() throws Exception {
        onView(withId(R.id.buttonReadFromUsb)).perform(click());
        sleep(1000);
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(scrollTo(), click());
        onView(withText(R.string.agree)).check(matches(isDisplayed()));
        onView(withText(R.string.agree)).perform(click());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(scrollTo());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(click());
        onView(withId(R.id.btnSaveDataFarmer)).perform(click());
        onView(withText("ขึ้นทะเบียนแปลงเพาะปลูก")).check(matches(isDisplayed()));
        onView(withId(R.id.btnAddRegisterPlantation)).perform(click());
        onView(withText("เพิ่มข้อมูลแปลง")).check(matches(isDisplayed()));
        onView(withId(R.id.spinnerParcelType)).check(matches(withSpinnerText(containsString("\"ส.ป.ก./สร 5ก\""))));
        onView(withId(R.id.spinnerParcelType)).perform(click());
        onView(withText("น.ส.5")).perform(click());
        onView(withId(R.id.spinnerParcelType)).perform(click());
        onView(withText("ส.ธ.1")).perform(click());
        onView(withId(R.id.spinnerParcelType)).perform(click());
        onView(withText("อื่นๆ")).perform(click());
        onView(withId(R.id.spinnerParcelType)).perform(click());
        onView(withText("โฉนด/น.ส.4")).perform(click());
        onView(withId(R.id.spinnerParcelType)).check(matches(withSpinnerText(containsString("โฉนด/น.ส.4"))));
        onView(withText("ตรวจสอบ")).check(matches(isDisplayed()));
        onView(withId(R.id.editTextMoo)).perform(typeText("1"));
        onView(withId(R.id.spinnerProvince)).perform(scrollTo(),click());
        onView(withText("ขอนแก่น")).perform(click());
        onView(withId(R.id.spinnerDistrict)).perform(click());
        onView(withText("พล")).perform(click());
        onView(withId(R.id.spinnerDistrict)).perform(click());
        onView(withText("หนองเรือ")).perform(click());
        onView(withId(R.id.spinnerDistrict)).perform(click());
        onView(withText("เมืองขอนแก่น")).perform(click());
        onView(withId(R.id.spinnerSubDistrict)).perform(click());
        onView(withText("พระลับ")).perform(click());
        onView(withId(R.id.spinnerSubDistrict)).perform(click());
        onView(withText("โคกสี")).perform(click());

        onView(withId(R.id.editTextLicenseNo)).perform(typeText("123456789"));
        onView(withId(R.id.editTextRaWangNumber)).perform(typeText("987654321"));
        onView(withId(R.id.editTextRai)).perform(scrollTo(),click(), clearText(), typeText("3"));
        onView(withId(R.id.editTextNgan)).perform(scrollTo(),click(), clearText(), typeText("4"));
        onView(withId(R.id.editTextSquareWah)).perform(click(), clearText(), typeText("5"));
        onView(withText("นายกิตติมศักดิ์ ศิวารัตน์")).perform(click());
        onView(withId(R.id.rdbInside)).perform(scrollTo());
        onView(withId(R.id.rdbInside)).perform(click());
        onView(withId(R.id.buttonSaveDetail)).perform(scrollTo());
        onView(withId(R.id.buttonSaveDetail)).perform(click());
    }

    @Test
    public void addNewDetailAgriculturalOperations() {
        onView(withId(R.id.rvRegisterPlantation)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, new ViewAction() {
                    @Override
                    public Matcher<View> getConstraints() {
                        return null;
                    }

                    @Override
                    public String getDescription() {
                        return null;
                    }

                    @Override
                    public void perform(UiController uiController, View view) {
                        View button = view.findViewById(R.id.btnAgricultureBusinessInformation);
                        button.performClick();
                    }
                }));
        onView(withText("การประกอบกิจกรรมการเกษตร")).check(matches(isDisplayed()));
        onView(withId(R.id.btnAddAgricultural)).perform(click());
        onView(withId(R.id.spinnerNamePlants)).perform(click());
        onView(withText("ข้าว")).perform(click());
        onView(withId(R.id.spinnerSpecies)).perform(click());
        onView(withText("ข้าวเหนียว")).perform(click());
        onView(withId(R.id.editTextPlantingDate)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2015, 11, 15));
        onView(withText(R.string.ok)).perform(click());
        onView(withId(R.id.buttonCalculateHarvestDate)).perform(click());
        onView(withId(R.id.editTextDaysHarvest)).perform(click(), clearText(),typeText("10"));
        onView(withId(R.id.buttonDaysHarvest)).perform(click());
        onView(withId(R.id.editTextHarvestDate)).check(matches(withText("25-11-2015")));

        onView(withId(R.id.editTextPlantingRai)).perform(typeText("5"));
        closeSoftKeyboard();
        onView(withId(R.id.editTextHarvestRai)).perform(typeText("5"));
        closeSoftKeyboard();
        onView(withId(R.id.buttonSaveDetail)).perform(scrollTo());
        onView(withId(R.id.buttonSaveDetail)).perform(click());
/*TODO
    ยัง เช็คข้อมูลส่วนของ card view ไม่ได้
  */
    }
}

