package th.in.tamis.success;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import th.in.tamis.activity.MainActivity;
import th.in.tamis.tamis.R;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.containsString;

@RunWith(AndroidJUnit4.class)
public class RegisterFarmerActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivity = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() throws Exception {
        onView(ViewMatchers.withId(R.id.buttonReadFromUsb)).perform(click());
        sleep(1000);
    }

    @Test
    public void foundProfile() {
        onView(withId(R.id.editTextPrefix)).check(matches(withText("นาย")));
        onView(withId(R.id.editTextFirstName)).check(matches(withText("กิตติมศักดิ์")));
        onView(withId(R.id.editTextLastName)).check(matches(withText("ศิวารัตน์")));

        onView(withId(R.id.editTextCitizenID)).check(matches(withText("2520100011499")));

        onView(withId(R.id.editTextDayBirthday)).check(matches(withText("07")));
        onView(withId(R.id.editTextMonthBirthday)).check(matches(withText("มีนาคม")));
        onView(withId(R.id.editTextYearBirthday)).check(matches(withText("2529")));

        onView(withId(R.id.spnStatus)).check(matches(withSpinnerText(containsString("โสด"))));
        onView(withId(R.id.editTextMobilePhone)).check(matches(withText("")));
        onView(withId(R.id.editTextHomePhone)).check(matches(withText("")));
    }

    @Test
    public void foundAddressDetail() {
        onView(withId(R.id.editTextAddressNo)).check(matches(withText("345/5")));
        onView(withId(R.id.editTextMoo)).check(matches(withText("8")));
        onView(withId(R.id.editTextAlley)).check(matches(withText("ข้าวสาร")));
        onView(withId(R.id.editTextSoi)).check(matches(withText("สุคนธวิท 44")));
        onView(withId(R.id.editTextRoad)).check(matches(withText("สุคนธวิท")));
        onView(withId(R.id.editTextSubDistrict)).check(matches(withText("ตลาดกระทุ่มแบน")));
        onView(withId(R.id.editTextDistrict)).check(matches(withText("กระทุ่มแบน")));
        onView(withId(R.id.editTextProvince)).check(matches(withText("สมุทรสาคร")));
        onView(withId(R.id.editTextZipCode)).check(matches(withText("")));
    }

    @Test
    public void foundChangeStatus() {
        onView(withId(R.id.spnStatus)).check(matches(withSpinnerText(containsString("โสด"))));
        onView(withId(R.id.spnStatus)).perform(click());
        onView(withText("หย่าร้าง")).perform(click());
        onView(withId(R.id.spnStatus)).check(matches(withSpinnerText(containsString("หย่าร้าง"))));
    }

    @Test
    public void insertPutMobilePhone() {
        onView(withId(R.id.editTextMobilePhone)).check(matches(withText("")));
        onView(withId(R.id.editTextMobilePhone)).perform(typeText("123456789"));
        onView(withId(R.id.editTextMobilePhone)).check(matches(withText("123456789")));
    }

    @Test
    public void insertHomePhone() {
        onView(withId(R.id.editTextHomePhone)).check(matches(withText("")));
        onView(withId(R.id.editTextHomePhone)).perform(typeText("123456789"));
        onView(withId(R.id.editTextHomePhone)).check(matches(withText("123456789")));
    }

    @Test
    public void insertZipCode() {
        onView(withId(R.id.editTextZipCode)).perform(scrollTo());
        onView(withId(R.id.editTextZipCode)).check(matches(withText("")));
        onView(withId(R.id.editTextZipCode)).perform(typeText("10250"));
        onView(withId(R.id.editTextZipCode)).check(matches(withText("10250")));
    }

    @Test
    public void clickButtonHouseholdRegistration() {
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(scrollTo());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(click());
        onView(withText(R.string.agree)).perform(click());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(scrollTo());
        onView(withId(R.id.buttonGotoHouseholdRegistration)).perform(click());
        onView(withId(R.id.textViewDataFromDOPA)).check(matches(withText("ข้อมูลจากกรมการปกครอง")));
    }
}