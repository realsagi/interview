package th.in.tamis.widget;

public interface IdentityView {
    Identity getIdentity();
}
