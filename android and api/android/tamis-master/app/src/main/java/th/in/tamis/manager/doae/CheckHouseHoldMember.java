package th.in.tamis.manager.doae;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.models.Farmer;
import th.in.tamis.models.MemberDOAE;

public class CheckHouseHoldMember implements Callback<MemberDOAE> {

    private onLoadHouseHoldMember onLoadHouseHoldMember;
    private Farmer farmer;

    public CheckHouseHoldMember(onLoadHouseHoldMember listener, Farmer farmer){
        this.onLoadHouseHoldMember = listener;
        this.farmer = farmer;
    }

    @Override
    public void onResponse(Response<MemberDOAE> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            onLoadHouseHoldMember.onLoadHouseHoldMemberFails("error 404");
        }
        else if(response.isSuccess()){
            onLoadHouseHoldMember.onLoadHouseHoldMemberSuccess(response.body(), farmer);
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadHouseHoldMember.onLoadHouseHoldMemberFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadHouseHoldMember.onLoadHouseHoldMemberFails("timeout");
        }
    }
}
