package th.in.tamis.manager.authentication;

import th.in.tamis.manager.http.HTTPManager;

public class AuthenticationServiceManager {
    public static Authentication getServiceLogin(){
        return HTTPManager.getInstance().getRetrofit().create(Authentication.class);
    }
}
