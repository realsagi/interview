package th.in.tamis.manager.parcel;

import th.in.tamis.manager.http.HTTPManager;

public class ParcelManager {
    public static APIServiceParcel getParcel(){
        return HTTPManager.getInstance().getRetrofit().create(APIServiceParcel.class);
    }
}
