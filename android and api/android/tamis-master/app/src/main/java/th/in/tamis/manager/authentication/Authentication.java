package th.in.tamis.manager.authentication;

import retrofit.Call;
import retrofit.http.POST;
import retrofit.http.Query;

public interface Authentication {

    @POST("/login")
    Call<String> userLogin(
            @Query("user_name") String userName,
            @Query("password") String password
    );

    @POST("/logout")
    Call<Boolean> userLogin(
            @Query("token") String token
    );

    @POST("/login/token")
    Call<String> userLoginByToken(
            @Query("token") String token
    );

}
