package th.in.tamis.service.authentication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import retrofit.Call;
import th.in.tamis.activity.AuthenticationActivity;
import th.in.tamis.activity.MainActivity;
import th.in.tamis.manager.Contextor;
import th.in.tamis.manager.authentication.Authentication;
import th.in.tamis.manager.authentication.AuthenticationServiceManager;
import th.in.tamis.manager.authentication.OnAuthentication;
import th.in.tamis.manager.authentication.TokenLoginManager;
import th.in.tamis.service.PreferencesService;

public class TokenLoginService implements OnAuthentication {

    private Context context;

    public TokenLoginService(Context context){
        this.context = context;
    }

    @Override
    public void connectServerLoginSuccess(String message) {
        String userId = PreferencesService.getStringPreferences("user");
        if(userId == null){
            PreferencesService.saveStringPreferences("user", message);
        }else {
            if(message.contains(userId)){
                PreferencesService.saveStringPreferences("user", message);
            }else {
                PreferencesService.clearAllKey();
                Intent intent = new Intent(context.getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        }
    }

    @Override
    public void connectServerLoginFails(String message) {
        if("".equals(message)) {
            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = Contextor.getInstance().getContext().getSharedPreferences("token", 0);
            editor = settings.edit();
            editor.remove("token").apply();

            Intent intent = new Intent(context.getApplicationContext(), AuthenticationActivity.class);
            context.startActivity(intent);
        }
    }

    public void loginByToken() {

        SharedPreferences settings;
        settings = Contextor.getInstance().getContext().getSharedPreferences("token", 0);
        String token = settings.getString("token", null);

        if(token != null && !"".equals(token)) {
            Authentication authentication = AuthenticationServiceManager.getServiceLogin();
            Call<String> login = authentication.userLoginByToken(token);
            login.enqueue(new TokenLoginManager(this));
        }
    }
}
