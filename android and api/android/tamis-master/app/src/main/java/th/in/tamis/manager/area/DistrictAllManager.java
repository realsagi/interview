package th.in.tamis.manager.area;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.OnLoadCacheAll;
import th.in.tamis.models.District;

public class DistrictAllManager implements Callback<District> {

    private OnLoadCacheAll onLoadAreaAll;

    public DistrictAllManager(OnLoadCacheAll listener) {
        this.onLoadAreaAll = listener;
    }

    @Override
    public void onResponse(Response<District> response, Retrofit retrofit) {
        if (response.isSuccess()) {
            onLoadAreaAll.onLoadDistrictAllSuccess(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadAreaAll.onLoadFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadAreaAll.onLoadFails("timeout");
        }
    }
}
