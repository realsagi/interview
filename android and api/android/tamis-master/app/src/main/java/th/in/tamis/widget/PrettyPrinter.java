package th.in.tamis.widget;

public interface PrettyPrinter {

    String print(String text);
    String separator();
}
