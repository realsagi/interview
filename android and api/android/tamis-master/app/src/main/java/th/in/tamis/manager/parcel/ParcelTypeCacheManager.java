package th.in.tamis.manager.parcel;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.OnLoadCacheAll;
import th.in.tamis.models.ParcelType;

public class ParcelTypeCacheManager implements Callback<ParcelType> {

    private OnLoadCacheAll onLoadAreaAll;

    public ParcelTypeCacheManager(OnLoadCacheAll listener){
        this.onLoadAreaAll = listener;
    }

    @Override
    public void onResponse(Response<ParcelType> response, Retrofit retrofit) {
        if(response.isSuccess()){
            onLoadAreaAll.onLoadParcelTypeAllSuccess(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadAreaAll.onLoadFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadAreaAll.onLoadFails("timeout");
        }
    }
}
