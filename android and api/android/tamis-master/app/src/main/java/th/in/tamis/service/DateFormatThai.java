package th.in.tamis.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateFormatThai {

    private DateFormatThai(){}

    private static DateFormatThai instance;

    public static DateFormatThai getInstance() {
        if (instance == null)
            instance = new DateFormatThai();
        return instance;
    }

    public String getNowThai(Calendar now){

        int day = now.get(Calendar.DATE);
        String month = toMonthString(String.valueOf(now.get(Calendar.MONTH) + 1));
        int year = getYearThai(now);

        return day + " " + month + " " + year;
    }

    private int getYearThai(Calendar now) {
        return now.get(Calendar.YEAR)+543;
    }

    public String toMonthString(String substringMonth) {
        switch (substringMonth){
            case "1":
            case "01":
                substringMonth = "มกราคม";
                break;
            case "2":
            case "02":
                substringMonth = "กุมภาพันธ์";
                break;
            case "3":
            case "03":
                substringMonth = "มีนาคม";
                break;
            case "4":
            case "04":
                substringMonth = "เมษายน";
                break;
            case "5":
            case "05":
                substringMonth = "พฤษภาคม";
                break;
            case "6":
            case "06":
                substringMonth = "มิถุนายน";
                break;
            case "7":
            case "07":
                substringMonth = "กรกฎาคม";
                break;
            case "8":
            case "08":
                substringMonth = "สิงหาคม";
                break;
            case "9":
            case "09":
                substringMonth = "กันยายน";
                break;
            case "10":
                substringMonth = "ตุลาคม";
                break;
            case "11":
                substringMonth = "พฤศจิกายน";
                break;
            case "12":
                substringMonth = "ธันวาคม";
                break;

        }
        return substringMonth;
    }

    public String getCurrentTime() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
        return "เวลา " + simpleDateFormat.format(date) + " น.";
    }
}
