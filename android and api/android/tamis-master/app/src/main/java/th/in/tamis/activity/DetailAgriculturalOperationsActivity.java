package th.in.tamis.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit.Call;
import th.in.tamis.manager.MainBus;
import th.in.tamis.manager.agriculture.APIServiceAgriculture;
import th.in.tamis.manager.agriculture.APIServiceAgriculture.AgricultureListener;
import th.in.tamis.manager.agriculture.AgricultureBreedManager;
import th.in.tamis.manager.agriculture.AgricultureDetailManager;
import th.in.tamis.manager.agriculture.AgricultureManager;
import th.in.tamis.manager.agriculture.AgricultureTypeManager;
import th.in.tamis.models.Address;
import th.in.tamis.models.AgricultureBreed;
import th.in.tamis.models.AgricultureDetail;
import th.in.tamis.models.AgricultureType;
import th.in.tamis.models.Coordinates;
import th.in.tamis.models.DetailAgriculturalOperations;
import th.in.tamis.models.DetailRegisterPlantation;
import th.in.tamis.models.FarmersOrganization;
import th.in.tamis.models.RegisterPlantation;
import th.in.tamis.service.AddressFormat;
import th.in.tamis.service.Area;
import th.in.tamis.service.DeedFormat;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.service.StringWithTag;
import th.in.tamis.service.authentication.TokenLoginService;
import th.in.tamis.service.file.FileManagement;
import th.in.tamis.service.image.Rotate;
import th.in.tamis.service.image.ScaledBitmap;
import th.in.tamis.tamis.R;
import th.in.tamis.widget.AreaPickerDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailAgriculturalOperationsActivity extends AppCompatActivity implements OnClickListener,
        AdapterView.OnItemSelectedListener, TextWatcher, DatePickerDialog.OnDateSetListener,
        DialogInterface.OnClickListener, OnMapReadyCallback, AgricultureListener {

    private EditText editTextLocation;
    private EditText editTextLocationNo;
    private EditText editTextArea;
    private EditText editTextRemainingSpaceArea;

    private TextView textViewRemainingArea;
    private TextView textViewArea;
    private TextView textViewLocation;
    private TextView textViewAgricultural;
    private TextView textViewParcelNumber;

    private EditText editTextPlantingRai;
    private EditText editTextPlantingNgan;
    private EditText editTextPlantingSquareWah;
    private EditText editTextHarvestRai;
    private EditText editTextHarvestNgan;
    private EditText editTextHarvestSquareWah;
    private EditText editTextPlantingDate;
    private EditText editTextHarvestDate;
    private EditText editTextEstimatedProductivity;
    private Button buttonCancelDetail;
    private Button buttonSaveDetail;
    private Spinner spinnerTypeAgriculture;
    private Spinner spinnerDetailAgriculture;
    private Spinner spinnerBreeds;
    private RadioGroup rdCategoryPlants;
    private DetailRegisterPlantation detailRegisterPlantation;
    private DetailAgriculturalOperations detailAgriculturalOperations;
    private String modeWork;
    private int position;
    private int positionDetail;
    private List<DetailAgriculturalOperations> listDetailRegisterPlantation;
    private RegisterPlantation registerPlantation;
    private Area areaOfPlanting;
    private Area areaOfHarvest;
    private Area areaShow;
    private Button buttonCalculatorProductivity;

    private Calendar startCalendar;
    private Calendar endCalendar;
    private ImageButton imageButtonPlantingDate;
    private DatePickerDialog datePickerDialogPlantingDate;
    private TextView textViewTitleDatePicker;
    private Button buttonCalculateHarvestDate;
    private EditText editTextGrowthDays;

    private EditText editTextProductivityPerRai;
    private int REQUEST_GOOGLE_MAP = 1;
    private int REQUEST_CAMERA = 0;
    private LinearLayout linearLayoutOfCategoryPlants;

    private ImageView imageViewErrorTypeAgriculture;
    private ImageView imageViewErrorDetailAgriculture;
    private ImageView imageViewErrorBreedAgriculture;

    private ImageView imageViewErrorPlantationDate;
    private ImageView imageViewErrorHarvestDate;

    private List<StringWithTag> detailList;
    private LinearLayout linearLayoutDetail;

    private List<StringWithTag> breedList;
    private LinearLayout linearLayoutBreed;

    private APIServiceAgriculture serviceAgriculture;

    private Boolean changeSpinnerBreeds = false;
    private Boolean changeSpinnerAgricultureType = false;
    private Boolean changeSpinnerDetail = false;
    private LinearLayout linearLayoutDetailAgriculturalMap;
    private SupportMapFragment supportMapFragment;
    private Button buttonDrawingMap;

    private ImageView imageViewErrorPlantationArea;
    private ImageView imageViewErrorHarvestArea;
    private ImageView imageViewEstimatedProductivity;

    int startDayOfMonth;
    int startMonthOfYears;
    int startYears;

    private RelativeLayout relativeLayoutHarvestDate;
    private LinearLayout linearLayoutHarvestDate;
    private LinearLayout linearLayoutProductivity;
    private RelativeLayout relativeLayoutProductivity;

    private String typeCodeName;
    private String detailCodeName;
    private String breedCodeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_agricultural_operations);
        Calendar calendar = createCalendar();

        int y = calendar.get(Calendar.YEAR) + 543;
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        setDateTimeField(y, m, d);
        setTitleDatePickerDialog();
        initInstance();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainBus.getInstance().register(this);
        new TokenLoginService(this).loginByToken();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainBus.getInstance().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonCancelDetail:
                finish();
                break;
            case R.id.buttonSaveDetail:
                if (validateFields()) {
                    resizeImage();
                    save();
                    finish();
                }
                break;
            case R.id.buttonCalculatorProductivity:
                calculatorProductivity();
                break;
            case R.id.imageButtonPlantingDate:
                datePickerDialogPlantingDate.setCustomTitle(textViewTitleDatePicker);
                datePickerDialogPlantingDate.show();
                datePickerDialogPlantingDate.getButton(DatePickerDialog.BUTTON_POSITIVE)
                        .setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                getResources().getDimensionPixelSize(R.dimen.font_size_input));

                datePickerDialogPlantingDate.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                        .setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                getResources().getDimensionPixelSize(R.dimen.font_size_input));


                break;
            case R.id.editTextPlantingDate:
                datePickerDialogPlantingDate.setCustomTitle(textViewTitleDatePicker);
                datePickerDialogPlantingDate.show();
                datePickerDialogPlantingDate.getButton(DatePickerDialog.BUTTON_POSITIVE)
                        .setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                getResources().getDimensionPixelSize(R.dimen.font_size_input));

                datePickerDialogPlantingDate.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                        .setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                getResources().getDimensionPixelSize(R.dimen.font_size_input));

                break;
            case R.id.editTextHarvestDate:
                if (("").equals(editTextHarvestDate.getText().toString())
                        && !("").equals(editTextPlantingDate.getText().toString())) {
                    createDialogAlertNoTitle(getString(R.string.pleaseClickCalculateProductivity));
                }
                break;
            case R.id.buttonCalculateHarvestDate:
                calculateHarvestDate();
                break;
            case R.id.buttonDrawingMap:
                Intent goToMap = new Intent(v.getContext(), MapActivity.class);
                goToMap.putExtra("parcel", new Gson().toJson(detailRegisterPlantation.getParcelDoc()));
                goToMap.putExtra("area", new Gson().toJson(areaOfPlanting));
                goToMap.putExtra("plantationMain", new Gson().toJson(detailRegisterPlantation.getCoordinates()));
                goToMap.putExtra("coordinateCenter", new Gson().toJson(detailRegisterPlantation.getCoordinatesCenter()));
                goToMap.putExtra("coordinateListOld", new Gson().toJson(detailAgriculturalOperations.getCoordinates()));
                startActivityForResult(goToMap, REQUEST_GOOGLE_MAP);
                break;
            case R.id.buttonCaptureAgricultural:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File fileImage = new FileManagement("AGRICULTURAL_").generatePath();
                detailAgriculturalOperations.setImageDetailAgriculture(fileImage);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(detailAgriculturalOperations.getImageDetailAgriculture()));
                startActivityForResult(Intent.createChooser(intent, "Take a picture with"), REQUEST_CAMERA);
                break;
            case R.id.editTextPlantingRai:
            case R.id.editTextPlantingNgan:
            case R.id.editTextPlantingSquareWah:
                Area areaOfPlanting = new Area(this.areaOfPlanting.getSquareMeter());
                AreaPickerDialog.OnAreaPickListener onDialogPickListenerOfPlanting = new AreaPickerDialog.OnAreaPickListener() {
                    @Override
                    public void onAreaPick(Area areaOfPlanting) {
                        setAreaOfPlanting(areaOfPlanting);
                        setValueAreaObjectByAreaFieldOfPlanting();
                    }

                    @Override
                    public void onCancel() {

                    }
                };
                AreaPickerDialog pickerDialogOfPlanting = new AreaPickerDialog(this, onDialogPickListenerOfPlanting);
                pickerDialogOfPlanting.show(areaOfPlanting);
                break;
            case R.id.editTextHarvestRai:
            case R.id.editTextHarvestNgan:
            case R.id.editTextHarvestSquareWah:
                Area areaOfHarvest = new Area(this.areaOfHarvest.getSquareMeter());
                AreaPickerDialog.OnAreaPickListener onDialogPickListenerOfHarvest = new AreaPickerDialog.OnAreaPickListener() {
                    @Override
                    public void onAreaPick(Area areaOfHarvest) {
                        setAreaOfHarvest(areaOfHarvest);
                        setValueAreaObjectByAreaFieldOfHarvest();
                    }

                    @Override
                    public void onCancel() {

                    }
                };
                AreaPickerDialog pickerDialogOfHarvest = new AreaPickerDialog(this, onDialogPickListenerOfHarvest);
                pickerDialogOfHarvest.show(areaOfHarvest);
                break;
        }
    }

    private void resizeImage() {
        if (detailAgriculturalOperations.getImageDetailAgriculture() != null) {
            try {
                Bitmap bitmap = BitmapFactory.decodeFile(detailAgriculturalOperations.getImageDetailAgriculture().getPath());
                Bitmap resized = Bitmap.createScaledBitmap(bitmap,
                        (int) (bitmap.getWidth() * 0.4),
                        (int) (bitmap.getHeight() * 0.4), true);

                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(detailAgriculturalOperations.getImageDetailAgriculture());
                    resized.compress(Bitmap.CompressFormat.JPEG, 100, out);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                detailAgriculturalOperations.setImageDetailAgriculture(null);
            }
        }
    }

    public void setAreaOfPlanting(Area areaDialogPlanting) {
        if (areaDialogPlanting == null)
            throw new NullPointerException("area must not be null");
        if (areaDialogPlanting.getSquareMeter() > areaShow.getSquareMeter()) {
            this.areaOfPlanting = new Area(0);
            setEmptyAreaOfPlantation();
            createDialogAlertNoTitle(getString(R.string.messageAlertPlantedAreaOverArea));
        } else {
            this.areaOfPlanting = areaDialogPlanting;
            editTextPlantingRai.setText(String.format("%d", areaOfPlanting.getRai()));
            editTextPlantingNgan.setText(String.format("%d", areaOfPlanting.getNgan()));
            editTextPlantingSquareWah.setText(String.format("%d", areaOfPlanting.getSquareWah()));
        }
        clearAreaOfHarvest();
    }

    public void setAreaOfHarvest(Area area) {
        if (area == null)
            throw new NullPointerException("area must not be null");
        if (area.getSquareMeter() > areaOfPlanting.getSquareMeter()) {
            this.areaOfHarvest = new Area(0);
            setEmptyAreaOfHarvest();
            createDialogAlertNoTitle(getString(R.string.messageAlertHarvestedAreaOverPlantedArea));
        } else {
            this.areaOfHarvest = area;
            editTextHarvestRai.setText(String.format("%d", areaOfHarvest.getRai()));
            editTextHarvestNgan.setText(String.format("%d", areaOfHarvest.getNgan()));
            editTextHarvestSquareWah.setText(String.format("%d", areaOfHarvest.getSquareWah()));
        }
        clearEstimatedProductivity();
    }

    private void setEmptyAreaOfPlantation() {
        editTextPlantingRai.setText("");
        editTextPlantingNgan.setText("");
        editTextPlantingSquareWah.setText("");
    }

    private void setEmptyAreaOfHarvest() {
        editTextHarvestRai.setText("");
        editTextHarvestNgan.setText("");
        editTextHarvestSquareWah.setText("");
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (("").equals(editTextPlantingDate.getText().toString())) {
            buttonCalculateHarvestDate.setEnabled(false);
        } else {
            buttonCalculateHarvestDate.setEnabled(true);
        }
        if (("").equals(editTextHarvestRai.getText().toString()) &&
                ("").equals(editTextHarvestNgan.getText().toString()) &&
                ("").equals(editTextHarvestSquareWah.getText().toString())) {
            buttonCalculatorProductivity.setEnabled(false);
        } else {
            buttonCalculatorProductivity.setEnabled(true);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void setValueAreaObjectByAreaFieldOfPlanting() {
        areaOfPlanting.setRai(Integer.valueOf(convertStringEmptyToZero(editTextPlantingRai.getText().toString())));
        areaOfPlanting.setNgan(Integer.valueOf(convertStringEmptyToZero(editTextPlantingNgan.getText().toString())));
        areaOfPlanting.setSquareWah(Integer.valueOf(convertStringEmptyToZero(editTextPlantingSquareWah.getText().toString())));
    }

    private void setValueAreaObjectByAreaFieldOfHarvest() {
        areaOfHarvest.setRai(Integer.valueOf(convertStringEmptyToZero(editTextHarvestRai.getText().toString())));
        areaOfHarvest.setNgan(Integer.valueOf(convertStringEmptyToZero(editTextHarvestNgan.getText().toString())));
        areaOfHarvest.setSquareWah(Integer.valueOf(convertStringEmptyToZero(editTextHarvestSquareWah.getText().toString())));
    }

    private String convertStringEmptyToZero(String valueText) {
        if (!valueText.equals("")) {
            return valueText;
        }
        return "0";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            Bitmap bitmap = new ScaledBitmap().getScaledBitmapFullImage(
                    createBitMapFromFile(detailAgriculturalOperations.getImageDetailAgriculture()));
            String filePath = detailAgriculturalOperations.getImageDetailAgriculture().getPath();
            setImageViewAgricultureFromCamera(bitmap, filePath);
        } else if (requestCode == REQUEST_GOOGLE_MAP && resultCode == RESULT_OK) {
            String coordinatesString = data.getStringExtra("coordinatesList");
            List<Coordinates> coordinatesList =
                    (List<Coordinates>) new Gson()
                            .fromJson(
                                    coordinatesString,
                                    new TypeToken<List<Coordinates>>() {
                                    }.getType());
            detailAgriculturalOperations.setCoordinates(coordinatesList);
            supportMapFragment.getMapAsync(this);
        }
    }

    private void showMapSnapShot(Intent data) {
        String filepath = data.getStringExtra("fileSnapShot");
        File file = new Gson().fromJson(filepath, File.class);
        setImageViewSnapShotAgriculture(createBitMapFromFile(file));
        detailAgriculturalOperations.setFileSnapShotMapAgriculture(file);
    }

    private ImageView setImageViewAgricultureFromCamera(Bitmap bitmap, String filePath) {
        ImageView imageViewShowImageCapture = (ImageView) findViewById(R.id.imageViewShowImageCapture);
        if (bitmap != null && filePath != null) {
            imageViewShowImageCapture.setImageBitmap(bitmap);
            imageViewShowImageCapture.setRotation(Rotate.Rotate(filePath));
            imageViewShowImageCapture.setVisibility(View.VISIBLE);
        }
        return imageViewShowImageCapture;
    }

    private ImageView setImageViewSnapShotAgriculture(Bitmap bitmap) {
        ImageView imageViewShowMap = (ImageView) findViewById(R.id.imageViewShowMap);
        if (bitmap != null) {
            imageViewShowMap.setImageBitmap(new ScaledBitmap().getScaledBitmapFullImage(bitmap));
            imageViewShowMap.setVisibility(View.VISIBLE);
        }
        return imageViewShowMap;
    }

    private Bitmap createBitMapFromFile(File file) {
        Bitmap bitmap = null;
        if (file != null) {
            bitmap = BitmapFactory.decodeFile(file.getPath());
        }
        return bitmap;
    }

    private void setTitleDatePickerDialog() {
        textViewTitleDatePicker = new TextView(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        textViewTitleDatePicker.setLayoutParams(layoutParams);
        textViewTitleDatePicker.setPadding(16, 16, 16, 16);
        textViewTitleDatePicker.setGravity(Gravity.CENTER);
        textViewTitleDatePicker.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.font_size_input));
        textViewTitleDatePicker.setText(R.string.selectDatePlanting);
        textViewTitleDatePicker.setTextColor(ContextCompat.getColor(this, R.color.colorDarkGray));
    }

    private void setDateTimeField(int year, int month, int dayOfMonth) {
        datePickerDialogPlantingDate = new DatePickerDialog(this, R.style.ProgressDialogCustom, this, year, month, dayOfMonth);
        Calendar calendar = createCalendar();
        int maxYear = year + 1000;
        calendar.set(maxYear, month, dayOfMonth);
        datePickerDialogPlantingDate.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        upDateTimeFiled(year, month, dayOfMonth);
        datePickerDialogPlantingDate.getDatePicker().setCalendarViewShown(false);
        datePickerDialogPlantingDate.setButton(DatePickerDialog.BUTTON_POSITIVE, getString(R.string.ok), datePickerDialogPlantingDate);

    }

    protected void upDateTimeFiled(int year, int month, int dayOfMonth) {
        datePickerDialogPlantingDate.updateDate(year, month, dayOfMonth);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        editTextHarvestDate.setText("");
        this.startDayOfMonth = dayOfMonth;
        this.startMonthOfYears = monthOfYear;
        this.startYears = year;
        startCalendar = createCalendarPlanting(startYears, startMonthOfYears, startDayOfMonth);
        editTextPlantingDate.setText(setFormatThaiDate(startCalendar));
    }

    private Calendar createCalendarPlanting(int year, int month, int dayOfMonth) {
        Calendar calendar = createCalendar();
        calendar.set(year, month, dayOfMonth);
        return calendar;
    }

    private void calculateHarvestDate() {
        final Dialog dialogDaysHarvest = new Dialog(this);
        dialogDaysHarvest.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDaysHarvest.setContentView(R.layout.dialog_calculate_days_harvest);

        TextView textViewTitle = (TextView) dialogDaysHarvest.findViewById(R.id.textViewTitle);
        textViewTitle.setText(R.string.pleaseInsertDayPlanting);

        dialogDaysHarvest.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogDaysHarvest.getWindow().getAttributes());
        dialogDaysHarvest.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        editTextGrowthDays = (EditText) dialogDaysHarvest.findViewById(R.id.editTextDaysHarvest);

        Button buttonDaysHarvest = (Button) dialogDaysHarvest.findViewById(R.id.buttonDaysHarvest);
        buttonDaysHarvest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                endCalendar = createCalendarPlanting(startYears, startMonthOfYears, startDayOfMonth);
                if (editTextGrowthDays.getText().toString().equals("") || editTextGrowthDays.getText().toString().equals("0")) {
                    createDialogAlertNoTitle(getString(R.string.pleaseInputNumberMoreThenZero));
                    editTextHarvestDate.setText("");
                    relativeLayoutHarvestDate.setVisibility(View.GONE);
                    linearLayoutHarvestDate.setVisibility(View.GONE);
                } else {
                    endCalendar.add(Calendar.DATE, Integer.parseInt(editTextGrowthDays.getText().toString()));
                    editTextHarvestDate.setText(setFormatThaiDate(endCalendar));
                    relativeLayoutHarvestDate.setVisibility(View.VISIBLE);
                    linearLayoutHarvestDate.setVisibility(View.VISIBLE);
                }

                dialogDaysHarvest.dismiss();
            }
        });
    }

    public String setFormatThaiDate(Calendar calendar) {
        return String.format("%d %s %d",
                calendar.get(Calendar.DAY_OF_MONTH),
                getResources().getStringArray(R.array.month_full_th)[calendar.get(Calendar.MONTH)],
                calendar.get(Calendar.YEAR));
    }

    private void calculatorProductivity() {
        final Dialog dialogProductivity = new Dialog(this);
        dialogProductivity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogProductivity.setContentView(R.layout.dialog_calculate_productivity);

        TextView textViewTitle = (TextView) dialogProductivity.findViewById(R.id.textViewTitle);
        textViewTitle.setText(R.string.pleaseInsertCountProductivityPerRai);

        dialogProductivity.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogProductivity.getWindow().getAttributes());
        dialogProductivity.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        editTextProductivityPerRai = (EditText) dialogProductivity.findViewById(R.id.editTextProductivityPerRai);

        Button buttonProductivityPerRai = (Button) dialogProductivity.findViewById(R.id.buttonProductivityPerRai);
        buttonProductivityPerRai.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editTextProductivityPerRai.getText().toString().equals("")) {
                    editTextEstimatedProductivity.setText(String.valueOf(
                            (((Double.parseDouble(editTextHarvestRai.getText().toString()) * 400)
                                    + (Double.parseDouble(editTextHarvestNgan.getText().toString()) * 100)
                                    + Double.parseDouble(editTextHarvestSquareWah.getText().toString())) *
                                    Double.parseDouble(editTextProductivityPerRai.getText().toString())) / 400));
                    relativeLayoutProductivity.setVisibility(View.VISIBLE);
                    linearLayoutProductivity.setVisibility(View.VISIBLE);
                } else {
                    relativeLayoutProductivity.setVisibility(View.GONE);
                    linearLayoutProductivity.setVisibility(View.GONE);
                }
                if(v != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                dialogProductivity.dismiss();
            }
        });
    }


    private boolean validateFields() {
        boolean isNextPage = true;
        if (spinnerTypeAgriculture.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectTypeAgriculture))) {
            imageViewErrorTypeAgriculture.setVisibility(View.VISIBLE);
            isNextPage = false;
        } else {
            imageViewErrorTypeAgriculture.setVisibility(View.INVISIBLE);
        }

        if (detailList.size() > 1) {
            if (spinnerDetailAgriculture.getSelectedItem().toString()
                    .equals(getString(R.string.pleaseSelectDetailAgriculture))) {
                imageViewErrorDetailAgriculture.setVisibility(View.VISIBLE);
                isNextPage = false;
            } else {
                imageViewErrorDetailAgriculture.setVisibility(View.INVISIBLE);
            }
        }

        if (breedList != null) {
            if (breedList.size() > 1) {
                if (spinnerBreeds.getSelectedItem().toString()
                        .equals(getString(R.string.pleaseSelectBreedAgriculture))) {
                    imageViewErrorBreedAgriculture.setVisibility(View.VISIBLE);
                    isNextPage = false;
                } else {
                    imageViewErrorBreedAgriculture.setVisibility(View.INVISIBLE);
                }
            }
        }

        if (editTextPlantingDate.getText().toString().equals("")) {
            imageViewErrorPlantationDate.setVisibility(View.VISIBLE);
            isNextPage = false;
        } else {
            imageViewErrorPlantationDate.setVisibility(View.INVISIBLE);
        }

        if (editTextPlantingRai.getText().toString().equals("")) {
            imageViewErrorPlantationArea.setVisibility(View.VISIBLE);
            isNextPage = false;
        } else {
            imageViewErrorPlantationArea.setVisibility(View.GONE);
        }

        if (editTextPlantingNgan.getText().toString().equals("")) {
            imageViewErrorPlantationArea.setVisibility(View.VISIBLE);
            isNextPage = false;
        } else {
            imageViewErrorPlantationArea.setVisibility(View.GONE);
        }

        if (editTextPlantingSquareWah.getText().toString().equals("")) {
            imageViewErrorPlantationArea.setVisibility(View.VISIBLE);
            isNextPage = false;
        } else {
            imageViewErrorPlantationArea.setVisibility(View.GONE);
        }

        if (editTextHarvestRai.getText().toString().equals("")) {
            imageViewErrorHarvestArea.setVisibility(View.VISIBLE);
            isNextPage = false;
        } else {
            imageViewErrorHarvestArea.setVisibility(View.GONE);
        }

        if (editTextHarvestNgan.getText().toString().equals("")) {
            imageViewErrorHarvestArea.setVisibility(View.VISIBLE);
            isNextPage = false;
        } else {
            imageViewErrorHarvestArea.setVisibility(View.GONE);
        }

        if (editTextHarvestSquareWah.getText().toString().equals("")) {
            imageViewErrorHarvestArea.setVisibility(View.VISIBLE);
            isNextPage = false;
        } else {
            imageViewErrorHarvestArea.setVisibility(View.GONE);
        }

        if (!isNextPage) {
            createDialogAlertNoTitle(getString(R.string.allFieldRequired));
        }

        Boolean harvestDateEmpty = false;
        if (editTextHarvestDate.getText().toString().equals("")) {
            imageViewErrorHarvestDate.setVisibility(View.VISIBLE);
            harvestDateEmpty = true;
            isNextPage = false;
        } else {
            imageViewErrorHarvestDate.setVisibility(View.INVISIBLE);
        }

        Boolean estimatedProductivity = false;
        if (editTextEstimatedProductivity.getText().toString().equals("")) {
            imageViewEstimatedProductivity.setVisibility(View.VISIBLE);
            estimatedProductivity = true;
            isNextPage = false;
        } else {
            imageViewEstimatedProductivity.setVisibility(View.GONE);
        }

        if(harvestDateEmpty || estimatedProductivity){
            String str = "กรุณา ";
            if(harvestDateEmpty){
                str += getString(R.string.harvestDateCalculator);
            }

            if(estimatedProductivity){
                if(harvestDateEmpty){
                    str += " และ ";
                }
                str += getString(R.string.calculatorProductivity);
            }
            createDialogAlertNoTitle(str);
        }
        return isNextPage;
    }

    private void createDialogAlertNoTitle(String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert_no_title);

        TextView textViewMessage = (TextView) dialogAlert.findViewById(R.id.textViewMessage);
        textViewMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonOK = (Button) dialogAlert.findViewById(R.id.buttonOK);
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }

    private void initInstance() {
        areaOfPlanting = new Area(0);
        areaOfHarvest = new Area(0);
        findId();
        setListener();
        loadDataFromPreferencesService();
        setDetailRegisterPlantation();
        setListDetailRegisterPlantation();
        setHeadDetail();
        loadTypeName();
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentMapDetailAgriculturalMap);
        supportMapFragment.getMapAsync(this);
        linearLayoutDetailAgriculturalMap.setVisibility(View.GONE);
    }

    private void findId() {
        editTextLocation = (EditText) findViewById(R.id.editTextLocation);
        editTextLocationNo = (EditText) findViewById(R.id.editTextLocationNo);
        editTextArea = (EditText) findViewById(R.id.editTextArea);
        editTextRemainingSpaceArea = (EditText) findViewById(R.id.editTextRemainingSpaceArea);

        setBoldTextTitle();

        buttonCancelDetail = (Button) findViewById(R.id.buttonCancelDetail);
        buttonSaveDetail = (Button) findViewById(R.id.buttonSaveDetail);
        spinnerTypeAgriculture = (Spinner) findViewById(R.id.spinnerNamePlants);
        spinnerDetailAgriculture = (Spinner) findViewById(R.id.spinnerSpecies);
        spinnerBreeds = (Spinner) findViewById(R.id.spinnerSeeds);
        rdCategoryPlants = (RadioGroup) findViewById(R.id.rdCategoryPlants);
        editTextPlantingRai = (EditText) findViewById(R.id.editTextPlantingRai);
        editTextPlantingNgan = (EditText) findViewById(R.id.editTextPlantingNgan);
        editTextPlantingSquareWah = (EditText) findViewById(R.id.editTextPlantingSquareWah);
        editTextHarvestRai = (EditText) findViewById(R.id.editTextHarvestRai);
        editTextHarvestNgan = (EditText) findViewById(R.id.editTextHarvestNgan);
        editTextHarvestSquareWah = (EditText) findViewById(R.id.editTextHarvestSquareWah);
        editTextPlantingDate = (EditText) findViewById(R.id.editTextPlantingDate);
        imageButtonPlantingDate = (ImageButton) findViewById(R.id.imageButtonPlantingDate);
        editTextHarvestDate = (EditText) findViewById(R.id.editTextHarvestDate);
        editTextEstimatedProductivity = (EditText) findViewById(R.id.editTextEstimatedProductivity);
        buttonCalculatorProductivity = (Button) findViewById(R.id.buttonCalculatorProductivity);
        buttonCalculateHarvestDate = (Button) findViewById(R.id.buttonCalculateHarvestDate);

        linearLayoutOfCategoryPlants = (LinearLayout) findViewById(R.id.linearLayoutOfCategoryPlants);

        imageViewErrorTypeAgriculture = (ImageView) findViewById(R.id.imageViewErrorTypeAgriculture);
        imageViewErrorDetailAgriculture = (ImageView) findViewById(R.id.imageViewErrorDetailAgriculture);
        imageViewErrorBreedAgriculture = (ImageView) findViewById(R.id.imageViewErrorBreedAgriculture);
        imageViewErrorPlantationDate = (ImageView) findViewById(R.id.imageViewErrorPlantationDate);
        imageViewErrorHarvestDate = (ImageView) findViewById(R.id.imageViewErrorHarvestDate);

        linearLayoutDetail = (LinearLayout) findViewById(R.id.linearLayoutDetail);
        linearLayoutBreed = (LinearLayout) findViewById(R.id.linearLayoutBreed);
        linearLayoutDetailAgriculturalMap = (LinearLayout) findViewById(R.id.linearLayoutDetailAgriculturalMap);

        imageViewErrorPlantationArea = (ImageView) findViewById(R.id.imageViewErrorPlantationArea);
        imageViewErrorHarvestArea = (ImageView) findViewById(R.id.imageViewErrorHarvestArea);
        imageViewEstimatedProductivity = (ImageView) findViewById(R.id.imageViewEstimatedProductivity);

        relativeLayoutHarvestDate = (RelativeLayout) findViewById(R.id.relativeLayoutHarvestDate);
        linearLayoutHarvestDate = (LinearLayout) findViewById(R.id.linearLayoutHarvestDate);

        linearLayoutProductivity = (LinearLayout) findViewById(R.id.linearLayoutProductivity);
        relativeLayoutProductivity = (RelativeLayout) findViewById(R.id.relativeLayoutProductivity);
    }

    private void setBoldTextTitle() {
        Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/THSarabunNewBold.ttf");
        textViewParcelNumber = (TextView) findViewById(R.id.textViewParcelNumber);
        textViewParcelNumber.setTypeface(typeface);

        textViewRemainingArea = (TextView) findViewById(R.id.textViewRemainingArea);
        textViewRemainingArea.setTypeface(typeface);

        textViewArea = (TextView) findViewById(R.id.textViewArea);
        textViewArea.setTypeface(typeface);

        textViewAgricultural = (TextView) findViewById(R.id.textViewAgricultural);
        textViewAgricultural.setTypeface(typeface);

        textViewLocation = (TextView) findViewById(R.id.textViewLocation);
        textViewLocation.setTypeface(typeface);
    }

    private void setListener() {
        buttonCancelDetail.setOnClickListener(this);
        buttonSaveDetail.setOnClickListener(this);

        spinnerTypeAgriculture.setOnItemSelectedListener(this);
        spinnerDetailAgriculture.setOnItemSelectedListener(this);
        spinnerBreeds.setOnItemSelectedListener(this);

        editTextPlantingRai.setOnClickListener(this);
        editTextPlantingNgan.setOnClickListener(this);
        editTextPlantingSquareWah.setOnClickListener(this);
        editTextHarvestRai.setOnClickListener(this);
        editTextHarvestNgan.setOnClickListener(this);
        editTextHarvestSquareWah.setOnClickListener(this);

        buttonCalculatorProductivity.setOnClickListener(this);
        buttonCalculateHarvestDate.setOnClickListener(this);

        editTextPlantingDate.addTextChangedListener(this);
        editTextHarvestDate.setOnClickListener(this);

        editTextPlantingRai.addTextChangedListener(this);
        editTextPlantingNgan.addTextChangedListener(this);
        editTextPlantingSquareWah.addTextChangedListener(this);
        editTextHarvestRai.addTextChangedListener(this);
        editTextHarvestNgan.addTextChangedListener(this);
        editTextHarvestSquareWah.addTextChangedListener(this);

        imageButtonPlantingDate.setOnClickListener(this);
        editTextPlantingDate.setOnClickListener(this);
        buttonDrawingMap = (Button) findViewById(R.id.buttonDrawingMap);
        buttonDrawingMap.setOnClickListener(this);
        Button buttonCaptureAgricultural = (Button) findViewById(R.id.buttonCaptureAgricultural);
        buttonCaptureAgricultural.setOnClickListener(this);
    }

    private void loadDataFromPreferencesService() {
        modeWork = getIntent().getStringExtra("mode");
        position = getIntent().getIntExtra("position", 0);
    }

    private void setDetailRegisterPlantation() {
        registerPlantation = (RegisterPlantation) PreferencesService.getPreferences("registerPlantation", RegisterPlantation.class);
        detailRegisterPlantation = registerPlantation.getDetailRegisterPlantation().get(position);

        if (modeWork.equals("Add new")) {
            detailAgriculturalOperations = new DetailAgriculturalOperations();
            buttonCalculateHarvestDate.setEnabled(false);
            buttonCalculatorProductivity.setEnabled(false);
        } else if (modeWork.equals("Edit")) {
            loadOldDataOfDetailAgriculturalOperations();
        }

        List<Coordinates> main = detailRegisterPlantation.getCoordinates();
        if (main == null || main.size() == 1) {
            buttonDrawingMap.setVisibility(View.GONE);
        }
    }

    private void setListDetailRegisterPlantation() {
        listDetailRegisterPlantation = detailRegisterPlantation.getDetailAgriculturalOperationses();
    }

    private void setHeadDetail() {
        Address address = detailRegisterPlantation.getAddress();
        AddressFormat addressFormat = AddressFormat.setAddressFormat(address);
        editTextLocation.setText(addressFormat.getFormat());
        editTextLocationNo.setText(detailRegisterPlantation.getLicenseNumber());

        DeedFormat deedFormat = DeedFormat.setDetailRegisterPlantationFormat(detailRegisterPlantation);
        editTextArea.setText(deedFormat.getFormat());


        Area areaOfDetailRegisterPlantation = new Area(0);
        areaOfDetailRegisterPlantation.setRai(detailRegisterPlantation.getRai());
        areaOfDetailRegisterPlantation.setNgan(detailRegisterPlantation.getNgan());
        areaOfDetailRegisterPlantation.setSquareWah(detailRegisterPlantation.getSquareWah());

        areaShow = new Area(0);
        areaShow.convertSquareMeterToRaiNganWah(areaOfDetailRegisterPlantation.getSquareMeter());

        List<DetailAgriculturalOperations> detailAgriculturalList = registerPlantation.getDetailRegisterPlantation().get(position).getDetailAgriculturalOperationses();
        if (detailAgriculturalList != null) {
            double sum = areaOfDetailRegisterPlantation.getSquareMeter();
            for (int index = 0; index < detailAgriculturalList.size(); index++) {
                Area areaDetailAgricultural = new Area(0);
                areaDetailAgricultural.setRai(detailAgriculturalList.get(index).getPlantedRai());
                areaDetailAgricultural.setNgan(detailAgriculturalList.get(index).getPlantedNgan());
                areaDetailAgricultural.setSquareWah(detailAgriculturalList.get(index).getPlantedSquareWah());
                sum -= areaDetailAgricultural.getSquareMeter();
            }
            if ("Edit".equals(modeWork)) {
                Area areaShowEdit = new Area(0);
                areaShowEdit.setRai(detailAgriculturalList.get(positionDetail).getPlantedRai());
                areaShowEdit.setNgan(detailAgriculturalList.get(positionDetail).getPlantedNgan());
                areaShowEdit.setSquareWah(detailAgriculturalList.get(positionDetail).getPlantedSquareWah());
                sum += areaShowEdit.getSquareMeter();
            }
            areaShow.setRai(0);
            areaShow.setNgan(0);
            areaShow.setSquareWah(0);

            areaShow.convertSquareMeterToRaiNganWah(sum);
        }

        editTextRemainingSpaceArea.setText(
                String.format("%d ไร่ %d งาน %d ตารางวา", areaShow.getRai(), areaShow.getNgan(), areaShow.getSquareWah())
        );
    }

    private void loadTypeName() {
        serviceAgriculture = AgricultureManager.getServiceAgriculture();

        SharedPreferences settings;
        settings = getSharedPreferences("agricultureType", 0);
        String data = settings.getString("agricultureType", null);
        AgricultureType agricultureType = new Gson().fromJson(data, AgricultureType.class);
        kasetTypeNameLoadDataSuccess(agricultureType);
        if (agricultureType == null) {
            Call<AgricultureType> call = serviceAgriculture.loadType();
            call.enqueue(new AgricultureTypeManager(this));
        }
    }

    private void loadOldDataOfDetailAgriculturalOperations() {
        positionDetail = getIntent().getIntExtra("positionDetail", 0);
        detailAgriculturalOperations = detailRegisterPlantation.getDetailAgriculturalOperationses()
                .get(positionDetail);
        loadCache();
    }

    private void loadCache() {
        startCalendar = createCalendar();
        startCalendar.setTimeInMillis(detailAgriculturalOperations.getPlantingDate());
        startDayOfMonth = startCalendar.get(Calendar.DAY_OF_MONTH);
        startMonthOfYears = startCalendar.get(Calendar.MONTH);
        startYears = startCalendar.get(Calendar.YEAR) + 543;
        startCalendar.set(startYears, startMonthOfYears, startDayOfMonth);
        editTextPlantingDate.setText(setFormatThaiDate(startCalendar));
        upDateTimeFiled(startYears, startMonthOfYears, startDayOfMonth);

        endCalendar = createCalendar();
        endCalendar.setTimeInMillis(detailAgriculturalOperations.getHarvestDate());
        int endDate = endCalendar.get(Calendar.DAY_OF_MONTH);
        int endMonth = endCalendar.get(Calendar.MONTH);
        int endYears = endCalendar.get(Calendar.YEAR) + 543;
        endCalendar.set(endYears, endMonth, endDate);
        editTextHarvestDate.setText(setFormatThaiDate(endCalendar));

        areaOfPlanting.setRai(detailAgriculturalOperations.getPlantedRai());
        areaOfPlanting.setNgan(detailAgriculturalOperations.getPlantedNgan());
        areaOfPlanting.setSquareWah(detailAgriculturalOperations.getPlantedSquareWah());

        editTextPlantingRai.setText(String.valueOf(detailAgriculturalOperations.getPlantedRai()));
        editTextPlantingNgan.setText(String.valueOf(detailAgriculturalOperations.getPlantedNgan()));
        editTextPlantingSquareWah.setText(String.valueOf(detailAgriculturalOperations.getPlantedSquareWah()));

        areaOfHarvest.setRai(detailAgriculturalOperations.getHarvestedRai());
        areaOfHarvest.setNgan(detailAgriculturalOperations.getHarvestedNgan());
        areaOfHarvest.setSquareWah(detailAgriculturalOperations.getHarvestedSquareWah());

        editTextHarvestRai.setText(String.valueOf(detailAgriculturalOperations.getHarvestedRai()));
        editTextHarvestNgan.setText(String.valueOf(detailAgriculturalOperations.getHarvestedNgan()));
        editTextHarvestSquareWah.setText(String.valueOf(detailAgriculturalOperations.getHarvestedSquareWah()));


        editTextEstimatedProductivity.setText(String.valueOf(detailAgriculturalOperations.getEstimatedProductivity()));

        setImageViewSnapShotAgriculture(createBitMapFromFile(detailAgriculturalOperations.getFileSnapShotMapAgriculture()));

        if (detailAgriculturalOperations.getImageDetailAgriculture() != null) {
            Bitmap bitmap = new ScaledBitmap().getScaledBitmapFullImage(
                    createBitMapFromFile(detailAgriculturalOperations.getImageDetailAgriculture()));
            String filePath = detailAgriculturalOperations.getImageDetailAgriculture().getPath();
            setImageViewAgricultureFromCamera(bitmap, filePath);
        }

    }

    @NonNull
    private Calendar createCalendar() {
        return Calendar.getInstance();
    }

    private void save() {
        setDataToModel();
        if ("Add new".equals(modeWork))
            modeNewToDetailAgriculturalOperationsModel();
        else if ("Edit".equals(modeWork))
            modeEditToDetailAgriculturalOperationsModel();
        detailRegisterPlantation.setDetailAgriculturalOperations(listDetailRegisterPlantation);
        PreferencesService.savePreferences("registerPlantation", registerPlantation);
    }

    private void modeEditToDetailAgriculturalOperationsModel() {
        listDetailRegisterPlantation.set(positionDetail, detailAgriculturalOperations);
    }

    private void modeNewToDetailAgriculturalOperationsModel() {
        if (listDetailRegisterPlantation == null) {
            listDetailRegisterPlantation = new ArrayList<>();
        }
        listDetailRegisterPlantation.add(detailAgriculturalOperations);
    }

    private void setDataToModel() {
        detailAgriculturalOperations.setNamePlants(typeCodeName);

        detailAgriculturalOperations.setBreeds("");
        if (spinnerBreeds.getSelectedItem() != null) {
            detailAgriculturalOperations.setBreeds(breedCodeName);
        }

        detailAgriculturalOperations.setDetails("");
        if (spinnerDetailAgriculture.getSelectedItem() != null) {
            detailAgriculturalOperations.setDetails(detailCodeName);
        }

        int selectID = rdCategoryPlants.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) findViewById(selectID);

        detailAgriculturalOperations.setCategoryPlants((radioButton.getText().toString()).substring(2));

        int startDate = startCalendar.get(Calendar.DAY_OF_MONTH);
        int startMonth = startCalendar.get(Calendar.MONTH);
        int startYears = startCalendar.get(Calendar.YEAR) - 543;

        startCalendar.set(startYears, startMonth, startDate);
        detailAgriculturalOperations.setPlantingDate(startCalendar.getTimeInMillis());


        int endDate = endCalendar.get(Calendar.DAY_OF_MONTH);
        int endMonth = endCalendar.get(Calendar.MONTH);
        int endYears = endCalendar.get(Calendar.YEAR) - 543;
        endCalendar.set(endYears, endMonth, endDate);
        detailAgriculturalOperations.setHarvestDate(endCalendar.getTimeInMillis());

        if (editTextEstimatedProductivity.getText().toString().equals(""))
            detailAgriculturalOperations.setEstimatedProductivity(0);
        else
            detailAgriculturalOperations.setEstimatedProductivity(Double.valueOf(editTextEstimatedProductivity.getText().toString()));

        detailAgriculturalOperations.setPlantedRai(Integer.parseInt(editTextPlantingRai.getText().toString()));
        detailAgriculturalOperations.setPlantedNgan(Integer.parseInt(editTextPlantingNgan.getText().toString()));
        detailAgriculturalOperations.setPlantedSquareWah(Integer.parseInt(editTextPlantingSquareWah.getText().toString()));

        detailAgriculturalOperations.setHarvestedRai(Integer.parseInt(editTextHarvestRai.getText().toString()));
        detailAgriculturalOperations.setHarvestedNgan(Integer.parseInt(editTextHarvestNgan.getText().toString()));
        detailAgriculturalOperations.setHarvestedSquareWah(Integer.parseInt(editTextHarvestSquareWah.getText().toString()));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        StringWithTag stringWithTag = (StringWithTag) parent.getItemAtPosition(position);
        SharedPreferences settings;
        switch (parent.getId()) {
            case R.id.spinnerNamePlants:
                typeCodeName = stringWithTag.tag.toString();
                settings = getSharedPreferences("agricultureDetail" + typeCodeName, 0);
                String agricultureDetailData = settings.getString("agricultureDetail" + typeCodeName, null);
                AgricultureDetail agricultureType = new Gson().fromJson(agricultureDetailData, AgricultureDetail.class);
                kasetDetailLoadDataSuccess(agricultureType);
                if (agricultureType == null) {
                    Call<AgricultureDetail> call = serviceAgriculture.loadDetail(Integer.parseInt(typeCodeName));
                    call.enqueue(new AgricultureDetailManager(this, typeCodeName));
                }


                linearLayoutOfCategoryPlants.setVisibility(View.VISIBLE);
                if (!parent.getItemAtPosition(position).toString().equals("ข้าว")) {
                    linearLayoutOfCategoryPlants.setVisibility(View.GONE);
                }
                changeSpinnerAgricultureType = true;
                break;

            case R.id.spinnerSpecies:
                detailCodeName = stringWithTag.tag.toString();
                settings = getSharedPreferences("agricultureBreed" + detailCodeName, 0);
                String data = settings.getString("agricultureBreed" + detailCodeName, null);
                AgricultureBreed agricultureBreed = new Gson().fromJson(data, AgricultureBreed.class);

                kasetBreedLoadDataSuccess(agricultureBreed);
                if (agricultureBreed == null) {
                    Call<AgricultureBreed> callBreed = serviceAgriculture.loadBreed(Integer.parseInt(detailCodeName));
                    callBreed.enqueue(new AgricultureBreedManager(this, detailCodeName));
                }

                changeSpinnerDetail = true;
                break;
            case R.id.spinnerSeeds:
                breedCodeName = stringWithTag.tag.toString();
                changeSpinnerBreeds = true;
                break;
        }
    }

    private void clearImageAll() {
        setImageViewSnapShotAgriculture(null).setVisibility(View.GONE);
        setImageViewAgricultureFromCamera(null, null).setVisibility(View.GONE);
        detailAgriculturalOperations.setFileSnapShotMapAgriculture(null);
        detailAgriculturalOperations.setCoordinates(null);
        linearLayoutDetailAgriculturalMap.setVisibility(View.GONE);
        detailAgriculturalOperations.setImageDetailAgriculture(null);
    }


    private void clearAreaPlanting() {
        this.areaOfPlanting = new Area(0);
        editTextPlantingRai.setText("");
        editTextPlantingNgan.setText("");
        editTextPlantingSquareWah.setText("");
    }

    private void clearAreaOfHarvest() {
        this.areaOfHarvest = new Area(0);
        editTextHarvestRai.setText("");
        editTextHarvestNgan.setText("");
        editTextHarvestSquareWah.setText("");

        relativeLayoutProductivity.setVisibility(View.GONE);
        linearLayoutProductivity.setVisibility(View.GONE);

        clearEstimatedProductivity();
        clearImageAll();
    }

    private void clearEstimatedProductivity() {
        editTextEstimatedProductivity.setText("");

        relativeLayoutProductivity.setVisibility(View.GONE);
        linearLayoutProductivity.setVisibility(View.GONE);
    }

    @Subscribe
    public void kasetTypeNameLoadDataSuccess(AgricultureType agricultureType) {
        if (agricultureType != null) {
            List<StringWithTag> typeNameList = listForSetArrayAdapter(agricultureType.getListOfAgricultureType());
            typeNameList.add(0, new StringWithTag(getString(R.string.pleaseSelectTypeAgriculture), 0));
            final ArrayAdapter kasetTypeNameAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, typeNameList);
            spinnerTypeAgriculture.setAdapter(kasetTypeNameAdapter);
            for (int index = 0; index < typeNameList.size(); index++) {
                if (typeNameList.get(index).tag.equals(detailAgriculturalOperations.getNamePlants())) {
                    spinnerTypeAgriculture.setSelection(index);
                    break;
                }
            }
        } else {
            List<StringWithTag> listStartTypeName = new ArrayList<>();
            listStartTypeName.add(0, new StringWithTag(getString(R.string.pleaseSelectTypeAgriculture), 0));
            final ArrayAdapter kasetTypeNameAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, listStartTypeName);
            spinnerTypeAgriculture.setAdapter(kasetTypeNameAdapter);
        }
    }

    @Subscribe
    public void kasetDetailLoadDataSuccess(AgricultureDetail agricultureDetail) {
        if (agricultureDetail != null) {
            detailList = listForSetArrayAdapter(agricultureDetail.getDetail());
            detailList.add(0, new StringWithTag(getString(R.string.pleaseSelectDetailAgriculture), 0));
            if (detailList.size() <= 1) {
                linearLayoutDetail.setVisibility(View.GONE);
                linearLayoutBreed.setVisibility(View.GONE);
            } else {
                linearLayoutDetail.setVisibility(View.VISIBLE);
            }

            final ArrayAdapter kasetDetailAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, detailList);
            spinnerDetailAgriculture.setAdapter(kasetDetailAdapter);
            for (int index = 0; index < detailList.size(); index++) {
                if (detailList.get(index).tag.equals(detailAgriculturalOperations.getDetails())) {
                    spinnerDetailAgriculture.setSelection(index);
                    break;
                }
            }
        } else {
            List<StringWithTag> listStartDetail = new ArrayList<>();
            listStartDetail.add(0, new StringWithTag(getString(R.string.pleaseSelectDetailAgriculture), 0));
            final ArrayAdapter kasetDetailAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, listStartDetail);
            spinnerDetailAgriculture.setAdapter(kasetDetailAdapter);
        }
    }

    @Subscribe
    public void kasetBreedLoadDataSuccess(AgricultureBreed agricultureBreed) {
        if (agricultureBreed != null) {
            if (agricultureBreed.getAgricultureBreed() != null) {
                breedList = listForSetArrayAdapter(agricultureBreed.getAgricultureBreed());
                breedList.add(0, new StringWithTag(getString(R.string.pleaseSelectBreedAgriculture), 0));
            }
            if (breedList != null) {
                if (breedList.size() <= 1) {
                    linearLayoutBreed.setVisibility(View.GONE);
                } else {
                    linearLayoutBreed.setVisibility(View.VISIBLE);
                }
            }
            final ArrayAdapter kasetBreedAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, breedList);
            spinnerBreeds.setAdapter(kasetBreedAdapter);
            for (int index = 0; index < breedList.size(); index++) {
                if (breedList.get(index).tag.equals(detailAgriculturalOperations.getBreeds())) {
                    spinnerBreeds.setSelection(index);
                    break;
                }
            }
        } else {
            List<StringWithTag> listStartBreed = new ArrayList<>();
            listStartBreed.add(0, new StringWithTag(getString(R.string.pleaseSelectBreedAgriculture), 0));
            final ArrayAdapter kasetBreedAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, listStartBreed);
            spinnerBreeds.setAdapter(kasetBreedAdapter);
        }
    }

    private List<StringWithTag> listForSetArrayAdapter(List<JsonObject> objects) {
        List<StringWithTag> lists = new ArrayList<>();
        for (int index = 0; index < objects.size(); index++) {
            lists.add(index, new StringWithTag(objects.get(index).get("name").getAsString(),
                    objects.get(index).get("id").getAsString()));
        }
        return lists;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        moveCameraToLocation(googleMap);
        createPolygon(googleMap);
    }

    private void createPolygon(GoogleMap googleMap) {
        if (detailAgriculturalOperations.getCoordinates() != null) {
            linearLayoutDetailAgriculturalMap.setVisibility(View.VISIBLE);
            PolygonOptions polygonOptions = new PolygonOptions();
            for (int index = 0; index < detailAgriculturalOperations.getCoordinates().size(); index++) {
                double latitude = detailAgriculturalOperations.getCoordinates().get(index).getLatitude();
                double longitude = detailAgriculturalOperations.getCoordinates().get(index).getLongitude();
                LatLng latLng = new LatLng(latitude, longitude);
                polygonOptions.add(latLng);
            }
            polygonOptions.strokeColor(0x6066BB6A);
            polygonOptions.fillColor(0x6066BB6A);
            googleMap.addPolygon(polygonOptions);
        }
    }

    private void moveCameraToLocation(GoogleMap googleMap) {
        if (detailAgriculturalOperations.getCoordinates() != null) {
            double latitude = detailAgriculturalOperations.getCoordinates().get(0).getLatitude();
            double longitude = detailAgriculturalOperations.getCoordinates().get(0).getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    @Override
    public void onSuccess(FarmersOrganization farmersOrganization) {

    }

    @Override
    public void onFails(String message) {
        createDialogAlert(getString(R.string.errorTimeout), getString(R.string.cannotConnectServer).concat(message));
    }

    private void createDialogAlert(String title, String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert);

        TextView textViewDialogAlertTitle = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertTitle);
        TextView textViewDialogAlertMessage = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertMessage);

        textViewDialogAlertTitle.setText(title);
        textViewDialogAlertMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonDialogAlertOK = (Button) dialogAlert.findViewById(R.id.buttonDialogAlertOK);
        buttonDialogAlertOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }
}
