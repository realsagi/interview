package th.in.tamis.service;

import th.in.tamis.models.Address;

public class AddressFormat {

    private AddressFormat(){}

    private static AddressFormat instance;
    private static Address objAddress;

    public static AddressFormat setAddressFormat(Address address){
        objAddress = address;
        if (instance == null)
            instance = new AddressFormat();
        return instance;
    }

    public String getFormat(){
        String moo = "หมู่ที่ "+objAddress.getMoo()+" ";
        if(objAddress.getMoo() == ""){
            moo = "";
        }
        String subDistrict = "ตำบล"+objAddress.getSubDistrictName()+" ";
        String district = "อำเภอ"+objAddress.getDistrictName()+" ";
        String province = "จังหวัด"+objAddress.getProvinceName();
        return moo+subDistrict+district+province;
    }

}