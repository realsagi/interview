package th.in.tamis.service.map;

import java.text.DecimalFormat;

public class ConvertUTMTOLatLong {

    public double utmXToLatitude(double x, double y, int zone, String hemisphere){

        double k0 = 0.9996;
        double drad = Math.PI / 180.0;
        double a = 6378137.0;
        double f = 1.0 / 298.2572236;
        double b = a * (1.0 - f);   // polar radius
        double e = Math.sqrt(1.0 - Math.pow(b, 2.0) / Math.pow(a, 2.0));

        double esq = (1.0 - (b / a) * (b / a));
        double e0sq = e * e / (1.0 - Math.pow(e, 2.0));
        double e1 = (1.0 - Math.sqrt(1.0 - Math.pow(e, 2.0))) / (1.0 + Math.sqrt(1.0 - Math.pow(e, 2.0)));
        double M0 = 0;
        double M;

        if ("N".equals(hemisphere))
            M = M0 + y / k0;    // Arc length along standard meridian.
        else
            M = M0 + (y - 10000000.0);

        double mu = M / (a * (1.0 - esq * (1.0 / 4.0 + esq * (3.0 / 64.0 + 5.0 * esq / 256.0))));
        double phi1 = mu + e1 * (3.0 / 2.0 - 27.0 * e1 * e1 / 32.0) * Math.sin(2.0 * mu) + e1 * e1 * (21.0 / 16.0 - 55.0 * e1 * e1 / 32.0) * Math.sin(4.0 * mu);   //Footprint Latitude
        phi1 = phi1 + e1 * e1 * e1 * (Math.sin(6.0 * mu) * 151.0 / 96.0 + e1 * Math.sin(8.0 * mu) * 1097.0 / 512.0);
        double C1 = e0sq * Math.pow(Math.cos(phi1), 2.0);
        double T1 = Math.pow(Math.tan(phi1), 2.0);
        double N1 = a / Math.sqrt(1.0 - Math.pow(e * Math.sin(phi1), 2.0));
        double R1 = N1 * (1.0 - Math.pow(e, 2.0)) / (1.0 - Math.pow(e * Math.sin(phi1), 2.0));
        double D = (x - 500000.0) / (N1 * k0);
        double phi = (D * D) * (1.0 / 2.0 - D * D * (5.0 + 3.0 * T1 + 10.0 * C1 - 4.0 * C1 * C1 - 9.0 * e0sq) / 24.0);
        phi = phi + Math.pow(D, 6) * (61.0 + 90.0 * T1 + 298.0 * C1 + 45.0 * T1 * T1 - 252.0 * e0sq - 3.0 * C1 * C1) / 720.0;
        phi = phi1 - (N1 * Math.tan(phi1) / R1) * phi;

        double lat = Math.floor(10000000.0 * phi / drad) / 10000000.0;

        return lat;
    }

    public double utmYToLongitude(double x, double y, int zone, String hemisphere){
        double k0 = 0.9996;
        double drad = Math.PI / 180.0;
        double a = 6378137.0;
        double f = 1.0 / 298.2572236;
        double b = a * (1.0 - f);   // polar radius
        double e = Math.sqrt(1.0 - Math.pow(b, 2.0) / Math.pow(a, 2.0));

        double esq = (1.0 - (b / a) * (b / a));
        double e0sq = e * e / (1.0 - Math.pow(e, 2.0));
        double zcm = 3.0 + 6.0 * (zone - 1.0) - 180.0;                         // Central meridian of zone
        double e1 = (1.0 - Math.sqrt(1.0 - Math.pow(e, 2.0))) / (1.0 + Math.sqrt(1.0 - Math.pow(e, 2.0)));
        double M0 = 0.0;
        double M;

        if ("N".equals(hemisphere))
            M = M0 + y / k0;    // Arc length along standard meridian.
        else
            M = M0 + (y - 10000000.0);

        double mu = M / (a * (1.0 - esq * (1.0 / 4.0 + esq * (3.0 / 64.0 + 5.0 * esq / 256.0))));
        double phi1 = mu + e1 * (3.0 / 2.0 - 27.0 * e1 * e1 / 32.0) * Math.sin(2.0 * mu) + e1 * e1 * (21.0 / 16.0 - 55.0 * e1 * e1 / 32.0) * Math.sin(4.0 * mu);   //Footprint Latitude
        phi1 = phi1 + e1 * e1 * e1 * (Math.sin(6.0 * mu) * 151.0 / 96.0 + e1 * Math.sin(8.0 * mu) * 1097.0 / 512.0);
        double C1 = e0sq * Math.pow(Math.cos(phi1), 2.0);
        double T1 = Math.pow(Math.tan(phi1), 2.0);
        double N1 = a / Math.sqrt(1.0 - Math.pow(e * Math.sin(phi1), 2.0));
        double D = (x - 500000.0) / (N1 * k0);

        double lng = D * (1.0 + D * D * ((-1.0 - 2.0 * T1 - C1) / 6.0 + D * D * (5.0 - 2.0 * C1 + 28.0 * T1 - 3.0 * C1 * C1 + 8.0 * e0sq + 24.0 * T1 * T1) / 120.0)) / Math.cos(phi1);
        lng = zcm + lng / drad;

        return lng;
    }

}
