package th.in.tamis.holder;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;

import java.io.File;
import java.util.List;

import th.in.tamis.activity.RegisterAgriculturalOperationsActivity;
import th.in.tamis.activity.DetailPlantationActivity;
import th.in.tamis.models.Coordinates;
import th.in.tamis.models.DetailAgriculturalOperations;
import th.in.tamis.service.image.ScaledBitmap;
import th.in.tamis.tamis.R;

public class RegisterPlantationHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
        OnMapReadyCallback {

    final private EditText editTextFullAddress;
    final private TextView tvNumber;
    final private TextView tvDeed;
    final private TextView tvRai;
    final private TextView tvNgan;
    final private TextView tvSquareWah;
    final private EditText editTextLongitude;
    final private EditText editTextLatitude;
    final private ImageView imageViewCapturePlantationMap;
    final private FrameLayout linearLayoutCardViewRegisterMap;
    private List<Coordinates> coordinates;
    private List<DetailAgriculturalOperations> subCoordinates;
    private FragmentManager fragmentManagerLocal;
    private TextView textViewStatusMap;
    private final Context context;
    private ImageView imageViewErrorTypeAgriculture;
    private ImageView imageViewIconMap;

    public RegisterPlantationHolder(View itemView, FragmentManager fragmentManager) {
        super(itemView);
        this.fragmentManagerLocal = fragmentManager;
        this.context = itemView.getContext();

        tvNumber = (TextView) itemView.findViewById(R.id.tvNumber);
        editTextFullAddress = (EditText) itemView.findViewById(R.id.editTextFullAddress);

        tvDeed = (TextView) itemView.findViewById(R.id.tvDeed);
        tvRai = (TextView) itemView.findViewById(R.id.editTextRai);
        tvNgan = (TextView) itemView.findViewById(R.id.editTextNgan);
        tvSquareWah = (TextView) itemView.findViewById(R.id.editTextSquareWah);
        editTextLongitude = (EditText) itemView.findViewById(R.id.editTextLongitude);
        editTextLatitude = (EditText) itemView.findViewById(R.id.editTextLatitude);
        imageViewCapturePlantationMap = (ImageView) itemView.findViewById(R.id.imageViewCapturePlantationMap);
        textViewStatusMap = (TextView) itemView.findViewById(R.id.textViewStatusMap);

        Button btnAgricultureBusinessInformation = (Button) itemView.findViewById(R.id.btnAgricultureBusinessInformation);
        btnAgricultureBusinessInformation.setOnClickListener(this);

        ImageButton buttonEditRegisterCardView = (ImageButton) itemView.findViewById(R.id.buttonEditRegisterCardView);
        buttonEditRegisterCardView.setOnClickListener(this);

        linearLayoutCardViewRegisterMap = (FrameLayout) itemView.findViewById(R.id.frameLayoutCardViewRegisterMap);
        linearLayoutCardViewRegisterMap.setVisibility(View.GONE);

        imageViewErrorTypeAgriculture = (ImageView) itemView.findViewById(R.id.imageViewErrorTypeAgriculture);
        imageViewIconMap = (ImageView) itemView.findViewById(R.id.imageViewIconMap);
    }

    public void setFullAddress(String address) {
        editTextFullAddress.setText(address);
    }

    public void setTvNumber(String number) {
        tvNumber.setText(number);
    }

    public void setTvDeed(String deed) {
        tvDeed.setText(deed);
    }

    public void setTvRai(String tvRai) {
        this.tvRai.setText(tvRai);
    }

    public void setTvNgan(String tvNgan) {
        this.tvNgan.setText(tvNgan);
    }

    public void setTvSquareWah(String tvSquareWah) {
        this.tvSquareWah.setText(tvSquareWah);
    }

    public void setEditTextLongitude(String longitudeX) {
        this.editTextLongitude.setText(longitudeX);
    }

    public void setEditTextLatitude(String latitudeY) {
        this.editTextLatitude.setText(latitudeY);
    }

    public void setImageViewCapturePlantationMap(File file) {
        Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
        imageViewCapturePlantationMap.setImageBitmap(new ScaledBitmap().getScaledBitmapForCardView(bitmap));
    }

    public void showMapOnCardView(boolean mapShow){
        if(mapShow){
//            Fragment fragmentMap = fragmentManagerLocal.findFragmentById(R.id.fragmentCardViewRegisterMap);
//            SupportMapFragment supportMapFragment = (SupportMapFragment) fragmentMap;
//            supportMapFragment.getMapAsync(this);
            linearLayoutCardViewRegisterMap.setVisibility(View.VISIBLE);
        }else {
            linearLayoutCardViewRegisterMap.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonEditRegisterCardView) {
            Intent intent = new Intent(v.getContext(), DetailPlantationActivity.class);
            intent.putExtra("mode","edit");
            intent.putExtra("positionPlantation", getAdapterPosition());
            v.getContext().startActivity(intent);
        } else if (v.getId() == R.id.btnAgricultureBusinessInformation) {
            Intent intent = new Intent(v.getContext(), RegisterAgriculturalOperationsActivity.class);
            intent.putExtra("position",getAdapterPosition());
            v.getContext().startActivity(intent);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        moveCameraToLocation(googleMap);
        createMainPolygon(googleMap);
        createSubPolygon(googleMap);
    }

    private void createSubPolygon(GoogleMap googleMap) {
        if(subCoordinates != null){
            for(int index = 0; index < subCoordinates.size(); index++){

                PolygonOptions polygonOptions = checkSubCoordinates(index);

                polygonOptions.strokeColor(0x6066BB6A);
                polygonOptions.fillColor(0x6066BB6A);
                googleMap.addPolygon(polygonOptions);
            }
        }
    }

    private PolygonOptions checkSubCoordinates(int index) {
        List<Coordinates> list = subCoordinates.get(index).getCoordinates();
        PolygonOptions polygonOptions = new PolygonOptions();
        if(list != null){
            for(Coordinates coordinate: list){
                double latitude = coordinate.getLatitude();
                double longitude = coordinate.getLongitude();
                LatLng latLng = new LatLng(latitude, longitude);
                polygonOptions.add(latLng);
            }
        }
        return polygonOptions;
    }

    private void createMainPolygon(GoogleMap googleMap) {
        if(coordinates != null){
            PolygonOptions polygonOptions = new PolygonOptions();
            for(int index = 0 ; index < coordinates.size() ; index ++){
                double latitude = coordinates.get(index).getLatitude();
                double longitude = coordinates.get(index).getLongitude();
                LatLng latLng = new LatLng(latitude, longitude);
                polygonOptions.add(latLng);
            }
            polygonOptions.strokeColor(0x90320000);
            polygonOptions.fillColor(0x90320000);
            googleMap.addPolygon(polygonOptions);
        }
    }

    private void moveCameraToLocation(GoogleMap googleMap) {
        if(coordinates != null){
            double latitude = coordinates.get(0).getLatitude();
            double longitude = coordinates.get(0).getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        }
    }

    public void setMainCoordinates(List<Coordinates> coordinates){
        this.coordinates = coordinates;
    }

    public void setSubCoordinates(List<DetailAgriculturalOperations> subCoordinates) {
        this.subCoordinates = subCoordinates;
    }

    public void setStatusDrawingMap(boolean statusDrawing){
        String str;

        if(statusDrawing){
            str = "วาดแผนที่แล้ว";
            imageViewErrorTypeAgriculture.setVisibility(View.GONE);
            imageViewIconMap.setVisibility(View.VISIBLE);
        }else {
            str = "ยังไม่วาดแผนที่";
            imageViewErrorTypeAgriculture.setVisibility(View.VISIBLE);
            imageViewIconMap.setVisibility(View.GONE);
        }
        textViewStatusMap.setText(str);
    }
}
