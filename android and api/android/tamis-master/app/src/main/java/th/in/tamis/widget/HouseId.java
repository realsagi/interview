package th.in.tamis.widget;

public class HouseId implements Identity {

    private static final int[] MULTIPLIER_TABLE = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
    public static final int LENGTH = 11;

    private String id;
    private PrettyPrinter printer = new IdPrettyPrinter() {
        @Override
        boolean positionToInsertSeparatorBefore(int position) {
            switch (position) {
                case 4:
                case 10:
                    return true;
            }
            return false;
        }
    };

    public HouseId(String id) {
        if (id == null)
            throw new NullPointerException("ID must not be null");
        this.id = id.replace(printer.separator(), "");
    }

    @Override
    public boolean isValidFormat() {
        return !(id.length() != LENGTH || !TextUtils.isDigitOnly(id));
    }

    protected int getCheckDigit() {
        if (!isValidFormat())
            return -1;
        int lastIndex = HouseId.LENGTH - 1;
        return Character.digit(id.charAt(lastIndex), 10);
    }

    @Override
    public boolean validate() {
        return isValidFormat() &&
                !TextUtils.isRepeatingNumber(id) &&
                calculateCheckDigit() == getCheckDigit();
    }

    protected int calculateCheckDigit() {
        int sum = 0;
        for (int position = 0; position < LENGTH - 1; position++) {
            sum += Character.digit(id.charAt(position), 10) * MULTIPLIER_TABLE[position];
        }
        int x = sum % 11;
        return (11 - x) % 10;
    }

    @Override
    public String prettyPrint() {
        return printer.print(id);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return printer.print(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HouseId)) return false;

        HouseId houseId = (HouseId) o;
        return id.equals(houseId.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
