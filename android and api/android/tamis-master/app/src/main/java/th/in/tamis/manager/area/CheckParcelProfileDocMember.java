package th.in.tamis.manager.area;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.models.ParcelDoc;

public class CheckParcelProfileDocMember implements Callback<Boolean> {

    private onLoadParcelProfileDocMember onLoadParcelProfileDocMember;
    private ParcelDoc parcelDoc;

    public CheckParcelProfileDocMember(onLoadParcelProfileDocMember listener, ParcelDoc parcelDoc){
        this.onLoadParcelProfileDocMember = listener;
        this.parcelDoc = parcelDoc;
    }

    @Override
    public void onResponse(Response<Boolean> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            onLoadParcelProfileDocMember.onLoadParcelProfileDocMemberFails("error 404");
        }else if(response.isSuccess()){
            onLoadParcelProfileDocMember.onLoadParcelProfileDocMemberSuccess(response.body(), parcelDoc);
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadParcelProfileDocMember.onLoadParcelProfileDocMemberFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadParcelProfileDocMember.onLoadParcelProfileDocMemberFails("timeout");
        }
    }
}
