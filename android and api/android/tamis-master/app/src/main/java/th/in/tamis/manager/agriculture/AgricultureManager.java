package th.in.tamis.manager.agriculture;

import th.in.tamis.manager.http.HTTPManager;

public class AgricultureManager {
    public static APIServiceAgriculture getServiceAgriculture(){
        return HTTPManager.getInstance().getRetrofit().create(APIServiceAgriculture.class);
    }
}
