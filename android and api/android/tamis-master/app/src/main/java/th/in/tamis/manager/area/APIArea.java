package th.in.tamis.manager.area;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;
import th.in.tamis.models.District;
import th.in.tamis.models.Province;
import th.in.tamis.models.SubDistrict;

public interface APIArea {
    @GET("/area/province")
    Call<Province> loadProvince();

    @GET("/area/all_district")
    Call<District> loadAllDistrict();

    @GET("/area/district")
    Call<District> loadDistrict(@Query("province_code") String province_code);

    @GET("/area/all_sub_district")
    Call<SubDistrict> loadAllSubDistrict();

    @GET("area/sub_district")
    Call<SubDistrict> loadSubDistrict(@Query("province_code") String provinceCode, @Query("district_code") String districtCode);

    @GET("/area/location_code")
    Call<String> loadLocationCode(@Query("province") String provinceName,
                                  @Query("district") String districtName,
                                  @Query("sub_district") String subDistrictName);

    @GET("/area/postcode")
    Call<String> loadPostCode(@Query("location_code") String locationCode);

    @GET("/area/profile_doc")
    Call<Boolean> checkProfileDoc(@Query("parcel_type") String parcelType,
                                  @Query("parcel_number") String parcelNumber,
                                  @Query("province_code") String provinceCode,
                                  @Query("district_code") String districtCode);

    interface AreaOnLoadData{
        void onLoadLocationCodeSuccess(String locationCode);
        void onLoadPostCodeSuccess(String postCode);
        void onLoadFails(String message);
    }
}
