package th.in.tamis;

import android.app.Application;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import retrofit.Call;
import th.in.tamis.manager.Contextor;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.service.file.FileManagement;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class TamisApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        Contextor.getInstance().init(getApplicationContext());
        new FileManagement().generateFolderTamis();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/THSarabunNew.ttf")
                .build()
        );

        PreferencesService.removeKey("user");
        PreferencesService.clearAllKey();
    }
}