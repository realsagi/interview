package th.in.tamis.widget;

import android.widget.EditText;

public class CitizenIdHandler extends IdentityEditTextHandler {

    public static final String DEFAULT_ERROR_MESSAGE = "รหัสประชาชน ไม่ถูกต้อง";
    private static final int MAX_LENGTH = 17;

    public CitizenIdHandler(EditText editText) {
        super(editText);
    }

    @Override
    protected int getMaxLenght() {
        return MAX_LENGTH;
    }

    @Override
    protected String getErrorMessage() {
        return DEFAULT_ERROR_MESSAGE;
    }

    @Override
    protected Identity onCreateNewId(String id) {
        return new CitizenId(id);
    }
}
