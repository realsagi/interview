package th.in.tamis.manager.agriculture;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.OnLoadCacheAll;
import th.in.tamis.models.AgricultureDetail;

public class AgricultureDetailAllManager implements Callback<AgricultureDetail> {

    private OnLoadCacheAll onLoadAreaAll;

    public AgricultureDetailAllManager(OnLoadCacheAll listener) {
        this.onLoadAreaAll = listener;
    }

    @Override
    public void onResponse(Response<AgricultureDetail> response, Retrofit retrofit) {
        if (response.isSuccess()) {
            onLoadAreaAll.onLoadAgricultureDetailSuccess(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadAreaAll.onLoadFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadAreaAll.onLoadFails("timeout");
        }
    }
}
