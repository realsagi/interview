package th.in.tamis.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import th.in.tamis.adapter.AgriculturalOperationsAdapter;
import th.in.tamis.models.Address;
import th.in.tamis.models.DetailRegisterPlantation;
import th.in.tamis.models.RegisterPlantation;
import th.in.tamis.service.AddressFormat;
import th.in.tamis.service.DeedFormat;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.tamis.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterAgriculturalOperationsActivity extends AppCompatActivity implements View.OnClickListener{

    private RecyclerView rvAgricultural;
    private AgriculturalOperationsAdapter agriculturalOperationsAdapter;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_agricultural_operations);
        initInstance();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initInstance() {
        position = getIntent().getIntExtra("position",0);

        RegisterPlantation registerPlantation = (RegisterPlantation) PreferencesService
                .getPreferences("registerPlantation", RegisterPlantation.class);

        DetailRegisterPlantation detailRegisterPlantation = registerPlantation
                .getDetailRegisterPlantation()
                .get(position);

        EditText edLocation = (EditText) findViewById(R.id.editTextLocation);
        Address address = detailRegisterPlantation.getAddress();
        AddressFormat addressFormat = AddressFormat.setAddressFormat(address);
        edLocation.setText(addressFormat.getFormat());

        EditText edLocationNo = (EditText) findViewById(R.id.editTextLocationNo);
        edLocationNo.setText(detailRegisterPlantation.getLicenseNumber());

        EditText editTextArea = (EditText) findViewById(R.id.editTextArea);
        DeedFormat deedFormat = DeedFormat.setDetailRegisterPlantationFormat(detailRegisterPlantation);
        editTextArea.setText(deedFormat.getFormat());

        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        ImageButton btnAddAgricultural = (ImageButton) findViewById(R.id.btnAddAgricultural);
        btnAddAgricultural.setOnClickListener(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RegisterAgriculturalOperationsActivity.this,
                LinearLayoutManager.VERTICAL,
                false);
        agriculturalOperationsAdapter = new AgriculturalOperationsAdapter();
        agriculturalOperationsAdapter.setPositionOfDetailRegisterPlantation(position);
        rvAgricultural = (RecyclerView) findViewById(R.id.rvAgricultural);
        rvAgricultural.setLayoutManager(mLayoutManager);

        setBoldText();
    }

    private void setBoldText() {
        Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/THSarabunNewBold.ttf");
        TextView textViewAgricultural = (TextView) findViewById(R.id.textViewAgricultural);
        textViewAgricultural.setTypeface(typeface);

        TextView textViewLocation = (TextView) findViewById(R.id.textViewLocation);
        textViewLocation.setTypeface(typeface);

        TextView textViewLocationNo = (TextView) findViewById(R.id.textViewParcelNumber);
        textViewLocationNo.setTypeface(typeface);

        TextView textViewArea = (TextView)findViewById(R.id.textViewArea);
        textViewArea.setTypeface(typeface);
    }

    @Override
    protected void onResume() {
        super.onResume();
        rvAgricultural.setAdapter(agriculturalOperationsAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnBack:
                finish();
                break;
            case R.id.btnAddAgricultural:
                Intent intent = new Intent(v.getContext(), DetailAgriculturalOperationsActivity.class);
                intent.putExtra("mode", "Add new");
                intent.putExtra("position",position);
                startActivity(intent);
                break;
        }
    }
}
