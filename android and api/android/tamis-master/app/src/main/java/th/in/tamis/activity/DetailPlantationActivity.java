package th.in.tamis.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import th.in.tamis.manager.MainBus;
import th.in.tamis.manager.area.APIArea;
import th.in.tamis.manager.area.AreaMangager;
import th.in.tamis.manager.area.CheckParcelProfileDocMember;
import th.in.tamis.manager.area.DistrictManager;
import th.in.tamis.manager.area.OnLoadArea;
import th.in.tamis.manager.area.ProvinceManager;
import th.in.tamis.manager.area.SubDistrictManager;
import th.in.tamis.manager.area.onLoadParcelProfileDocMember;
import th.in.tamis.manager.parcel.APIServiceParcel;
import th.in.tamis.manager.parcel.OnLoadParcel;
import th.in.tamis.manager.parcel.ParcelDetailManager;
import th.in.tamis.manager.parcel.ParcelManager;
import th.in.tamis.manager.parcel.ParcelTypeManager;
import th.in.tamis.models.Address;
import th.in.tamis.models.Coordinates;
import th.in.tamis.models.DetailRegisterPlantation;
import th.in.tamis.models.District;
import th.in.tamis.models.Farmer;
import th.in.tamis.models.ParcelDoc;
import th.in.tamis.models.ParcelType;
import th.in.tamis.models.Province;
import th.in.tamis.models.RegisterPlantation;
import th.in.tamis.models.SubDistrict;
import th.in.tamis.service.Area;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.service.StringWithTag;
import th.in.tamis.service.authentication.TokenLoginService;
import th.in.tamis.service.file.FileManagement;
import th.in.tamis.service.image.Rotate;
import th.in.tamis.service.image.ScaledBitmap;
import th.in.tamis.tamis.R;
import th.in.tamis.widget.AreaPickerDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;

public class DetailPlantationActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemSelectedListener, Dialog.OnClickListener, TextWatcher,
        OnMapReadyCallback, onLoadParcelProfileDocMember, OnLoadParcel, OnLoadArea {

    private Spinner spinnerLicenseType;
    private EditText editTextLicenseNumber;
    private EditText editTextRaWangNumber;
    private EditText editTextRai;
    private EditText editTextNgan;
    private EditText editTextSquareWah;
    private EditText editTextCorporateOwnershipLicense;
    private EditText editTextMoo;
    private Spinner spinnerProvince;
    private Spinner spinnerDistrict;
    private Spinner spinnerSubDistrict;
    private RadioGroup radioGroupIrrigationArea;
    private RadioGroup radioGroupHolding;
    private Address address;
    private Button buttonCancelDetail;
    private Button buttonSaveDetail;
    private Button buttonGotoMap;
    private DetailRegisterPlantation detailRegisterPlantation;
    private Button buttonValidateLicenseNumber;
    private RegisterPlantation registerPlantation;
    private List<DetailRegisterPlantation> listRegisterPlantation;
    private Area area;
    private String provinceCode;
    private String districtCode;
    private String subdistrictCode;
    private String type;
    private APIServiceParcel serviceParcel;
    private APIArea serviceArea;
    private Button buttonCapturePlantation;
    private int REQUEST_CAMERA = 0;
    private int REQUEST_GOOGLE_MAP = 1;

    private Boolean modeEdit = false;
    private Boolean changeLicenseType = false;
    private Boolean changeFirstSubDistrict = false;

    private int positionOfList;

    private EditText editTextLatitudePlantations;
    private EditText editTextLongitudePlantations;

    private ImageView imageViewErrorParcelType;
    private ImageView imageViewErrorProvince;
    private ImageView imageViewErrorDistrict;
    private ImageView imageViewErrorSubDistrict;
    private ImageView imageViewErrorLicenseNo;

    private LinearLayout linearLayoutMap;
    private SupportMapFragment supportMapFragment;
    private List<StringWithTag> listSubDistrict;
    private ParcelDoc parcelDoc;

    private Boolean btnGoToMap = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_plantation);
        initInstance();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainBus.getInstance().register(this);
        new TokenLoginService(this).loginByToken();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainBus.getInstance().unregister(this);
    }

    private void initInstance() {
        findId();
        setListener();
        setClassVariable();
        Farmer farmer = (Farmer) PreferencesService.getPreferences("farmer", Farmer.class);
        editTextCorporateOwnershipLicense.setText(String.format("%s%s %s", farmer.getPreName(), farmer.getFirstName(), farmer.getLastName()));

        positionOfList = getIntent().getIntExtra("positionPlantation", 0);
        setDetailPlantationForActivity(positionOfList);

        loadProvince();
        loadParcelType();

        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentMapDetailPlantation);
        supportMapFragment.getMapAsync(this);
        linearLayoutMap.setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        googleMap.setMapType(MAP_TYPE_SATELLITE);
        moveCameraToLocation(googleMap);
        createPolygon(googleMap);
    }

    private void moveCameraToLocation(GoogleMap googleMap) {
        if (detailRegisterPlantation.getCoordinates() != null) {
            double latitude = detailRegisterPlantation.getCoordinates().get(0).getLatitude();
            double longitude = detailRegisterPlantation.getCoordinates().get(0).getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        }
    }

    private void createPolygon(GoogleMap googleMap) {
        if (detailRegisterPlantation.getCoordinates() != null) {
            linearLayoutMap.setVisibility(View.VISIBLE);
            PolygonOptions polygonOptions = new PolygonOptions();
            for (int index = 0; index < detailRegisterPlantation.getCoordinates().size(); index++) {
                double latitude = detailRegisterPlantation.getCoordinates().get(index).getLatitude();
                double longitude = detailRegisterPlantation.getCoordinates().get(index).getLongitude();
                LatLng latLng = new LatLng(latitude, longitude);
                polygonOptions.add(latLng);
            }
            polygonOptions.strokeColor(0x609666BB);
            polygonOptions.fillColor(0x609666BB);
            googleMap.addPolygon(polygonOptions);
        }
    }

    public void setDetailPlantationForActivity(int position) {
        String mode = getIntent().getStringExtra("mode");
        if (mode != null && "edit".endsWith(mode)) {
            modeEdit = true;
            detailRegisterPlantation = registerPlantation.getDetailRegisterPlantation().get(position);
            editTextCorporateOwnershipLicense.setText(detailRegisterPlantation.getCorporateOwnershipLicense());
            setViewForShowDetailRegisterPlantation();
        }
    }

    private void findId() {
        spinnerLicenseType = (Spinner) findViewById(R.id.spinnerParcelType);
        editTextLicenseNumber = (EditText) findViewById(R.id.editTextLicenseNo);
        editTextRaWangNumber = (EditText) findViewById(R.id.editTextRaWangNumber);
        editTextRai = (EditText) findViewById(R.id.editTextRai);
        editTextNgan = (EditText) findViewById(R.id.editTextNgan);
        editTextSquareWah = (EditText) findViewById(R.id.editTextSquareWah);
        editTextCorporateOwnershipLicense = (EditText) findViewById(R.id.editTextCorporateOwnershipLicense);
        editTextMoo = (EditText) findViewById(R.id.editTextMoo);
        spinnerProvince = (Spinner) findViewById(R.id.spinnerProvince);
        spinnerDistrict = (Spinner) findViewById(R.id.spinnerDistrict);
        spinnerSubDistrict = (Spinner) findViewById(R.id.spinnerSubDistrict);
        radioGroupIrrigationArea = (RadioGroup) findViewById(R.id.radioGroupIrrigationArea);
        radioGroupHolding = (RadioGroup) findViewById(R.id.radioGroupHolding);
        buttonCancelDetail = (Button) findViewById(R.id.buttonCancelDetail);
        buttonSaveDetail = (Button) findViewById(R.id.buttonSaveDetail);
        buttonSaveDetail.setEnabled(false);
        buttonGotoMap = (Button) findViewById(R.id.buttonGotoMap);
        buttonGotoMap.setEnabled(false);
        buttonValidateLicenseNumber = (Button) findViewById(R.id.buttonValidateLicenseNumber);
        buttonCapturePlantation = (Button) findViewById(R.id.buttonCapturePlantation);

        imageViewErrorParcelType = (ImageView) findViewById(R.id.imageViewErrorParcelType);
        imageViewErrorProvince = (ImageView) findViewById(R.id.imageViewErrorProvince);
        imageViewErrorDistrict = (ImageView) findViewById(R.id.imageViewErrorDistrict);
        imageViewErrorSubDistrict = (ImageView) findViewById(R.id.imageViewErrorSubDistrict);
        imageViewErrorLicenseNo = (ImageView) findViewById(R.id.imageViewErrorLicenseNo);

        editTextLatitudePlantations = (EditText) findViewById(R.id.editTextLatitudePlantations);
        editTextLongitudePlantations = (EditText) findViewById(R.id.editTextLongitudePlantations);
        linearLayoutMap = (LinearLayout) findViewById(R.id.linearLayoutMap);
    }

    private void setListener() {
        spinnerLicenseType.setOnItemSelectedListener(this);
        spinnerProvince.setOnItemSelectedListener(this);
        spinnerDistrict.setOnItemSelectedListener(this);
        spinnerSubDistrict.setOnItemSelectedListener(this);
        buttonCancelDetail.setOnClickListener(this);
        buttonSaveDetail.setOnClickListener(this);
        buttonGotoMap.setOnClickListener(this);
        buttonValidateLicenseNumber.setOnClickListener(this);
        editTextRai.setOnClickListener(this);
        editTextNgan.setOnClickListener(this);
        editTextSquareWah.setOnClickListener(this);
        buttonCapturePlantation.setOnClickListener(this);


        editTextMoo.addTextChangedListener(this);
        editTextLicenseNumber.addTextChangedListener(this);
        editTextRaWangNumber.addTextChangedListener(this);
        editTextRai.addTextChangedListener(this);
        editTextNgan.addTextChangedListener(this);
        editTextSquareWah.addTextChangedListener(this);
    }

    private void setClassVariable() {
        registerPlantation = (RegisterPlantation) PreferencesService.getPreferences("registerPlantation", RegisterPlantation.class);
        if (registerPlantation == null) {
            registerPlantation = new RegisterPlantation();
        }

        listRegisterPlantation = registerPlantation.getDetailRegisterPlantation();
        if (listRegisterPlantation == null) {
            listRegisterPlantation = new ArrayList<>();
        }

        address = new Address();
        detailRegisterPlantation = new DetailRegisterPlantation();
        area = new Area(0);

        if(detailRegisterPlantation.getParcelDoc() == null){
            parcelDoc = new ParcelDoc();
            parcelDoc.setVerifyDOL(false);
            detailRegisterPlantation.setParcelDoc(parcelDoc);
        }
    }

    private void loadProvince() {
        serviceArea = AreaMangager.getArea();
        SharedPreferences settings;
        settings = getSharedPreferences("province", 0);
        String data = settings.getString("province", null);
        Province province = new Gson().fromJson(data, Province.class);
        onLoadProvincesSuccess(province);
        if (province == null) {
            Call<Province> call = serviceArea.loadProvince();
            call.enqueue(new ProvinceManager(this));
        }
    }

    private void loadParcelType() {
        serviceParcel = ParcelManager.getParcel();

        SharedPreferences settings;
        settings = getSharedPreferences("parcelType", 0);
        String data = settings.getString("parcelType", null);
        ParcelType parcelType = new Gson().fromJson(data, ParcelType.class);

        onLoadParcelTypeSuccess(parcelType);
        if (parcelType == null) {
            Call<ParcelType> call = serviceParcel.getParcelType();
            call.enqueue(new ParcelTypeManager(this));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonValidateLicenseNumber:
                clearWhenLicenseNumberChange();
                if (validateLicenseNumberByType()) {
                    Call<ParcelDoc> callParcel = serviceParcel.getParcel(
                            provinceCode,
                            districtCode,
                            type,
                            editTextLicenseNumber.getText().toString());
                    callParcel.enqueue(new ParcelDetailManager(this));
                }
                break;
            case R.id.buttonCancelDetail:
                finish();
                break;
            case R.id.buttonGotoMap:
                if(validateGoToMap()) {
                    ParcelDoc parcelDoc = detailRegisterPlantation.getParcelDoc();
                    RadioButton radioButtonHolding = (RadioButton) findViewById(radioGroupHolding.getCheckedRadioButtonId());
                    Intent goToMap = new Intent(v.getContext(), MapActivity.class);
                    goToMap.putExtra("parcel", new Gson().toJson(parcelDoc));
                    goToMap.putExtra("area", new Gson().toJson(area));
                    goToMap.putExtra("locationAddress",
                            spinnerProvince.getSelectedItem().toString() +
                                    spinnerDistrict.getSelectedItem().toString() +
                                    spinnerSubDistrict.getSelectedItem().toString());
                    goToMap.putExtra("coordinateListOld", new Gson().toJson(detailRegisterPlantation.getCoordinates()));
                    goToMap.putExtra("locationCenter", new Gson().toJson(detailRegisterPlantation.getCoordinatesCenter()));
                    goToMap.putExtra("holding", radioButtonHolding.getText().toString());
                    startActivityForResult(goToMap, REQUEST_GOOGLE_MAP);
                }
                    break;
            case R.id.buttonSaveDetail:
                Call<Boolean> profileDoc = serviceArea.checkProfileDoc(
                        type,
                        editTextLicenseNumber.getText().toString(),
                        provinceCode,
                        districtCode);
                profileDoc.enqueue(new CheckParcelProfileDocMember(this, detailRegisterPlantation.getParcelDoc()));
                break;
            case R.id.buttonCapturePlantation:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File fileImage = new FileManagement("DETAIL_PLANTATION_").generatePath();
                detailRegisterPlantation.setImageDetailPlantation(fileImage);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(detailRegisterPlantation.getImageDetailPlantation()));
                startActivityForResult(Intent.createChooser(intent, "Take a picture with"), REQUEST_CAMERA);
                break;
            case R.id.editTextRai:
            case R.id.editTextNgan:
            case R.id.editTextSquareWah:
                Area area = new Area(this.area.getSquareMeter());
                AreaPickerDialog.OnAreaPickListener onDialogPickListener = new AreaPickerDialog.OnAreaPickListener() {
                    @Override
                    public void onAreaPick(Area area) {
                        setArea(area);
                        editTextRai.setText(String.format("%d", area.getRai()));
                        editTextNgan.setText(String.format("%d", area.getNgan()));
                        editTextSquareWah.setText(String.format("%d", area.getSquareWah()));
                        setValueAreaObjectByAreaField();
                        changeStatusButton(area);
                    }

                    @Override
                    public void onCancel() {

                    }
                };
                AreaPickerDialog pickerDialog = new AreaPickerDialog(this, onDialogPickListener);
                pickerDialog.show(area);
                break;
        }
    }

    private void changeStatusButton(Area area) {
        if (area.getRai() > 0 || area.getNgan() > 0 || area.getSquareWah() > 0) {
            buttonGotoMap.setEnabled(true);
            buttonSaveDetail.setEnabled(true);
        } else {
            buttonGotoMap.setEnabled(false);
            buttonSaveDetail.setEnabled(false);
        }
    }

    public void setArea(Area area) {
        if (area == null)
            throw new NullPointerException("area must not be null");
        this.area = area;
    }


    public boolean validateGoToMap() {
        boolean isVerifyGoToMap = true;

        btnGoToMap = false;
        if(!IsValidateParcelNumber()){
            isVerifyGoToMap = false;
        }
        if (spinnerSubDistrict.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectSubDistrict))) {
            imageViewErrorSubDistrict.setVisibility(View.VISIBLE);
            isVerifyGoToMap = false;
        } else {
            imageViewErrorSubDistrict.setVisibility(View.INVISIBLE);
        }


        if (IsTextFieldEmpty(editTextRai.getText().toString())) {
            editTextRai.setError(getString(R.string.pleaseInputRai));
            isVerifyGoToMap = false;
        }
        if (IsTextFieldEmpty(editTextNgan.getText().toString())) {
            editTextNgan.setError(getString(R.string.pleaseInputNang));
            isVerifyGoToMap = false;
        }
        if (IsTextFieldEmpty(editTextSquareWah.getText().toString())) {
            editTextSquareWah.setError(getString(R.string.pleaseInputSquareWah));
            isVerifyGoToMap = false;
        }
        if (editTextRai.getText().toString().equals("0") && editTextNgan.getText().toString().equals("0") && editTextSquareWah.getText().toString().equals("0")) {
            editTextRai.setError("กรุณากรอกพื้นที่ตามเอกสารสิทธิ์");
            editTextNgan.setError("กรุณากรอกพื้นที่ตามเอกสารสิทธิ์");
            editTextSquareWah.setError("กรุณากรอกพื้นที่ตามเอกสารสิทธิ์");
            isVerifyGoToMap = false;
        }

        if (!isVerifyGoToMap) {
            createDialogAlertNoTitle(getString(R.string.allFieldRequired));
        }

        btnGoToMap = true;
        return isVerifyGoToMap;
    }


    public boolean validateLicenseNumberByType() {
        boolean isVerifyParcel = true;

        if(!IsValidateParcelNumber()){
            isVerifyParcel = false;
        }

        if (!isVerifyParcel) {
            createDialogAlertNoTitle(getString(R.string.allFieldRequired));
        }

        return isVerifyParcel;
    }

    private boolean IsValidateParcelNumber() {
        Boolean isVerifyParcel = true;
        if (spinnerLicenseType.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectParcelType))) {
            imageViewErrorParcelType.setVisibility(View.VISIBLE);
            isVerifyParcel = false;
        } else {
            imageViewErrorParcelType.setVisibility(View.INVISIBLE);
        }

        if (spinnerProvince.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectProvince))) {
            imageViewErrorProvince.setVisibility(View.VISIBLE);
            isVerifyParcel = false;
        } else {
            imageViewErrorProvince.setVisibility(View.INVISIBLE);
        }

        if (spinnerDistrict.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectDistrict))) {
            imageViewErrorDistrict.setVisibility(View.VISIBLE);
            isVerifyParcel = false;
        } else {
            imageViewErrorDistrict.setVisibility(View.INVISIBLE);
        }

        if (IsTextFieldEmpty(editTextLicenseNumber.getText().toString())) {
            imageViewErrorLicenseNo.setVisibility(View.VISIBLE);
            if (detailRegisterPlantation.getParcelDoc().getVerifyDOL().equals(false) && btnGoToMap.equals(true)) {
                clearDetailLicenseNumberAll();
            }
            isVerifyParcel = false;
        } else {
            imageViewErrorLicenseNo.setVisibility(View.INVISIBLE);
        }
        return isVerifyParcel;
    }

    private boolean validateFields() {
        boolean isGoingNextPage = true;

        RadioButton landForRent = (RadioButton) findViewById(radioGroupHolding.getCheckedRadioButtonId());
        RadioButton other = (RadioButton) findViewById(radioGroupHolding.getCheckedRadioButtonId());

        if (spinnerLicenseType.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectParcelType))) {
            imageViewErrorParcelType.setVisibility(View.VISIBLE);
            isGoingNextPage = false;
        } else {
            imageViewErrorParcelType.setVisibility(View.INVISIBLE);
        }

        if (spinnerProvince.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectProvince))) {
            imageViewErrorProvince.setVisibility(View.VISIBLE);
            isGoingNextPage = false;
        } else {
            imageViewErrorProvince.setVisibility(View.INVISIBLE);
        }

        if (spinnerDistrict.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectDistrict))) {
            imageViewErrorDistrict.setVisibility(View.VISIBLE);
            isGoingNextPage = false;
        } else {
            imageViewErrorDistrict.setVisibility(View.INVISIBLE);
        }

        if (spinnerSubDistrict.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectSubDistrict))) {
            imageViewErrorSubDistrict.setVisibility(View.VISIBLE);
            isGoingNextPage = false;
        } else {
            imageViewErrorSubDistrict.setVisibility(View.INVISIBLE);
        }

        if (IsTextFieldEmpty(editTextLicenseNumber.getText().toString())) {
            imageViewErrorLicenseNo.setVisibility(View.VISIBLE);
            isGoingNextPage = false;
        } else {
            imageViewErrorLicenseNo.setVisibility(View.INVISIBLE);
        }

        if (IsTextFieldEmpty(editTextRai.getText().toString())) {
            editTextRai.setError(getString(R.string.pleaseInputRai));
            isGoingNextPage = false;
        }
        if (IsTextFieldEmpty(editTextNgan.getText().toString())) {
            editTextNgan.setError(getString(R.string.pleaseInputNang));
            isGoingNextPage = false;
        }
        if (IsTextFieldEmpty(editTextSquareWah.getText().toString())) {
            editTextSquareWah.setError(getString(R.string.pleaseInputSquareWah));
            isGoingNextPage = false;
        }
        if (editTextRai.getText().toString().equals("0") && editTextNgan.getText().toString().equals("0") && editTextSquareWah.getText().toString().equals("0")) {
            editTextRai.setError("กรุณากรอกพื้นที่ตามเอกสารสิทธิ์");
            editTextNgan.setError("กรุณากรอกพื้นที่ตามเอกสารสิทธิ์");
            editTextSquareWah.setError("กรุณากรอกพื้นที่ตามเอกสารสิทธิ์");
            isGoingNextPage = false;
        }

        if (!isGoingNextPage) {
            createDialogAlertNoTitle(getString(R.string.allFieldRequired));
        }

        if (landForRent.getText().toString().equals(getString(R.string.landForRent)) &&
                detailRegisterPlantation.getCoordinates() == null) {
            createDialogAlertNoTitle(getString(R.string.pleaseDrawMap));

            isGoingNextPage = false;
        }
        if (other.getText().toString().equals(getText(R.string.etc)) &&
                detailRegisterPlantation.getCoordinates() == null) {
            createDialogAlertNoTitle(getString(R.string.pleaseDrawMap));
            isGoingNextPage = false;
        }

        return isGoingNextPage;
    }

    private boolean IsTextFieldEmpty(String textField) {
        if (textField.equals("")) {
            return true;
        }
        return false;
    }

    private void saveData() {

        detailRegisterPlantation.setLicenseType(type);
        address.setMoo(editTextMoo.getText().toString());
        address.setProvince(provinceCode);
        address.setDistrict(districtCode);
        address.setSubDistrict(subdistrictCode);

        address.setProvinceName(spinnerProvince.getSelectedItem().toString());
        address.setDistrictName(spinnerDistrict.getSelectedItem().toString());
        address.setSubDistrictName(spinnerSubDistrict.getSelectedItem().toString());

        detailRegisterPlantation.setAddress(address);

        detailRegisterPlantation.setLicenseNumber(editTextLicenseNumber.getText().toString());
        detailRegisterPlantation.setRaWangNumber(editTextRaWangNumber.getText().toString());

        Coordinates coordinates = new Coordinates();
        if (!("").equals(editTextLatitudePlantations.getText().toString())
                && !("").equals(editTextLongitudePlantations.getText().toString())) {
            coordinates.setLatitude(Double.parseDouble(editTextLatitudePlantations.getText().toString()));
            coordinates.setLongitude(Double.parseDouble(editTextLongitudePlantations.getText().toString()));
            detailRegisterPlantation.setCoordinatesCenter(coordinates);
        }

        detailRegisterPlantation.setRai(Integer.parseInt(editTextRai.getText().toString()));
        detailRegisterPlantation.setNgan(Integer.parseInt(editTextNgan.getText().toString()));
        detailRegisterPlantation.setSquareWah(Double.parseDouble(editTextSquareWah.getText().toString()));
        detailRegisterPlantation.setCorporateOwnershipLicense(editTextCorporateOwnershipLicense.getText().toString());

        RadioButton radioButtonIrrigationArea = (RadioButton) findViewById(radioGroupIrrigationArea.getCheckedRadioButtonId());
        detailRegisterPlantation.setIrrigationArea(radioButtonIrrigationArea.getText().toString());

        RadioButton radioButtonHolding = (RadioButton) findViewById(radioGroupHolding.getCheckedRadioButtonId());
        detailRegisterPlantation.setHolding(radioButtonHolding.getText().toString());

        if(detailRegisterPlantation.getParcelDoc().getVerifyDOL().equals(false)){
            parcelDoc.setProvinceName(address.getProvince());
            parcelDoc.setDistrictName(address.getDistrict());
            parcelDoc.setSubDistrictName(address.getSubDistrict());

            if(coordinates.getLongitude() == null){
                parcelDoc.setMapY(0.0);
            }else {
                parcelDoc.setMapY(coordinates.getLongitude());
            }
            if(coordinates.getLatitude() == null){
                parcelDoc.setMapX(0.0);
            }else {
                parcelDoc.setMapX(coordinates.getLatitude());
            }

            parcelDoc.setUtmMap(detailRegisterPlantation.getRaWangNumber());
            parcelDoc.setParcelNumber(detailRegisterPlantation.getLicenseNumber());
            parcelDoc.setParcelTypeID(Integer.parseInt(type));
            detailRegisterPlantation.setParcelDoc(parcelDoc);
        }

        if (modeEdit) {
            listRegisterPlantation.set(positionOfList, detailRegisterPlantation);
        } else {
            listRegisterPlantation.add(detailRegisterPlantation);
        }

        registerPlantation.setDetailRegisterPlantation(listRegisterPlantation);
        PreferencesService.savePreferences("registerPlantation", registerPlantation);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            Bitmap bitmap = BitmapFactory.decodeFile(detailRegisterPlantation.getImageDetailPlantation().getPath());
            setImageViewPlantationFromCamera(bitmap).setVisibility(View.VISIBLE);
        } else if (requestCode == REQUEST_GOOGLE_MAP && resultCode == RESULT_OK) {
            String coordinatesString = data.getStringExtra("coordinatesList");
            List<Coordinates> coordinatesList = (List<Coordinates>) new Gson()
                    .fromJson(coordinatesString, new TypeToken<List<Coordinates>>() {
                    }.getType());
            detailRegisterPlantation.setCoordinates(coordinatesList);

            Boolean parcelDocVerify = detailRegisterPlantation.getParcelDoc().getVerifyDOL();
            if (parcelDocVerify.equals(false)) {
                String coordinateCenterString = data.getStringExtra("coordinateCenter");
                LatLng coordinateCenter = new Gson().fromJson(coordinateCenterString, LatLng.class);
                Coordinates coordinates = new Coordinates();
                coordinates.setLatitude(coordinateCenter.latitude);
                coordinates.setLongitude(coordinateCenter.longitude);
                detailRegisterPlantation.setCoordinatesCenter(coordinates);
            }
            editTextLatitudePlantations.setText(String.valueOf(detailRegisterPlantation.getCoordinatesCenter().getLatitude()));
            editTextLongitudePlantations.setText(String.valueOf(detailRegisterPlantation.getCoordinatesCenter().getLongitude()));

            supportMapFragment.getMapAsync(this);
        }
    }

    private void showMapSnapShot(Intent data) {
        String filepath = data.getStringExtra("fileSnapShot");
        File file = new Gson().fromJson(filepath, File.class);
        Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
        setImageSnapShot(bitmap).setVisibility(View.VISIBLE);
        detailRegisterPlantation.setFileSnapShotMap(file);
    }

    public ImageView setImageViewPlantationFromCamera(Bitmap bitmap) {
        ImageView imageViewPlantation = (ImageView) findViewById(R.id.imageViewPlantation);
        if (bitmap != null) {
            imageViewPlantation.setImageBitmap(new ScaledBitmap().getScaledBitmapFullImage(bitmap));
            imageViewPlantation.setRotation(Rotate.Rotate(detailRegisterPlantation.getImageDetailPlantation().getPath()));
        }
        return imageViewPlantation;
    }

    public ImageView setImageSnapShot(Bitmap bitmap) {
        ImageView imageViewSnapShot = (ImageView) findViewById(R.id.imageViewSnapShot);
        if (bitmap != null) {
            imageViewSnapShot.setImageBitmap(new ScaledBitmap().getScaledBitmapFullImage(bitmap));
        }
        return imageViewSnapShot;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        StringWithTag stringWithTag = (StringWithTag) parent.getItemAtPosition(position);
        SharedPreferences settings;
        switch (parent.getId()) {
            case R.id.spinnerParcelType:
                if (changeLicenseType) {
                    clearDetailWhenLicenseTypeChange();
                }
                if (getString(R.string.parcelNS4Kor).equals(parent.getItemAtPosition(position).toString()))
                    buttonValidateLicenseNumber.setVisibility(View.VISIBLE);
                else
                    buttonValidateLicenseNumber.setVisibility(View.GONE);
                type = stringWithTag.tag.toString();
                changeLicenseType = true;
                break;
            case R.id.spinnerProvince:
                provinceCode = stringWithTag.tag.toString();

                settings = getSharedPreferences("district" + provinceCode, 0);
                String dataDistrict = settings.getString("district" + provinceCode, null);
                District district = new Gson().fromJson(dataDistrict, District.class);

                onLoadDistrictSuccess(district);
                if (district == null) {
                    Call<District> callProvince = serviceArea.loadDistrict(provinceCode);
                    callProvince.enqueue(new DistrictManager(this, provinceCode));
                }

                spinnerSubDistrict.setOnItemSelectedListener(this);
                setEnabledVerifyParcelNumber();
                break;
            case R.id.spinnerDistrict:
                districtCode = stringWithTag.tag.toString();

                settings = getSharedPreferences("sub_district" + provinceCode + districtCode, 0);
                String dataSubDistrict = settings.getString("sub_district" + provinceCode + districtCode, null);
                SubDistrict subdistrict = new Gson().fromJson(dataSubDistrict, SubDistrict.class);

                onLoadSubDistrictSuccess(subdistrict);
                if (subdistrict == null) {
                    Call<SubDistrict> callDistrict = serviceArea.loadSubDistrict(provinceCode, districtCode);
                    callDistrict.enqueue(new SubDistrictManager(this, provinceCode, districtCode));
                }

                spinnerSubDistrict.setOnItemSelectedListener(this);
                setEnabledVerifyParcelNumber();
                break;
            case R.id.spinnerSubDistrict:
                if (changeFirstSubDistrict) {
                    clearDetailLicenseNumberAll();
                }
                subdistrictCode = stringWithTag.tag.toString();
                changeFirstSubDistrict = true;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void convertAreaField() {
        changeStatusButton(area);
        editTextRai.setText(String.format("%d", area.getRai()));
        editTextNgan.setText(String.format("%d", area.getNgan()));
        editTextSquareWah.setText(String.format("%d", area.getSquareWah()));
    }

    private void setValueAreaObjectByAreaField() {
        area.setRai(Integer.valueOf(editTextRai.getText().toString()));
        area.setNgan(Integer.parseInt(editTextNgan.getText().toString()));
        area.setSquareWah(Integer.parseInt(editTextSquareWah.getText().toString()));
    }

    @Subscribe
    public void onLoadProvincesSuccess(Province provinces) {
        if (provinces != null) {
            if (provinces.getProvinces() != null) {
                List<StringWithTag> listProvince = listForSetArrayAdapter(provinces.getProvinces(), "province_name", "province_id");
                listProvince.add(0, new StringWithTag(getString(R.string.pleaseSelectProvince), 0));
                spinnerProvince.setAdapter(getArrayAdapterForSpinner(listProvince));
                if (detailRegisterPlantation.getAddress() != null) {
                    setSelectionOfAdapterWithCompareOldData(listProvince, detailRegisterPlantation.getAddress().getProvince(), spinnerProvince);
                }
            }
        } else {
            spinnerProvince.setAdapter(null);
        }
    }

    @Subscribe
    public void onLoadDistrictSuccess(District districts) {
        if (districts != null) {
            if (districts.getDistricts() != null) {
                List<StringWithTag> listDistrict = listForSetArrayAdapter(districts.getDistricts(), "district_name", "district_id");
                listDistrict.add(0, new StringWithTag(getString(R.string.pleaseSelectDistrict), 0));
                spinnerDistrict.setAdapter(getArrayAdapterForSpinner(listDistrict));
                if (detailRegisterPlantation.getAddress() != null) {
                    setSelectionOfAdapterWithCompareOldData(listDistrict, detailRegisterPlantation.getAddress().getDistrict(), spinnerDistrict);
                }
            }
        } else {
            List<StringWithTag> listStartDistrict = new ArrayList<>();
            listStartDistrict.add(0, new StringWithTag(getString(R.string.pleaseSelectDistrict), 0));
            spinnerDistrict.setAdapter(getArrayAdapterForSpinner(listStartDistrict));

            List<StringWithTag> listStartSubDistrict = new ArrayList<>();
            listStartSubDistrict.add(0, new StringWithTag(getString(R.string.pleaseSelectSubDistrict), 0));
            spinnerSubDistrict.setAdapter(getArrayAdapterForSpinner(listStartSubDistrict));
        }
    }

    @Subscribe
    public void onLoadSubDistrictSuccess(SubDistrict subDistricts) {
        if (subDistricts != null) {
            if (subDistricts.getSubDistricts() != null) {
                listSubDistrict = listForSetArrayAdapter(subDistricts.getSubDistricts(), "sub_district_name", "sub_district_id");
                listSubDistrict.add(0, new StringWithTag(getString(R.string.pleaseSelectSubDistrict), 0));
                spinnerSubDistrict.setAdapter(getArrayAdapterForSpinner(listSubDistrict));
                if (detailRegisterPlantation.getAddress() != null) {
                    setSelectionOfAdapterWithCompareOldData(listSubDistrict, detailRegisterPlantation.getAddress().getSubDistrict(), spinnerSubDistrict);
                }
            }
        } else {
            List<StringWithTag> listStartSubDistrict = new ArrayList<>();
            listStartSubDistrict.add(0, new StringWithTag(getString(R.string.pleaseSelectSubDistrict), 0));
            spinnerSubDistrict.setAdapter(getArrayAdapterForSpinner(listStartSubDistrict));
        }
    }

    @Subscribe
    public void onLoadParcelTypeSuccess(ParcelType parcelType) {
        if (parcelType != null) {
            if (parcelType.getParcelType() != null) {
                List<StringWithTag> listParcelType = listForSetArrayAdapter(parcelType.getParcelType(), "parcel_type_name", "parcel_no");
                listParcelType.add(0, new StringWithTag(getString(R.string.pleaseSelectParcelType), 0));
                spinnerLicenseType.setAdapter(getArrayAdapterForSpinner(listParcelType));
                setSelectionOfAdapterWithCompareOldData(listParcelType, detailRegisterPlantation.getLicenseType(), spinnerLicenseType);
            }
        }else {
            List<StringWithTag> listStartParcelType = new ArrayList<>();
            listStartParcelType.add(0, new StringWithTag(getString(R.string.pleaseSelectParcelType), 0));
            spinnerLicenseType.setAdapter(getArrayAdapterForSpinner(listStartParcelType));
        }
    }

    private void setSelectionOfAdapterWithCompareOldData(List<StringWithTag> listStringWithTag, String Code, Spinner spinner) {
        for (int index = 0; index < listStringWithTag.size(); index++) {
            if (listStringWithTag.get(index).tag.toString().equals(Code)) {
                spinner.setSelection(index);
                break;
            }
        }
    }

    @Subscribe
    public void onLoadParcelSuccess(ParcelDoc parcelDoc) {
        if (parcelDoc.getSubDistrictName() != null) {
            parcelDoc.setVerifyDOL(true);
            parcelDoc.setParcelTypeID(Integer.parseInt(type));
            detailRegisterPlantation.setParcelDoc(parcelDoc);
            for (int index = 0; index < listSubDistrict.size(); index++) {
                if (parcelDoc.getSubDistrictName().equals(listSubDistrict.get(index).string)) {
                    spinnerSubDistrict.setOnItemSelectedListener(null);
                    spinnerSubDistrict.setSelection(index);
                    subdistrictCode = listSubDistrict.get(index).tag.toString();
                    setEnabledVerifyParcelNumber();
                    break;
                }
            }

            createDialogAlertParcel(parcelDoc, getString(R.string.verifyByDOL));
            editTextRaWangNumber.setText(parcelDoc.getUtmMap());
            Coordinates coordinates = new Coordinates();
            coordinates.setLatitude(parcelDoc.getMapX());
            coordinates.setLongitude(parcelDoc.getMapY());
            detailRegisterPlantation.setCoordinatesCenter(coordinates);
            editTextLatitudePlantations.setText(String.valueOf(parcelDoc.getMapX()));
            editTextLongitudePlantations.setText(String.valueOf(parcelDoc.getMapY()));
        } else {
            parcelDoc.setVerifyDOL(false);
            spinnerSubDistrict.setOnItemSelectedListener(this);
            setEnabledVerifyParcelNumber();
            createDialogAlertNoTitle(String.format("ไม่พบเลขเอกสารสิทธิ์ หมายเลข : %s", editTextLicenseNumber.getText().toString()));
            clearDetailLicenseNumberAll();

        }
    }

    private void createDialogAlertParcel(ParcelDoc parcelDoc, String titleMessage) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert_parcel);

        TextView textViewDialogAlertTitle = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertTitleParcel);
        textViewDialogAlertTitle.setText(titleMessage);

        EditText editTextDialogParcelNumber = (EditText) dialogAlert.findViewById(R.id.editTextDialogParcelNumber);
        EditText editTextRaWang = (EditText) dialogAlert.findViewById(R.id.editTextRaWang);
        EditText editTextLatitude = (EditText) dialogAlert.findViewById(R.id.editTextLatitude);
        EditText editTextLongitude = (EditText) dialogAlert.findViewById(R.id.editTextLongitude);
        EditText editTextAreaParcel = (EditText) dialogAlert.findViewById(R.id.editTextAreaParcel);

        editTextDialogParcelNumber.setText(String.valueOf(parcelDoc.getParcelNumber()));
        editTextRaWang.setText(parcelDoc.getUtmMap());
        editTextLatitude.setText(String.valueOf(parcelDoc.getMapX()));
        editTextLongitude.setText(String.valueOf(parcelDoc.getMapY()));
        editTextAreaParcel.setText("-");

        dialogAlert.show();

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonDialogAlertOK = (Button) dialogAlert.findViewById(R.id.buttonDialogAlertOKParcel);
        buttonDialogAlertOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }

    private void createDialogAlertNoTitle(String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert_no_title);

        TextView textViewMessage = (TextView) dialogAlert.findViewById(R.id.textViewMessage);
        textViewMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonOK = (Button) dialogAlert.findViewById(R.id.buttonOK);
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }

    @Override
    public void onLoadParcelProfileDocMemberSuccess(boolean isMember, ParcelDoc parcelDoc) {
        if (!isMember) {
            if (validateFields()) {
                saveData();
                resizeImage();
                finish();
            }
        } else {
            createDialogAlert("ไม่สามารถใช้เลขที่เอกสารสิทธิ์นี้ลงทะเบียนได้",
                    "หมายเลขเอกสารสิทธิ์นี้ได้มีการลงทะเบียนไว้แล้ว");
        }
    }

    private void resizeImage() {
        if (detailRegisterPlantation.getImageDetailPlantation() != null) {
            try {
                Bitmap bitmap = BitmapFactory.decodeFile(detailRegisterPlantation.getImageDetailPlantation().getPath());
                Bitmap resized = Bitmap.createScaledBitmap(bitmap,
                        (int) (bitmap.getWidth() * 0.4),
                        (int) (bitmap.getHeight() * 0.4), true);

                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(detailRegisterPlantation.getImageDetailPlantation());
                    resized.compress(Bitmap.CompressFormat.JPEG, 100, out);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                detailRegisterPlantation.setImageDetailPlantation(null);
            }
        }
    }

    @Override
    public void onLoadParcelProfileDocMemberFails(String message) {
        createDialogAlert(getString(R.string.errorTimeout),
                getString(R.string.cannotConnectServer).concat(message));
    }

    private void clearDetailWhenLicenseTypeChange() {
        spinnerProvince.setSelection(0);
        spinnerDistrict.setSelection(0);
        spinnerSubDistrict.setOnItemSelectedListener(this);
        setEnabledVerifyParcelNumber();
        if (detailRegisterPlantation.getParcelDoc().getVerifyDOL().equals(false)) {
            spinnerSubDistrict.setSelection(0);
            editTextMoo.setText("");
            clearDetailLicenseNumberAll();
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    private List<StringWithTag> listForSetArrayAdapter(List<JsonObject> objects, String name, String id) {
        List<StringWithTag> lists = new ArrayList<>();
        for (int index = 0; index < objects.size(); index++) {
            if (objects.get(index).get(id) != null) {
                lists.add(new StringWithTag(objects.get(index).get(name).getAsString(),
                        objects.get(index).get(id).getAsString()));
            }
        }
        return lists;
    }

    private ArrayAdapter<String> getArrayAdapterForSpinner(int id) {
        String[] stringArray = getResources().getStringArray(id);
        return new ArrayAdapter<>(this, R.layout.spinner_item, stringArray);
    }

    private ArrayAdapter getArrayAdapterForSpinner(List lists) {
        return new ArrayAdapter(this, R.layout.spinner_item, lists);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    public void setViewForShowDetailRegisterPlantation() {
        if (detailRegisterPlantation.getAddress() != null) {
            editTextMoo.setText(detailRegisterPlantation.getAddress().getMoo());
        }
        editTextLicenseNumber.setText(detailRegisterPlantation.getLicenseNumber());
        editTextRai.setText(String.valueOf(detailRegisterPlantation.getRai()));
        editTextNgan.setText(String.valueOf(detailRegisterPlantation.getNgan()));
        editTextSquareWah.setText(String.valueOf(detailRegisterPlantation.getSquareWah()));
        editTextRaWangNumber.setText(detailRegisterPlantation.getRaWangNumber());
        if (detailRegisterPlantation.getCoordinatesCenter() != null) {
            editTextLatitudePlantations.setText(verifyDoubleNullToStringBlank(detailRegisterPlantation.getCoordinatesCenter().getLatitude()));
            editTextLongitudePlantations.setText(verifyDoubleNullToStringBlank(detailRegisterPlantation.getCoordinatesCenter().getLongitude()));
        }

        if (detailRegisterPlantation.getImageDetailPlantation() != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(detailRegisterPlantation.getImageDetailPlantation().getPath());
            setImageViewPlantationFromCamera(bitmap).setVisibility(View.VISIBLE);
        }

        if (detailRegisterPlantation.getFileSnapShotMap() != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(detailRegisterPlantation.getFileSnapShotMap().getPath());
            setImageSnapShot(bitmap).setVisibility(View.VISIBLE);
        }

        if (detailRegisterPlantation.getIrrigationArea().equals(getText(R.string.insideIrrigated))) {
            radioGroupIrrigationArea.check(R.id.rdbInside);
        } else {
            radioGroupIrrigationArea.check(R.id.rdbOutside);
        }

        if (detailRegisterPlantation.getHolding().equals(getText(R.string.householder))) {
            radioGroupHolding.check(R.id.rdbHousehold);
        } else if (detailRegisterPlantation.getHolding().equals(getText(R.string.landForRent))) {
            radioGroupHolding.check(R.id.rdbLandForRent);
        } else {
            radioGroupHolding.check(R.id.rdbOther);
        }

        area.setRai(detailRegisterPlantation.getRai());
        area.setNgan(detailRegisterPlantation.getNgan());
        area.setSquareWah(detailRegisterPlantation.getSquareWah());

        setEnabledVerifyParcelNumber();
        convertAreaField();
    }

    private String verifyDoubleNullToStringBlank(Double param) {
        if (param == null || param == 0) {
            return "";
        }
        return String.valueOf(param);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(btnGoToMap) {
            if (changeLicenseType && changeFirstSubDistrict) {
                if (editTextMoo.getText() == s) {
                    editTextLicenseNumber.setText("");
                    clearWhenLicenseNumberChange();
                }
                if (editTextLicenseNumber.getText() == s) {
                    parcelDoc = new ParcelDoc();
                    parcelDoc.setVerifyDOL(false);
                    detailRegisterPlantation.setParcelDoc(parcelDoc);
                    clearWhenLicenseNumberChange();
                }
                if (editTextRaWangNumber.getText() == s) {
                    clearWhenRawangNumberChange();
                }
                if (editTextRai.getText() == s
                        || editTextNgan.getText() == s
                        || editTextSquareWah.getText() == s) {
                    clearPictureForCamera();
                    clearMap();
                }
            }
        }
    }

    private void clearDetailLicenseNumberAll() {
        parcelDoc = new ParcelDoc();
        parcelDoc.setVerifyDOL(false);
        detailRegisterPlantation.setParcelDoc(parcelDoc);
        detailRegisterPlantation.setCoordinatesCenter(null);
        editTextLicenseNumber.setText("");
        clearWhenLicenseNumberChange();
    }

    private void clearWhenLicenseNumberChange() {
        spinnerSubDistrict.setOnItemSelectedListener(this);
        setEnabledVerifyParcelNumber();
        detailRegisterPlantation.setLicenseNumber(null);
        detailRegisterPlantation.setRaWangNumber(null);

        editTextRaWangNumber.setText("");
        clearWhenRawangNumberChange();
    }

    private void setEnabledVerifyParcelNumber() {
        if (detailRegisterPlantation.getParcelDoc().getVerifyDOL().equals(true)) {
            setEnabledFieldFalse(spinnerSubDistrict);
            setEnabledFieldFalse(editTextLatitudePlantations);
            setEnabledFieldFalse(editTextLongitudePlantations);
            setEnabledFieldFalse(editTextRaWangNumber);
        } else {
            setEnabledFieldTrue(spinnerSubDistrict);
            setEnabledFieldTrue(editTextLatitudePlantations);
            setEnabledFieldTrue(editTextLongitudePlantations);
            setEnabledFieldTrue(editTextRaWangNumber);
        }
    }

    private void clearWhenRawangNumberChange() {
        editTextLatitudePlantations.setText("");
        editTextLongitudePlantations.setText("");

        area.setRai(0);
        area.setNgan(0);
        area.setSquareWah(0);

        editTextRai.setText("");
        editTextNgan.setText("");
        editTextSquareWah.setText("");

        setEnabledVerifyParcelNumber();

        changeStatusButton(area);

        clearPictureForCamera();
        clearMap();
    }

    private void clearPictureForCamera() {
        setImageSnapShot(null).setVisibility(View.GONE);
        setImageViewPlantationFromCamera(null).setVisibility(View.GONE);
        detailRegisterPlantation.setFileSnapShotMap(null);
        detailRegisterPlantation.setImageDetailPlantation(null);
    }

    private void clearMap() {
        detailRegisterPlantation.setCoordinates(null);
        linearLayoutMap.setVisibility(View.GONE);
    }

    public void setEnabledFieldFalse(Spinner field) {
        field.setEnabled(false);
    }

    public void setEnabledFieldFalse(EditText field) {
        field.setEnabled(false);
    }

    private void setEnabledFieldTrue(EditText field) {
        field.setEnabled(true);
    }

    private void setEnabledFieldTrue(Spinner field) {
        field.setEnabled(true);
    }

    @Override
    public void onLoadParcelFails(String message) {
        createDialogAlert(getString(R.string.errorTimeout),
                getString(R.string.cannotConnectServer).concat(message));
    }

    @Override
    public void onLoadAreaFails(String message) {
        createDialogAlert(getString(R.string.errorTimeout),
                getString(R.string.cannotConnectServer).concat(message));
    }

    private void createDialogAlert(String title, String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert);

        TextView textViewDialogAlertTitle = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertTitle);
        TextView textViewDialogAlertMessage = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertMessage);

        textViewDialogAlertTitle.setText(title);
        textViewDialogAlertMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonDialogAlertOK = (Button) dialogAlert.findViewById(R.id.buttonDialogAlertOK);
        buttonDialogAlertOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }
}