package th.in.tamis.widget;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class CitizenIdEditText extends EditText implements IdentityView  {

    public CitizenIdEditText(Context context) {
        super(context);
        initialHandler();
    }

    public CitizenIdEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialHandler();
    }

    public CitizenIdEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialHandler();
    }

    private IdentityEditTextHandler idHandler;

    private void initialHandler() {
        idHandler = new CitizenIdHandler(this);
    }

    @Override
    public Identity getIdentity() {
        return idHandler.getId();
    }
}
