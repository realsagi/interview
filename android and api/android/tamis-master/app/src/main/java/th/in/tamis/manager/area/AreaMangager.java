package th.in.tamis.manager.area;

import th.in.tamis.manager.http.HTTPManager;

public class AreaMangager {
    public static APIArea getArea(){
        return HTTPManager.getInstance().getRetrofit().create(APIArea.class);
    }
}
