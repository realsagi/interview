package th.in.tamis.manager.area;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.Contextor;
import th.in.tamis.manager.MainBus;
import th.in.tamis.models.Province;

public class ProvinceManager implements Callback<Province>{

    private OnLoadArea onLoadArea;
    public ProvinceManager(OnLoadArea listener){
        this.onLoadArea = listener;
    }

    @Override
    public void onResponse(Response<Province> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            onLoadArea.onLoadAreaFails("error 404");
        }
        if(response.isSuccess()){

            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = Contextor.getInstance().getContext().getSharedPreferences("province", 0);
            editor = settings.edit();
            String data = new Gson().toJson(response.body());
            editor.putString("province", data);
            editor.apply();

            MainBus.getInstance().post(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
//        if(t.toString().contains("ConnectException")) {
//            onLoadArea.onLoadAreaFails("Network is unreachable");
//        }
//        else if(t.toString().contains("SocketTimeoutException")){
//            onLoadArea.onLoadAreaFails("timeout");
//        }
    }
}
