package th.in.tamis.manager.agriculture;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.Contextor;
import th.in.tamis.models.AgricultureDetail;
import th.in.tamis.manager.MainBus;

public class AgricultureDetailManager implements Callback<AgricultureDetail> {

    private APIServiceAgriculture.AgricultureListener agricultureListener;
    private String typeCodeName;

    public AgricultureDetailManager(APIServiceAgriculture.AgricultureListener listener, String typeCodeName){
        agricultureListener = listener;
        this.typeCodeName = typeCodeName;
    }

    @Override
    public void onResponse(Response<AgricultureDetail> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            agricultureListener.onFails("error 404");
        }
        if(response.isSuccess()){
            AgricultureDetail agricultureDetail = new AgricultureDetail();
            agricultureDetail.setDetail(response.body().getDetail());

            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = Contextor.getInstance().getContext().getSharedPreferences("agricultureDetail"+typeCodeName, 0);
            editor = settings.edit();
            String data = new Gson().toJson(agricultureDetail);
            editor.putString("agricultureDetail"+typeCodeName, data);
            editor.apply();

            MainBus.getInstance().post(agricultureDetail);
        }
    }

    @Override
    public void onFailure(Throwable t) {
//        if(t.toString().contains("ConnectException")) {
//            agricultureListener.onFails("Network is unreachable");
//        }
//        else if(t.toString().contains("SocketTimeoutException")){
//            agricultureListener.onFails("timeout");
//        }
    }
}
