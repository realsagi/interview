package th.in.tamis.service.map;

public class ConvertLatLongToUTM {

    double DatumWGS84A = 6378137.0;
    double DatumWGS84B = 6356752.314;
    double UTMScaleFactor = 0.9996;

    public int convertLongitudeToZone(double latitude){
        return (int) (( (latitude + 180) / 6) + 1);
    }

    public double covertLatitudeToUTMX(double latitude, double longitude){

        double n = getN(latitude);
        double phi = convertLatLongToRadius(latitude);

        double lambda = convertLatLongToRadius(longitude);
        double lambda0 = utmCentralMeridian(convertLongitudeToZone(longitude));
        double l = lambda - lambda0;

        double t = Math.tan (phi);
        double t2 = t * t;

        double l3coef = 1.0 - t2 + getNu2(latitude);
        double l5coef = 5.0 - 18.0 * t2 + (t2 * t2) + 14.0 * getNu2(latitude) - 58.0 * t2 * getNu2(latitude);
        double l7coef = 61.0 - 479.0 * t2 + 179.0 * (t2 * t2) - (t2 * t2 * t2);

        double x = n * Math.cos (phi) * l +
                (n / 6.0 * Math.pow (Math.cos (phi), 3.0) * l3coef * Math.pow (l, 3.0)) +
                (n / 120.0 * Math.pow (Math.cos (phi), 5.0) * l5coef * Math.pow (l, 5.0)) +
                (n / 5040.0 * Math.pow (Math.cos (phi), 7.0) * l7coef * Math.pow (l, 7.0));

        x = x * UTMScaleFactor + 500000.0;
        return x;
    }

    public double covertLongitudeToUTMY(double latitude, double longitude){

        double phi = convertLatLongToRadius(latitude);
        double n = getN(latitude);
        double lambda = convertLatLongToRadius(longitude);
        double lambda0 = utmCentralMeridian(convertLongitudeToZone(longitude));
        double l = lambda - lambda0;
        double t = Math.tan (phi);
        double t2 = t * t;

        double l4coef = 5.0 - t2 + 9 * getNu2(latitude) + 4.0 * (getNu2(latitude) * getNu2(latitude));

        double l6coef = 61.0 - 58.0 * t2 + (t2 * t2) + 270.0 * getNu2(latitude) - 330.0 * t2 * getNu2(latitude);

        double l8coef = 1385.0 - 3111.0 * t2 + 543.0 * (t2 * t2) - (t2 * t2 * t2);

        double y = arcLengthOfMeridian(phi)
                + (t / 2.0 * n * Math.pow (Math.cos (phi), 2.0) * Math.pow (l, 2.0))
                + (t / 24.0 * n * Math.pow (Math.cos (phi), 4.0) * l4coef * Math.pow (l, 4.0))
                + (t / 720.0 * n * Math.pow (Math.cos (phi), 6.0) * l6coef * Math.pow (l, 6.0))
                + (t / 40320.0 * n * Math.pow (Math.cos (phi), 8.0) * l8coef * Math.pow (l, 8.0));

        y = y * UTMScaleFactor;
        if (y < 0.0) {
            y = y + 10000000.0;
        }
        return y;
    }

    public double arcLengthOfMeridian(double phi){
        double alpha, beta, gamma, delta, epsilon, n;
        double result;

        n = (DatumWGS84A - DatumWGS84B) / (DatumWGS84A + DatumWGS84B);

        alpha = ((DatumWGS84A + DatumWGS84B) / 2.0)
                * (1.0 + (Math.pow (n, 2.0) / 4.0) + (Math.pow (n, 4.0) / 64.0));

        beta = (-3.0 * n / 2.0) + (9.0 * Math.pow (n, 3.0) / 16.0)
                + (-3.0 * Math.pow (n, 5.0) / 32.0);

        gamma = (15.0 * Math.pow (n, 2.0) / 16.0)
                + (-15.0 * Math.pow (n, 4.0) / 32.0);

        delta = (-35.0 * Math.pow (n, 3.0) / 48.0)
                + (105.0 * Math.pow (n, 5.0) / 256.0);

        epsilon = (315.0 * Math.pow (n, 4.0) / 512.0);

        result = alpha
                * (phi + (beta * Math.sin (2.0 * phi))
                + (gamma * Math.sin (4.0 * phi))
                + (delta * Math.sin (6.0 * phi))
                + (epsilon * Math.sin (8.0 * phi)));

        return result;
    }

    public double utmCentralMeridian(int zone){
        return convertLatLongToRadius(-183.0 + (zone * 6.0));
    }

    public String getHemisphere(double latitude){
        if (latitude < 0){
            return "S";
        }
        return "N";
    }

    public double getN(double latitude){
        return Math.pow(DatumWGS84A, 2.0) / (DatumWGS84B * Math.sqrt(1 + getNu2(latitude)));
    }

    public double getNu2(double latitude){
        double phi = convertLatLongToRadius(latitude);
        return getEp2() * (Math.pow(Math.cos(phi), 2.0));
    }

    public double getEp2(){
        return (Math.pow(DatumWGS84A, 2.0) - (Math.pow(DatumWGS84B, 2.0))) / (Math.pow(DatumWGS84B, 2.0));
    }

    public double convertLatLongToRadius(double coordinate){
        double pi = 3.14159265358979;
        return (coordinate / 180.0) * pi;
    }
}
