package th.in.tamis.manager.person;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.MainBus;
import th.in.tamis.models.HouseHoldMember;

public class HouseHoldMemberManager implements Callback<HouseHoldMember>{

    private onLoadPerson onLoadPerson;

    public HouseHoldMemberManager(onLoadPerson listener){
        this.onLoadPerson = listener;
    }

    @Override
    public void onResponse(Response<HouseHoldMember> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            onLoadPerson.timeout("error 404");
        }
        if(response.isSuccess()) {
            HouseHoldMember houseHoldMember = response.body();
            if (houseHoldMember.equals("{}")) {
                houseHoldMember = new HouseHoldMember();
            }
            MainBus.getInstance().post(houseHoldMember);
        }

        if(response.raw().code() == 500){
            onLoadPerson.timeout("error 500");
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadPerson.timeout("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadPerson.timeout("timeout");
        }
    }

}
