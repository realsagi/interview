package th.in.tamis.usbreader;

import android.content.Context;
import android.os.AsyncTask;

import com.idvision.androididcardlib.iCardUsbReader;

import th.in.tamis.models.Address;
import th.in.tamis.models.Farmer;
import th.in.tamis.tamis.BuildConfig;
import th.in.tamis.usb.reader.RegisterUSB;
import th.in.tamis.usb.reader.SmartCardProvider;
import th.in.tamis.usb.reader.ThaiCitizenCard;
import th.in.tamis.usb.reader.model.HolderProfile;
import th.in.tamis.usbreader.datatest.ThaiCitizenCardOfKittimasak;

public class ReadDataTask extends AsyncTask<Void, Integer, Farmer> {
    final private SmartCardProvider smartCardProvider;
    final private OnCardReaderListener onCardReaderListener;
    final private RegisterUSB registerUSB;
    private ThaiCitizenCard thaiCitizenCard;

    public ReadDataTask(Context context) {
        onCardReaderListener = (OnCardReaderListener) context;
        smartCardProvider = new SmartCardProvider(context);
        registerUSB = (RegisterUSB) context;
        thaiCitizenCard = createThaiCitizenCard(smartCardProvider.getReader());
    }

    @Override
    protected Farmer doInBackground(Void... params) {
        return readProfileOnSmartCard();
    }

    @Override
    protected void onPostExecute(Farmer farmer) {
        super.onPostExecute(farmer);
        if (farmer != null) {
            onCardReaderListener.onReadDataSuccess(farmer);
        } else {
            onCardReaderListener.onError();
        }
    }

    private synchronized Farmer readProfileOnSmartCard() {
        Farmer farmer = null;
        thaiCitizenCard = createThaiCitizenCard(smartCardProvider.getReader());
        if (thaiCitizenCard.checkStatusOfCardReader()) {

            HolderProfile holderProfile = thaiCitizenCard.getHolderProfile();
            if (holderProfile != null) {
                farmer = new Farmer();
                farmer.setImagePerson(thaiCitizenCard.getHolderPhoto());
                farmer.setPreName(holderProfile.getThaiName().getPreName());
                farmer.setFirstName(holderProfile.getThaiName().getFirstName());
                farmer.setLastName(holderProfile.getThaiName().getLastName());
                farmer.setCitizenId(holderProfile.getId());
                farmer.setBirthDate(holderProfile.getBirthday());

                Address farmerAddress = new Address();
                farmerAddress.setHouseNo(holderProfile.getAddress().getHouseNumber());
                farmerAddress.setMoo(holderProfile.getAddress().getMoo());
                farmerAddress.setAlley(holderProfile.getAddress().getAlley());
                farmerAddress.setSoi(holderProfile.getAddress().getSoi());
                farmerAddress.setRoad(holderProfile.getAddress().getRoad());
                farmerAddress.setSubDistrict(holderProfile.getAddress().getSubDistrict());
                farmerAddress.setDistrict(holderProfile.getAddress().getDistrict());
                farmerAddress.setProvince(holderProfile.getAddress().getProvince());
                farmer.setAddress(farmerAddress);
            }

            thaiCitizenCard.close();
        }
        return farmer;
    }

    private ThaiCitizenCard createThaiCitizenCard(iCardUsbReader reader) {
        String mode = BuildConfig.MODE;

        if("dev".equals(mode)) {
            registerUSB.onRegisterUSBSuccess();
            return new ThaiCitizenCardOfKittimasak(reader);
        }
        return new ThaiCitizenCard(reader);
    }
}
