package th.in.tamis.holder;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import th.in.tamis.activity.DetailAgriculturalOperationsActivity;
import th.in.tamis.models.Coordinates;
import th.in.tamis.tamis.R;

public class AgriculturalOperationsHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
        OnMapReadyCallback {

    final private TextView tvNumber;
    final private EditText edNamePlant;
    final private EditText edSeeds;
    final private EditText editTextEstimatedProductivity;
    final private EditText edPlantingDate;
    final private EditText edPlantedRai;
    final private EditText edPlantedNgan;
    final private EditText edPlantedSquareWah;
    final private EditText edHarvestDate;
    final private EditText edHarvestRai;
    final private EditText edHarvestNgan;
    final private EditText edHarvestSquareWah;
    final private ImageView imageViewSnapShotMap;
    final private FrameLayout frameLayoutCardViewAgriculturalMap;
    private FragmentManager fragmentManagerLocal;
    private List<Coordinates> coordinates;
    private TextView textViewStatusSubMap;

    private ImageView imageViewErrorTypeAgriculture;
    private ImageView imageViewIconMap;

    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

    public AgriculturalOperationsHolder(View itemView, FragmentManager fragmentManager) {
        super(itemView);
        this.fragmentManagerLocal = fragmentManager;

        tvNumber = (TextView) itemView.findViewById(R.id.tvNumber);
        edNamePlant = (EditText) itemView.findViewById(R.id.edNamePlant);
        edSeeds = (EditText) itemView.findViewById(R.id.edSeeds);
        editTextEstimatedProductivity = (EditText) itemView.findViewById(R.id.editTextEstimatedProductivity);
        edPlantingDate = (EditText) itemView.findViewById(R.id.editTextPlantingDate);
        edPlantedRai = (EditText) itemView.findViewById(R.id.edPlantedRai);
        edPlantedNgan = (EditText) itemView.findViewById(R.id.edPlantedNgan);
        edPlantedSquareWah = (EditText) itemView.findViewById(R.id.edPlantedSquareWah);
        edHarvestDate = (EditText) itemView.findViewById(R.id.editTextHarvestDate);
        edHarvestRai = (EditText) itemView.findViewById(R.id.edHarvestRai);
        edHarvestNgan = (EditText) itemView.findViewById(R.id.edHarvestNgan);
        edHarvestSquareWah = (EditText) itemView.findViewById(R.id.edHarvestSquareWah);
        imageViewSnapShotMap = (ImageView) itemView.findViewById(R.id.imageViewSnapShotMap);
        textViewStatusSubMap = (TextView) itemView.findViewById(R.id.textViewStatusSubMap);
        ImageButton btnAgricultureBusinessInformation = (ImageButton) itemView.findViewById(R.id.btnAgricultureBusinessInformation);
        btnAgricultureBusinessInformation.setOnClickListener(this);

        frameLayoutCardViewAgriculturalMap = (FrameLayout) itemView.findViewById(R.id.frameLayoutCardViewAgriculturalMap);
        frameLayoutCardViewAgriculturalMap.setVisibility(View.GONE);

        imageViewErrorTypeAgriculture = (ImageView) itemView.findViewById(R.id.imageViewErrorTypeAgriculture);
        imageViewIconMap = (ImageView)itemView.findViewById(R.id.imageViewIconMap);
    }
    private Calendar createCalendar() {
        return Calendar.getInstance();
    }

    public void setTvNumber(String number) {
        this.tvNumber.setText(number);
    }

    public void setEdNamePlant(String namePlant) {
        this.edNamePlant.setText(namePlant);
    }

    public void setEdSeeds(String seeds) {
        this.edSeeds.setText(seeds);
    }

    public void setEditTextEstimatedProductivity(String estimatedProductivity) {
        this.editTextEstimatedProductivity.setText(estimatedProductivity + " " + itemView.getContext().getString(R.string.kilogram));
    }

    public void setEdPlantingDate(String plantingDate) {
        Calendar calendarPlantingDate = createCalendar();
        calendarPlantingDate.setTimeInMillis(Long.parseLong(plantingDate));
        this.edPlantingDate.setText(setFormatThaiDate(calendarPlantingDate));
    }

    public void setEdPlantedRai(String plantedRai) {
        this.edPlantedRai.setText(plantedRai);
    }

    public void setEdPlantedNgan(String plantedNgan) {
        this.edPlantedNgan.setText(plantedNgan);
    }

    public void setEdPlantedSquareWah(String plantedSquareWah) {
        this.edPlantedSquareWah.setText(plantedSquareWah);
    }

    public void setEdHarvestDate(String harvestDate) {
        Calendar calendarHarvestDate = createCalendar();
        calendarHarvestDate.setTimeInMillis(Long.parseLong(harvestDate));
        this.edHarvestDate.setText(setFormatThaiDate(calendarHarvestDate));
    }

    public String setFormatThaiDate(Calendar calendar) {
        return String.format("%d %s %d",
                calendar.get(Calendar.DAY_OF_MONTH),
                itemView.getResources().getStringArray(R.array.month_full_th)[calendar.get(Calendar.MONTH)],
                calendar.get(Calendar.YEAR)+543);
    }

    public void setEdHarvestRai(String harvestRai) {
        this.edHarvestRai.setText(harvestRai);
    }

    public void setEdHarvestNgan(String harvestNgan) {
        this.edHarvestNgan.setText(harvestNgan);
    }

    public void setEdHarvestSquareWah(String harvestSquareWah) {
        this.edHarvestSquareWah.setText(harvestSquareWah);
    }

    public void setImageViewSnapShotMap(Bitmap bitmap){
        imageViewSnapShotMap.setImageBitmap(bitmap);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAgricultureBusinessInformation:
                Intent intent = new Intent(v.getContext(), DetailAgriculturalOperationsActivity.class);
                intent.putExtra("mode", "Edit");
                intent.putExtra("positionDetail", getAdapterPosition());
                v.getContext().startActivity(intent);
                break;
        }
    }

    public void showMapOnCardView(boolean mapShow){
        if(mapShow){
//            Fragment fragmentMap = fragmentManagerLocal.findFragmentById(R.id.fragmentCardViewAgriculturalMap);
//            SupportMapFragment supportMapFragment = (SupportMapFragment) fragmentMap;
//            supportMapFragment.getMapAsync(this);
            frameLayoutCardViewAgriculturalMap.setVisibility(View.VISIBLE);
        }else {
            frameLayoutCardViewAgriculturalMap.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        moveCameraToLocation(googleMap);
        createMainPolygon(googleMap);
    }

    private void moveCameraToLocation(GoogleMap googleMap) {
        if(coordinates != null){
            double latitude = coordinates.get(0).getLatitude();
            double longitude = coordinates.get(0).getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        }
    }

    private void createMainPolygon(GoogleMap googleMap) {
        if(coordinates != null){
            PolygonOptions polygonOptions = new PolygonOptions();
            for(int index = 0 ; index < coordinates.size() ; index ++){
                double latitude = coordinates.get(index).getLatitude();
                double longitude = coordinates.get(index).getLongitude();
                LatLng latLng = new LatLng(latitude, longitude);
                polygonOptions.add(latLng);
            }
            polygonOptions.strokeColor(0x90320000);
            polygonOptions.fillColor(0x90320000);
            googleMap.addPolygon(polygonOptions);
        }
    }

    public void setCoordinates(List<Coordinates> coordinates){
        this.coordinates = coordinates;
    }

    public void setStatusDrawingMap(boolean statusDrawing){
        String str = "";
        if(statusDrawing){
            str = "วาดแผนที่แล้ว";
            imageViewErrorTypeAgriculture.setVisibility(View.GONE);
            imageViewIconMap.setVisibility(View.VISIBLE);
        }else {
            str = "ยังไม่วาดแผนที่";
            imageViewErrorTypeAgriculture.setVisibility(View.VISIBLE);
            imageViewIconMap.setVisibility(View.GONE);
        }
        textViewStatusSubMap.setText(str);
    }
}
