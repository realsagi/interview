package th.in.tamis.service.file;

import android.os.Environment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileManagement {

    String imageFileName;

    public FileManagement(String preface){
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = preface + timeStamp + ".jpg";
    }
    public FileManagement(){

    }
    public File generatePath(){
        return new File(Environment.getExternalStorageDirectory(), "DCIM/Tamis/" + imageFileName);
    }
    public void generateFolderTamis(){
        new File(Environment.getExternalStorageDirectory(), "DCIM/Tamis/").mkdir();
    }
}
