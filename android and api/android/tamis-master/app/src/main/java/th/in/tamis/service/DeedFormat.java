package th.in.tamis.service;

import th.in.tamis.models.DetailRegisterPlantation;

public class DeedFormat {

    private static DeedFormat instance;
    private static DetailRegisterPlantation objDetailRegisterPlantation;

    private DeedFormat(){}

    public static DeedFormat setDetailRegisterPlantationFormat(DetailRegisterPlantation detail){
        objDetailRegisterPlantation = detail;
        if (instance == null)
            instance = new DeedFormat();
        return instance;
    }

    public String getFormat(){
        String rai = String.valueOf(objDetailRegisterPlantation.getRai())+" ไร่ ";
        String ngan = String.valueOf(objDetailRegisterPlantation.getNgan()+" งาน ");
        String squareWah = String.valueOf(objDetailRegisterPlantation.getSquareWah()+" ตารางวา");
        return rai+ngan+squareWah;
    }
}
