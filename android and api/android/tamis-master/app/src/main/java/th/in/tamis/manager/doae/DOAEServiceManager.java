package th.in.tamis.manager.doae;

import th.in.tamis.manager.http.HTTPManager;

public class DOAEServiceManager {
    public static APIDOAEServiceMember getDOAEService(){
        return HTTPManager.getInstance().getRetrofit().create(APIDOAEServiceMember.class);
    }
}
