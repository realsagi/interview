package th.in.tamis.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.List;

import th.in.tamis.models.Coordinates;
import th.in.tamis.models.DetailAgriculturalOperations;
import th.in.tamis.models.DetailRegisterPlantation;
import th.in.tamis.models.RegisterPlantation;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.tamis.R;
import th.in.tamis.holder.AgriculturalOperationsHolder;

public class AgriculturalOperationsAdapter extends RecyclerView.Adapter<AgriculturalOperationsHolder>{

    private int positionOfDetailRegisterPlantation;
    private static View itemView;
    @Override
    public AgriculturalOperationsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        try {
            itemView = layoutInflater.inflate(R.layout.card_view_agricultural_operation, parent, false);
        }catch (InflateException exception){

        }
        FragmentManager fragmentManager = ((AppCompatActivity) parent.getContext()).getSupportFragmentManager();
        return new AgriculturalOperationsHolder(itemView, fragmentManager);
    }

    @Override
    public void onBindViewHolder(AgriculturalOperationsHolder holder, int position) {
        holder.setTvNumber(String.valueOf(position + 1) + ".");
        holder.setEdNamePlant(getDetailAgriculturalOperations().get(position).getNamePlants());
        holder.setEdSeeds(getDetailAgriculturalOperations().get(position).getBreeds());
        holder.setEditTextEstimatedProductivity(String.valueOf(getDetailAgriculturalOperations().get(position).getEstimatedProductivity()));
        holder.setEdPlantingDate(String.valueOf(getDetailAgriculturalOperations().get(position).getPlantingDate()));
        holder.setEdPlantedRai(String.valueOf(getDetailAgriculturalOperations().get(position).getPlantedRai()));
        holder.setEdPlantedNgan(String.valueOf(getDetailAgriculturalOperations().get(position).getPlantedNgan()));
        holder.setEdPlantedSquareWah(String.valueOf(getDetailAgriculturalOperations().get(position).getPlantedSquareWah()));
        holder.setEdHarvestDate(String.valueOf(getDetailAgriculturalOperations().get(position).getHarvestDate()));
        holder.setEdHarvestRai(String.valueOf(getDetailAgriculturalOperations().get(position).getHarvestedRai()));
        holder.setEdHarvestNgan(String.valueOf(getDetailAgriculturalOperations().get(position).getHarvestedNgan()));
        holder.setEdHarvestSquareWah(String.valueOf(getDetailAgriculturalOperations().get(position).getHarvestedSquareWah()));
        File file = getDetailAgriculturalOperations().get(position).getFileSnapShotMapAgriculture();
        if(file != null){
            //holder.setImageViewSnapShotMap(BitmapFactory.decodeFile(file.getPath()));
        }
        List<Coordinates> coordinates = getDetailAgriculturalOperations().get(position).getCoordinates();
        holder.showMapOnCardView(false);
        holder.setStatusDrawingMap(false);
        if(coordinates != null){
            holder.setStatusDrawingMap(true);
            holder.setCoordinates(coordinates);
            holder.showMapOnCardView(true);
        }
    }

    @Override
    public int getItemCount() {
        if(getDetailAgriculturalOperations() == null)
            return 0;
        return getDetailAgriculturalOperations().size();
    }

    public void setPositionOfDetailRegisterPlantation(int position){
        this.positionOfDetailRegisterPlantation = position;
    }

    private List<DetailAgriculturalOperations> getDetailAgriculturalOperations(){
        return getDetailRegisterPlantation().getDetailAgriculturalOperationses();
    }

    private DetailRegisterPlantation getDetailRegisterPlantation(){
        RegisterPlantation registerPlantation = (RegisterPlantation) PreferencesService
                .getPreferences("registerPlantation", RegisterPlantation.class);
        return registerPlantation
                .getDetailRegisterPlantation()
                .get(positionOfDetailRegisterPlantation);
    }

}
