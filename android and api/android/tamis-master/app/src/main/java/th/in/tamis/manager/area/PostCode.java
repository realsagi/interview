package th.in.tamis.manager.area;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PostCode implements Callback<String> {

    private APIArea.AreaOnLoadData onLoadData;

    public PostCode(APIArea.AreaOnLoadData onLoadData) {
        this.onLoadData = onLoadData;
    }

    @Override
    public void onResponse(Response<String> response, Retrofit retrofit) {
        if (response.raw().code() == 404) {
            onLoadData.onLoadFails("error 404");
        }
        if (response.isSuccess()) {
            onLoadData.onLoadPostCodeSuccess(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadData.onLoadFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadData.onLoadFails("timeout");
        }
    }
}
