package th.in.tamis.manager.agriculture;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.OnLoadCacheAll;
import th.in.tamis.models.AgricultureType;

public class AgricultureTypeAllManager implements Callback<AgricultureType> {

    private OnLoadCacheAll onLoadAreaAll;

    public AgricultureTypeAllManager(OnLoadCacheAll listener){
        this.onLoadAreaAll = listener;
    }

    @Override
    public void onResponse(Response<AgricultureType> response, Retrofit retrofit) {
        if (response.isSuccess()) {
            onLoadAreaAll.onLoadAgricultureTypeSuccess(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadAreaAll.onLoadFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadAreaAll.onLoadFails("timeout");
        }
    }
}
