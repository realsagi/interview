package th.in.tamis.manager.agriculture;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;
import th.in.tamis.models.AgricultureBreed;
import th.in.tamis.models.AgricultureDetail;
import th.in.tamis.models.FarmersOrganization;
import th.in.tamis.models.AgricultureType;

public interface APIServiceAgriculture {
    @GET("/agriculture/type")
    Call<AgricultureType> loadType();

    @GET("/agriculture/breed_all")
    Call<AgricultureBreed> loadBreedAll();

    @GET("/agriculture/breed")
    Call<AgricultureBreed> loadBreed(@Query("detail_id") int detailId);

    @GET("/agriculture/detail_all")
    Call<AgricultureDetail> loadDetailAll();

    @GET("/agriculture/detail")
    Call<AgricultureDetail> loadDetail(@Query("type_id") int typeId);

    @GET("/agriculture/community")
    Call<FarmersOrganization> loadCommunity();

    interface AgricultureListener{
        void onSuccess(FarmersOrganization farmersOrganization);
        void onFails(String message);
    }
}
