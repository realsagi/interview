package th.in.tamis.manager;

import th.in.tamis.models.AgricultureBreed;
import th.in.tamis.models.AgricultureDetail;
import th.in.tamis.models.AgricultureType;
import th.in.tamis.models.District;
import th.in.tamis.models.FarmersOrganization;
import th.in.tamis.models.ParcelType;
import th.in.tamis.models.ProfileCareer;
import th.in.tamis.models.Province;
import th.in.tamis.models.SubDistrict;

public interface OnLoadCacheAll {
    void onLoadProvinceAllSuccess(Province province);
    void onLoadDistrictAllSuccess(District district);
    void onLoadSubDistrictAllSuccess(SubDistrict subDistrict);

    void onLoadParcelTypeAllSuccess(ParcelType parcelType);

    void onLoadCareerProfileSuccess(ProfileCareer profileCareer);

    void onLoadAgricultureTypeSuccess(AgricultureType agricultureType);
    void onLoadAgricultureDetailSuccess(AgricultureDetail agricultureDetail);
    void onLoadAgricultureBreedSuccess(AgricultureBreed agricultureBreed);

    void onLoadAgricultureCommunitySuccess(FarmersOrganization farmersOrganization);

    void onLoadFails(String message);
}
