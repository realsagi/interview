package th.in.tamis.manager.agriculture;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.Contextor;
import th.in.tamis.models.AgricultureBreed;
import th.in.tamis.manager.MainBus;

public class AgricultureBreedManager implements Callback<AgricultureBreed> {

    private APIServiceAgriculture.AgricultureListener agricultureListener;
    private String detailCodeName;

    public AgricultureBreedManager(APIServiceAgriculture.AgricultureListener listener, String detailCodeName){
        agricultureListener = listener;
        this.detailCodeName = detailCodeName;
    }

    @Override
    public void onResponse(Response<AgricultureBreed> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            agricultureListener.onFails("error 404");
        }
        if(response.isSuccess()){
            AgricultureBreed agricultureBreed = new AgricultureBreed();
            agricultureBreed.setAgricultureBreed(response.body().getAgricultureBreed());

            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = Contextor.getInstance().getContext().getSharedPreferences("agricultureBreed"+detailCodeName, 0);
            editor = settings.edit();
            String data = new Gson().toJson(agricultureBreed);
            editor.putString("agricultureBreed"+detailCodeName, data);
            editor.apply();

            MainBus.getInstance().post(agricultureBreed);
        }
    }

    @Override
    public void onFailure(Throwable t) {
//        if(t.toString().contains("ConnectException")) {
//            agricultureListener.onFails("Network is unreachable");
//        }
//        else if(t.toString().contains("SocketTimeoutException")){
//            agricultureListener.onFails("timeout");
//        }
    }
}
