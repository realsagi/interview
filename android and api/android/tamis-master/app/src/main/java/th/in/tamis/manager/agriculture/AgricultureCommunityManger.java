package th.in.tamis.manager.agriculture;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.models.FarmersOrganization;

public class AgricultureCommunityManger implements Callback<FarmersOrganization>{

    private APIServiceAgriculture.AgricultureListener agricultureListener;

    public AgricultureCommunityManger(APIServiceAgriculture.AgricultureListener listener){
        agricultureListener = listener;
    }

    @Override
    public void onResponse(Response<FarmersOrganization> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            agricultureListener.onFails("error 404");
        }else if(response.isSuccess()){
            agricultureListener.onSuccess(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            agricultureListener.onFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            agricultureListener.onFails("timeout");
        }
    }
}
