package th.in.tamis.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.PolyUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import th.in.tamis.models.Coordinates;
import th.in.tamis.models.ParcelDoc;
import th.in.tamis.service.Area;
import th.in.tamis.service.authentication.TokenLoginService;
import th.in.tamis.service.calculateAreaOfGPSPolygonOnEarthInSquareMeters;
import th.in.tamis.service.file.FileManagement;
import th.in.tamis.tamis.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.me.jstott.jcoord.UTMRef;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;
import static com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, OnMapClickListener,
        OnClickListener, OnMarkerDragListener, SnapshotReadyCallback, DialogInterface.OnClickListener,
        TextWatcher {

    private GoogleMap googleMap;
    private LatLng latLngCenter;
    private List<MarkerOptions> markerOptionsList;
    private List<Marker> markerList;
    private Polygon polygon;
    private Area AreaReal;
    private File file;
    private Double areaDrawing;
    private Marker markerCenter;
    private Boolean modeDetailAddDetailAgricultural;
    private List<MarkerOptions> mainMarkerList;
    private List<LatLng> mainLatLongList;
    private String holding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        setSupportMapFragment(this);
        setButtonListener(this);
        setAutoCompleteListener(this);

        List<Coordinates> coordinateOld = convertStringJsonCoordinatesListToList(getStringFromIntentByKey("coordinateListOld"));
        setMarkerOptionsList(convertCoordinateListToMarkerOptionList(coordinateOld, null));

        holding = getStringFromIntentByKey("holding");

        Area area = convertStringJsonAreaToObject(getStringFromIntentByKey("area"));
        setAreaReal(area);
        setTextViewResultCertificateByArea(area.getDescription());

        ParcelDoc parcelDoc = convertStringJsonParcelNs4ToObject(getStringFromIntentByKey("parcel"));
        LatLng parcelLng = createLatLngParcelNS4(parcelDoc);

        String locationAddress = getStringFromIntentByKey("locationAddress");
        LatLng location = getLocationFromAddress(locationAddress);

        String locationCenterString = getStringFromIntentByKey("locationCenter");
        Coordinates coordinatesCenter = createCoordinatesFromString(locationCenterString);
        LatLng locationCenter = createLatLngFromCoordinate(coordinatesCenter);

        String plantationMain = getStringFromIntentByKey("plantationMain");
        List<Coordinates> coordinateMainList = convertStringJsonCoordinatesListToList(plantationMain);
        List<MarkerOptions> mainMarkerList = convertCoordinateListToMarkerOptionList(coordinateMainList, getString(R.string.yellow));
        setMainMarkerList(mainMarkerList);
        setMainLatLongList(createListLatLongFromListMarkerOptions(mainMarkerList));

        setCenterParcel(parcelLng, location, locationCenter);
        setTexViewOfLocation(getLatLngCenter());

        setBoldText();
    }

    private void setBoldText() {
        Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/THSarabunNewBold.ttf");
        TextView textViewCurrentLocation = (TextView) findViewById(R.id.textViewCurrentLocation);
        textViewCurrentLocation.setTypeface(typeface);
    }

    private void setVisibleTextViewCurrentLocation(int visible){
        TextView textViewCurrentLocation = (TextView) findViewById(R.id.textViewCurrentLocation);
        textViewCurrentLocation.setVisibility(visible);
    }

    private void setVisibleLinearLayoutTextViewCoodinateCenter(int visible){
        LinearLayout linearLayoutTextViewCoordinateCenter = (LinearLayout) findViewById(R.id.linearLayoutTextViewCoordinateCenter);
        linearLayoutTextViewCoordinateCenter.setVisibility(visible);
    }

    private void setVisibleLinearLayoutEditTextCoordinate(int visible){
        LinearLayout linearLayoutEditTextCoordinate = (LinearLayout) findViewById(R.id.linearLayoutEditTextCoordinate);
        linearLayoutEditTextCoordinate.setVisibility(visible);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new TokenLoginService(this).loginByToken();
    }

    private List<LatLng> createListLatLongFromListMarkerOptions(List<MarkerOptions> mainMarkerList) {
        if (mainMarkerList != null) {
            List<LatLng> list = new ArrayList<>();
            for (int index = 0; index < mainMarkerList.size(); index++) {
                list.add(mainMarkerList.get(index).getPosition());
            }
            return list;
        }
        return null;
    }

    private LatLng createLatLngFromCoordinate(Coordinates coordinatesCenter) {
        if (coordinatesCenter != null) {
            return new LatLng(coordinatesCenter.getLatitude(), coordinatesCenter.getLongitude());
        }
        return null;
    }

    private Coordinates createCoordinatesFromString(String locationCenterString) {
        if (locationCenterString != null) {
            return new Gson().fromJson(locationCenterString, Coordinates.class);
        }
        return null;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setCenterParcel(LatLng parcelLng, LatLng location, LatLng locationCenter) {
        setLatLngCenter(selectLocation(parcelLng, location, locationCenter));
        LatLng centerLatLng = convertStringJsonLatLngToObjec(getStringFromIntentByKey("coordinateCenter"));
        setModeDetailAddDetailAgricultural(false);
        if (centerLatLng != null) {
            setModeDetailAddDetailAgricultural(true);
            setLatLngCenter(centerLatLng);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setOnMapClickListener(this);
        googleMap.setMapType(MAP_TYPE_SATELLITE);
        googleMap.setOnMarkerDragListener(this);
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        setGoogleMap(googleMap);

        moveCameraToLocation(latLngCenter, 18);
        createMarkerCenter(getLatLngCenter());
        DeleteMarkerList(getMarkerList());
        setMarkerList(DrawingMarkerOnMapByListOfMarkerOptions(getMarkerOptionsList()));

        DeletePolygonOnMap(getPolygon());
        PolygonOptions polygonOptions = createPolygonOptionsFromMarkerOptionsList(getMarkerOptionsList());
        setPolygon(DrawingPolygonOnMap(polygonOptions));
        removeMarker(getMarkerCenter());
        createMarkerCenter(getLatLngCenter());

        drawingMainMarkerList(getMainMarkerList());
        setAreaDrawing(calculateAreaAndShowOnTextView(polygonOptions.getPoints()));
    }

    @Override
    public void onMapClick(LatLng latLng) {

        setVisibleTextViewCurrentLocation(View.GONE);
        setVisibleLinearLayoutTextViewCoodinateCenter(View.GONE);
        setVisibleLinearLayoutEditTextCoordinate(View.GONE);

        if (drawingMapOverMainPlantation(latLng, getMainLatLongList())) {
            BitmapDescriptor icon = CreateIcon(null);
            int indexPosition = 1;
            if (getMarkerOptionsList() != null) {
                indexPosition = getMarkerOptionsList().size() + 1;
            }
            MarkerOptions markerOptions = CreateMarkerOptions(latLng, icon, String.valueOf(indexPosition));
            List<MarkerOptions> listMarkerOptions = addMarkerOptionsToList(markerOptions, getMarkerOptionsList());
            setMarkerOptionsList(listMarkerOptions);
            DeleteMarkerList(getMarkerList());
            setMarkerList(DrawingMarkerOnMapByListOfMarkerOptions(listMarkerOptions));

            PolygonOptions polygonOptions = createPolygonOptionsFromMarkerOptionsList(listMarkerOptions);
            DeletePolygonOnMap(getPolygon());
            setPolygon(DrawingPolygonOnMap(polygonOptions));
            removeMarker(getMarkerCenter());
            createMarkerCenter(getLatLngCenter());
            setAreaDrawing(calculateAreaAndShowOnTextView(polygonOptions.getPoints()));
        } else {
            Toast.makeText(this, R.string.drawingMapOverMainMapOnly, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean drawingMapOverMainPlantation(LatLng newPosition, List<LatLng> areaDrawing) {
        if (areaDrawing.size() != 0) {
            return PolyUtil.containsLocation(newPosition, areaDrawing, true);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonDeleteMap) {
            DeleteMarkerList(getMarkerList());
            ClearList(getMarkerOptionsList());
            DeletePolygonOnMap(getPolygon());
            PolygonOptions polygonOptions = createPolygonOptionsFromMarkerOptionsList(getMainMarkerList());
            setAreaDrawing(calculateAreaAndShowOnTextView(polygonOptions.getPoints()));
        } else if (v.getId() == R.id.buttonUndoPressedMarker) {
            undoMarkerOnMap(getMarkerOptionsList());
        } else if (v.getId() == R.id.buttonCancelDetail) {
            finish();
        } else if (v.getId() == R.id.btnSaveDetailMap) {
            if (getMarkerOptionsList().size() == 1 && holding != null && holding.equals(getString(R.string.householder))) {
                setLatLngCenter(getMarkerOptionsList().get(0).getPosition());
                setResultForReturn();
                finish();
            } else if (!VerifyAreaDrawing()) {
                createDialogAlertNoTitle(getString(R.string.areaMoreThan) + getTextViewResultCertificateByArea() + ")");
            } else {
                List<LatLng> latLngList = createListLatLongFromListMarkerOptions(getMarkerOptionsList());
                if (drawingMapOverMainPlantation(getLatLngCenter(), latLngList) && getMainLatLongList().size() == 0) {
                    setResultForReturn();
                    finish();
                }else if(getMainLatLongList().size() > 0){
                    setResultForReturn();
                    finish();
                }else {
                    createDialogAlertNoTitle(getString(R.string.alertMessageDrawingMapOutCenterPointYellow));
                }
            }
        } else if (v.getId() == R.id.buttonFindLocation) {
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = getListAddressGeoCoderFromLocationName(getAutoCompleteListener(), geocoder, 1);
            LatLng location = createLatLngParcelNS4(getLatitude(addresses), getLongitude(addresses));
            moveCameraToLocation(location, 18);
        } else if (v.getId() == R.id.buttonSearchByUTM) {
            final Dialog dialogUTM = new Dialog(this);
            dialogUTM.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogUTM.setContentView(R.layout.dialog_utm);

            final EditText editTextUtmX = (EditText) dialogUTM.findViewById(R.id.editTextUtmX);
            final EditText editTextUtmY = (EditText) dialogUTM.findViewById(R.id.editTextUtmY);
            final Spinner spinnerZone = (Spinner) dialogUTM.findViewById(R.id.spinnerZone);

            final String[] thailandUTMZone = {"กรุณาเลือกโซน", "47N", "47P", "47Q", "48N", "48P", "48Q"};

            ArrayAdapter<String> adapterZoneGroup = new ArrayAdapter<>(this,
                    R.layout.spinner_item, thailandUTMZone);
            spinnerZone.setAdapter(adapterZoneGroup);

            dialogUTM.show();
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(dialogUTM.getWindow().getAttributes());
            dialogUTM.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                    WindowManager.LayoutParams.WRAP_CONTENT);

            Button buttonUTMCancel = (Button) dialogUTM.findViewById(R.id.buttonUTMCancel);
            buttonUTMCancel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogUTM.dismiss();
                }
            });

            Button buttonUTMOK = (Button) dialogUTM.findViewById(R.id.buttonUTMOK);
            buttonUTMOK.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if ("".equals(editTextUtmX.getText()) ||
                            "".equals(editTextUtmY.getText()) ||
                            0 == spinnerZone.getSelectedItemPosition()) {
                        dialogUTM.dismiss();
                    } else {
                    String selectedZone = spinnerZone.getSelectedItem().toString();

                    UTMRef utmRef = new UTMRef(Double.parseDouble(editTextUtmX.getText().toString()),
                            Double.parseDouble(editTextUtmY.getText().toString()),
                            selectedZone.charAt(2),
                            Integer.valueOf(selectedZone.substring(0, 2)));
                    uk.me.jstott.jcoord.LatLng convertedLatLng = utmRef.toLatLng();
                    LatLng latLng = new LatLng(convertedLatLng.getLat(),
                            convertedLatLng.getLng());
                    moveCameraToLocation(latLng, 18);
                    setTextViewLatitude(String.valueOf(convertedLatLng.getLat()));
                    setTextViewLongitude(String.valueOf(convertedLatLng.getLng()));

                    removeMarker(getMarkerCenter());
                    setLatLngCenter(latLng);
                    createMarkerCenter(getLatLngCenter());

                    dialogUTM.dismiss();
                    }
                }
            });
        }
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        if (drawingMapOverMainPlantation(marker.getPosition(), getMainLatLongList())) {
            int index = findIndexMarkerFromList(marker);
            List<MarkerOptions> list = ChangeValueMarkerOptionsList(getMarkerOptionsList(), index, marker.getPosition());

            PolygonOptions polygonOptions = createPolygonOptionsFromMarkerOptionsList(list);
            DeletePolygonOnMap(getPolygon());
            setPolygon(DrawingPolygonOnMap(polygonOptions));
            removeMarker(getMarkerCenter());
            createMarkerCenter(getLatLngCenter());
            setAreaDrawing(calculateAreaAndShowOnTextView(polygonOptions.getPoints()));
        } else {
            Toast.makeText(this, R.string.drawingMapOverMainMapOnly, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
    }

    @Override
    public void onSnapshotReady(Bitmap bitmap) {
        try {
            FileOutputStream out = new FileOutputStream(getFile());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    private void createDialogAlertNoTitle(String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert_no_title);

        TextView textViewMessage = (TextView) dialogAlert.findViewById(R.id.textViewMessage);
        textViewMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonOK = (Button) dialogAlert.findViewById(R.id.buttonOK);
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }

    private void drawingMainMarkerList(List<MarkerOptions> listMainMarker) {
        if (listMainMarker != null) {
            PolygonOptions polygonMain = createPolygonOptionsFromMarkerOptionsList(listMainMarker);
            polygonMain.strokeColor(0x609666BB);
            polygonMain.fillColor(0x609666BB);
            DrawingPolygonOnMap(polygonMain);
        }
    }

    private LatLng convertStringJsonLatLngToObjec(String coordinateCenter) {
        LatLng center = null;
        if (coordinateCenter != null) {
            center = new Gson().fromJson(coordinateCenter, LatLng.class);
        }
        return center;
    }

    public LatLng calculateCentroid(List<LatLng> points) {
        double[] centroid = {0.0, 0.0};
        for (int i = 0; i < points.size(); i++) {
            centroid[0] += points.get(i).latitude;
            centroid[1] += points.get(i).longitude;
        }
        int totalPoints = points.size();
        centroid[0] = centroid[0] / totalPoints;
        centroid[1] = centroid[1] / totalPoints;
        return new LatLng(centroid[0], centroid[1]);
    }

    private boolean AreaRealLessThanAreaDrawing() {
        return getAreaReal().getSquareMeter() < getAreaDrawing();
    }

    private Boolean VerifyAreaDrawing() {
        if (getAreaDrawing() != null) {
            int plus10Percent = getAreaReal().getSquareMeter() + (getAreaReal().getSquareMeter() * 10 / 100);
            int minus10Percent = getAreaReal().getSquareMeter() - (getAreaReal().getSquareMeter() * 10 / 100);
            return minus10Percent <= getAreaDrawing() && getAreaDrawing() <= plus10Percent;
        }
        return false;
    }

    private void setResultForReturn() {
        File fileSnapShotMap = CreateFileForSnapShot();
        snapShot(this);

        List<Coordinates> coordinatesList = getListCoordinates(getMarkerOptionsList());

        Intent data = new Intent();
        data.putExtra("coordinatesList", new Gson().toJson(coordinatesList));
        data.putExtra("coordinateCenter", new Gson().toJson(getLatLngCenter()));
        data.putExtra("fileSnapShot", new Gson().toJson(fileSnapShotMap));
        setResult(Activity.RESULT_OK, data);
    }

    private File CreateFileForSnapShot() {
        File fileSnapShotMap = new FileManagement("GoogleMap_").generatePath();
        setFile(fileSnapShotMap);
        return fileSnapShotMap;
    }

    private List<MarkerOptions> convertCoordinateListToMarkerOptionList(List<Coordinates> coordinatesList, String colour) {
        List<MarkerOptions> list = new ArrayList<>();
        if (coordinatesList != null) {
            list = addCoordinatesListToListMarkerOptions(coordinatesList, colour);
        }
        return list;
    }

    private List<MarkerOptions> addCoordinatesListToListMarkerOptions(List<Coordinates> coordinatesList, String colour) {
        List<MarkerOptions> list = new ArrayList<>();
        for (int index = 0; index < coordinatesList.size(); index++) {
            Coordinates coordinates = coordinatesList.get(index);
            LatLng latLng = createLatLngParcelNS4(coordinates.getLatitude(), coordinates.getLongitude());
            BitmapDescriptor icon = CreateIcon(colour);
            list.add(CreateMarkerOptions(latLng, icon, String.valueOf(index + 1)));
        }
        return list;
    }

    private List<Coordinates> convertStringJsonCoordinatesListToList(String coordinateListOld) {
        return (List<Coordinates>) new Gson().fromJson(coordinateListOld, new TypeToken<List<Coordinates>>() {
        }.getType());
    }

    private List<Coordinates> getListCoordinates(List<MarkerOptions> list) {
        List<Coordinates> coordinatesList = new ArrayList<>();
        if (list != null) {
            for (MarkerOptions markerOptions : list) {
                coordinatesList.add(createCoordinates(markerOptions));
            }
        }
        return coordinatesList;
    }

    private Coordinates createCoordinates(MarkerOptions markerOptions) {
        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude(markerOptions.getPosition().latitude);
        coordinates.setLongitude(markerOptions.getPosition().longitude);
        return coordinates;
    }

    private void snapShot(MapActivity mapActivity) {
        getGoogleMap().snapshot(mapActivity);
    }

    private LatLng createLatLngParcelNS4(ParcelDoc parcelDoc) {
        if (parcelDoc != null) {
            return createLatLngParcelNS4(parcelDoc.getMapX(), parcelDoc.getMapY());
        }
        return null;
    }

    private void undoMarkerOnMap(List<MarkerOptions> list) {
        DeleteLastIndexOfMarkerOptionsList(list);
        DeleteMarkerList(getMarkerList());
        setMarkerList(DrawingMarkerOnMapByListOfMarkerOptions(list));

        PolygonOptions polygonOptions = createPolygonOptionsFromMarkerOptionsList(list);
        DeletePolygonOnMap(getPolygon());
        setPolygon(DrawingPolygonOnMap(polygonOptions));
        removeMarker(getMarkerCenter());
        createMarkerCenter(getLatLngCenter());
        calculateAreaAndShowOnTextView(polygonOptions.getPoints());
    }

    private Double calculateAreaAndShowOnTextView(List<LatLng> points) {
        Double areaOfMarkerDrawing = getAreaMarkerDrawing(points);
        Area area = setAreaOfMarkerDrawingToArea(areaOfMarkerDrawing);
        setTextViewAreaDrawing(area.getDescription());
        return areaOfMarkerDrawing;
    }

    private PolygonOptions createPolygonOptionsFromMarkerOptionsList(List<MarkerOptions> listMarkerOptions) {
        return CreatePolygonOptions(listMarkerOptions);
    }

    private void removeMarker(Marker marker) {
        if (marker != null) {
            marker.remove();
        }
    }

    private Double getAreaMarkerDrawing(List<LatLng> points) {
        return calculateAreaOfGPSPolygonOnEarthInSquareMeters.calCulate(points);
    }

    private void DeleteLastIndexOfMarkerOptionsList(List<MarkerOptions> list) {
        if (markerList.size() > 0) {
            list.remove(list.size() - 1);
            setMarkerOptionsList(list);
        }
    }

    private void DeletePolygonOnMap(Polygon polygon) {
        if (polygon != null) {
            polygon.remove();
        }
    }

    private Polygon DrawingPolygonOnMap(PolygonOptions polygonOptions) {
        if (polygonOptions.getPoints().size() > 0) {
            return getGoogleMap().addPolygon(polygonOptions);
        }
        return null;
    }

    private PolygonOptions CreatePolygonOptions(List<MarkerOptions> list) {
        PolygonOptions polygonOptions = new PolygonOptions();
        if (list != null) {
            for (MarkerOptions marker : list) {
                polygonOptions.add(marker.getPosition());
            }
        }
        polygonOptions.strokeColor(0x6066BB6A);
        polygonOptions.fillColor(0x6066BB6A);
        return polygonOptions;
    }

    private List<MarkerOptions> ChangeValueMarkerOptionsList(List<MarkerOptions> list, int index, LatLng position) {
        list.remove(index);
        BitmapDescriptor icon = CreateIcon(null);
        MarkerOptions markerOptions = CreateMarkerOptions(position, icon, String.valueOf(index + 1));
        list.add(index, markerOptions);
        setMarkerOptionsList(list);
        return list;
    }

    private int findIndexMarkerFromList(Marker marker) {
        int result = 0;
        List<Marker> markers = getMarkerList();
        for (int index = 0; index < markers.size(); index++) {
            if (markers.get(index).hashCode() == marker.hashCode()) {
                result = index;
                break;
            }
        }
        return result;
    }

    private BitmapDescriptor CreateIcon(String colour) {
        if (getString(R.string.yellow).equals(colour)) {
            return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
        }
        return BitmapDescriptorFactory.defaultMarker();
    }

    private List<Marker> DrawingMarkerOnMapByListOfMarkerOptions(List<MarkerOptions> list) {
        List<Marker> listMarker = new ArrayList<>();
        if (list.size() > 0) {
            for (MarkerOptions markerOptions : list) {
                listMarker.add(DrawingMarkerOnMap(markerOptions));
            }
        }
        return listMarker;
    }

    private void createMarkerCenter(LatLng center) {
        if (center != null) {
            BitmapDescriptor icon = CreateIcon(getString(R.string.yellow));
            MarkerOptions markerOptions = CreateMarkerOptions(center, icon, null);
            setMarkerCenter(DrawingMarkerOnMap(markerOptions));
        }
    }

    private void DeleteMarkerList(List<Marker> markerList) {
        if (markerList != null) {
            for (Marker marker : markerList) {
                DeleteMarkerOnMap(marker);
            }
            ClearList(getMarkerList());
        }
    }

    private void ClearList(List list) {
        list.clear();
    }

    private void DeleteMarkerOnMap(Marker marker) {
        marker.remove();
    }

    private List<MarkerOptions> addMarkerOptionsToList(MarkerOptions markerOptions, List list) {
        list.add(markerOptions);
        return list;
    }

    private Marker DrawingMarkerOnMap(MarkerOptions markerOptions) {
        Marker marker = getGoogleMap().addMarker(markerOptions);
        marker.setDraggable(true);
        marker.showInfoWindow();
        return marker;
    }

    private MarkerOptions CreateMarkerOptions(LatLng latLng, BitmapDescriptor icon, String title) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(icon);
        if (title != null) {
            markerOptions.title(title);
        }
        return markerOptions;
    }

    private String getStringFromIntentByKey(String key) {
        return getIntent().getStringExtra(key);
    }

    private LatLng selectLocation(LatLng parcelLng, LatLng location, LatLng locationCenter) {
        Double latitude = location.latitude;
        Double longitude = location.longitude;
        if (locationCenter != null) {
            latitude = locationCenter.latitude;
            longitude = locationCenter.longitude;
        }
        if (checkNullParcelLng(parcelLng)) {
            latitude = parcelLng.latitude;
            longitude = parcelLng.longitude;
        }
        return new LatLng(latitude, longitude);
    }

    private boolean checkNullParcelLng(LatLng parcelLng) {
        Boolean check = false;
        if (parcelLng != null) {
            check = true;
        }
        return check;
    }

    private LatLng createLatLngParcelNS4(Double lat, Double lng) {
        LatLng latLng = null;
        if (lat != null && lng != null) {
            latLng = new LatLng(lat, lng);
        }
        return latLng;
    }

    private void setSupportMapFragment(MapActivity mapActivity) {
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(mapActivity);
    }

    private Area convertStringJsonAreaToObject(String area) {
        return new Gson().fromJson(area, Area.class);
    }

    private ParcelDoc convertStringJsonParcelNs4ToObject(String parcelNS4) {
        return new Gson().fromJson(parcelNS4, ParcelDoc.class);
    }

    public void setTextViewLatitude(String latitude) {
        EditText tvCurrentLatitude = (EditText) findViewById(R.id.editTextCurrentLatitude);
        tvCurrentLatitude.setText(latitude);
    }

    public void setTextViewLongitude(String longitude) {
        EditText editTextCurrentLongitude = (EditText) findViewById(R.id.editTextCurrentLongitude);
        editTextCurrentLongitude.setText(longitude);
    }

    public void setTextViewResultCertificateByArea(String area) {
        EditText editTextResultCertificateByArea = (EditText) findViewById(R.id.editTextResultCertificateByArea);
        editTextResultCertificateByArea.setText(area);
    }

    public String getTextViewResultCertificateByArea() {
        EditText editTextResultCertificateByArea = (EditText) findViewById(R.id.editTextResultCertificateByArea);
        return editTextResultCertificateByArea.getText().toString();
    }

    private void moveCameraToLocation(LatLng latLng, int zoom) {
        getGoogleMap().moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    private LatLng getLocationFromAddress(String locationAddress) {
        Geocoder geocoder = new Geocoder(getBaseContext());
        List<Address> addresses = getListAddressGeoCoderFromLocationName(locationAddress, geocoder, 1);
        LatLng latLng = createLatLngParcelNS4(getLatitude(addresses), getLongitude(addresses));
        return latLng;
    }

    private double getLongitude(List<Address> addresses) {
        double longitude = 0;
        if (isAddressNotnull(addresses)) {
            longitude = addresses.get(0).getLongitude();
        }
        return longitude;
    }

    private double getLatitude(List<Address> addresses) {
        double latitude = 0;
        if (isAddressNotnull(addresses)) {
            latitude = addresses.get(0).getLatitude();
        }
        return latitude;
    }

    private boolean isAddressNotnull(List<Address> addresses) {
        if (addresses.size() > 0) {
            return addresses.get(0) != null;
        }
        return false;
    }

    private List<Address> getListAddressGeoCoderFromLocationName(String locationAddress, Geocoder geocoder, int row) {
        List<Address> addresses = new ArrayList<>();
        if (locationAddress != null) {
            try {
                addresses = geocoder.getFromLocationName(locationAddress, row);
            } catch (IOException e) {
                Log.d("ERROR CannotGetLocation", e.getMessage());
            }
        }
        return addresses;
    }

    public void setTexViewOfLocation(LatLng texViewOfLocation) {
        setTextViewLatitude(String.valueOf(texViewOfLocation.latitude));
        setTextViewLongitude(String.valueOf(texViewOfLocation.longitude));
    }

    public GoogleMap getGoogleMap() {
        return googleMap;
    }

    public void setGoogleMap(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    public LatLng getLatLngCenter() {
        return latLngCenter;
    }

    public void setLatLngCenter(LatLng latLngCenter) {
        this.latLngCenter = latLngCenter;
    }

    public List<MarkerOptions> getMarkerOptionsList() {
        return markerOptionsList;
    }

    public void setMarkerOptionsList(List<MarkerOptions> markerOptionsList) {
        this.markerOptionsList = markerOptionsList;
    }

    public List<Marker> getMarkerList() {
        return markerList;
    }

    public void setMarkerList(List<Marker> markerList) {
        this.markerList = markerList;
    }

    public void setButtonListener(MapActivity buttonListener) {
        Button buttonCancelDetail = (Button) findViewById(R.id.buttonCancelDetail);
        buttonCancelDetail.setOnClickListener(buttonListener);

        Button btnSaveDetailMap = (Button) findViewById(R.id.btnSaveDetailMap);
        btnSaveDetailMap.setOnClickListener(buttonListener);

        Button buttonDeleteMap = (Button) findViewById(R.id.buttonDeleteMap);
        buttonDeleteMap.setOnClickListener(buttonListener);

        Button buttonUndoPressedMarker = (Button) findViewById(R.id.buttonUndoPressedMarker);
        buttonUndoPressedMarker.setOnClickListener(buttonListener);

        Button buttonFindLocation = (Button) findViewById(R.id.buttonFindLocation);
        buttonFindLocation.setOnClickListener(buttonListener);

        Button buttonSearchByUTM = (Button) findViewById(R.id.buttonSearchByUTM);
        buttonSearchByUTM.setOnClickListener(buttonListener);
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }

    public Area setAreaOfMarkerDrawingToArea(Double areaOfMarkerDrawingToArea) {
        Area area = new Area(0);
        area.convertSquareMeterToRaiNganWah(areaOfMarkerDrawingToArea);
        return area;
    }

    public void setTextViewAreaDrawing(String textViewAreaDrawing) {
        TextView textViewResultAreaRealDraw = (TextView) findViewById(R.id.editTextResultAreaRealDraw);
        textViewResultAreaRealDraw.setText(textViewAreaDrawing);
    }

    public Area getAreaReal() {
        return AreaReal;
    }

    public void setAreaReal(Area areaReal) {
        AreaReal = areaReal;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Double getAreaDrawing() {
        return areaDrawing;
    }

    public void setAreaDrawing(Double areDrawing) {
        this.areaDrawing = areDrawing;
    }

    public Marker getMarkerCenter() {
        return markerCenter;
    }

    public void setMarkerCenter(Marker markerCenter) {
        this.markerCenter = markerCenter;
    }

    public Boolean getModeDetailAddDetailAgricultural() {
        return modeDetailAddDetailAgricultural;
    }

    public void setModeDetailAddDetailAgricultural(Boolean modeDetailAddDetailAgricultural) {
        this.modeDetailAddDetailAgricultural = modeDetailAddDetailAgricultural;
    }

    public List<MarkerOptions> getMainMarkerList() {
        return mainMarkerList;
    }

    public void setMainMarkerList(List<MarkerOptions> mainMarkerList) {
        this.mainMarkerList = mainMarkerList;
    }

    public List<LatLng> getMainLatLongList() {
        return mainLatLongList;
    }

    public void setMainLatLongList(List<LatLng> mainLatLongList) {
        this.mainLatLongList = mainLatLongList;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!"".equals(s)) {
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = getListAddressGeoCoderFromLocationName(s.toString(), geocoder, 10);
            List<String> stringAddress = getStringAddress(addresses);
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter(this, R.layout.spinner_item, stringAddress);
            setAdapterAutoComplete(arrayAdapter);
        }
    }

    private List<String> getStringAddress(List<Address> addresses) {
        List<String> stringAddress = new ArrayList<>();
        for (int index = 0; index < addresses.size(); index++) {
            stringAddress.add(addresses.get(index).getFeatureName());
        }
        return stringAddress;
    }

    public void setAdapterAutoComplete(ArrayAdapter<String> address) {
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        autoCompleteTextView.setAdapter(address);
    }

    public void setAutoCompleteListener(MapActivity editTextListener) {
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        autoCompleteTextView.addTextChangedListener(editTextListener);
    }

    public String getAutoCompleteListener() {
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        return autoCompleteTextView.getText().toString();
    }
}
