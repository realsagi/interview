package th.in.tamis.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import retrofit.Call;
import th.in.tamis.manager.authentication.Authentication;
import th.in.tamis.manager.authentication.AuthenticationServiceManager;
import th.in.tamis.manager.authentication.LoginManager;
import th.in.tamis.manager.authentication.OnAuthentication;
import th.in.tamis.manager.authentication.TokenLoginManager;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.tamis.BuildConfig;
import th.in.tamis.tamis.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AuthenticationActivity extends AppCompatActivity implements View.OnClickListener, OnAuthentication{

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setButtonListener(this);

        String mode = BuildConfig.MODE;
        if ("dev".equals(mode)) {
            EditText textViewEmail = (EditText) findViewById(R.id.editTextUsername);
            textViewEmail.setText("ec40101004");
            EditText editTextPassword = (EditText) findViewById(R.id.editTextPassword);
            editTextPassword.setText("123456");
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences settings;
        settings = getSharedPreferences("token", 0);
        String token = settings.getString("token", null);
        loginByToken(token);
    }

    private void loginByToken(String token) {
        if (token != null && !"".equals(token)) {
            Authentication authentication = AuthenticationServiceManager.getServiceLogin();
            Call<String> login = authentication.userLoginByToken(token);
            login.enqueue(new TokenLoginManager(this));
            createDialogPress(R.string.loginMessage);
        }
    }

    private void createDialogPress(int messageId) {
        progressDialog = new ProgressDialog(this, R.style.ProgressDialogCustom);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getResources().getString(messageId));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setProgress(0);
        progressDialog.show();
    }

    public void setButtonListener(AuthenticationActivity buttonListener) {
        Button buttonSignIn = (Button) findViewById(R.id.buttonSignIn);
        buttonSignIn.setOnClickListener(buttonListener);
    }

    @Override
    public void onClick(View v) {

        SharedPreferences settings;
        settings = getSharedPreferences("token", 0);
        String token = settings.getString("token", null);

        if (token != null && !"".equals(token) && token.length() > 14) {
            loginByToken(token);
        } else {
            Authentication authentication = AuthenticationServiceManager.getServiceLogin();
            Call<String> login = authentication.userLogin(getUsername(), getPassword());
            login.enqueue(new LoginManager(this));
            createDialogPress(R.string.loginMessage);
        }
    }

    public String getUsername() {
        EditText textViewEmail = (EditText) findViewById(R.id.editTextUsername);
        return textViewEmail.getText().toString();
    }

    public String getPassword() {
        EditText editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        return editTextPassword.getText().toString();
    }

    @Override
    public void connectServerLoginSuccess(String message) {
        progressDialog.dismiss();
        if (message.length() > 14) {
            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = getSharedPreferences("token", 0);
            editor = settings.edit();
            editor.putString("token", message);
            editor.apply();
        }
        String userId = PreferencesService.getStringPreferences("user");
        if (userId == null) {
            if (message.length() < 14){
                PreferencesService.savePreferences("user", message);
            }
            Intent gotoMain = new Intent(this, MainActivity.class);
            startActivity(gotoMain);
        } else {
            if (message.length() < 14){
                PreferencesService.savePreferences("user", message);
            }
            finish();
        }

    }

    @Override
    public void connectServerLoginFails(String message) {
        progressDialog.dismiss();
        if (!"".equals(message)) {
            if ("มีการใช้งานชื่อผู้ใช้งานนี้ที่อุปกรณ์อื่นแล้ว".equals(message)) {
                createDialogAlert(getString(R.string.errorTimeout), message);
            } else {
                createDialogAlert(getString(R.string.errorTimeout), message);
            }
        } else {
            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = getSharedPreferences("token", 0);
            editor = settings.edit();
            editor.remove("token").apply();
        }
    }

    private void createDialogAlert(String title, String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert);

        TextView textViewDialogAlertTitle = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertTitle);
        TextView textViewDialogAlertMessage = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertMessage);

        textViewDialogAlertTitle.setText(title);
        textViewDialogAlertMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonDialogAlertOK = (Button) dialogAlert.findViewById(R.id.buttonDialogAlertOK);
        buttonDialogAlertOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }
}
