package th.in.tamis.widget;

import android.text.*;
import android.text.method.DigitsKeyListener;
import android.widget.EditText;

public abstract class IdentityEditTextHandler implements TextWatcher {

    protected EditText editText;
    private Identity id;

    public IdentityEditTextHandler(EditText editText) {
        this.editText = editText;
        this.id = onCreateNewId(editText.getText().toString());
        initialize();
    }

    protected void initialize() {
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(getMaxLenght()),});
        editText.setKeyListener(DigitsKeyListener.getInstance(false, true));
        editText.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        onIdChanged(editable);
    }

    public void onIdChanged(Editable editable) {
        Identity id = onCreateNewId(editable.toString());
        updateText(id);
        updateErrorMessage(id);
    }

    private void updateText(Identity id) {
        if (!id.equals(this.id)) {
            this.id = id;
            editText.setText(id.prettyPrint());
            Selection.setSelection(editText.getEditableText(), editText.length());
        }
    }

    private void updateErrorMessage(Identity id) {
        if (id.isValidFormat()) {
            //editText.setError(id.validate() ? null : getErrorMessage());
        } else {
            editText.setError(null);
        }
    }


    public Identity getId() {
        return id;
    }

    protected abstract int getMaxLenght();

    protected abstract String getErrorMessage();

    protected abstract Identity onCreateNewId(String id);
}
