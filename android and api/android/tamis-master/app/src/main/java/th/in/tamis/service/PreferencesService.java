package th.in.tamis.service;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Type;

import th.in.tamis.manager.Contextor;

public class PreferencesService{

    private static SharedPreferences settings;
    private static SharedPreferences.Editor editor;
    public final static String FARMER = "farmer";
    public final static String HOUSEHOLDMEMBER = "householdMembers";
    public final static String REGISTERPLANTATION = "registerPlantation";

    public static void savePreferences(String key, Object object){
        settings = Contextor.getInstance().getContext().getSharedPreferences("data", 0);
        editor = settings.edit();
        String data = new Gson().toJson(object);
        editor.putString(key, data);
        editor.apply();
    }

    public static void saveIntPreferences(String key, int dataInt){
        settings = Contextor.getInstance().getContext().getSharedPreferences("data", 0);
        editor = settings.edit();
        editor.putString(key, String.valueOf(dataInt));
        editor.apply();
    }

    public static void saveStringPreferences(String key, String str){
        settings = Contextor.getInstance().getContext().getSharedPreferences("data", 0);
        editor = settings.edit();
        editor.putString(key, str);
        editor.apply();
    }

    public static Object getPreferences(String key, Class c){
        settings = Contextor.getInstance().getContext().getSharedPreferences("data", 0);
        String data = settings.getString(key, null);
        return new Gson().fromJson(data, c);
    }

    public static Object getPreferences(String key, Type t){
        settings = Contextor.getInstance().getContext().getSharedPreferences("data", 0);
        String data = settings.getString(key, null);
        return new Gson().fromJson(data, t);
    }

    public static int getIntPreferences(String key){
        settings = Contextor.getInstance().getContext().getSharedPreferences("data", 0);
        String data = settings.getString(key, null);
        assert data != null;
        return Integer.parseInt(data);
    }

    public static String getStringPreferences(String key){
        settings = Contextor.getInstance().getContext().getSharedPreferences("data", 0);
        return settings.getString(key, null);
    }

    public static void removeKey(String key){
        settings = Contextor.getInstance().getContext().getSharedPreferences("data", 0);
        editor = settings.edit();
        editor.remove(key).apply();
    }

    public static void clearAllKey(){
        settings = Contextor.getInstance().getContext().getSharedPreferences("data", 0);
        editor = settings.edit();
        editor.clear().apply();
    }
}
