package th.in.tamis.manager.parcel;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.Contextor;
import th.in.tamis.manager.MainBus;
import th.in.tamis.models.ParcelType;

public class ParcelTypeManager implements Callback<ParcelType> {

    private OnLoadParcel onLoadParcel;

    public ParcelTypeManager(OnLoadParcel listener){
        this.onLoadParcel = listener;
    }

    @Override
    public void onResponse(Response<ParcelType> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            onLoadParcel.onLoadParcelFails("error 404");
        }

        if(response.isSuccess()){

            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = Contextor.getInstance().getContext().getSharedPreferences("parcelType", 0);
            editor = settings.edit();
            String data = new Gson().toJson(response.body());
            editor.putString("parcelType", data);
            editor.apply();

            MainBus.getInstance().post(response.body());
        }

        if(response.raw().code() == 500){
            onLoadParcel.onLoadParcelFails("error 500");
        }
    }

    @Override
    public void onFailure(Throwable t) {
//        if(t.toString().contains("ConnectException")) {
//            onLoadParcel.onLoadParcelFails("Network is unreachable");
//        }
//        else if(t.toString().contains("SocketTimeoutException")){
//            onLoadParcel.onLoadParcelFails("timeout");
//        }
    }
}
