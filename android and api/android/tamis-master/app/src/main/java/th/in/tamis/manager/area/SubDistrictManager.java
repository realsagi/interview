package th.in.tamis.manager.area;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.Contextor;
import th.in.tamis.manager.MainBus;
import th.in.tamis.models.SubDistrict;

public class SubDistrictManager implements Callback<SubDistrict>{

    private OnLoadArea onLoadArea;
    private String provinceCode;
    private String districtCode;
    public SubDistrictManager(OnLoadArea listener, String provinceCode, String districtCode){
        this.onLoadArea = listener;
        this.provinceCode = provinceCode;
        this.districtCode = districtCode;
    }

    @Override
    public void onResponse(Response<SubDistrict> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            onLoadArea.onLoadAreaFails("error 404");
        }
        if(response.isSuccess()){

            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = Contextor.getInstance().getContext().getSharedPreferences("sub_district"+provinceCode+districtCode, 0);
            editor = settings.edit();
            String data = new Gson().toJson(response.body());
            editor.putString("sub_district"+provinceCode+districtCode, data);
            editor.apply();

            MainBus.getInstance().post(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
//        if(t.toString().contains("ConnectException")) {
//            onLoadArea.onLoadAreaFails("Network is unreachable");
//        }
//        else if(t.toString().contains("SocketTimeoutException")){
//            onLoadArea.onLoadAreaFails("timeout");
//        }
    }
}
