package th.in.tamis.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

import th.in.tamis.models.FarmersOrganization;
import th.in.tamis.models.HouseHoldMember;
import th.in.tamis.models.Person;
import th.in.tamis.tamis.R;
import th.in.tamis.holder.HouseholdRegistrationHolder;
import th.in.tamis.service.PreferencesService;

public class HouseholdRegistrationAdapter extends RecyclerView.Adapter<HouseholdRegistrationHolder> {

    @Override
    public HouseholdRegistrationHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        View itemCardView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_household_registeration, viewGroup, false);
        return new HouseholdRegistrationHolder(itemCardView);
    }

    @Override
    public void onBindViewHolder(HouseholdRegistrationHolder holder, int position) {
        holder.setFirstName(holder.getItemPerson().getPerson().get(position).getFirstName() + " " +
                holder.getItemPerson().getPerson().get(position).getLastName());

        holder.setCitizenID(holder.getItemPerson().getPerson().get(position).getCitizenId());
        holder.setSex(holder.getItemPerson().getPerson().get(position).getSex());
        holder.setHelpWork(holder.getItemPerson().getPerson().get(position).isStatusHelpWork());
        holder.setDetailStatusOrganization("ยังไม่เป็นสมาชิกองค์กรใดๆ");
        if(holder.getItemPerson().getPerson().get(position).getFarmersOrganization() != null) {
            holder.setDetailStatusOrganization(
                    loadListCheckboxChecked(
                            holder.getItemPerson().getPerson().get(position).getFarmersOrganization()
                    )
            );
        }
    }

    private String loadListCheckboxChecked(FarmersOrganization farmersOrganization) {
        FarmersOrganization organization = (FarmersOrganization)
                PreferencesService.getPreferences("FarmerOrganization", FarmersOrganization.class);
        List<String> listOfCheckboxChecked = farmersOrganization.getIdList();
        if (listOfCheckboxChecked != null) {
            return selectOrganizationChecked(listOfCheckboxChecked, organization.getOrganizationsList());
        }
        return "";
    }

    private String selectOrganizationChecked(List<String> listOfCheckboxChecked, List<FarmersOrganization.Organizations> organizationsList) {
        String result = "";
        for(int indexOrganization = 0 ; indexOrganization < organizationsList.size() ; indexOrganization++){
            for(int indexListChecked = 0 ; indexListChecked < listOfCheckboxChecked.size() ; indexListChecked++){
                if(listOfCheckboxChecked.get(indexListChecked).equals(organizationsList.get(indexOrganization).getId())){
                    result += organizationsList.get(indexOrganization).getName() + "\n";
                }
            }
        }
        return result;
    }

    @Override
    public int getItemCount() {
        HouseHoldMember houseHoldMember = (HouseHoldMember) PreferencesService
                .getPreferences("householdMembers", HouseHoldMember.class);
        List<Person> persons = houseHoldMember.getPerson();
        return persons.size();
    }
}
