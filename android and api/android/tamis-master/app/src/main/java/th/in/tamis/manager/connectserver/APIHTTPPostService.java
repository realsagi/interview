package th.in.tamis.manager.connectserver;

import com.squareup.okhttp.RequestBody;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import th.in.tamis.models.AllData;

public interface APIHTTPPostService {

    @POST("/add_new_farmer")
    Call<AllData> addNewFarmer(@Body AllData allData);

    @Multipart
    @POST("/upload_image")
    Call<String> uploadImages(@Part("photo") RequestBody photo,
                              @Part("path_name") String path_name,
                              @Part("citizen_id") String citizen_id);
}
