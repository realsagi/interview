package th.in.tamis.usbreader;


import android.content.Context;

public interface CardReader {
    void registerUSB(Context context);
    void readData();

}
