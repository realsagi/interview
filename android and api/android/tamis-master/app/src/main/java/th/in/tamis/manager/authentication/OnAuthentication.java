package th.in.tamis.manager.authentication;

public interface OnAuthentication {
    void connectServerLoginSuccess(String message);
    void connectServerLoginFails(String message);
}
