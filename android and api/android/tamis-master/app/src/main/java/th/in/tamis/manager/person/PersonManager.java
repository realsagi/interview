package th.in.tamis.manager.person;

import th.in.tamis.manager.http.HTTPManager;

public class PersonManager {
    public static APIPerson getPerson(){
        return HTTPManager.getInstance().getRetrofit().create(APIPerson.class);
    }
}
