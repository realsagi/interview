package th.in.tamis.service.image;

import android.graphics.Bitmap;

public class ScaledBitmap {
    public Bitmap getScaledBitmapForCardView(Bitmap bitmap){
        final int dstWidth = 732;
        final int dstHeight = 350;
        return Bitmap.createScaledBitmap(bitmap, dstWidth, dstHeight, false);
    }
    public Bitmap getScaledBitmapFullImage(Bitmap bitmap){
        final int dstWidth = 736;
        final int dstHeight = 827;
        return Bitmap.createScaledBitmap(bitmap, dstWidth, dstHeight, false);
    }
    public Bitmap getScaledBitmapForProfile(Bitmap bitmap){
        final int dstWidth = 297;
        final int dstHeight = 355;
        return Bitmap.createScaledBitmap(bitmap, dstWidth, dstHeight, false);
    }
}
