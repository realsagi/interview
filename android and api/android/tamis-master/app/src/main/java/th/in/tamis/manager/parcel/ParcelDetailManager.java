package th.in.tamis.manager.parcel;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.MainBus;
import th.in.tamis.models.ParcelDoc;

public class ParcelDetailManager implements Callback<ParcelDoc>{

    private OnLoadParcel onLoadParcel;

    public ParcelDetailManager(OnLoadParcel listener){
        this.onLoadParcel = listener;
    }

    @Override
    public void onResponse(Response<ParcelDoc> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            onLoadParcel.onLoadParcelFails("error 404");
        }

        if(response.isSuccess()){
            ParcelDoc parcelDoc = response.body();
            if (parcelDoc.equals("{}")) {
                parcelDoc = new ParcelDoc();
            }
            MainBus.getInstance().post(parcelDoc);
        }

        if(response.raw().code() == 500){
            onLoadParcel.onLoadParcelFails("error 500");
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadParcel.onLoadParcelFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadParcel.onLoadParcelFails("timeout");
        }
    }
}
