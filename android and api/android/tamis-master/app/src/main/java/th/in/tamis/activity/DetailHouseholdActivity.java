package th.in.tamis.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

import th.in.tamis.models.FarmersOrganization;
import th.in.tamis.models.HouseHoldMember;
import th.in.tamis.models.Person;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.service.authentication.TokenLoginService;
import th.in.tamis.tamis.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailHouseholdActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup rgStatusHelpWork;
    private boolean statusHelp;
    private HouseHoldMember houseHoldMember;
    private Person person;
    private LinearLayout linearLayoutForCreateCheckbox;
    private FarmersOrganization farmersOrganization;
    private List<CheckBox> checkBoxList;
    private Typeface typeface;

    public DetailHouseholdActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_household);

        typeface = Typeface.createFromAsset(this.getAssets(), "fonts/THSarabunNew.ttf");

        Button btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        Button btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);

        linearLayoutForCreateCheckbox = (LinearLayout) findViewById(R.id.linearLayoutForCreateCheckbox);
        rgStatusHelpWork = (RadioGroup) findViewById(R.id.rgStatusHelpWork);
        RadioButton rbHelp = (RadioButton) findViewById(R.id.rbHelp);
        RadioButton rbNotHelp = (RadioButton) findViewById(R.id.rbNotHelp);

        int positionFarmerItem = PreferencesService.getIntPreferences("positionFarmerItem");
        houseHoldMember = (HouseHoldMember) PreferencesService.getPreferences("householdMembers", HouseHoldMember.class);
        person = houseHoldMember.getPerson().get(positionFarmerItem);

        rbHelp.setChecked(person.isStatusHelpWork());
        rbHelp.setTypeface(typeface);
        rbHelp.setTextColor(ContextCompat.getColor(this, R.color.colorDarkGray));
        rbHelp.setPadding(20, 20, 20, 20);
        rbHelp.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.font_size_input));

        rbNotHelp.setChecked(!person.isStatusHelpWork());
        rbNotHelp.setTypeface(typeface);
        rbNotHelp.setTextColor(ContextCompat.getColor(this, R.color.colorDarkGray));
        rbNotHelp.setPadding(20, 20, 20, 20);
        rbNotHelp.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.font_size_input));

        farmersOrganization = person.getFarmersOrganization();
        if(farmersOrganization == null) {
            farmersOrganization = new FarmersOrganization();
        }
        createCheckbox();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new TokenLoginService(this).loginByToken();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void createCheckbox() {
        checkBoxList = new ArrayList<>();
        FarmersOrganization organization = (FarmersOrganization)
                PreferencesService.getPreferences("FarmerOrganization", FarmersOrganization.class);
        for(int index = 0 ; index < organization.getOrganizationsList().size() ; index ++) {
            CheckBox checkBox = new CheckBox(this);
            checkBox.setId(Integer.parseInt(organization.getOrganizationsList().get(index).getId()));
            checkBox.setTextColor(ContextCompat.getColor(this, R.color.colorDarkGray));
            checkBox.setTypeface(typeface);
            checkBox.setPadding(16,20,0,20);
            checkBox.setButtonDrawable(R.drawable.selector_checkbox);
            checkBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.font_size_input));
            checkBox.setText(organization.getOrganizationsList().get(index).getName());
            setChecked(organization, index, checkBox);
            linearLayoutForCreateCheckbox.addView(checkBox);
            checkBoxList.add(checkBox);
        }
    }

    private void setChecked(FarmersOrganization organization, int index, CheckBox checkBox) {
        if(farmersOrganization.getIdList() != null)
            selectOrganizationChecked(organization, index, checkBox);
    }

    private void selectOrganizationChecked(FarmersOrganization organization, int index, CheckBox checkBox) {
        for(int indexOfIdList = 0 ; indexOfIdList < farmersOrganization.getIdList().size() ; indexOfIdList ++){
            compareCheckedWithOrganization(organization, index, checkBox, indexOfIdList);
        }
    }

    private void compareCheckedWithOrganization(FarmersOrganization organization, int index, CheckBox checkBox, int indexOfIdList) {
        if(farmersOrganization.getIdList().get(indexOfIdList).equals(organization.getOrganizationsList().get(index).getId()))
            checkBox.setChecked(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCancel:
                break;
            case R.id.btnSave:
                saveStage();
                break;
        }
        finish();
    }

    private void saveStage() {
        switch (rgStatusHelpWork.getCheckedRadioButtonId()){
            case R.id.rbHelp:
                statusHelp = true;
                break;
            case R.id.rbNotHelp:
                statusHelp = false;
                break;
        }
        person.setStatusHelpWork(statusHelp);

        List<String> checkboxChecked = CheckedCheckBox(checkBoxList);
        if(checkboxChecked.size() > 0) {
            farmersOrganization.setIdList(checkboxChecked);
            person.setFarmersOrganization(farmersOrganization);
        }else {
            farmersOrganization.setIdList(null);
            person.setFarmersOrganization(null);
        }
        PreferencesService.savePreferences("householdMembers", houseHoldMember);
    }

    private List<String> CheckedCheckBox(List<CheckBox> checkBoxList) {
        List<String> listCheckboxChecked =  new ArrayList<>();
        for(int index = 0 ; index < checkBoxList.size() ; index++) {
            if(checkBoxList.get(index).isChecked()) {
                listCheckboxChecked.add(String.valueOf(checkBoxList.get(index).getId()));
            }
        }
        return listCheckboxChecked;
    }
}
