package th.in.tamis.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import th.in.tamis.adapter.RegisterPlantationAdapter;
import th.in.tamis.models.DetailRegisterPlantation;
import th.in.tamis.models.Farmer;
import th.in.tamis.models.RegisterPlantation;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.service.authentication.TokenLoginService;
import th.in.tamis.tamis.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterPlantationActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rvRegisterPlantation;

    private Button btnVerifyInformationAndSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_plantation);
        initInstance();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initInstance() {
        Farmer farmer = (Farmer) PreferencesService.getPreferences("farmer", Farmer.class);

        TextView textViewCitizenId = (TextView) findViewById(R.id.editTextCitizenId);
        textViewCitizenId.setText(String.format("%s-%s-%s-%s-%s",
                farmer.getCitizenId().substring(0, 1),
                farmer.getCitizenId().substring(1, 5),
                farmer.getCitizenId().substring(5, 10),
                farmer.getCitizenId().substring(10, 12),
                farmer.getCitizenId().substring(12, 13)));
        TextView textViewFullName = (TextView) findViewById(R.id.editTextFullName);
        textViewFullName.setText(String.format("%s %s %s",
                farmer.getPreName(),
                farmer.getFirstName(),
                farmer.getLastName()));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RegisterPlantationActivity.this,
                LinearLayoutManager.VERTICAL,
                false);

        Button btnHouseholdInformation = (Button) findViewById(R.id.btnHouseholdInformation);
        btnHouseholdInformation.setOnClickListener(this);

        ImageButton btnRegisterPlantation = (ImageButton) findViewById(R.id.btnAddRegisterPlantation);
        btnRegisterPlantation.setOnClickListener(this);

        btnVerifyInformationAndSave = (Button) findViewById(R.id.btnVerifyInformationAndSave);
        btnVerifyInformationAndSave.setOnClickListener(this);

        rvRegisterPlantation = (RecyclerView) findViewById(R.id.rvRegisterPlantation);
        rvRegisterPlantation.setLayoutManager(mLayoutManager);

        setBoldText();
    }

    private void setBoldText() {
        Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/THSarabunNewBold.ttf");
        TextView textViewFullName = (TextView) findViewById(R.id.textViewFullName);
        textViewFullName.setTypeface(typeface);

        TextView textViewCitizenId = (TextView) findViewById(R.id.textViewCitizenId);
        textViewCitizenId.setTypeface(typeface);
    }

    @Override
    protected void onResume() {
        super.onResume();
        rvRegisterPlantation.setAdapter(new RegisterPlantationAdapter());

        RegisterPlantation registerPlantation = (RegisterPlantation) PreferencesService.getPreferences("registerPlantation", RegisterPlantation.class);
        if (registerPlantation != null) {
            List<DetailRegisterPlantation> listRegisterPlantation = registerPlantation.getDetailRegisterPlantation();
            for (DetailRegisterPlantation detailRegisterPlantation : listRegisterPlantation) {
                if (detailRegisterPlantation.getDetailAgriculturalOperationses() != null) {
                    btnVerifyInformationAndSave.setEnabled(true);
                    break;
                }
            }
        }
        new TokenLoginService(this).loginByToken();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnHouseholdInformation:
                finish();
                break;
            case R.id.btnAddRegisterPlantation:
                Intent IntentOfDetailPlantationActivity = new Intent(v.getContext(), DetailPlantationActivity.class);
                v.getContext().startActivity(IntentOfDetailPlantationActivity);
                break;
            case R.id.btnVerifyInformationAndSave:
                Intent IntentOfSignAuthenticationActivity = new Intent(v.getContext(), SignAuthenticationActivity.class);
                v.getContext().startActivity(IntentOfSignAuthenticationActivity);
                break;
        }
    }
}
