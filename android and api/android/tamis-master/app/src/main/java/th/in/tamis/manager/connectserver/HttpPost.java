package th.in.tamis.manager.connectserver;

import th.in.tamis.manager.http.HTTPManager;

public class HttpPost {


    public static APIHTTPPostService getService() {
        return HTTPManager.getInstance().getRetrofit().create(APIHTTPPostService.class);
    }
}
