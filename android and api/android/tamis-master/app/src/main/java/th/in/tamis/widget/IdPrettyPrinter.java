package th.in.tamis.widget;

public abstract class IdPrettyPrinter implements PrettyPrinter {

    public static final String DEFAULT_SEPARATOR = "-";

    @Override
    public String print(String id) {
        id = id.trim();
        StringBuilder idFormatted = new StringBuilder();
        for (int i = 0; i < id.length(); i++) {
            if (positionToInsertSeparatorBefore(i))
                idFormatted.append(separator());
            idFormatted.append(id.charAt(i));
        }
        return idFormatted.toString();
    }

    abstract boolean positionToInsertSeparatorBefore(int position);

    @Override
    public String separator() {
        return DEFAULT_SEPARATOR;
    }


}
