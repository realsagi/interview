package th.in.tamis.manager.career;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.OnLoadCacheAll;
import th.in.tamis.models.ProfileCareer;

public class CareerProfileCacheManager implements Callback<ProfileCareer> {

    private OnLoadCacheAll onLoadAreaAll;

    public CareerProfileCacheManager(OnLoadCacheAll listener){
        this.onLoadAreaAll = listener;
    }

    @Override
    public void onResponse(Response<ProfileCareer> response, Retrofit retrofit) {
        if(response.isSuccess()){
            onLoadAreaAll.onLoadCareerProfileSuccess(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadAreaAll.onLoadFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadAreaAll.onLoadFails("timeout");
        }
    }
}
