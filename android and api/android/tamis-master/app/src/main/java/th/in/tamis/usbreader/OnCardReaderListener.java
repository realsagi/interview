package th.in.tamis.usbreader;

import th.in.tamis.models.Farmer;

public interface OnCardReaderListener {
    void onError();
    void onReadDataSuccess(Farmer farmer);
}
