/*
 * Copyright (c) 2015 NECTEC
 *   National Electronics and Computer Technology Center, Thailand
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package th.in.tamis.widget;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import th.in.tamis.service.Area;
import th.in.tamis.tamis.R;


/**
 * Dialog for select area's size in Thai Unit of measurement
 */
public class AreaPickerDialog extends AlertDialog {

    public static final String TITLE = "ระบุขนาดพื้นที่";
    public static final String TITLE_SEPARATOR = " : ";
    private NumberPicker rai;
    private NumberPicker ngan;
    private NumberPicker squareWa;
    private OnAreaPickListener onAreaPickListener;
    private TextView textViewTitleDatePicker;

    private OnClickListener onPositiveButtonClick = new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            rai.clearFocus();
            ngan.clearFocus();
            squareWa.clearFocus();
            Area area = Area.fromRaiNganSqaureWa(rai.getValue(), ngan.getValue(), squareWa.getValue());
            onAreaPickListener.onAreaPick(area);
            View v  = getCurrentFocus();
            if(v != null) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
            dismiss();
        }
    };

    private OnClickListener onNegativeButtonClick = new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            onAreaPickListener.onCancel();
            dismiss();
        }
    };

    private OnValueChangeListener raiNganSquareWaChangeListener = new OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker numberPicker, int i, int i1) {
            Area area = Area.fromRaiNganSqaureWa(rai.getValue(), ngan.getValue(), squareWa.getValue());
            updateTitle(area);
        }
    };

    public AreaPickerDialog(Context context, OnAreaPickListener listener) {
        super(context);
        this.onAreaPickListener = listener;

        setTitlePickerDialog();
        setCustomTitle(textViewTitleDatePicker);

        setupView(context);
    }

    private void setTitlePickerDialog() {
        textViewTitleDatePicker = new TextView(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        textViewTitleDatePicker.setLayoutParams(layoutParams);
        textViewTitleDatePicker.setPadding(10, 10, 10, 10);
        textViewTitleDatePicker.setGravity(Gravity.CENTER);
        textViewTitleDatePicker.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimensionPixelSize(R.dimen.font_size_input));
        textViewTitleDatePicker.setText(TITLE);
        textViewTitleDatePicker.setTextColor(Color.parseColor("#FFFFFF"));
        textViewTitleDatePicker.setBackgroundColor(Color.parseColor("#66BB6A"));
    }

    private void setupView(Context context) {
        View view = InflateView(context);
        setView(view);
        findView(view);
        initRai();
        initNgan();
        initSquareWa();
        initButton();
    }

    @SuppressLint("InflateParams")
    private View InflateView(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.area_picker_dialog, null, false);
    }

    private void findView(View view) {
        rai = (NumberPicker) view.findViewById(R.id.rai);
        ngan = (NumberPicker) view.findViewById(R.id.ngan);
        squareWa = (NumberPicker) view.findViewById(R.id.squareWa);
    }

    private void initSquareWa() {
        squareWa.setMaxValue(99);
        squareWa.setMinValue(0);
        squareWa.setValue(0);
        squareWa.setOnValueChangedListener(raiNganSquareWaChangeListener);
    }

    private void initNgan() {
        ngan.setMaxValue(3);
        ngan.setMinValue(0);
        ngan.setValue(0);
        ngan.setOnValueChangedListener(raiNganSquareWaChangeListener);
    }

    private void initRai() {
        rai.setMaxValue(10000);
        rai.setMinValue(0);
        rai.setValue(0);
        rai.setOnValueChangedListener(raiNganSquareWaChangeListener);

    }
    private void initButton() {
        setButton(BUTTON_POSITIVE, getContext().getString(R.string.ok), onPositiveButtonClick);
        setButton(BUTTON_NEGATIVE, getContext().getString(R.string.btnCancel), onNegativeButtonClick);
    }

    public void show(Area area) {
        updateValue(area);
        updateTitle(area);
        this.show();

        getButton(BUTTON_POSITIVE).setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/THSarabunNew.ttf"));
        getButton(BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimensionPixelSize(R.dimen.font_size_input));

        getButton(BUTTON_NEGATIVE).setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/THSarabunNew.ttf"));
        getButton(BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimensionPixelSize(R.dimen.font_size_input));
    }

    private void updateValue(Area area) {
        rai.setValue(area.getRai());
        ngan.setValue(area.getNgan());
        squareWa.setValue(area.getSquareWah());
    }

    private void updateTitle(Area area) {
        StringBuilder builder = new StringBuilder(TITLE);
        String detail = area.prettyPrint();
        if (!TextUtils.isEmpty(detail))
            builder.append(TITLE_SEPARATOR).append(detail);
        textViewTitleDatePicker.setText(builder);
        setCustomTitle(textViewTitleDatePicker);
    }


    public interface OnAreaPickListener {
        void onAreaPick(Area area);

        void onCancel();
    }

}
