package th.in.tamis.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import retrofit.Call;
import th.in.tamis.manager.connectserver.APIHTTPPostService;
import th.in.tamis.manager.connectserver.HttpManage;
import th.in.tamis.manager.connectserver.HttpPost;
import th.in.tamis.manager.connectserver.UploadImagesManager;
import th.in.tamis.models.AllData;
import th.in.tamis.models.Farmer;
import th.in.tamis.models.HouseHoldMember;
import th.in.tamis.models.RegisterPlantation;
import th.in.tamis.service.DateFormatThai;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.service.authentication.TokenLoginService;
import th.in.tamis.service.file.FileManagement;
import th.in.tamis.tamis.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ConfirmRegisterFarmerActivity extends AppCompatActivity implements View.OnClickListener {
    private Bitmap bitmapSignAuthentication;
    private APIHTTPPostService serviceAddMember;
    private Farmer farmer;
    private HouseHoldMember householdMembers;
    private RegisterPlantation registerPlantation;
    private byte[] byteArraySign;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_rigister_farmer);
        initInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new TokenLoginService(this).loginByToken();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initInstance() {
        ImageView imageViewShowLicense = (ImageView) findViewById(R.id.imageViewShowLicense);
        imageViewShowLicense.setOnClickListener(this);

        farmer = (Farmer) PreferencesService.getPreferences(PreferencesService.FARMER, Farmer.class);

        TextView textViewNameSignature = (TextView) findViewById(R.id.textViewNameSignature);
        textViewNameSignature.setText(String.format("%s %s %s", farmer.getPreName(),
                farmer.getFirstName(), farmer.getLastName()));

        TextView textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewDate.setText(DateFormatThai.getInstance().getNowThai(Calendar.getInstance()));

        TextView textViewTime = (TextView) findViewById(R.id.textViewTime);
        textViewTime.setText(DateFormatThai.getInstance().getCurrentTime());

        Button btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);

        if (getIntent().hasExtra("bitmapImage")) {
            byteArraySign = getIntent().getByteArrayExtra("bitmapImage");
            bitmapSignAuthentication = BitmapFactory.decodeByteArray(byteArraySign, 0, byteArraySign.length);
            imageViewShowLicense.setImageBitmap(bitmapSignAuthentication);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSend:
                try {
                    FileOutputStream outputStream = new FileOutputStream(
                            new FileManagement("SignAuthentication_").generatePath());
                    int quality = 90;
                    bitmapSignAuthentication.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                createDialogProgress();
                addNewMember();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                PreferencesService.clearAllKey();
                break;
            case R.id.imageViewShowLicense:
                finish();
                break;
        }
    }

    private void addNewMember() {
        householdMembers = (HouseHoldMember) PreferencesService.getPreferences(PreferencesService.HOUSEHOLDMEMBER, HouseHoldMember.class);
        registerPlantation = (RegisterPlantation) PreferencesService.getPreferences(PreferencesService.REGISTERPLANTATION, RegisterPlantation.class);

        AllData allData = new AllData(farmer, householdMembers, registerPlantation, Integer.parseInt(PreferencesService.getStringPreferences("user")));
        serviceAddMember = HttpPost.getService();
        uploadImageNewMember();
        uploadData(allData);
    }

    private void createDialogProgress() {
        progressDialog = new ProgressDialog(this, R.style.ProgressDialogCustom);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getResources().getString(R.string.sending_data));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setProgress(0);
        progressDialog.show();
    }

    private void createDialogAlertNoTitle(String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert_no_title);

        TextView textViewMessage = (TextView) dialogAlert.findViewById(R.id.textViewMessage);
        textViewMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonOK = (Button) dialogAlert.findViewById(R.id.buttonOK);
        buttonOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                progressDialog.dismiss();
                dialogAlert.dismiss();
            }
        });
    }
    private void uploadImageNewMember() {
        if (foundImageFile(farmer.getFileImageProFile())) {
            File fileImageProfile = farmer.getFileImageProFile();
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageProfile);
            Call<String> call = serviceAddMember.uploadImages(requestBody, "PROFILE.jpg", farmer.getCitizenId());
            call.enqueue(new UploadImagesManager());
            farmer.setNameImageProfile("PROFILE.JPG");
        }

        for (int i = 0; i < registerPlantation.getDetailRegisterPlantation().size(); i++) {
            if (foundImageFile(registerPlantation.getDetailRegisterPlantation().get(i).getImageDetailPlantation())) {
                File fileImageDetailPlantation = registerPlantation.getDetailRegisterPlantation().get(i).getImageDetailPlantation();
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageDetailPlantation);
                Call<String> call = serviceAddMember.uploadImages(requestBody, "DETAIL_PLANTATION_" + (i + 1) + ".JPG", farmer.getCitizenId());
                call.enqueue(new UploadImagesManager());
                registerPlantation.getDetailRegisterPlantation().get(i).setFileNameImagePlantation("DETAIL_PLANTATION_" + (i + 1) + ".JPG");
            }

            if (registerPlantation.getDetailRegisterPlantation().get(i).getDetailAgriculturalOperationses() != null) {
                for (int j = 0; j < registerPlantation.getDetailRegisterPlantation().get(i).getDetailAgriculturalOperationses().size(); j++) {
                    if (foundImageFile(registerPlantation.getDetailRegisterPlantation().get(i).getDetailAgriculturalOperationses().get(j).getImageDetailAgriculture())) {
                        File fileImageDetailAgricultural = registerPlantation.getDetailRegisterPlantation().get(i).getDetailAgriculturalOperationses().get(j).getImageDetailAgriculture();
                        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageDetailAgricultural);
                        Call<String> call = serviceAddMember.uploadImages(requestBody, "AGRICULTURAL_" + (i + 1) + "_" + (j + 1) + ".JPG", farmer.getCitizenId());
                        call.enqueue(new UploadImagesManager());
                        registerPlantation.getDetailRegisterPlantation().get(i).getDetailAgriculturalOperationses().get(j).setFileNameImageActivity("AGRICULTURAL_" + (i + 1) + "_" + (j + 1) + ".JPG");
                    }
                }
            }
        }

        File fileImageSign = new FileManagement("SIGN_").generatePath();
        try {
            FileOutputStream out = new FileOutputStream(fileImageSign.getPath());
            out.write(byteArraySign);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageSign);
        Call<String> call = serviceAddMember.uploadImages(requestBody, "SIGN.JPG", farmer.getCitizenId());
        call.enqueue(new UploadImagesManager());
        farmer.setNameImageSign("SIGN.JPG");
    }

    private boolean foundImageFile(File file) {
        return file != null;
    }

    private void uploadData(AllData allData) {
        Call<AllData> member = serviceAddMember.addNewFarmer(allData);
        member.enqueue(new HttpManage());
    }
}
