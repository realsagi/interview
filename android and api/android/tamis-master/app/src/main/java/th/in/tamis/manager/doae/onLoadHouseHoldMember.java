package th.in.tamis.manager.doae;

import th.in.tamis.models.Farmer;
import th.in.tamis.models.MemberDOAE;

public interface onLoadHouseHoldMember {
    void onLoadHouseHoldMemberSuccess(MemberDOAE memberDOAE, Farmer farmer);
    void onLoadHouseHoldMemberFails(String message);
}
