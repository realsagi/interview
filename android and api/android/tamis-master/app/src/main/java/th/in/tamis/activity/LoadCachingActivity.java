package th.in.tamis.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import th.in.tamis.manager.Contextor;
import th.in.tamis.manager.OnLoadCacheAll;
import th.in.tamis.manager.agriculture.APIServiceAgriculture;
import th.in.tamis.manager.agriculture.AgricultureBreedAllManager;
import th.in.tamis.manager.agriculture.AgricultureCommunityCacheManager;
import th.in.tamis.manager.agriculture.AgricultureDetailAllManager;
import th.in.tamis.manager.agriculture.AgricultureManager;
import th.in.tamis.manager.agriculture.AgricultureTypeAllManager;
import th.in.tamis.manager.area.APIArea;
import th.in.tamis.manager.area.AreaMangager;
import th.in.tamis.manager.area.DistrictAllManager;
import th.in.tamis.manager.area.ProvinceAllManager;
import th.in.tamis.manager.area.SubDistrictAllManager;
import th.in.tamis.manager.career.APIServiceCareer;
import th.in.tamis.manager.career.CareerManager;
import th.in.tamis.manager.career.CareerProfileCacheManager;
import th.in.tamis.manager.parcel.APIServiceParcel;
import th.in.tamis.manager.parcel.ParcelManager;
import th.in.tamis.manager.parcel.ParcelTypeCacheManager;
import th.in.tamis.models.AgricultureBreed;
import th.in.tamis.models.AgricultureDetail;
import th.in.tamis.models.AgricultureType;
import th.in.tamis.models.District;
import th.in.tamis.models.FarmersOrganization;
import th.in.tamis.models.ParcelType;
import th.in.tamis.models.ProfileCareer;
import th.in.tamis.models.Province;
import th.in.tamis.models.SubDistrict;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.tamis.R;

public class LoadCachingActivity extends AppCompatActivity implements OnLoadCacheAll {

    LinearLayout linearlayoutLoadCache;
    int checkCount = 0;
    int CountAll = 9;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_cache);

        linearlayoutLoadCache = (LinearLayout) findViewById(R.id.linearlayoutLoadCache);

        TextView textViewHead = new TextView(this);
        textViewHead.setText("โหลด cache มี "+ CountAll +" รายการ");
        linearlayoutLoadCache.addView(textViewHead);

        APIArea apiArea = AreaMangager.getArea();
        APIServiceParcel apiServiceParcel = ParcelManager.getParcel();
        APIServiceCareer apiServiceCareer = CareerManager.getCareer();
        APIServiceAgriculture apiServiceAgriculture = AgricultureManager.getServiceAgriculture();

        String cachingProvince = getCache("cachingProvince");
        if(cachingProvince == null) {
            Call<Province> provinceCall = apiArea.loadProvince();
            provinceCall.enqueue(new ProvinceAllManager(this));
        }else {
            Province province = new Gson().fromJson(cachingProvince, Province.class);
            checkCount++;
            TextView textView = new TextView(this);
            textView.setText(checkCount +". กำลังโหลดรายชื่อโหลดจัดหวัด มี "+province.getProvinces().size()+" รายการ");
            linearlayoutLoadCache.addView(textView);
            nextIntent();
        }

        String cachingDistrict = getCache("cachingDistrict");
        if(cachingDistrict == null) {
            Call<District> districtCall = apiArea.loadAllDistrict();
            districtCall.enqueue(new DistrictAllManager(this));
        }else {
            District district = new Gson().fromJson(cachingDistrict, District.class);
            checkCount++;
            TextView textView = new TextView(this);
            textView.setText(checkCount +". กำลังโหลดรายชื่ออำเภอ มี "+district.getDistricts().size()+" รายการ");
            linearlayoutLoadCache.addView(textView);
            nextIntent();
        }

        String cachingSubDistrict = getCache("cachingSubDistrict");
        if(cachingSubDistrict == null) {
            Call<SubDistrict> subDistrictCall = apiArea.loadAllSubDistrict();
            subDistrictCall.enqueue(new SubDistrictAllManager(this));
        }else {
            SubDistrict subDistrict = new Gson().fromJson(cachingSubDistrict, SubDistrict.class);
            checkCount++;
            TextView textView = new TextView(this);
            textView.setText(checkCount +". กำลังโหลดราชื่อตำบล มี "+subDistrict.getSubDistricts().size()+" รายการ");
            linearlayoutLoadCache.addView(textView);
            nextIntent();
        }

        String cachingParcelType = getCache("cachingParcelType");
        if(cachingParcelType == null) {
            Call<ParcelType> parcelTypeCall = apiServiceParcel.getParcelType();
            parcelTypeCall.enqueue(new ParcelTypeCacheManager(this));
        }else {
            ParcelType parcelType = new Gson().fromJson(cachingParcelType, ParcelType.class);
            checkCount++;
            TextView textView = new TextView(this);
            textView.setText(checkCount +". กำลังโหลดรายชื่อประเภทเอกสาร มี "+parcelType.getParcelType().size()+" รายการ");
            linearlayoutLoadCache.addView(textView);
            nextIntent();
        }

        String cachingProfileCareer = getCache("cachingProfileCareer");
        if(cachingProfileCareer == null) {
            Call<ProfileCareer> profileCareerCall = apiServiceCareer.getCareer();
            profileCareerCall.enqueue(new CareerProfileCacheManager(this));
        }else {
            ProfileCareer profileCareer = new Gson().fromJson(cachingProfileCareer, ProfileCareer.class);
            checkCount++;
            TextView textView = new TextView(this);
            textView.setText(checkCount +". กำลังโหลดรายชื่ออาชีพหลักอาชีพรอง มี "+profileCareer.getCareer().size()+" รายการ");
            linearlayoutLoadCache.addView(textView);
            nextIntent();
        }

        String cachingAgricultureType = getCache("cachingAgricultureType");
        if(cachingAgricultureType == null) {
            Call<AgricultureType> agricultureTypeCall = apiServiceAgriculture.loadType();
            agricultureTypeCall.enqueue(new AgricultureTypeAllManager(this));
        }else {
            AgricultureType agricultureType = new Gson().fromJson(cachingAgricultureType, AgricultureType.class);
            checkCount++;
            TextView textView = new TextView(this);
            textView.setText(checkCount +". กำลังรายชื่อโหลดประเภทพืชเกษตร มี "+agricultureType.getListOfAgricultureType().size()+" รายการ");
            linearlayoutLoadCache.addView(textView);
            nextIntent();
        }

        String cachingAgricultureDetail = getCache("cachingAgricultureDetail");
        if(cachingAgricultureDetail == null) {
            Call<AgricultureDetail> agricultureDetailCall = apiServiceAgriculture.loadDetailAll();
            agricultureDetailCall.enqueue(new AgricultureDetailAllManager(this));
        }else {
            AgricultureDetail agricultureDetail = new Gson().fromJson(cachingAgricultureDetail, AgricultureDetail.class);
            checkCount++;
            TextView textView = new TextView(this);
            textView.setText(checkCount +". กำลังโหลดรายชื่อรายละเอียดประเภทพืช มี "+agricultureDetail.getDetail().size()+" รายการ");
            linearlayoutLoadCache.addView(textView);
            nextIntent();
        }

        String cachingAgricultureBreed = getCache("cachingAgricultureBreed");
        if(cachingAgricultureBreed == null) {
            Call<AgricultureBreed> agricultureBreedCall = apiServiceAgriculture.loadBreedAll();
            agricultureBreedCall.enqueue(new AgricultureBreedAllManager(this));
        }else {
            AgricultureBreed agricultureBreed = new Gson().fromJson(cachingAgricultureBreed, AgricultureBreed.class);
            checkCount++;
            TextView textView = new TextView(this);
            textView.setText(checkCount +". กำลังโหลดรายชื่อพันธ์พืชเกษตร มี "+agricultureBreed.getAgricultureBreed().size()+" รายการ");
            linearlayoutLoadCache.addView(textView);
            nextIntent();
        }

        String cachingFarmersOrganization = getCache("cachingFarmersOrganization");
        if(cachingFarmersOrganization == null) {
            Call<FarmersOrganization> farmersOrganizationCall = apiServiceAgriculture.loadCommunity();
            farmersOrganizationCall.enqueue(new AgricultureCommunityCacheManager(this));
        }else {
            FarmersOrganization farmersOrganization = new Gson().fromJson(cachingFarmersOrganization, FarmersOrganization.class);
            checkCount++;
            TextView textView = new TextView(this);
            textView.setText(checkCount +". กำลังโหลดรายชื่อกิจกรรมการเกษตร มี "+farmersOrganization.getOrganizationsList().size()+" รายการ");
            linearlayoutLoadCache.addView(textView);
            nextIntent();
        }
    }

    @Override
    public void onLoadProvinceAllSuccess(Province province) {

        String dataCache = new Gson().toJson(province);
        saveCache("cachingProvince", dataCache);

        checkCount++;
        TextView textView = new TextView(this);
        textView.setText(checkCount +". กำลังโหลดรายชื่อโหลดจัดหวัด มี "+province.getProvinces().size()+" รายการ");
        linearlayoutLoadCache.addView(textView);

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = Contextor.getInstance().getContext().getSharedPreferences("province", 0);
        editor = settings.edit();
        String data = new Gson().toJson(province);
        editor.putString("province", data);
        editor.apply();

        textView.setText(textView.getText()+" สำเร็จ");
        nextIntent();
    }

    @Override
    public void onLoadDistrictAllSuccess(District districtOnLoad) {

        String dataCache = new Gson().toJson(districtOnLoad);
        saveCache("cachingDistrict", dataCache);

        checkCount++;
        TextView textView = new TextView(this);
        textView.setText(checkCount +". กำลังโหลดรายชื่ออำเภอ มี "+districtOnLoad.getDistricts().size()+" รายการ");
        linearlayoutLoadCache.addView(textView);

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        List<JsonObject> districtList = districtOnLoad.getDistricts();
        for (int index = 0; index < districtList.size(); index++) {
            String provinceCode = districtOnLoad.getDistricts().get(index).get("province_id").getAsString();

            settings = Contextor.getInstance().getContext().getSharedPreferences("district" + provinceCode, 0);
            String dataDistrict = settings.getString("district" + provinceCode, null);
            District district = new Gson().fromJson(dataDistrict, District.class);

            if (district != null) {
                List<JsonObject> districtOldList = district.getDistricts();
                if(districtOldList != null) {
                    districtOldList.add(districtList.get(index));
                    district.setDistricts(districtOldList);
                    settings = Contextor.getInstance().getContext().getSharedPreferences("district" + provinceCode, 0);
                    editor = settings.edit();
                    String data = new Gson().toJson(district);
                    editor.putString("district" + provinceCode, data);
                    editor.apply();
                }
            }else {
                settings = Contextor.getInstance().getContext().getSharedPreferences("district" + provinceCode, 0);
                editor = settings.edit();

                List<JsonObject> jsonObjectListNew = new ArrayList<>();
                jsonObjectListNew.add(districtList.get(index));

                District districtNew = new District();
                districtNew.setDistricts(jsonObjectListNew);
                String data = new Gson().toJson(districtNew);

                editor.putString("district" + provinceCode, data);
                editor.apply();
            }
        }

        textView.setText(textView.getText()+" สำเร็จ");
        nextIntent();
    }

    @Override
    public void onLoadSubDistrictAllSuccess(SubDistrict subDistrictOnLoad) {

        String dataCache = new Gson().toJson(subDistrictOnLoad);
        saveCache("cachingSubDistrict", dataCache);

        checkCount++;
        TextView textView = new TextView(this);
        textView.setText(checkCount +". กำลังโหลดราชื่อตำบล มี "+subDistrictOnLoad.getSubDistricts().size()+" รายการ");
        linearlayoutLoadCache.addView(textView);

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        List<JsonObject> subDistrictsList = subDistrictOnLoad.getSubDistricts();
        for (int index = 0; index < subDistrictsList.size(); index++) {
            String provinceCode = subDistrictOnLoad.getSubDistricts().get(index).get("province_id").getAsString();
            String districtCode = subDistrictOnLoad.getSubDistricts().get(index).get("district_id").getAsString();

            settings = Contextor.getInstance().getContext().getSharedPreferences("sub_district"+provinceCode+districtCode, 0);
            String dataDistrict = settings.getString("sub_district"+provinceCode+districtCode, null);
            SubDistrict subDistrict = new Gson().fromJson(dataDistrict, SubDistrict.class);

            if (subDistrict != null) {
                List<JsonObject> subDistrictOldList = subDistrict.getSubDistricts();
                if(subDistrictOldList != null) {
                    subDistrictOldList.add(subDistrictsList.get(index));
                    subDistrict.setSubDistricts(subDistrictOldList);

                    settings = Contextor.getInstance().getContext().getSharedPreferences("sub_district"+provinceCode+districtCode, 0);
                    editor = settings.edit();
                    String data = new Gson().toJson(subDistrict);
                    editor.putString("sub_district"+provinceCode+districtCode, data);
                    editor.apply();
                }
            }else {
                settings = Contextor.getInstance().getContext().getSharedPreferences("sub_district"+provinceCode+districtCode, 0);
                editor = settings.edit();

                List<JsonObject> jsonObjectListNew = new ArrayList<>();
                jsonObjectListNew.add(subDistrictsList.get(index));

                SubDistrict subDistrictNew = new SubDistrict();
                subDistrictNew.setSubDistricts(jsonObjectListNew);
                String data = new Gson().toJson(subDistrictNew);

                editor.putString("sub_district"+provinceCode+districtCode, data);
                editor.apply();
            }

        }

        textView.setText(textView.getText()+" สำเร็จ");
        nextIntent();
    }

    @Override
    public void onLoadParcelTypeAllSuccess(ParcelType parcelType) {

        String dataCache = new Gson().toJson(parcelType);
        saveCache("cachingParcelType", dataCache);

        checkCount++;
        TextView textView = new TextView(this);
        textView.setText(checkCount +". กำลังโหลดรายชื่อประเภทเอกสาร มี "+parcelType.getParcelType().size()+" รายการ");
        linearlayoutLoadCache.addView(textView);

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = Contextor.getInstance().getContext().getSharedPreferences("parcelType", 0);
        editor = settings.edit();
        String data = new Gson().toJson(parcelType);
        editor.putString("parcelType", data);
        editor.apply();

        textView.setText(textView.getText()+" สำเร็จ");
        nextIntent();
    }

    @Override
    public void onLoadCareerProfileSuccess(ProfileCareer profileCareer) {

        String dataCache = new Gson().toJson(profileCareer);
        saveCache("cachingProfileCareer", dataCache);

        checkCount++;
        TextView textView = new TextView(this);
        textView.setText(checkCount +". กำลังโหลดรายชื่ออาชีพหลักอาชีพรอง มี "+profileCareer.getCareer().size()+" รายการ");
        linearlayoutLoadCache.addView(textView);

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = Contextor.getInstance().getContext().getSharedPreferences("profileCareer", 0);
        editor = settings.edit();
        String data = new Gson().toJson(profileCareer);
        editor.putString("profileCareer", data);
        editor.apply();

        textView.setText(textView.getText()+" สำเร็จ");
        nextIntent();
    }

    @Override
    public void onLoadAgricultureTypeSuccess(AgricultureType agricultureTypeOnLoad) {

        String dataCache = new Gson().toJson(agricultureTypeOnLoad);
        saveCache("cachingAgricultureType", dataCache);

        checkCount++;
        TextView textView = new TextView(this);
        textView.setText(checkCount +". กำลังรายชื่อโหลดประเภทพืชเกษตร มี "+agricultureTypeOnLoad.getListOfAgricultureType().size()+" รายการ");
        linearlayoutLoadCache.addView(textView);

        AgricultureType agricultureType = new AgricultureType();
        agricultureType.setListOfAgricultureType(agricultureTypeOnLoad.getListOfAgricultureType());

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = Contextor.getInstance().getContext().getSharedPreferences("agricultureType", 0);
        editor = settings.edit();
        String data = new Gson().toJson(agricultureType);
        editor.putString("agricultureType", data);
        editor.apply();

        textView.setText(textView.getText()+" สำเร็จ");
        nextIntent();
    }

    @Override
    public void onLoadAgricultureDetailSuccess(AgricultureDetail agricultureDetailOnLoad) {

        String dataCache = new Gson().toJson(agricultureDetailOnLoad);
        saveCache("cachingAgricultureDetail", dataCache);

        checkCount++;
        TextView textView = new TextView(this);
        textView.setText(checkCount +". กำลังโหลดรายชื่อรายละเอียดประเภทพืช มี "+agricultureDetailOnLoad.getDetail().size()+" รายการ");
        linearlayoutLoadCache.addView(textView);

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        List<JsonObject> detailList = agricultureDetailOnLoad.getDetail();
        for (int index = 0; index < detailList.size(); index++) {
            String typeCodeName = agricultureDetailOnLoad.getDetail().get(index).get("type_code").getAsString();

            settings = Contextor.getInstance().getContext().getSharedPreferences("agricultureDetail" + typeCodeName, 0);
            String agricultureDetailString = settings.getString("agricultureDetail" + typeCodeName, null);
            AgricultureDetail agricultureDetail = new Gson().fromJson(agricultureDetailString, AgricultureDetail.class);

            if (agricultureDetail != null) {
                List<JsonObject> agricultureDetailOldList = agricultureDetail.getDetail();

                if (agricultureDetailOldList != null) {

                    agricultureDetailOldList.add(detailList.get(index));
                    agricultureDetail.setDetail(agricultureDetailOldList);

                    settings = Contextor.getInstance().getContext().getSharedPreferences("agricultureDetail" + typeCodeName, 0);
                    editor = settings.edit();
                    String data = new Gson().toJson(agricultureDetail);
                    editor.putString("agricultureDetail" + typeCodeName, data);
                    editor.apply();
                }

            } else {
                settings = Contextor.getInstance().getContext().getSharedPreferences("agricultureDetail" + typeCodeName, 0);
                editor = settings.edit();

                List<JsonObject> jsonObjectListNew = new ArrayList<>();
                jsonObjectListNew.add(detailList.get(index));

                AgricultureDetail agricultureDetailNew = new AgricultureDetail();
                agricultureDetailNew.setDetail(jsonObjectListNew);
                String data = new Gson().toJson(agricultureDetailNew);

                editor.putString("agricultureDetail" + typeCodeName, data);
                editor.apply();
            }

        }

        textView.setText(textView.getText()+" สำเร็จ");
        nextIntent();
    }

    @Override
    public void onLoadAgricultureBreedSuccess(AgricultureBreed agricultureBreedOnLoad) {

        String dataCache = new Gson().toJson(agricultureBreedOnLoad);
        saveCache("cachingAgricultureBreed", dataCache);

        checkCount++;
        TextView textView = new TextView(this);
        textView.setText(checkCount +". กำลังโหลดรายชื่อพันธ์พืชเกษตร มี "+agricultureBreedOnLoad.getAgricultureBreed().size()+" รายการ");
        linearlayoutLoadCache.addView(textView);

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        List<JsonObject> jsonObjectList = agricultureBreedOnLoad.getAgricultureBreed();
        for (int index = 0; index < jsonObjectList.size(); index++) {
            if (agricultureBreedOnLoad.getAgricultureBreed().get(index).get("detail_code") != null) {

                String detailCodeName = agricultureBreedOnLoad.getAgricultureBreed().get(index).get("detail_code").getAsString();

                settings = Contextor.getInstance().getContext().getSharedPreferences("agricultureBreed"+detailCodeName, 0);
                String agricultureDetailString = settings.getString("agricultureBreed"+detailCodeName, null);
                AgricultureBreed agricultureBreed = new Gson().fromJson(agricultureDetailString, AgricultureBreed.class);

                if (agricultureBreed != null) {
                    List<JsonObject> agricultureBreedOldList = agricultureBreed.getAgricultureBreed();

                    if (agricultureBreedOldList != null) {

                        agricultureBreedOldList.add(jsonObjectList.get(index));
                        agricultureBreed.setAgricultureBreed(agricultureBreedOldList);

                        settings = Contextor.getInstance().getContext().getSharedPreferences("agricultureBreed"+detailCodeName, 0);
                        editor = settings.edit();
                        String data = new Gson().toJson(agricultureBreed);
                        editor.putString("agricultureBreed"+detailCodeName, data);
                        editor.apply();
                    }

                } else {
                    settings = Contextor.getInstance().getContext().getSharedPreferences("agricultureBreed"+detailCodeName, 0);
                    editor = settings.edit();

                    List<JsonObject> jsonObjectListNew = new ArrayList<>();
                    jsonObjectListNew.add(jsonObjectList.get(index));

                    AgricultureBreed agricultureBreedNew = new AgricultureBreed();
                    agricultureBreedNew.setAgricultureBreed(jsonObjectListNew);
                    String data = new Gson().toJson(agricultureBreedNew);

                    editor.putString("agricultureBreed"+detailCodeName, data);
                    editor.apply();
                }

            }
        }

        textView.setText(textView.getText()+" สำเร็จ");
        nextIntent();
    }

    @Override
    public void onLoadAgricultureCommunitySuccess(FarmersOrganization farmersOrganization) {

        String dataCache = new Gson().toJson(farmersOrganization);
        saveCache("cachingFarmersOrganization", dataCache);

        checkCount++;
        TextView textView = new TextView(this);
        textView.setText(checkCount +". กำลังโหลดรายชื่อกิจกรรมการเกษตร มี "+farmersOrganization.getOrganizationsList().size()+" รายการ");
        linearlayoutLoadCache.addView(textView);

        PreferencesService.savePreferences("FarmerOrganization", farmersOrganization);

        textView.setText(textView.getText()+" สำเร็จ");
        nextIntent();
    }

    public void nextIntent(){
        if(checkCount == CountAll) {
            Intent intent = new Intent(this, AuthenticationActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onLoadFails(String message) {
        Intent intent = new Intent(this, AuthenticationActivity.class);
        startActivity(intent);
        finish();
    }

    public void saveCache(String key, String data){
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = Contextor.getInstance().getContext().getSharedPreferences("caching", 0);
        editor = settings.edit();
        editor.putString(key, data);
        editor.apply();
    }

    public String getCache(String key){
        SharedPreferences settings;
        settings = Contextor.getInstance().getContext().getSharedPreferences("caching", 0);
        return settings.getString(key, null);
    }
}
