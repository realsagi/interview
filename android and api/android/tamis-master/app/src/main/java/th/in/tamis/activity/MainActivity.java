package th.in.tamis.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import retrofit.Call;
import th.in.tamis.manager.MainBus;
import th.in.tamis.manager.doae.APIDOAEServiceMember;
import th.in.tamis.manager.doae.DOAEManager;
import th.in.tamis.manager.doae.DOAEServiceManager;
import th.in.tamis.manager.doae.onLoadDoae;
import th.in.tamis.manager.person.APIPerson;
import th.in.tamis.manager.person.PersonManager;
import th.in.tamis.manager.person.PersonProfileManager;
import th.in.tamis.manager.person.onLoadPerson;
import th.in.tamis.models.Farmer;
import th.in.tamis.models.MemberDOAE;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.service.authentication.TokenLoginService;
import th.in.tamis.tamis.R;
import th.in.tamis.usb.reader.RegisterUSB;
import th.in.tamis.usbreader.CardReader;
import th.in.tamis.usbreader.OnCardReaderListener;
import th.in.tamis.usbreader.UsbCardReader;
import th.in.tamis.widget.CitizenIdEditText;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        OnCardReaderListener, RegisterUSB, Dialog.OnClickListener, onLoadDoae, onLoadPerson {

    private CardReader usbSmartCard;
    private ProgressDialog progressDialog;
    private Button buttonReadFromUsb;
    private CitizenIdEditText editTextVerifyByCitizenId;
    private EditText editTextSpecifyBuddhistEra;
    private String citizenId;

    private ImageView imageViewErrorSpecifyBuddhistEra;
    private ImageView imageViewErrorVerifyByCitizenId;

    private Farmer farmer = new Farmer();
    private APIPerson servicePerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initInstance();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainBus.getInstance().register(this);
        new TokenLoginService(this).loginByToken();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainBus.getInstance().unregister(this);
    }

    private void initInstance() {
        editTextVerifyByCitizenId = (CitizenIdEditText) findViewById(R.id.editTextVerifyByCitizenId);
        editTextSpecifyBuddhistEra = (EditText) findViewById(R.id.editTextSpecifyBuddhistEra);
        buttonReadFromUsb = (Button) findViewById(R.id.buttonReadFromUsb);
        imageViewErrorSpecifyBuddhistEra = (ImageView) findViewById(R.id.imageViewErrorSpecifyBuddhistEra);
        imageViewErrorVerifyByCitizenId = (ImageView) findViewById(R.id.imageViewErrorVerifyByCitizenId);
        buttonReadFromUsb.setEnabled(false);
        buttonReadFromUsb.setOnClickListener(this);
        Button buttonVerifyByCitizenId = (Button) findViewById(R.id.buttonVerifyByCitizenId);
        buttonVerifyByCitizenId.setOnClickListener(this);
        registerUsb();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonReadFromUsb:
                createDialogPress(R.string.loadingDataFromSmartCartD);
                usbSmartCard.readData();
                break;
            case R.id.buttonVerifyByCitizenId:
                if (validateFields()) {
                    farmer.setCitizenId(citizenId);
                    loadProfileFromAPI();
                }
                break;
        }
    }

    private boolean validateFields() {
        boolean verifyField = true;
        if (!editTextVerifyByCitizenId.getText().toString().equals("") && editTextVerifyByCitizenId.getText().length() == 17) {
            citizenId = editTextVerifyByCitizenId.getText().toString().replace("-", "");
            if (checkSumCitizenId(citizenId)) {

                imageViewErrorVerifyByCitizenId.setVisibility(View.INVISIBLE);
            } else {
                verifyField = false;
                createDialogAlertNoTitle("ท่านกรอกหมายเลขบัตรประชาชนไม่ถูกต้อง");
                imageViewErrorVerifyByCitizenId.setVisibility(View.VISIBLE);
            }
        } else {
            verifyField = false;
            createDialogAlertNoTitle(getString(R.string.allFieldRequired));
            imageViewErrorVerifyByCitizenId.setVisibility(View.VISIBLE);
        }

        if (!editTextSpecifyBuddhistEra.getText().toString().equals("") && editTextSpecifyBuddhistEra.getText().length() == 4) {
            imageViewErrorSpecifyBuddhistEra.setVisibility(View.INVISIBLE);
        } else {
            verifyField = false;
            imageViewErrorSpecifyBuddhistEra.setVisibility(View.VISIBLE);
        }
        return verifyField;
    }

    private boolean checkSumCitizenId(String citizenId) {
        if (citizenId.length() != 13)
            return false;

        int sum = 0;

        for (int i = 0; i < 12; i++) {
            sum += Integer.parseInt(String.valueOf(citizenId.charAt(i))) * (13 - i);
        }

        return citizenId.charAt(12) - '0' == ((11 - (sum % 11)) % 10);
    }

    private void loadProfileFromAPI() {
        createDialogProgress();
        servicePerson = PersonManager.getPerson();
        Call<Farmer> call = servicePerson.getProfile(farmer.getCitizenId());
        call.enqueue(new PersonProfileManager(this));
    }

    private void createDialogProgress() {
        progressDialog = new ProgressDialog(this, R.style.ProgressDialogCustom);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getResources().getString(R.string.loadingDataFromDOAE));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setProgress(0);
        progressDialog.show();
    }

    @Subscribe
    public void onLoadProfileSuccess(Farmer profile) {
        progressDialog.dismiss();
        String birthYear = profile.getBirthDate().substring(0, 4);
        if (editTextSpecifyBuddhistEra.getText().toString().equals(birthYear)) {
            PreferencesService.savePreferences("farmerFromVerifyByCitizenId", profile);
            checkMemberDoae(citizenId);
        } else {
            createDialogAlertNoTitle(getString(R.string.cannotFindOutProfile));
        }
    }

    private void createDialogAlertNoTitle(String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert_no_title);

        TextView textViewMessage = (TextView) dialogAlert.findViewById(R.id.textViewMessage);
        textViewMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonOK = (Button) dialogAlert.findViewById(R.id.buttonOK);
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onError() {
        createDialogAlert(getString(R.string.titleUSBConnectFails),
                getString(R.string.messageUSBConnectFails));
        progressDialog.dismiss();
        registerUsb();
    }

    @Override
    public void onReadDataSuccess(Farmer farmer) {
        progressDialog.dismiss();
        PreferencesService.savePreferences("farmer", farmer);
        checkMemberDoae(farmer.getCitizenId());
    }

    private void checkMemberDoae(String citizenId) {
        createDialogPress(R.string.loadDataFromDOAE);
        APIDOAEServiceMember serviceMember = DOAEServiceManager.getDOAEService();
        Call<MemberDOAE> member = serviceMember.loadMember(citizenId);
        member.enqueue(new DOAEManager(this));
    }

    @Override
    public void timeout(String timeout) {
        progressDialog.dismiss();
        registerUsb();

        createDialogAlert(getString(R.string.errorTimeout),
                getString(R.string.cannotConnectServer).concat(timeout));
    }

    @Subscribe
    public void onCheckedMemberDOAE(MemberDOAE memberDOAE) {
        progressDialog.dismiss();
        if (memberDOAE.getCitizenId() != null) {
            createDialogAlert(getString(R.string.statusMemberOfDoae),
                    getString(R.string.youIsMember));
        } else {
            registerUsb();
            Intent intentRegisterFarmer = new Intent(this, RegisterFarmerActivity.class);
            startActivity(intentRegisterFarmer);
        }
    }

    private void createDialogAlert(String title, String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert);

        TextView textViewDialogAlertTitle = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertTitle);
        TextView textViewDialogAlertMessage = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertMessage);

        textViewDialogAlertTitle.setText(title);
        textViewDialogAlertMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonDialogAlertOK = (Button) dialogAlert.findViewById(R.id.buttonDialogAlertOK);
        buttonDialogAlertOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }

    private void registerUsb() {
        usbSmartCard = new UsbCardReader();
        usbSmartCard.registerUSB(this);
    }

    @Override
    public void onRegisterUSBSuccess() {
        buttonReadFromUsb.setEnabled(true);
    }

    @Override
    public void onRegisterUSBFails() {
        buttonReadFromUsb.setEnabled(false);
    }

    private void createDialogPress(int messageId) {
        progressDialog = new ProgressDialog(this, R.style.ProgressDialogCustom);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getResources().getString(messageId));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setProgress(0);
        progressDialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }
}