package th.in.tamis.manager.career;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.Contextor;
import th.in.tamis.manager.MainBus;
import th.in.tamis.models.ProfileCareer;

public class CareerProfileManager implements Callback<ProfileCareer> {

    private OnLoadCareer onLoadCareer;

    public CareerProfileManager(OnLoadCareer listener){
        this.onLoadCareer = listener;
    }

    @Override
    public void onResponse(Response<ProfileCareer> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            onLoadCareer.onLoadCareerFails("error 404");
        }
        if(response.isSuccess()){

            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = Contextor.getInstance().getContext().getSharedPreferences("profileCareer", 0);
            editor = settings.edit();
            String data = new Gson().toJson(response.body());
            editor.putString("profileCareer", data);
            editor.apply();

            MainBus.getInstance().post(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
//        if(t.toString().contains("ConnectException")) {
//            onLoadCareer.onLoadCareerFails("Network is unreachable");
//        }
//        else if(t.toString().contains("SocketTimeoutException")){
//            onLoadCareer.onLoadCareerFails("timeout");
//        }
    }
}
