package th.in.tamis.manager.parcel;

public interface OnLoadParcel {
    void onLoadParcelFails(String message);
}
