package th.in.tamis.activity;

import android.content.Context;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;

import th.in.tamis.service.authentication.TokenLoginService;
import th.in.tamis.tamis.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignAuthenticationActivity extends AppCompatActivity implements View.OnClickListener{

    private GestureOverlayView gestureViewSignature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_authentication);
        initInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new TokenLoginService(this).loginByToken();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initInstance() {
        gestureViewSignature = (GestureOverlayView) findViewById(R.id.gestureViewSignature);

        Button buttonDelete = (Button) findViewById(R.id.buttonDelete);
        buttonDelete.setOnClickListener(this);

        Button buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSave:
                gestureViewSignature.setDrawingCacheEnabled(true);
                Bitmap bitmap = Bitmap.createBitmap(gestureViewSignature.getDrawingCache());
                gestureViewSignature.setDrawingCacheEnabled(false);

                ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
                
                Intent intent = new Intent(SignAuthenticationActivity.this, ConfirmRegisterFarmerActivity.class);
                intent.putExtra("bitmapImage", byteStream.toByteArray());
                startActivity(intent);
                break;
            case R.id.buttonDelete:
                gestureViewSignature.cancelClearAnimation();
                gestureViewSignature.clear(true);
                break;
        }
    }
}
