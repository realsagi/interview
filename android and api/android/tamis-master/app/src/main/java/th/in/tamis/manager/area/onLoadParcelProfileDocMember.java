package th.in.tamis.manager.area;

import th.in.tamis.models.ParcelDoc;

public interface onLoadParcelProfileDocMember {
    void onLoadParcelProfileDocMemberSuccess(boolean isMember, ParcelDoc parcelDoc);
    void onLoadParcelProfileDocMemberFails(String message);
}
