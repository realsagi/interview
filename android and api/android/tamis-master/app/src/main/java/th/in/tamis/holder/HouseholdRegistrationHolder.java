package th.in.tamis.holder;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import retrofit.Call;
import th.in.tamis.activity.DetailHouseholdActivity;
import th.in.tamis.manager.agriculture.APIServiceAgriculture;
import th.in.tamis.manager.agriculture.AgricultureCommunityManger;
import th.in.tamis.manager.agriculture.AgricultureManager;
import th.in.tamis.models.FarmersOrganization;
import th.in.tamis.models.HouseHoldMember;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.tamis.R;

public class HouseholdRegistrationHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
        APIServiceAgriculture.AgricultureListener {

    private final HouseHoldMember houseHoldMember;
    private final Context context;
    private final TextView firstName;
    private final TextView sex;
    private final TextView citizenID;
    private final TextView helpWork;

    private LinearLayout linearLayoutForShowOrganization;

    public HouseholdRegistrationHolder(View itemView) {
        super(itemView);
        this.context = itemView.getContext();

        houseHoldMember = (HouseHoldMember) PreferencesService.getPreferences("householdMembers", HouseHoldMember.class);

        firstName = (TextView) itemView.findViewById(R.id.textViewPersonName);
        sex = (TextView) itemView.findViewById(R.id.textViewSex);
        citizenID = (TextView) itemView.findViewById(R.id.textViewCitizenId);
        helpWork = (TextView) itemView.findViewById(R.id.textViewHelpWork);

        linearLayoutForShowOrganization = (LinearLayout) itemView.findViewById(R.id.linearLayoutForShowOrganization);

        ImageButton btnToAddDetail = (ImageButton) itemView.findViewById(R.id.buttonAddDetailHousehold);
        btnToAddDetail.setOnClickListener(this);
    }

    public HouseHoldMember getItemPerson() {
        return houseHoldMember;
    }

    public void setFirstName(String firstName) {
        this.firstName.setText(firstName);
    }

    public void setCitizenID(String citizenID) {
        this.citizenID.setText(String.format("%s-%s-%s-%s-%s",
                citizenID.substring(0, 1),
                citizenID.substring(1, 5),
                citizenID.substring(5, 10),
                citizenID.substring(10, 12),
                citizenID.substring(12, 13)));
    }

    public void setSex(String sex) {
        this.sex.setText(sex);
    }

    public void setHelpWork(boolean helpWork) {
        if (helpWork) {
            this.helpWork.setText(R.string.helpFarmer);
            this.helpWork.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
        } else {
            this.helpWork.setText(R.string.noHelpFarmer);
            this.helpWork.setTextColor(ContextCompat.getColor(context, R.color.colorRed));
        }
    }

    public void setDetailStatusOrganization(String detailStatusOrganization) {
        List<String> str = Arrays.asList(detailStatusOrganization.split("\n"));
        linearLayoutForShowOrganization.removeAllViews();
        for (int index = 0; index < str.size(); index++) {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/THSarabunNew.ttf");
            CheckBox checkBox = new CheckBox(context);
            checkBox.setId(index);
            checkBox.setTypeface(typeface);
            checkBox.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGray));
            checkBox.setPadding(0, 0, 0, 0);
            checkBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimensionPixelSize(R.dimen.font_size_input));
            checkBox.setFocusable(false);

            if (!detailStatusOrganization.equals("ยังไม่เป็นสมาชิกองค์กรใดๆ")) {
                checkBox.setButtonDrawable(R.drawable.green_dot_icons_360);
                checkBox.setText(str.get(index));
            } else {
                checkBox.setButtonDrawable(R.drawable.ic_alert_red_48);
                checkBox.setText("ยังไม่เป็นสมาชิกองค์กรใดๆ");
                linearLayoutForShowOrganization.addView(checkBox);
                break;
            }
            linearLayoutForShowOrganization.addView(checkBox);
        }
    }

    @Override
    public void onClick(View v) {
        FarmersOrganization farmersOrganization = (FarmersOrganization)
                PreferencesService.getPreferences("FarmerOrganization", FarmersOrganization.class);
        if (farmersOrganization == null) {
            loadOrganizations();
        } else {
            gotoAddDetailHoseHoldActivity();
        }
    }

    private void loadOrganizations() {
        Call<FarmersOrganization> farmersOrganizationCall = AgricultureManager.getServiceAgriculture().loadCommunity();
        farmersOrganizationCall.enqueue(new AgricultureCommunityManger(this));
    }

    @Override
    public void onSuccess(FarmersOrganization farmersOrganization) {
        PreferencesService.savePreferences("FarmerOrganization", farmersOrganization);
        gotoAddDetailHoseHoldActivity();
    }

    @Override
    public void onFails(String message) {
        createDialogAlert(context.getString(R.string.errorTimeout),
                context.getString(R.string.cannotConnectServer).concat(message));
    }

    public void gotoAddDetailHoseHoldActivity() {
        if (houseHoldMember.getPerson().get(getAdapterPosition()) == null) {
            houseHoldMember.getPerson().get(getAdapterPosition()).setFarmersOrganization(new FarmersOrganization());
        }
        PreferencesService.saveIntPreferences("positionFarmerItem", getAdapterPosition());
        Intent intentAddDetailHouseholdActivity = new Intent(context, DetailHouseholdActivity.class);
        context.startActivity(intentAddDetailHouseholdActivity);
    }

    private void createDialogAlert(String title, String message) {
        final Dialog dialogAlert = new Dialog(context);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert);

        TextView textViewDialogAlertTitle = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertTitle);
        TextView textViewDialogAlertMessage = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertMessage);

        textViewDialogAlertTitle.setText(title);
        textViewDialogAlertMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int)(context.getResources().getDisplayMetrics().widthPixels*0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonDialogAlertOK = (Button) dialogAlert.findViewById(R.id.buttonDialogAlertOK);
        buttonDialogAlertOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }
}

