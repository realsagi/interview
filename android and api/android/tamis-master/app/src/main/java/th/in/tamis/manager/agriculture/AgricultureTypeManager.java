package th.in.tamis.manager.agriculture;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.Contextor;
import th.in.tamis.models.AgricultureType;
import th.in.tamis.manager.MainBus;

public class AgricultureTypeManager implements Callback<AgricultureType> {

    private APIServiceAgriculture.AgricultureListener agricultureListener;

    public AgricultureTypeManager(APIServiceAgriculture.AgricultureListener listener){
        agricultureListener = listener;
    }

    @Override
    public void onResponse(Response<AgricultureType> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            agricultureListener.onFails("error 404");
        }
        if (response.isSuccess()) {
            AgricultureType agricultureType = new AgricultureType();
            agricultureType.setListOfAgricultureType(response.body().getListOfAgricultureType());

            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = Contextor.getInstance().getContext().getSharedPreferences("agricultureType", 0);
            editor = settings.edit();
            String data = new Gson().toJson(agricultureType);
            editor.putString("agricultureType", data);
            editor.apply();

            MainBus.getInstance().post(agricultureType);
        }
    }

    @Override
    public void onFailure(Throwable t) {
//        if(t.toString().contains("ConnectException")) {
//            agricultureListener.onFails("Network is unreachable");
//        }
//        else if(t.toString().contains("SocketTimeoutException")){
//            agricultureListener.onFails("timeout");
//        }
    }
}
