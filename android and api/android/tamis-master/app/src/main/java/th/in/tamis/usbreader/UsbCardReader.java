package th.in.tamis.usbreader;

import android.content.Context;


public class UsbCardReader implements CardReader {

    private  ReadDataTask readdatatask;

    @Override
    public void registerUSB(Context context) {
        readdatatask = new ReadDataTask(context);
    }

    @Override
    public void readData() {
        readdatatask.execute();
    }
}
