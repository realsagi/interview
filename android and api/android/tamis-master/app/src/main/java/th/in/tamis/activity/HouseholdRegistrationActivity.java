package th.in.tamis.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import th.in.tamis.adapter.HouseholdRegistrationAdapter;
import th.in.tamis.models.Farmer;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.service.authentication.TokenLoginService;
import th.in.tamis.tamis.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HouseholdRegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView addressID;
    private RecyclerView recyclerViewHouseholdMember;
    private LinearLayoutManager linearLayoutManager;
    private HouseholdRegistrationAdapter householdRegistrationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_household_registration);
        initInstance();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initInstance() {
        Button bntSaveDataFarmer = (Button) findViewById(R.id.btnSaveDataFarmer);
        bntSaveDataFarmer.setOnClickListener(this);
        Button btnRegistration = (Button) findViewById(R.id.btnRegistration);
        btnRegistration.setOnClickListener(this);
        addressID = (TextView) findViewById(R.id.etAddressID);

        recyclerViewHouseholdMember = (RecyclerView) findViewById(R.id.rvHouseholdMemberRecyclerView);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        householdRegistrationAdapter = new HouseholdRegistrationAdapter();

        setBoldText();
    }

    private void setBoldText() {
        Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/THSarabunNewBold.ttf");
        TextView textViewDataFromDOPA = (TextView) findViewById(R.id.textViewDataFromDOPA);
        textViewDataFromDOPA.setTypeface(typeface);

        TextView textViewNumberOfHousehold = (TextView) findViewById(R.id.textViewNumberOfHousehold);
        textViewNumberOfHousehold.setTypeface(typeface);

        TextView textViewHouseholdMember = (TextView) findViewById(R.id.textViewHouseholdMember);
        textViewHouseholdMember.setTypeface(typeface);
    }

    @Override
    protected void onResume(){
        super.onResume();
        Farmer farmer = (Farmer) PreferencesService.getPreferences("farmer", Farmer.class);
        addressID.setText(String.format("%s-%s-%s",
                farmer.getAddress().getAddressID().substring(0, 4),
                farmer.getAddress().getAddressID().substring(4, 10),
                farmer.getAddress().getAddressID().substring(10, 11)));

        int positionFarmerItem = PreferencesService.getIntPreferences("positionFarmerItem");

        recyclerViewHouseholdMember.setLayoutManager(linearLayoutManager);
        recyclerViewHouseholdMember.scrollToPosition(positionFarmerItem);
        recyclerViewHouseholdMember.setAdapter(householdRegistrationAdapter);
        new TokenLoginService(this).loginByToken();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRegistration:
                finish();
                break;
            case R.id.btnSaveDataFarmer:
                Intent intent = new Intent(HouseholdRegistrationActivity.this, RegisterPlantationActivity.class);
                startActivity(intent);
                break;
        }
    }
}
