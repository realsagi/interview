package th.in.tamis.widget;

public interface Identity {

    boolean isValidFormat();
    boolean validate();
    String prettyPrint();
    String getId();
}
