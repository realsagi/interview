package th.in.tamis.manager.authentication;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LogoutManager implements Callback<Boolean> {

    @Override
    public void onResponse(Response<Boolean> response, Retrofit retrofit) {
        if(response.raw().code() == 404){

        }
        if(response.isSuccess()){

        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {

        }
        else if(t.toString().contains("SocketTimeoutException")){

        }

    }
}
