package th.in.tamis.manager.career;

import th.in.tamis.manager.http.HTTPManager;

public class CareerManager {
    public static APIServiceCareer getCareer(){
        return HTTPManager.getInstance().getRetrofit().create(APIServiceCareer.class);
    }
}
