package th.in.tamis.manager.person;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;
import th.in.tamis.models.Farmer;
import th.in.tamis.models.HouseHoldMember;

public interface APIPerson {

    @GET("/personal_profile")
    Call<Farmer> getProfile(@Query("citizen_id") String citzenId);

    @GET("/household_members")
    Call<HouseHoldMember> loadHouseHoldMember(@Query("citizen_id") String citzenId);
}
