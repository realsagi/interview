package th.in.tamis.manager.parcel;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;
import th.in.tamis.models.ParcelDoc;
import th.in.tamis.models.ParcelType;

public interface APIServiceParcel {
    @GET("/parcel")
    Call<ParcelDoc> getParcel(@Query("province_code") String provinceCode,
                              @Query("district_code") String districtCode,
                              @Query("type") String type,
                              @Query("parcel_no") String ns4No);
    @GET("/parcel_type")
    Call<ParcelType> getParcelType();
}
