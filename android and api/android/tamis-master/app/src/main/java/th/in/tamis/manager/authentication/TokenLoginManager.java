package th.in.tamis.manager.authentication;


import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class TokenLoginManager implements Callback<String> {

    private OnAuthentication authentication;

    public TokenLoginManager(OnAuthentication authentication){
        this.authentication = authentication;
    }

    @Override
    public void onResponse(Response<String> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            authentication.connectServerLoginFails("error 404");
        }
        if(response.isSuccess()){
            if(response.body() == null){
                authentication.connectServerLoginFails("");
            }else {
                authentication.connectServerLoginSuccess(response.body());
            }
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            authentication.connectServerLoginFails("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            authentication.connectServerLoginFails("timeout");
        }
    }
}
