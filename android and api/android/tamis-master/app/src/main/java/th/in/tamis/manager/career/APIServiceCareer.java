package th.in.tamis.manager.career;

import retrofit.Call;
import retrofit.http.GET;
import th.in.tamis.models.ProfileCareer;

public interface APIServiceCareer {

    @GET("/career")
    Call<ProfileCareer> getCareer();
}
