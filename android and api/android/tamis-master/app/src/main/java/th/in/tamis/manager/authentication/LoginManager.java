package th.in.tamis.manager.authentication;


import android.util.Log;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginManager implements Callback<String> {

    private OnAuthentication authentication;

    public LoginManager(OnAuthentication authentication){
        this.authentication = authentication;
    }

    @Override
    public void onResponse(Response<String> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            authentication.connectServerLoginFails("error 404");
        }
        if(response.isSuccess()){
            if("false".equals(response.body())){
                authentication.connectServerLoginFails("มีการใช้งานชื่อผู้ใช้งานนี้ที่อุปกรณ์อื่นแล้ว");
            }else {
                authentication.connectServerLoginSuccess(response.body());
            }
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("Network is unreachable")) {
            authentication.connectServerLoginFails("โปรดตรวจสอบการเชื่อมต่ออินเทอร์เน็ต");
        }
        else if(t.toString().contains("Connection refused")){
            authentication.connectServerLoginFails("โปรดตรวจสอบการทำงานเครื่องคอมพิวเตอร์แม่ข่าย");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            authentication.connectServerLoginFails("หมดเวลา กรุณาเชื่อมต่อใหม่");
        }

    }
}
