package th.in.tamis.manager.doae;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;
import th.in.tamis.models.MemberDOAE;

public interface APIDOAEServiceMember {

    @GET("/member")
    Call<MemberDOAE> loadMember(@Query("citizen_id")String citizenId);

    @GET("/member/household_id")
    Call<MemberDOAE> checkHouseHoldMember(@Query("citizen_id")String citizenId);
}
