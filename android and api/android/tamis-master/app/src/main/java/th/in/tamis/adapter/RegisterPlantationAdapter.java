package th.in.tamis.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import th.in.tamis.models.Address;
import th.in.tamis.models.DetailRegisterPlantation;
import th.in.tamis.models.RegisterPlantation;
import th.in.tamis.service.AddressFormat;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.tamis.R;
import th.in.tamis.holder.RegisterPlantationHolder;

public class RegisterPlantationAdapter extends RecyclerView.Adapter<RegisterPlantationHolder> {
    private static View itemView;
    @Override
    public RegisterPlantationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        try {
            itemView = layoutInflater.inflate(R.layout.card_view_register_plantation, parent, false);
        }catch (InflateException exceptions){

        }
        FragmentManager fragmentManager = ((AppCompatActivity) parent.getContext()).getSupportFragmentManager();
        return new RegisterPlantationHolder(itemView, fragmentManager);
    }

    @Override
    public void onBindViewHolder(RegisterPlantationHolder holder, int position) {

        RegisterPlantation registerPlantation = (RegisterPlantation) PreferencesService
                .getPreferences("registerPlantation", RegisterPlantation.class);

        List<DetailRegisterPlantation> listRegisterPlantation = registerPlantation.getDetailRegisterPlantation();

        holder.setTvNumber(position + 1 + ".");

        Address address = listRegisterPlantation.get(position).getAddress();

        holder.setFullAddress(AddressFormat.setAddressFormat(address).getFormat());

        holder.setTvDeed(listRegisterPlantation.get(position).getLicenseNumber());
        holder.setTvRai(String.valueOf(listRegisterPlantation.get(position).getRai()));
        holder.setTvNgan(String.valueOf(listRegisterPlantation.get(position).getNgan()));
        holder.setTvSquareWah(String.valueOf(listRegisterPlantation.get(position).getSquareWah()));
        if (listRegisterPlantation.get(position).getFileSnapShotMap() != null) {
            holder.setImageViewCapturePlantationMap(listRegisterPlantation.get(position).getFileSnapShotMap());
        }
        showLatLng(holder, position, listRegisterPlantation);
        showMap(holder, position, listRegisterPlantation);
    }

    private void showMap(RegisterPlantationHolder holder, int position, List<DetailRegisterPlantation> listRegisterPlantation) {
        holder.showMapOnCardView(false);
        holder.setStatusDrawingMap(false);
        if (listRegisterPlantation.get(position).getCoordinates() != null) {
            holder.setStatusDrawingMap(true);
            holder.setMainCoordinates(listRegisterPlantation.get(position).getCoordinates());
            holder.setSubCoordinates(listRegisterPlantation.get(position).getDetailAgriculturalOperationses());
            holder.showMapOnCardView(true);
        }
    }

    private void showLatLng(RegisterPlantationHolder holder, int position, List<DetailRegisterPlantation> listRegisterPlantation) {
        if (listRegisterPlantation.get(position).getCoordinatesCenter() != null) {
            holder.setEditTextLatitude(verifyDoubleNullToStringBlank(listRegisterPlantation.get(position).getCoordinatesCenter().getLatitude()));
            holder.setEditTextLongitude(verifyDoubleNullToStringBlank(listRegisterPlantation.get(position).getCoordinatesCenter().getLongitude()));
        }
    }

    private String verifyDoubleNullToStringBlank(Double param) {
        if (param == null || param == 0) {
            return "-";
        }
        return String.valueOf(param);
    }

    @Override
    public int getItemCount() {
        RegisterPlantation registerPlantation = (RegisterPlantation) PreferencesService
                .getPreferences("registerPlantation", RegisterPlantation.class);
        if (registerPlantation != null) {
            List<DetailRegisterPlantation> listRegisterPlantation = registerPlantation.getDetailRegisterPlantation();
            if (listRegisterPlantation != null)
                return listRegisterPlantation.size();
        }
        return 0;
    }
}
