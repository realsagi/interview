package th.in.tamis.manager.person;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.MainBus;
import th.in.tamis.models.Farmer;

public class PersonProfileManager implements Callback<Farmer> {

    private onLoadPerson onLoadPerson;

    public PersonProfileManager(onLoadPerson listener){
        this.onLoadPerson = listener;
    }

    @Override
    public void onResponse(Response<Farmer> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            onLoadPerson.timeout("error 404");
        }

        if (response.isSuccess()) {
            Farmer farmer = response.body();
            if (farmer.equals("{}")) {
                farmer = new Farmer();
            }
            MainBus.getInstance().post(farmer);
        }

        if(response.raw().code() == 500){
            onLoadPerson.timeout("error 500");
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadPerson.timeout("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadPerson.timeout("timeout");
        }
    }
}