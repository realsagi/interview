package th.in.tamis.service;

import java.text.DecimalFormat;

public class Area {
    public static final int SQUARE_METER_PER_RAI = 1600;
    public static final int SQUARE_METER_PER_NGAN = 400;
    public static final float SQUARE_METER_PER_SQUARE_WA = 4;
    public static final String RAI = "ไร่";
    public static final String NGAN = "งาน";
    public static final String SQUARE_WA = "ตารางวา";
    private int rai = 0;
    private int ngan = 0;
    private int squareWah = 0;
    private int sizeSquareMeter;

    public Area(int size) {
        this.sizeSquareMeter = size;

        extractToRaiNganSquareWa();
        roundRaiNganSqaureWa();
    }

    public static Area fromSquareMeter(int sqaureMeter) {
        return new Area(sqaureMeter);
    }

    public String prettyPrint() {
        return new PrettyPrinter().print();
    }

    private void extractToRaiNganSquareWa() {
        rai = squareMeterToRai(sizeSquareMeter);
        ngan = squareMeterToNgan(sizeSquareMeter);
        squareWah = squareMeterToSquareWa(sizeSquareMeter);
    }

    public static int squareMeterToRai(int squareMeter) {
        return squareMeter / SQUARE_METER_PER_RAI;
    }

    public static int squareMeterToNgan(int squareMeter) {
        return (squareMeter % SQUARE_METER_PER_RAI) / SQUARE_METER_PER_NGAN;
    }

    public static int squareMeterToSquareWa(int squareMeter) {
        float squareWa = (squareMeter % SQUARE_METER_PER_NGAN) / SQUARE_METER_PER_SQUARE_WA;
        return Math.round(squareWa);
    }

    public static Area fromRaiNganSqaureWa(int rai, int ngan, int squareWa) {
        return new Area(RaiToSqMeter(rai, ngan, squareWa));
    }

    public static int RaiToSqMeter(int rai, int ngan, int tarangwa) {
        float sqMeter = (rai * SQUARE_METER_PER_RAI) + (ngan * SQUARE_METER_PER_NGAN) + (tarangwa * SQUARE_METER_PER_SQUARE_WA);
        return Math.round(sqMeter);
    }

    private void roundRaiNganSqaureWa() {
        if (squareWah == 100) {
            ngan++;
            squareWah = 0;
        }
        if (ngan == 4) {
            rai++;
            ngan = 0;
        }
    }


    public void setRai(int rai) {
        this.rai = rai;
        processArea();
    }

    public void setNgan(int ngan) {
        this.ngan = ngan;
        processArea();
    }

    public void setSquareWah(int squareWah) {
        this.squareWah = squareWah;
        processArea();
    }

    public int getRai() {
        return rai;
    }

    public int getNgan() {
        return ngan;
    }

    public int getSquareWah() {
        return squareWah;
    }

    private void processArea() {
        if (this.squareWah >= 100) {
            this.ngan = (int) (this.ngan + this.squareWah / 100);
            this.squareWah = this.squareWah % 100;
        }
        if (this.ngan >= 4) {
            this.rai = this.rai + this.ngan / 4;
            this.ngan = this.ngan % 4;
        }
    }

    public String convertSquareMeterToRaiNganWah(double squareMeter) {
        setSquareWah((int) squareMeter/ 4);
        return getDescription();
    }

    public int getSquareMeter() {
        return (getRai() * 1600) + (getNgan() * 400) + (getSquareWah() * 4);
    }

    public String getDescription() {
        return getRai() + " ไร่ " + getNgan() + " งาน " + new DecimalFormat("##.##").format(getSquareWah()) + " ตารางวา";
    }

    private class PrettyPrinter {

        public static final String SPACE = " ";
        private StringBuilder stringBuilder;

        public String print() {
            stringBuilder = new StringBuilder();
            appendRai();
            appendNgan();
            appendSquareWa();
            return stringBuilder.toString().trim();
        }

        private void appendSquareWa() {
            if (squareWah > 0) {
                stringBuilder.append(SPACE).append(squareWah).append(SPACE).append(SQUARE_WA);
            }
        }

        private void appendNgan() {
            if (ngan > 0) {
                stringBuilder.append(SPACE).append(ngan).append(SPACE).append(NGAN);
            }
        }

        private void appendRai() {
            if (rai > 0) {
                stringBuilder.append(rai).append(SPACE).append(RAI);
            }
        }
    }
}
