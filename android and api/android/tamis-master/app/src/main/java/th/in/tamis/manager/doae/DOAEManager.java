package th.in.tamis.manager.doae;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import th.in.tamis.manager.MainBus;
import th.in.tamis.models.MemberDOAE;

public class DOAEManager implements Callback<MemberDOAE>{

    private onLoadDoae onLoadDoae;

    public DOAEManager(onLoadDoae onLoadDoae){
        this.onLoadDoae = onLoadDoae;
    }

    @Override
    public void onResponse(Response<MemberDOAE> response, Retrofit retrofit) {
        if(response.raw().code() == 404){
            onLoadDoae.timeout("error 404");
        }
        else if(response.isSuccess()){
            MainBus.getInstance().post(response.body());
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(t.toString().contains("ConnectException")) {
            onLoadDoae.timeout("Network is unreachable");
        }
        else if(t.toString().contains("SocketTimeoutException")){
            onLoadDoae.timeout("timeout");
        }
    }
}
