package th.in.tamis.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Call;
import th.in.tamis.manager.MainBus;
import th.in.tamis.manager.area.APIArea;
import th.in.tamis.manager.area.AreaMangager;
import th.in.tamis.manager.area.LocationCode;
import th.in.tamis.manager.area.PostCode;
import th.in.tamis.manager.career.APIServiceCareer;
import th.in.tamis.manager.career.CareerManager;
import th.in.tamis.manager.career.CareerProfileManager;
import th.in.tamis.manager.career.OnLoadCareer;
import th.in.tamis.manager.doae.APIDOAEServiceMember;
import th.in.tamis.manager.doae.CheckHouseHoldMember;
import th.in.tamis.manager.doae.DOAEServiceManager;
import th.in.tamis.manager.doae.onLoadHouseHoldMember;
import th.in.tamis.manager.person.APIPerson;
import th.in.tamis.manager.person.HouseHoldMemberManager;
import th.in.tamis.manager.person.PersonManager;
import th.in.tamis.manager.person.PersonProfileManager;
import th.in.tamis.manager.person.onLoadPerson;
import th.in.tamis.models.Address;
import th.in.tamis.models.Farmer;
import th.in.tamis.models.HouseHoldMember;
import th.in.tamis.models.MemberDOAE;
import th.in.tamis.models.ProfileCareer;
import th.in.tamis.service.DateFormatThai;
import th.in.tamis.service.PreferencesService;
import th.in.tamis.service.StringWithTag;
import th.in.tamis.service.authentication.TokenLoginService;
import th.in.tamis.service.file.FileManagement;
import th.in.tamis.service.image.Rotate;
import th.in.tamis.service.image.ScaledBitmap;
import th.in.tamis.tamis.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterFarmerActivity extends AppCompatActivity implements View.OnClickListener,
        APIArea.AreaOnLoadData, DialogInterface.OnClickListener, onLoadPerson, onLoadHouseHoldMember,
        OnLoadCareer, AdapterView.OnItemSelectedListener {

    private Farmer farmer;
    private EditText editTextMobilePhone;
    private EditText editTextHomePhone;
    private EditText editTextZipCode;
    private EditText editTextPrefix;
    private ProgressDialog progressDialog;
    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextCitizenID;
    private EditText editTextDayBirthday;
    private EditText editTextMonthBirthday;
    private EditText editTextYearBirthday;
    private EditText editTextAddressNo;
    private EditText editTextProvince;
    private EditText editTextSubDistrict;
    private EditText editTextDistrict;
    private EditText editTextMoo;
    private EditText editTextAlley;
    private EditText editTextSoi;
    private EditText editTextRoad;
    private Button buttonGotoHouseholdRegistration;
    private ImageView imagePerson;
    private boolean updateProfileSuccess = false;
    private boolean manualRegister = false;
    private APIPerson servicePerson;
    private APIArea areaService;
    private int requestCamera = 0;
    private Spinner spinnerCareerMain;
    private Spinner spinnerCareerMinor;
    private Spinner spinnerStatus;

    private ArrayList<HashMap<String, String>> feedListUpdateProfile = new ArrayList<HashMap<String, String>>();

    private LinearLayout layoutZipCode;

    private LinearLayout layoutAfterValidate;
    private View layoutBorder;

    private ImageView imageViewErrorStatus;
    private ImageView imageViewErrorCareerMain;
    private ImageView imageViewErrorZipCode;

    private String careerMainCode;
    private String careerMinorCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_farmer);
        findId();

        servicePerson = PersonManager.getPerson();

        loadCareer();
        farmer = (Farmer) PreferencesService.getPreferences("farmer", Farmer.class);
        if (farmer != null) {
            setTextOnViewGroup(farmer);
        }

        Farmer farmerFromVerifyByCitizenId = (Farmer) PreferencesService.getPreferences("farmerFromVerifyByCitizenId", Farmer.class);
        if (farmerFromVerifyByCitizenId != null) {
            farmer = farmerFromVerifyByCitizenId;
            setTextOnViewGroup(farmer);
            layoutAfterValidate.setVisibility(View.VISIBLE);
            layoutBorder.setVisibility(View.VISIBLE);
            layoutZipCode.setVisibility(View.VISIBLE);
            PreferencesService.savePreferences("farmer", farmer);
            createDialogProgress();
            callHouseHoldMember();
            updateProfileSuccess = true;
            manualRegister = true;
            buttonGotoHouseholdRegistration.setText(R.string.btnHouseholdRegistrationPage);
        }

        PreferencesService.savePreferences("positionFarmerItem", 0);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void findId() {
        imagePerson = (ImageView) findViewById(R.id.imagePerson);
        editTextPrefix = (EditText) findViewById(R.id.editTextPrefix);
        editTextFirstName = (EditText) findViewById(R.id.editTextFirstName);
        editTextLastName = (EditText) findViewById(R.id.editTextLastName);
        editTextCitizenID = (EditText) findViewById(R.id.editTextCitizenID);
        editTextDayBirthday = (EditText) findViewById(R.id.editTextDayBirthday);
        editTextMonthBirthday = (EditText) findViewById(R.id.editTextMonthBirthday);
        editTextYearBirthday = (EditText) findViewById(R.id.editTextYearBirthday);
        editTextMobilePhone = (EditText) findViewById(R.id.editTextMobilePhone);
        editTextHomePhone = (EditText) findViewById(R.id.editTextHomePhone);
        editTextAddressNo = (EditText) findViewById(R.id.editTextAddressNo);
        editTextMoo = (EditText) findViewById(R.id.editTextMoo);
        editTextAlley = (EditText) findViewById(R.id.editTextAlley);
        editTextSoi = (EditText) findViewById(R.id.editTextSoi);
        editTextRoad = (EditText) findViewById(R.id.editTextRoad);
        editTextSubDistrict = (EditText) findViewById(R.id.editTextSubDistrict);
        editTextDistrict = (EditText) findViewById(R.id.editTextDistrict);
        editTextProvince = (EditText) findViewById(R.id.editTextProvince);
        editTextZipCode = (EditText) findViewById(R.id.editTextZipCode);
        buttonGotoHouseholdRegistration = (Button) findViewById(R.id.buttonGotoHouseholdRegistration);
        buttonGotoHouseholdRegistration.setText(R.string.checkProfileFromDopa);
        buttonGotoHouseholdRegistration.setOnClickListener(this);
        Button imageButtonCaptureProfile = (Button) findViewById(R.id.imageButtonCameraCaptureProfile);
        imageButtonCaptureProfile.setOnClickListener(this);
        Button imageButtonDialogPhoto = (Button) findViewById(R.id.imageButtonDialogPhoto);
        imageButtonDialogPhoto.setOnClickListener(this);

        layoutZipCode = (LinearLayout) findViewById(R.id.layoutZipCode);
        layoutAfterValidate = (LinearLayout) findViewById(R.id.layoutAfterValidate);
        layoutBorder = findViewById(R.id.layoutBorder);

        spinnerCareerMain = (Spinner) findViewById(R.id.spinnerCareerMain);
        spinnerCareerMain.setOnItemSelectedListener(this);
        spinnerCareerMinor = (Spinner) findViewById(R.id.spinnerCareerMinor);
        spinnerCareerMinor.setOnItemSelectedListener(this);


        imageViewErrorStatus = (ImageView) findViewById(R.id.imageViewErrorStatus);
        imageViewErrorCareerMain = (ImageView) findViewById(R.id.imageViewErrorCareerMain);
        imageViewErrorZipCode = (ImageView) findViewById(R.id.imageViewErrorZipCode);
    }

    private void loadCareer() {
        APIServiceCareer serviceCareer = CareerManager.getCareer();

        SharedPreferences settings;
        settings = getSharedPreferences("profileCareer", 0);
        String dataDistrict = settings.getString("profileCareer", null);
        ProfileCareer profileCareer = new Gson().fromJson(dataDistrict, ProfileCareer.class);
        onLoadProfileCareerSuccess(profileCareer);
        if (profileCareer == null) {
            Call<ProfileCareer> call = serviceCareer.getCareer();
            call.enqueue(new CareerProfileManager(this));
        }
    }

    private void setTextOnViewGroup(Farmer farmer) {
        if (farmer.getImagePerson() != null && farmer.getFileImageProFile() == null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(farmer.getImagePerson(), 0, farmer.getImagePerson().length);
            imagePerson.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 297, 355, false));
        }
        setProfile(farmer);
        if (farmer.getAddress() != null) {
            setAddress(farmer);
        }
        spinnerStatus = (Spinner) findViewById(R.id.spnStatus);
        String[] statusGroup = getResources().getStringArray(R.array.arrayStatus);
        ArrayAdapter<String> adapterStatus = new ArrayAdapter<>(this,
                R.layout.spinner_item, statusGroup);
        spinnerStatus.setAdapter(adapterStatus);
        farmer.getStatus();
    }

    private void setProfile(Farmer farmer) {
        editTextPrefix.setText(farmer.getPreName());
        editTextFirstName.setText(farmer.getFirstName());
        editTextLastName.setText(farmer.getLastName());
        editTextCitizenID.setText(String.format("%s-%s-%s-%s-%s",
                farmer.getCitizenId().substring(0, 1),
                farmer.getCitizenId().substring(1, 5),
                farmer.getCitizenId().substring(5, 10),
                farmer.getCitizenId().substring(10, 12),
                farmer.getCitizenId().substring(12, 13)));
        editTextMobilePhone.setText(farmer.getMobilePhone());
        editTextHomePhone.setText(farmer.getHomePhone());
        String strDay = "-";
        String strMonth = "-";
        if (!farmer.getBirthDate().substring(6, 8).equals("00")) {
            strDay = farmer.getBirthDate().substring(6, 8);
        }
        if (!farmer.getBirthDate().substring(4, 6).equals("00")) {
            strMonth = farmer.getBirthDate().substring(4, 6);
        }
        editTextDayBirthday.setText(strDay);
        editTextMonthBirthday.setText(DateFormatThai.getInstance()
                .toMonthString(strMonth));
        editTextYearBirthday.setText(farmer.getBirthDate().substring(0, 4));
    }

    private void setAddress(Farmer farmer) {
        editTextAddressNo.setText(verifyParam(farmer.getAddress().getHouseNo()));
        editTextMoo.setText(verifyParam(farmer.getAddress().getMoo()));
        editTextAlley.setText(verifyParam(farmer.getAddress().getAlley()));
        editTextSoi.setText(verifyParam(farmer.getAddress().getSoi()));
        editTextRoad.setText(verifyParam(farmer.getAddress().getRoad()));
        editTextProvince.setText(farmer.getAddress().getProvince());
        editTextSubDistrict.setText(farmer.getAddress().getSubDistrict());
        editTextDistrict.setText(farmer.getAddress().getDistrict());
        editTextZipCode.setText(verifyParam(farmer.getAddress().getZipCode()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonGotoHouseholdRegistration:
                if (updateProfileSuccess) {
                    nextIntent();
                } else {
                    updateProfileFromDOPA();
                }
                break;
            case R.id.imageButtonCameraCaptureProfile:
                startIntentCamera();
                break;
            case R.id.imageButtonDialogPhoto:
                showDialogPhotoFarmer();
                break;
        }
    }

    private void startIntentCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File fileImage = new FileManagement("PROFILE_").generatePath();
        farmer.setFileImageProFile(fileImage);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(farmer.getFileImageProFile()));
        startActivityForResult(Intent.createChooser(intent, "Take a picture with"), requestCamera);
    }

    private void showDialogPhotoFarmer() {
        if (farmer.getFileImageProFile() != null) {
            try {
                final Dialog dialogPhotoFarmer = new Dialog(this);
                dialogPhotoFarmer.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogPhotoFarmer.setContentView(R.layout.dialog_show_photo_farmer);

                ImageView imageViewPerson = (ImageView) dialogPhotoFarmer.findViewById(R.id.imageViewPerson);
                Bitmap bitmap = BitmapFactory.decodeFile(farmer.getFileImageProFile().getPath());
                imageViewPerson.setImageBitmap(new ScaledBitmap().getScaledBitmapForProfile(bitmap));
                imageViewPerson.setRotation(Rotate.Rotate(farmer.getFileImageProFile().getPath()));

                dialogPhotoFarmer.show();
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.copyFrom(dialogPhotoFarmer.getWindow().getAttributes());
                dialogPhotoFarmer.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                        WindowManager.LayoutParams.WRAP_CONTENT);

                Button buttonCloseDialog = (Button) dialogPhotoFarmer.findViewById(R.id.buttonCloseDialog);
                buttonCloseDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogPhotoFarmer.dismiss();
                    }
                });
            } catch (Exception e) {
                farmer.setFileImageProFile(null);
                createDialogAlertNoTitle(getString(R.string.noPicture));
            }
        } else {
            createDialogAlertNoTitle(getString(R.string.noPicture));
        }
    }

    private void nextIntent() {
        if (validateField()) {
            resizeImage();
            PreferencesService.savePreferences("farmer", farmer);
            Intent intentHouseholdRegistration = new Intent(RegisterFarmerActivity.this, HouseholdRegistrationActivity.class);
            startActivity(intentHouseholdRegistration);
        }
    }

    private void resizeImage() {
        if (farmer.getFileImageProFile() != null) {
            try {
                Bitmap bitmap = BitmapFactory.decodeFile(farmer.getFileImageProFile().getPath());
                Bitmap resized = Bitmap.createScaledBitmap(bitmap,
                        (int) (bitmap.getWidth() * 0.4),
                        (int) (bitmap.getHeight() * 0.4), true);

                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(farmer.getFileImageProFile());
                    resized.compress(Bitmap.CompressFormat.JPEG, 100, out);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                farmer.setFileImageProFile(null);
            }
        }
    }

    private void updateProfileFromDOPA() {
        if (!updateProfileSuccess) {
            loadProfileFromAPI();
        }
    }

    private boolean validateField() {
        boolean isGoingNextPage = true;
        if (spinnerStatus.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectStatus))) {
            imageViewErrorStatus.setVisibility(View.VISIBLE);
            isGoingNextPage = false;
        } else {
            imageViewErrorStatus.setVisibility(View.INVISIBLE);
            farmer.setStatus(spinnerStatus.getSelectedItemPosition());
        }

        if (fieldNotQualifyWithSize(editTextMobilePhone, 9)) {
            editTextMobilePhone.setError(getText(R.string.pleaseCheckYourPhoneNumber));
            isGoingNextPage = false;
        } else {
            farmer.setMobilePhone(editTextMobilePhone.getText().toString());
        }
        if (fieldNotQualifyWithSize(editTextHomePhone, 9)) {
            editTextHomePhone.setError(getText(R.string.pleaseCheckYourPhoneNumber));
            isGoingNextPage = false;
        } else {
            farmer.setHomePhone(editTextHomePhone.getText().toString());
        }

        if (spinnerCareerMain.getSelectedItem().toString()
                .equals(getString(R.string.pleaseSelectCareerMain))) {
            imageViewErrorCareerMain.setVisibility(View.VISIBLE);
            isGoingNextPage = false;
        } else {
            imageViewErrorCareerMain.setVisibility(View.INVISIBLE);
            farmer.setCareerMain(careerMainCode);
            farmer.setCareerMinor(careerMinorCode);
        }

        if (fieldNotQualifyWithSize(editTextZipCode, 5)) {
            imageViewErrorZipCode.setVisibility(View.VISIBLE);
            isGoingNextPage = false;
        } else {
            farmer.getAddress().setZipCode(editTextZipCode.getText().toString());
            imageViewErrorZipCode.setVisibility(View.INVISIBLE);
        }

        if (!isGoingNextPage) {
            createDialogAlertNoTitle(getString(R.string.allFieldRequired));
        }
        return isGoingNextPage;
    }

    private void createDialogAlertNoTitle(String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert_no_title);

        TextView textViewMessage = (TextView) dialogAlert.findViewById(R.id.textViewMessage);
        textViewMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonOK = (Button) dialogAlert.findViewById(R.id.buttonOK);
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }

    private boolean fieldNotQualifyWithSize(EditText editText, int size) {
        return isNotEmptyField(editText) && isValueInFieldLessThan(editText, size);
    }

    private void loadProfileFromAPI() {
        createDialogProgress();
        Call<Farmer> call = servicePerson.getProfile(farmer.getCitizenId());
        call.enqueue(new PersonProfileManager(this));
    }

    private void createDialogProgress() {
        progressDialog = new ProgressDialog(this, R.style.ProgressDialogCustom);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getResources().getString(R.string.loadingDataFromDOAE));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setProgress(0);
        progressDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainBus.getInstance().register(this);
        new TokenLoginService(this).loginByToken();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainBus.getInstance().unregister(this);
    }

    @Subscribe
    public void onLoadProfileSuccess(Farmer profile) {
        if (profile.getCitizenId() != null) {
            APIDOAEServiceMember serviceMember = DOAEServiceManager.getDOAEService();
            Call<MemberDOAE> member = serviceMember.checkHouseHoldMember(profile.getCitizenId());
            member.enqueue(new CheckHouseHoldMember(this, profile));
        } else {
            checkProfile(profile);
        }
    }

    @Override
    public void onLoadHouseHoldMemberSuccess(MemberDOAE memberDOAE, Farmer profile) {
        progressDialog.dismiss();
        if (memberDOAE.getHouseHoldNumber() != null) {
            if (!memberDOAE.getHouseHoldNumber().equals(profile.getAddress().getAddressID())) {
                checkProfile(profile);
            } else {
                createDialogAlertNoTitle("ไม่สามารถขึ้นทะเบียนได้เนื่องจากท่านเป็นสมาชิกในครับเรือนของ "
                        + memberDOAE.getPreName()
                        + memberDOAE.getFirstName()
                        + " "
                        + memberDOAE.getLastName());
            }
        } else {
            checkProfile(profile);
        }
    }

    private void checkProfile(Farmer profile) {
        if (profile.getCitizenId() != null) {
            layoutAfterValidate.setVisibility(View.VISIBLE);
            layoutBorder.setVisibility(View.VISIBLE);
            layoutZipCode.setVisibility(View.VISIBLE);
            compareProfileOldWithProfileNew(profile);
            PreferencesService.savePreferences("farmer", farmer);
            setTextOnViewGroup(farmer);
            callHouseHoldMember();
            updateProfileSuccess = true;
            buttonGotoHouseholdRegistration.setText(R.string.btnHouseholdRegistrationPage);
        } else {
            createDialogAlertNoTitle(getString(R.string.cannotFindOutProfile));
            progressDialog.dismiss();
        }
    }

    private void compareProfileOldWithProfileNew(Farmer profile) {

        if (compareStringParam(farmer.getPreName(), profile.getPreName())) {
            setProfileUpdateToListView(R.string.preFix, verifyParam(farmer.getPreName()), verifyParam(profile.getPreName()), feedListUpdateProfile);
            farmer.setPreName(profile.getPreName());
        }

        if (compareStringParam(farmer.getFirstName(), profile.getFirstName())) {
            setProfileUpdateToListView(R.string.firstName, verifyParam(farmer.getFirstName()), verifyParam(profile.getFirstName()), feedListUpdateProfile);
            farmer.setFirstName(profile.getFirstName());
        }

        if (compareStringParam(farmer.getLastName(), profile.getLastName())) {
            setProfileUpdateToListView(R.string.lastName, verifyParam(farmer.getLastName()), verifyParam(profile.getLastName()), feedListUpdateProfile);
            farmer.setLastName(profile.getLastName());
        }

        String birthDateFromCard = convertDateToMobileFormat(farmer.getBirthDate());
        String birthDateUpdate = convertDateToMobileFormat(profile.getBirthDate());
        if (!birthDateFromCard.equals(birthDateUpdate)) {
            setProfileUpdateToListView(R.string.birthDate, birthDateFromCard, birthDateUpdate, feedListUpdateProfile);
            farmer.setBirthDate(profile.getBirthDate());
        }

        Address address = farmer.getAddress();
        if (address == null) {
            address = new Address();
        }
        Address profileAddressFromAPI = profile.getAddress();

        if (compareStringParam(address.getHouseNo(), profileAddressFromAPI.getHouseNo())) {
            setProfileUpdateToListView(R.string.homeAddressNo, verifyParam(address.getHouseNo()), verifyParam(profileAddressFromAPI.getHouseNo()), feedListUpdateProfile);
            address.setHouseNo(profileAddressFromAPI.getHouseNo());
        }

        if (compareStringParam(address.getMoo(), profileAddressFromAPI.getMoo())) {
            setProfileUpdateToListView(R.string.moo, verifyParam(address.getMoo()), verifyParam(profileAddressFromAPI.getMoo()), feedListUpdateProfile);
            address.setMoo(profileAddressFromAPI.getMoo());
        }

        if (compareStringParam(address.getAlley(), profileAddressFromAPI.getAlley())) {
            setProfileUpdateToListView(R.string.alley, verifyParam(address.getAlley()), verifyParam(profileAddressFromAPI.getAlley()), feedListUpdateProfile);
            address.setAlley(profileAddressFromAPI.getAlley());
        }

        if (compareStringParam(address.getSoi(), profileAddressFromAPI.getSoi())) {
            setProfileUpdateToListView(R.string.soi, verifyParam(address.getSoi()), verifyParam(profileAddressFromAPI.getSoi()), feedListUpdateProfile);
            address.setSoi(profileAddressFromAPI.getSoi());
        }

        if (compareStringParam(address.getProvince(), profileAddressFromAPI.getProvince())) {
            setProfileUpdateToListView(R.string.province, verifyParam(address.getProvince()), verifyParam(profileAddressFromAPI.getProvince()), feedListUpdateProfile);
            address.setProvince(profileAddressFromAPI.getProvince());
        }

        if (compareStringParam(address.getDistrict(), profileAddressFromAPI.getDistrict())) {
            setProfileUpdateToListView(R.string.district, verifyParam(address.getDistrict()), verifyParam(profileAddressFromAPI.getDistrict()), feedListUpdateProfile);
            address.setDistrict(profileAddressFromAPI.getDistrict());
        }

        if (compareStringParam(address.getSubDistrict(), profileAddressFromAPI.getSubDistrict())) {
            setProfileUpdateToListView(R.string.subDistrict, verifyParam(address.getSubDistrict()), verifyParam(profileAddressFromAPI.getSubDistrict()), feedListUpdateProfile);
            address.setSubDistrict(profileAddressFromAPI.getSubDistrict());
        }

        address.setAddressID(verifyParam(profileAddressFromAPI.getAddressID()));

        farmer.setSex(verifyParam(profile.getSex()));
        farmer.setAddress(address);
        farmer.setPersonStatusId(profile.getPersonStatusId());
        farmer.setPersonStatus(verifyParam(profile.getPersonStatus()));
        farmer.setAge(verifyParam(profile.getAge()));
        farmer.setSexId(profile.getSexId());
    }

    private void setProfileUpdateToListView(int title, String profileFromUSB, String ProfileFromDOPA, ArrayList<HashMap<String, String>> listData) {
        HashMap<String, String> mapData = new HashMap<>();
        mapData.put("title", getString(title));
        mapData.put("profileFromUSB", profileFromUSB);
        mapData.put("ProfileFromDOPA", ProfileFromDOPA);

        listData.add(mapData);
    }

    private boolean compareStringParam(String paramFarmer, String paramFromAPI) {
        return !verifyParam(paramFarmer).equals(verifyParam(paramFromAPI));
    }

    private String verifyParam(String param) {
        if (param != null && !param.equals("") && !param.equals("00")) {
            return param;
        }
        return "-";
    }

    private String convertDateToMobileFormat(String DataDate) {
        String strDay = "-";
        String strMonth = "-";
        if (DataDate == null) {
            return "-";
        }
        if (!DataDate.substring(6, 8).equals("00")) {
            strDay = DataDate.substring(6, 8);
        }
        if (!DataDate.substring(4, 6).equals("00")) {
            strMonth = DateFormatThai.getInstance()
                    .toMonthString(DataDate.substring(4, 6));
        }
        String year = DataDate.substring(0, 4);
        return strDay.concat(" / ").concat(strMonth).concat(" / ").concat(year);
    }

    private void callHouseHoldMember() {
        Call<HouseHoldMember> call = servicePerson.loadHouseHoldMember(farmer.getCitizenId());
        call.enqueue(new HouseHoldMemberManager(this));
    }

    @Subscribe
    public void onLoadHouseHoldMemberSuccess(HouseHoldMember houseHoldMember) {
        PreferencesService.savePreferences("householdMembers", houseHoldMember);
        loadLocationCode(farmer.getAddress().getProvince(),
                farmer.getAddress().getDistrict(),
                farmer.getAddress().getSubDistrict());
    }

    private void loadLocationCode(String province, String district, String subDistrict) {
        areaService = AreaMangager.getArea();
        Call<String> call = areaService.loadLocationCode(province, district, subDistrict);
        call.enqueue(new LocationCode(this));
    }

    private boolean isValueInFieldLessThan(EditText field, int size) {
        return field.getText().toString().length() < size;
    }

    private boolean isNotEmptyField(EditText field) {
        return field.getText().toString().length() != 0;

    }

    @Override
    public void onLoadLocationCodeSuccess(String locationCode) {
        Call<String> call = areaService.loadPostCode(locationCode);
        call.enqueue(new PostCode(this));
    }

    @Override
    public void onLoadPostCodeSuccess(String postCode) {
        farmer.getAddress().setZipCode(postCode);
        editTextZipCode.setText(postCode);

        if (!manualRegister) {
            setProfileUpdateToListView(R.string.zipCode, "-", postCode, feedListUpdateProfile);

            final Dialog dialogAlert = new Dialog(this);
            dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogAlert.setContentView(R.layout.dialog_update_profile_from_dopa);
            TextView textViewTitle = (TextView) dialogAlert.findViewById(R.id.textViewTitle);
            textViewTitle.setText(R.string.updateProfileFromDOPA);

            ListView listViewUpdateProfile = (ListView) dialogAlert.findViewById(R.id.listViewProfileFromUSB);

            SimpleAdapter simpleAdapter = new SimpleAdapter(this, feedListUpdateProfile, R.layout.listview_update_profile_from_dopa,
                    new String[]{"title", "profileFromUSB", "ProfileFromDOPA"},
                    new int[]{R.id.textViewTitleMessage, R.id.textViewProfileFromUSB, R.id.textViewProfileFromDOPA});
            listViewUpdateProfile.setAdapter(simpleAdapter);


            dialogAlert.show();
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
            dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                    WindowManager.LayoutParams.WRAP_CONTENT);

            Button buttonDialogAlertOK = (Button) dialogAlert.findViewById(R.id.buttonAdmittedToTheShockOfTheDepartment);
            buttonDialogAlertOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogAlert.dismiss();
                }
            });
        }
        progressDialog.dismiss();
    }

    @Subscribe
    public void onLoadProfileCareerSuccess(ProfileCareer profileCareer) {
        if (profileCareer != null) {
            if (profileCareer.getCareer() != null) {
                List<StringWithTag> listProfileCareer = listForSetArrayAdapter(profileCareer.getCareer(), "name", "id");
                List<StringWithTag> listProfileCareerMinor = listForSetArrayAdapter(profileCareer.getCareer(), "name", "id");
                spinnerCareerMain.setAdapter(getArrayAdapterForSpinner(listProfileCareer, "กรุณาเลือกอาชีพหลัก"));
                spinnerCareerMinor.setAdapter(getArrayAdapterForSpinner(listProfileCareerMinor, "กรุณาเลือกอาชีพรอง"));
            }
        } else {
            spinnerCareerMain.setAdapter(getArrayAdapterForSpinner(new ArrayList<>(), "กรุณาเลือกอาชีพหลัก"));
            spinnerCareerMinor.setAdapter(getArrayAdapterForSpinner(new ArrayList<>(), "กรุณาเลือกอาชีพรอง"));
        }
    }

    private List<StringWithTag> listForSetArrayAdapter(List<JsonObject> objects, String name, String id) {
        List<StringWithTag> lists = new ArrayList<>();
        for (int index = 0; index < objects.size(); index++) {
            if (objects.get(index).get(id) != null) {
                lists.add(new StringWithTag(objects.get(index).get(name).getAsString(),
                        objects.get(index).get(id).getAsString()));
            }
        }
        return lists;
    }

    private ArrayAdapter getArrayAdapterForSpinner(List lists, String firstList) {
        lists.add(0, new StringWithTag(firstList, 0));
        return new ArrayAdapter(this, R.layout.spinner_item, lists);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    @Override
    public void timeout(String timeout) {
        errorNetwork(timeout);
    }

    @Override
    public void onLoadHouseHoldMemberFails(String message) {
        errorNetwork(message);
    }

    private void errorNetwork(String timeout) {
        progressDialog.dismiss();
        createDialogAlert(getString(R.string.errorTimeout),
                getString(R.string.cannotConnectServer).concat(timeout));
    }

    @Override
    public void onLoadFails(String message) {
        createDialogAlert(getString(R.string.errorTimeout),
                getString(R.string.cannotConnectServer).concat(message));
    }

    @Override
    public void onLoadCareerFails(String message) {
        createDialogAlert(getString(R.string.errorTimeout),
                getString(R.string.cannotConnectServer).concat(message));
    }

    private void createDialogAlert(String title, String message) {
        final Dialog dialogAlert = new Dialog(this);
        dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAlert.setContentView(R.layout.dialog_alert);

        TextView textViewDialogAlertTitle = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertTitle);
        TextView textViewDialogAlertMessage = (TextView) dialogAlert.findViewById(R.id.textViewDialogAlertMessage);

        textViewDialogAlertTitle.setText(title);
        textViewDialogAlertMessage.setText(message);

        dialogAlert.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialogAlert.getWindow().getAttributes());
        dialogAlert.getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90),
                WindowManager.LayoutParams.WRAP_CONTENT);

        Button buttonDialogAlertOK = (Button) dialogAlert.findViewById(R.id.buttonDialogAlertOK);
        buttonDialogAlertOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlert.dismiss();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        StringWithTag stringWithTag = (StringWithTag) parent.getItemAtPosition(position);
        switch (parent.getId()) {
            case R.id.spinnerCareerMain:
                careerMainCode = stringWithTag.tag.toString();
                break;
            case R.id.spinnerCareerMinor:
                careerMinorCode = stringWithTag.tag.toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}