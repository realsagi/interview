package th.in.tamis.service;

import junit.framework.TestCase;

import org.junit.Test;

import th.in.tamis.models.DetailRegisterPlantation;

public class DeedFormatTest extends TestCase{

    private DetailRegisterPlantation detailRegisterPlantation;

    @Override
    protected void setUp(){
        detailRegisterPlantation = new DetailRegisterPlantation();
        detailRegisterPlantation.setRai(5);
        detailRegisterPlantation.setNgan(10);
        detailRegisterPlantation.setSquareWah(50.00);
    }

    @Test
    public void testGetFormatOfDeed(){
        DeedFormat deedFormat = DeedFormat.setDetailRegisterPlantationFormat(detailRegisterPlantation);
        String expect = "5 ไร่ 10 งาน 50.0 ตารางวา";
        String actual = deedFormat.getFormat();
        assertEquals(expect, actual);
    }
}