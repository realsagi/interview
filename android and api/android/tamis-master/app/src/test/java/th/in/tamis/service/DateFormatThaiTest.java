package th.in.tamis.service;

import junit.framework.TestCase;

import org.junit.Test;

import java.lang.Override;
import java.util.Calendar;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class DateFormatThaiTest extends TestCase{

    private DateFormatThai dateFormatThai;

    @Override
    protected void setUp(){
        dateFormatThai = DateFormatThai.getInstance();
    }

    @Test
    public void testConvertNumber01ToJanuary(){
        String january = dateFormatThai.toMonthString("01");
        assertEquals("มกราคม", january);
    }

    @Test
    public void testConverNumber1ToJanuary(){
        String january = dateFormatThai.toMonthString("1");
        assertEquals("มกราคม", january);
    }

    @Test
    public void testConvertNumber02ToFebruary(){
        String february = dateFormatThai.toMonthString("02");
        assertEquals("กุมภาพันธ์", february);
    }

    @Test
    public void testConvertNumber2ToFebruary(){
        String february = dateFormatThai.toMonthString("2");
        assertEquals("กุมภาพันธ์", february);
    }

    @Test
    public void testConvertNumber03ToMarch(){
        String march = dateFormatThai.toMonthString("03");
        assertEquals("มีนาคม", march);
    }

    @Test
    public void testConvertNumber3ToMarch(){
        String march = dateFormatThai.toMonthString("3");
        assertEquals("มีนาคม", march);
    }

    @Test
    public void testConvertNumber04ToApril(){
        String april = dateFormatThai.toMonthString("04");
        assertEquals("เมษายน", april);
    }

    @Test
    public void testConvertNumber4ToApril(){
        String april = dateFormatThai.toMonthString("4");
        assertEquals("เมษายน", april);
    }

    @Test
    public void testConvertNumber05ToMay(){
        String may = dateFormatThai.toMonthString("05");
        assertEquals("พฤษภาคม", may);
    }

    @Test
    public void testConvertNumber5ToMay(){
        String may = dateFormatThai.toMonthString("5");
        assertEquals("พฤษภาคม", may);
    }

    @Test
    public void testConvertNumber06ToJune(){
        String june = dateFormatThai.toMonthString("06");
        assertEquals("มิถุนายน", june);
    }

    @Test
    public void testConvertNumber6ToJune(){
        String june = dateFormatThai.toMonthString("6");
        assertEquals("มิถุนายน", june);
    }

    @Test
    public void testConvertNumber07ToJuly(){
        String july = dateFormatThai.toMonthString("07");
        assertEquals("กรกฎาคม", july);
    }

    @Test
    public void testConvertNumber7ToJuly(){
        String july = dateFormatThai.toMonthString("7");
        assertEquals("กรกฎาคม", july);
    }

    @Test
    public void testConvertNumber08ToAugust(){
        String august = dateFormatThai.toMonthString("08");
        assertEquals("สิงหาคม", august);
    }

    @Test
    public void testConvertNumber8ToAugust(){
        String august = dateFormatThai.toMonthString("8");
        assertEquals("สิงหาคม", august);
    }

    @Test
    public void testConvertNumber09ToSeptember(){
        String september = dateFormatThai.toMonthString("09");
        assertEquals("กันยายน", september);
    }

    @Test
    public void testConvertNumber9ToSeptember(){
        String september = dateFormatThai.toMonthString("9");
        assertEquals("กันยายน", september);
    }

    @Test
    public void testConvertNumber10ToOctober(){
        String october = dateFormatThai.toMonthString("10");
        assertEquals("ตุลาคม", october);
    }

    @Test
    public void testConvertNumber11ToNovember(){
        String november = dateFormatThai.toMonthString("11");
        assertEquals("พฤศจิกายน", november);
    }

    @Test
    public void testConvertNumber12ToDecember(){
        String december = dateFormatThai.toMonthString("12");
        assertEquals("ธันวาคม", december);
    }

    @Test
    public void testGetNowThai(){

        Calendar mockNowThai = mock(Calendar.getInstance().getClass());
        when(mockNowThai.get(Calendar.DATE)).thenReturn(1);
        when(mockNowThai.get(Calendar.MONTH)).thenReturn(0);
        when(mockNowThai.get(Calendar.YEAR)).thenReturn(2000);

        String nowThai = dateFormatThai.getNowThai(mockNowThai);

        assertEquals("1 มกราคม 2543", nowThai);
    }
}