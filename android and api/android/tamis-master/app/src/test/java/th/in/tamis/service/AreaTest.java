package th.in.tamis.service;

import junit.framework.TestCase;

import org.junit.Test;

public class AreaTest extends TestCase{
    private Area area = new Area(0);

    @Test
    public void testSetRai0SetNgan0SetSquareWah100(){
        area.setSquareWah(100);
        assertEquals(0, area.getRai());
        assertEquals(0.0, area.getSquareWah());
        assertEquals(1, area.getNgan());
    }

    @Test
    public void testSetRai0SetNgan0SetSquareWah200(){
        area.setSquareWah(200);
        assertEquals(0, area.getRai());
        assertEquals(0.0, area.getSquareWah());
        assertEquals(2, area.getNgan());
    }

    @Test
    public void testSetRai0SetNgan0SetSquareWah130(){
        area.setSquareWah(130);
        assertEquals(0, area.getRai());
        assertEquals(1, area.getNgan());
        assertEquals(30.0, area.getSquareWah());
    }

    @Test
    public void testSetRai0SetNgan0SetSquareWah250(){
        area.setSquareWah(255);
        assertEquals(0, area.getRai());
        assertEquals(2, area.getNgan());
        assertEquals(55.0, area.getSquareWah());
    }

    public void testSetRai0SetNgan4SetSquareWah0(){
        area.setNgan(4);
        assertEquals(1, area.getRai());
        assertEquals(0, area.getNgan());
        assertEquals(0.0, area.getSquareWah());
    }

    public void testSetRai0SetNgan5SetSquareWah0(){
        area.setNgan(5);
        assertEquals(1, area.getRai());
        assertEquals(1, area.getNgan());
        assertEquals(0.0, area.getSquareWah());
    }

    public void testSetRai4SetNgan70SetSquareWah900(){
        area.setRai(4);
        area.setNgan(70);
        area.setSquareWah(900);
        assertEquals(23, area.getRai());
        assertEquals(3, area.getNgan());
        assertEquals(0.0, area.getSquareWah());
    }

    public void testSetRai10SetNgan97SetSquareWah1250(){
        area.setRai(10);
        area.setNgan(97);
        area.setSquareWah(1250);
        assertEquals(37, area.getRai());
        assertEquals(1, area.getNgan());
        assertEquals(50.0, area.getSquareWah());
    }

    public void testConvertSquareMeterToRaiNganWah(){
        assertEquals("0 ไร่ 0 งาน 1.5 ตารางวา", area.convertSquareMeterToRaiNganWah(6));
    }

    public void testConvertSquareMeterToRaiNganWahSendSquareWhaOver99(){
        assertEquals("0 ไร่ 1 งาน 25.21 ตารางวา", area.convertSquareMeterToRaiNganWah(500.8438));
    }
}