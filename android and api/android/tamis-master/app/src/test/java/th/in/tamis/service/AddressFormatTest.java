package th.in.tamis.service;

import junit.framework.TestCase;

import th.in.tamis.models.Address;

import org.junit.Test;
import static org.junit.Assert.*;

public class AddressFormatTest extends TestCase{

    private Address address;

    @Override
    protected void setUp(){
        address = new Address();
        address.setMoo("5");
        address.setSubDistrict("หนองนาคำ");
        address.setDistrict("ขี้เหล็ก");
        address.setProvince("อุดีธานอน");
    }

    @Test
    public void testGetFormatOfAddress(){
        AddressFormat addressFormat = AddressFormat.setAddressFormat(address);
        String expect = "หมู่ที่ 5 ตำบลหนองนาคำ อำเภอขี้เหล็ก จังหวัดอุดีธานอน";
        String actual = addressFormat.getFormat();
        assertEquals(expect, actual);
    }
}