package th.in.tamis.service;

import junit.framework.TestCase;

import org.junit.Test;

import th.in.tamis.service.map.ConvertUTMTOLatLong;


public class convertUTMToLatLong extends TestCase{

    double expectLatitude = 16.42706;
    double expectLongitude = 102.8372735;

    double x = 269057.99434997316;
    double y = 1817408.3406415917;
    int zone = 48;
    String hemisphere = "N";

    @Test
    public void testUtmXToLatitude(){
        ConvertUTMTOLatLong convertUTMTOLatLong = new ConvertUTMTOLatLong();
        double actual = convertUTMTOLatLong.utmXToLatitude(x, y, zone, hemisphere);
        assertEquals(expectLatitude, actual, 0.000001);
    }

    @Test
    public void testUtmYTOLongitude(){
        ConvertUTMTOLatLong convertUTMTOLatLong = new ConvertUTMTOLatLong();
        double actual = convertUTMTOLatLong.utmYToLongitude(x, y, zone, hemisphere);
        assertEquals(expectLongitude, actual, 0.000000001);
    }
}
