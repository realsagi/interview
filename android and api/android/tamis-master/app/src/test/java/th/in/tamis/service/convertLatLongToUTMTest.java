package th.in.tamis.service;

import junit.framework.TestCase;

import org.junit.Test;

import th.in.tamis.service.map.ConvertLatLongToUTM;

public class convertLatLongToUTMTest extends TestCase{

    double latitude = 16.42706;
    double longitude = 102.8372735;

    double expectX = 269057.99434997316;
    double expectY = 1817408.3406415917;
    int expectZone = 48;
    String expectHemisphere = "N";

    double expectRadiusOfLatitude = 0.2867062834226594;
    double expectRadiusOfLongitude = 1.794849016348911;
    double expectEp2 = 0.006739496819936062;
    double expectNu2 = 0.0062005208808970326;
    double expectN = 6379845.0105212135;

    @Test
    public void testConvertLatToZone(){
        ConvertLatLongToUTM convertLatLongToUTM = new ConvertLatLongToUTM();
        int actual = convertLatLongToUTM.convertLongitudeToZone(longitude);
        assertEquals(expectZone, actual);
    }

    @Test
    public void testConvertToRadius(){
        ConvertLatLongToUTM convertLatLongToUTM = new ConvertLatLongToUTM();
        double radiusLatitude = convertLatLongToUTM.convertLatLongToRadius(latitude);
        assertEquals(expectRadiusOfLatitude, radiusLatitude);

        double radiusLongitude = convertLatLongToUTM.convertLatLongToRadius(longitude);
        assertEquals(expectRadiusOfLongitude, radiusLongitude);
    }

    @Test
    public void testGetEp2(){
        ConvertLatLongToUTM convertLatLongToUTM = new ConvertLatLongToUTM();
        double ep2 = convertLatLongToUTM.getEp2();
        assertEquals(expectEp2, ep2);
    }

    @Test
    public void testGetNu2(){
        ConvertLatLongToUTM convertLatLongToUTM = new ConvertLatLongToUTM();
        double ep2 = convertLatLongToUTM.getNu2(latitude);
        assertEquals(expectNu2, ep2);
    }

    @Test
    public void testGetN(){
        ConvertLatLongToUTM convertLatLongToUTM = new ConvertLatLongToUTM();
        double ep2 = convertLatLongToUTM.getN(latitude);
        assertEquals(expectN, ep2);
    }

    @Test
    public void testConvertLatitudeToUTMX(){
        ConvertLatLongToUTM convertLatLongToUTM = new ConvertLatLongToUTM();
        double x = convertLatLongToUTM.covertLatitudeToUTMX(latitude, longitude);
        assertEquals(expectX, x);
    }

    @Test
    public void testcovertLongitudeToUTMX(){
        ConvertLatLongToUTM convertLatLongToUTM = new ConvertLatLongToUTM();
        double y = convertLatLongToUTM.covertLongitudeToUTMY(latitude, longitude);
        assertEquals(expectY, y);
    }

    @Test
    public void testGetHemisphere(){
        ConvertLatLongToUTM convertLatLongToUTM = new ConvertLatLongToUTM();
        String hemisphere = convertLatLongToUTM.getHemisphere(latitude);
        assertEquals(expectHemisphere, hemisphere);
    }
}
