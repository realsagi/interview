package th.in.tamis.usbcardreader;

import th.in.tamis.usb.reader.ThaiCitizenCard;

import com.idvision.androididcardlib.iCardUsbReader;

import th.in.tamis.usbreader.datatest.ThaiCitizenCardOfKittimasak;

import org.junit.Test;

import static org.junit.Assert.*;

public class FarmerAddressTest{
    iCardUsbReader reader = null;
    ThaiCitizenCard farmer = new ThaiCitizenCardOfKittimasak(null);

    @Test
    public void testGetHouseNumber() {
        String expect = "345/5";
        String actual = farmer.getHolderProfile().getAddress().getHouseNumber();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetMoo() {
        String expect = "8";
        String actual = farmer.getHolderProfile().getAddress().getMoo();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetAlley() {
        String expect = "ข้าวสาร";
        String actual = farmer.getHolderProfile().getAddress().getAlley();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetSoi() {
        String expect = "สุคนธวิท 44";
        String actual = farmer.getHolderProfile().getAddress().getSoi();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetRoad() {
        String expect = "สุคนธวิท";
        String actual = farmer.getHolderProfile().getAddress().getRoad();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetSubDistrict() {
        String expect = "ตลาดกระทุ่มแบน";
        String actual = farmer.getHolderProfile().getAddress().getSubDistrict();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetDistrict() {
        String expect = "กระทุ่มแบน";
        String actual = farmer.getHolderProfile().getAddress().getDistrict();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetProvince() {
        String expect = "สมุทรสาคร";
        String actual = farmer.getHolderProfile().getAddress().getProvince();
        assertEquals(expect, actual);
    }

}