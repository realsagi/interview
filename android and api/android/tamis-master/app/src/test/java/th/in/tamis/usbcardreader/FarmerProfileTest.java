package th.in.tamis.usbcardreader;

import th.in.tamis.usb.reader.ThaiCitizenCard;

import com.idvision.androididcardlib.iCardUsbReader;

import th.in.tamis.usbreader.datatest.ThaiCitizenCardOfKittimasak;

import org.junit.Test;

import static org.junit.Assert.*;

public class FarmerProfileTest{
    iCardUsbReader reader = null;
    ThaiCitizenCard farmer = new ThaiCitizenCardOfKittimasak(null);

    @Test
    public void testGetPreName() {
        String expect = "นาย";
        String actual = farmer.getHolderProfile().getThaiName().getPreName();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetFirstName() {
        String expect = "กิตติมศักดิ์";
        String actual = farmer.getHolderProfile().getThaiName().getFirstName();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetLastName() {
        String expect = "ศิวารัตน์";
        String actual = farmer.getHolderProfile().getThaiName().getLastName();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetId() {
        String expect = "2520100011499";
        String actual = farmer.getHolderProfile().getId();
        assertEquals(expect, actual);
    }

    @Test
    public void testGetBrithday() {
        String expect = "25290307";
        String actual = farmer.getHolderProfile().getBirthday();
        assertEquals(expect, actual);
    }
}