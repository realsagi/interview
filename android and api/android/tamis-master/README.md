# tamis
Repository for TAMIS project

# Build command
./gradlew build --info

# Espresso command
./gradlew :App:connectedAndroidTest

# stubby4j command
stubby_faarm_api
go to path stubby_faarm_api
java -jar stub4j-3.2.3.jar -d faarm_api/faarm.yml -l <<IP ADDRESS>> -w

stubby_government_api
go to path stubby_government_api
java -jar stubby4j-3.2.3.jar -d government_api/government.yml -l 192.168.100.9 -w
