# Profile Test Server

*IP   : 27.254.142.84
*USER : ubuntu
*PASS : spr1nt3r

# ตรวจสอบและติดตั้ง Java SDK

*1. ตรวจสอบเวอร์ชั่น Java

    java -version

*2. ติดตั้ง Java SDK 8

    sudo add-apt-repository ppa:webupd8team/java
    sudo apt-get update
    sudo apt-get install oracle-java8-installer
    java -version

*3. คั้งค่า Java Environment

    sudo apt-get install oracle-java8-set-default

*4 ตั้งค่า JAVA_HOME

    which java
    vi .bashrc
    JAVA_HOME=/usr/bin/java





