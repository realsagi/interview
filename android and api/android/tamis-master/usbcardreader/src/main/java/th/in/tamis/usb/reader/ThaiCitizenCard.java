package th.in.tamis.usb.reader;

import com.idvision.androididcardlib.iCardUsbReader;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import th.in.tamis.usb.reader.model.HolderProfile;

public class ThaiCitizenCard {
    private iCardUsbReader reader = null;
    private int lastError;
    private short lastSW;
    private String atrString;
    private String appletVersion;

    public ThaiCitizenCard(iCardUsbReader reader) {
        this.reader = reader;
        this.lastError = 0;
        this.lastSW = 0;
        this.atrString = null;
        this.appletVersion = null;
    }

    public int getLastError() {
        return this.lastError;
    }

    public String getCardAtrString() {
        return this.atrString;
    }

    public boolean checkStatusOfCardReader() {
        this.lastError = -2001;


        if (cardNotPerformed()) return false;

        byte[] atr = new byte[32];
        int len = this.reader.cardConnect(atr);
        if (len == 0) {
            this.lastError = -2002;
            return false;
        }

        this.atrString = "";
        for (int i = 0; i < len; i++) {
            this.atrString += String.format("%02X ",
                    Byte.valueOf(atr[i]));
        }

        short sw = selectApplet();
        if ((sw & 0xFF00) != 24832) {
            this.lastError = -2003;
            return false;
        }

        byte[] buffer = new byte[16];
        if (!readBlock((byte) 0, 0, 4, buffer)) {
            this.lastError = -2004;
            return false;
        }
        try {
            this.appletVersion = new String(buffer, 0, 4, "TIS620");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        this.lastError = 0;
        return true;
    }

    private boolean cardNotPerformed() {
        if (this.reader == null)
            return true;
        if (!this.reader.getReaderStatus())
            return true;
        if (!this.reader.getCardStatus())
            return true;
        return false;
    }

    public void close() {
        if (this.reader != null)
            this.reader.cardDisconnect();
    }

    public HolderProfile getHolderProfile() {
        byte[] profile = new byte[400];
        byte[] address = new byte[160];
        boolean isProfileLoaded = false;
        boolean isAddressLoaded = false;

        if (this.appletVersion.equals("0002")) {
            Arrays.fill(address, (byte) 32);
            isProfileLoaded = readBlock((byte) 1, 0, 377, profile);
            isAddressLoaded = readBlock((byte) 0, 4, 150, address);
        }

        if (this.appletVersion.equals("0003")) {
            isProfileLoaded = readBlock((byte) 0, 0, 377, profile);
            isAddressLoaded = readBlock((byte) 0, 5497, 160, address);
        }

        if (failToReadData(isProfileLoaded, isAddressLoaded))
            return null;

        return new HolderProfile(profile, address);
    }

    private boolean failToReadData(boolean isProfileLoaded, boolean isAddressLoaded) {
        return !(isProfileLoaded && isAddressLoaded);
    }

    public byte[] getHolderPhoto() {
        byte[] buffer = new byte[5120];
        if ((this.appletVersion.equals("0002"))
                && (readBlock((byte) 1, 381, 5116, buffer))) {
            return buffer;
        }

        if ((this.appletVersion.equals("0003"))
                && (readBlock((byte) 0, 379, 5118, buffer))) {
            return buffer;
        }

        return null;
    }


    private short getResponse(byte[] buffer, int startIndex, int length) {
        byte[] APDUResponse = { 0, -64, 0, 0, 64 };
        byte[] res = new byte[512];
        APDUResponse[4] = ((byte) (length & 0xFF));
        int resLen = this.reader.exchangeAPDU(APDUResponse, 5, res);
        if (resLen <= 0)
            return -1;

        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.order(ByteOrder.BIG_ENDIAN);
        bb.put(res[(resLen - 2)]);
        bb.put(res[(resLen - 1)]);
        short lastSW = bb.getShort(0);
        if (lastSW == -28672) {
            System.arraycopy(res, 0, buffer, startIndex, resLen - 2);
        }
        return lastSW;
    }

    private short selectApplet() {
        byte[] res = new byte[32];
        int resLen = 0;
        byte[] APDUSelect = { 0, -92, 4, 0, 8, -96, 0, 0, 0, 84, 72, 0, 1 };
        resLen = this.reader.exchangeAPDU(APDUSelect, 13, res);
        if (resLen <= 0)
            return -1;

        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.order(ByteOrder.BIG_ENDIAN);
        bb.put(res[0]);
        bb.put(res[1]);
        return bb.getShort(0);
    }

    private short readBinary(byte block, int offset, int length) {
        byte[] res = new byte[512];
        byte[] APDURead = { -128, -80, 0, 0, 2, 0, 64 };

        APDURead[2] = ((byte) (offset >>> 8 & 0xFF));
        APDURead[3] = ((byte) (offset & 0xFF));
        APDURead[6] = ((byte) (length & 0xFF));

        int resLen = this.reader.exchangeAPDU(APDURead, 7, res);
        if (resLen > 0) {
            ByteBuffer bb = ByteBuffer.allocate(2);
            bb.order(ByteOrder.BIG_ENDIAN);
            bb.put(res[0]);
            bb.put(res[1]);
            this.lastSW = bb.getShort(0);
            return this.lastSW;

        }

        return -1;
    }

    private boolean readBlock(byte block, int offset, int length, byte[] buffer) {
        int currentOffset = offset;
        int startIndex = 0;

        while (length > 0) {
            int size = length;
            if (size > 250)
                size = 250;
            short sw = readBinary(block, currentOffset, size);
            if ((sw & 0xFF00) != 24832) {
                return false;
            }
            sw = getResponse(buffer, startIndex, size);
            if (sw != -28672) {
                return false;
            }
            currentOffset += size;
            startIndex += size;
            length -= size;
        }

        return true;
    }
}
