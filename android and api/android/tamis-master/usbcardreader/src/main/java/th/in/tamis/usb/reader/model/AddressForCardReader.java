package th.in.tamis.usb.reader.model;

public class AddressForCardReader {

    final private String[] addressArray;

    public AddressForCardReader(String[] address){
        addressArray = address;
    }

    public String getHouseNumber() {
        return addressArray[0].trim();
    }

    public String getMoo() {
        return detailAddress(1, 7);
    }

    public String getAlley() {
        return detailAddress(2, 4);
    }

    public String getSoi() {
        return detailAddress(3, 3);
    }

    public String getRoad() {
        return detailAddress(4, 3);
    }

    public String getSubDistrict() {
        return detailAddress(5, 4);
    }

    public String getDistrict() {
        String district = "";
        if(isEmpty(addressArray[6])){
            if(addressArray[6].contains("อำเภอ"))
                district = addressArray[6].substring(5).trim();
            else
                district = addressArray[6].substring(3).trim();
        }
        return district;
    }

    public String getProvince() {
        String province = "";
        if(isEmpty(addressArray[7])){
            if(addressArray[7].contains("จังหวัด"))
                province = addressArray[7].substring(7).trim();
            else
                province = addressArray[7].trim();
        }
        return province;
    }

    private String detailAddress(int index, int subString){
        try {
            return isEmpty(addressArray[index]) ? addressArray[index].substring(subString).trim() : "";
        }catch (Exception error){
            return "";
        }
    }

    private boolean isEmpty(CharSequence str){
        return str != null && str.length() != 0;
    }

    @Override
    public String toString() {
        String a = "";
        for (String s : addressArray) {
            a += s+":";
        }
        return  a;
    }

}
