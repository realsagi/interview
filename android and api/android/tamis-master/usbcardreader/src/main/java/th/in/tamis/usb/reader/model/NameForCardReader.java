package th.in.tamis.usb.reader.model;

public class NameForCardReader {

    final private String[] nameArray;

    public NameForCardReader(String[] name){
        this.nameArray = name;
    }

    public String getPreName() {
        return nameArray[0];
    }

    public String getFirstName() {
        return nameArray[1];
    }

    public String getLastName() {
        return nameArray[3];
    }
}
