package th.in.tamis.usb.reader;

public interface RegisterUSB {
    void onRegisterUSBSuccess();
    void onRegisterUSBFails();
}
