package th.in.tamis.usb.reader.model;

import java.io.UnsupportedEncodingException;

public class HolderProfile {

    final private byte[] profile;
    final private byte[] address;

    public HolderProfile(byte[] profile, byte[] address) {
        this.profile = profile;
        this.address = address;
    }

    public AddressForCardReader getAddress() {
        return new AddressForCardReader(bytes2String(this.address, 0, 160).split("#"));
    }

    public NameForCardReader getThaiName() {
        return new NameForCardReader(bytes2String(profile, 17, 100).split("#"));
    }

    public String getId() {
        return bytes2String(profile, 4, 13);
    }

    public String getBirthday() {
        return bytes2String(profile, 217, 8);
    }

    private String bytes2String(byte[] buffer, int startIndex, int length) {
        byte[] bytesInput = new byte[length];
        System.arraycopy(buffer, startIndex, bytesInput, 0, length);
        String textOut = null;
        try {
            textOut = new String(bytesInput, "TIS620");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (textOut != null) {
            textOut = textOut.trim();
        }
        return textOut;
    }

}
