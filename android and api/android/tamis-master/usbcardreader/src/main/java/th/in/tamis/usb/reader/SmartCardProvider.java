package th.in.tamis.usb.reader;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

import com.idvision.androididcardlib.iCardUsbReader;

import java.util.HashMap;

public class SmartCardProvider extends BroadcastReceiver{

    final private Context mContext;
    private iCardUsbReader sReader;
    private UsbManager mUsbManager;
    private UsbDevice mDevice;
    private static final String TAG = "SmartCardProvider";
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private RegisterUSB registerUSB;

    public SmartCardProvider(Context context) {
        mContext = context;
        registerReaderReceiver(mContext);
        registerUSB = (RegisterUSB) context;
    }

    private void registerReaderReceiver(Context context) {

        mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);

        context.registerReceiver(this, filter);
        requestPermission();
        sReader = new iCardUsbReader();
    }

    private void requestPermission() {
        PendingIntent mPermissionIntent = PendingIntent.getBroadcast(mContext,
                0, new Intent(ACTION_USB_PERMISSION), 0);
        HashMap<String, UsbDevice> map = mUsbManager.getDeviceList();
        for (UsbDevice device : map.values()) {
            if (device.getDeviceClass() == UsbConstants.USB_CLASS_PER_INTERFACE) {
                mDevice = device;
                mUsbManager.requestPermission(device, mPermissionIntent);
            }
        }
    }

    public iCardUsbReader getReader() {
        return sReader;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_USB_PERMISSION.equals(action)) {
            synchronized (this) {
                mDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    sReader.init(mUsbManager, mDevice);
                }
            }
        } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
            sReader.init(mUsbManager, mDevice);
            if (!sReader.getReaderStatus()) {
                sReader.close();
                sReader = null;
                registerUSB.onRegisterUSBSuccess();
                registerReaderReceiver(mContext);
            }
        } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
            sReader.close();
            sReader = null;
            registerUSB.onRegisterUSBFails();
            registerReaderReceiver(mContext);
        }
    }
}
